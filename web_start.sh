#!/usr/bin/env bash

# @file web_start.sh
# @brief Simple test server for running the web build
# @author Antonio Prates <hello@aprates.dev>
# @version 1.3.4
# @date 2023-11-17
#
# @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
# Copyright (c) 2022-2023, Antonio Prates <hello@aprates.dev>
# Licensed under the GNU General Public License v3.0.
# See LICENSE file in the project root for full license.

docker run --rm -it -p 5000:80 -v ./web:/usr/share/nginx/html:ro nginx
