# Related Projects and Spin-Offs

This file lists various projects and tools written in FatScript, that may be of interest to developers. These projects showcase different aspects of the FatScript ecosystem and provide useful resources for extending its functionality.

- [chef](https://gitlab.com/fatscript/chef): language-agnostic, git-based package manager

- [fatpack](https://gitlab.com/fatscript/fatpack): bundler/packager for the FatScript Playground

- [HTMLbox](https://gitlab.com/aprates/htmlbox): framework for creating HTML pages programmatically

- [Fatastic](https://gitlab.com/aprates/fatastic): Elasticsearch client tailored for FatScript

- [zlib.fat](https://gitlab.com/aprates/zlib): zlib interface for FatScript using libffi

- [qrcode.fat](https://gitlab.com/aprates/qrcode): qrencode interface for FatScript using libffi

- [scheduler](https://gitlab.com/aprates/scheduler): tasks scheduling/running in sync and async modes

- [actors](https://gitlab.com/aprates/actors): actor-based concurrency models for FatScript

- [todo-app](https://gitlab.com/aprates/fat-todo-app): a simple CLI to-do app written in FatScript

- [FARTlet](https://gitlab.com/aprates/fartlet): a port of FIGlet for large letters in ASCII

- [fortune](https://gitlab.com/aprates/fortune): a port of Unix fortune, using motivate database

- [lolcat](https://gitlab.com/aprates/lolcat): a port of the popular console output colorizer

- [GPT-TUI](https://gitlab.com/aprates/gpt-tui): ChatGPT-like TUI for OpenAI models

- [remusi](https://gitlab.com/aprates/remusi): REMove Unused Scala Imports by analyzing sbt logs

- [secret-santa](https://gitlab.com/aprates/secret-santa): Secret Santa mail sender Christmas events

- [XMLoaf](https://gitlab.com/aprates/xmloaf): a rudimentary XML codec implementation

- [fatForm](https://gitlab.com/aprates/fatform): TUI form component library tailored for FatScript

- [calculator](https://gitlab.com/aprates/calculator): a simple fatForm library demo

- [matrix-screensaver](https://gitlab.com/aprates/matrix-screensaver): a dynamic Matrix-style screensaver example app

- [ppm2ascii.fat](https://gitlab.com/aprates/ppm2ascii): a conversor for Portable Pixmap (PPM) to ASCII art

- [space-ship-game](https://gitlab.com/aprates/space-ship-game): a terminal side-scroller example game

---

### Contribute Your Project

If you have a project written or that somehow contributes to FatScript eco-system or one of its spin-offs, we welcome you to contribute by opening a merge request to have it listed here, please see [CONTRIBUTING.md](CONTRIBUTING.md).
