<!-- This issue requests a technical writer review as required for documentation
     content that was merged without one. -->


## References

Merged MR that introduced documentation requiring review:

Related issue(s):

## Further Details

<!-- Any additional context, questions, or notes for the technical writer. -->


/label ~documentation ~"Technical Writing"
