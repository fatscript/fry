# FRY 🍳

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0) [![pipeline status](https://gitlab.com/fatscript/fry/badges/main/pipeline.svg?ignore_skipped=true)](https://gitlab.com/fatscript/fry/-/commits/main) [![version](https://img.shields.io/badge/version-4.1.0-yellow.svg)](https://gitlab.com/fatscript/fry/-/releases/v4.1.0)

`fry` is a free interpreter and runtime environment for FatScript, a programming language designed for building console-based applications.

## About

`fry` could stand for "Fat Runtime Yard", or maybe "For Real Yumminess", or even "Frackin' Repl, Yo", whichever better suits your palate. It's like an open pan used for frying your fat code.

For more information about FatScript language specification and syntax, please check:

- [FatScript.org](https://fatscript.org) (official docs)
- [YouTube Channel](https://www.youtube.com/@fatscript)
- [Code Samples](sample)
- [Test Cases](test)

## Usage

- Start the REPL:

```bash
fry
```

- Execute a FatScript file:

```bash
fry sample/dice.fat
```

- Show CLI options:

```bash
fry --help
```

## Web Playground

For quick and convenient testing, run your code directly in the [FatScript Playground](https://fatscript.org/playground). The playground features a REPL and an intuitive interface that allows you to load scripts from file.

## Installation

`fry` is designed for GNU/Linux, but it might also work on [other operating systems](#os-support).

For Arch-based distributions, install via [fatscript-fry](https://aur.archlinux.org/packages/fatscript-fry) AUR package.

For other distributions, try the auto-install script:

```bash
curl -sSL https://gitlab.com/fatscript/fry/raw/main/get_fry.sh -o get_fry.sh;
bash get_fry.sh || sudo bash get_fry.sh
```

Or, to install `fry` manually:

- Clone the repository:

```bash
git clone --recursive https://gitlab.com/fatscript/fry.git
```

- Then, run the installation script:

```bash
cd fry
./install.sh
```

### Dependencies

If the installation fails, you may be missing some dependencies. `fry` requires `git`, `gcc` and `libcurl` to build. For example, to install these dependencies on Debian/Ubuntu, run:

```bash
apt update
apt install git gcc libcurl4-openssl-dev
```

#### Back-end for text input

`linenoise` is a lightweight dependency and an alternative to `readline`, maintained as a submodule. If it was not included during the initial `git clone` operation, you can rectify this with the following commands:

```bash
git submodule init
git submodule update
```

If you prefer to link against `readline`, just ensure it is installed by running:

```bash
apt install libreadline-dev
```

### Extras

For syntax highlighting and code formatting, check out the [extras](extras) section.

For projects and tools related to FatScript, check out the [resources](RESOURCES.md) file.

## OS Support

`fry` is compatible with the following operating systems:

- GNU/Linux
- Android (with Termux)
- ChromeOS (with Linux enabled)
- MacOS (with Command Line Tools)
- iOS (with iSH)
- Windows (with WSL)

See details about setup on specific platforms [here](https://fatscript.org/en/general/setup.html#os-support).

## Docker image

`fry` is also available as a [docker image](https://hub.docker.com/r/fatscript/fry/tags):

```bash
docker run --rm -it fatscript/fry
```

To run a FatScript file with docker, use something like:

```bash
docker run --rm -it -v ~/project:/app fatscript/fry prog.fat
```

## Troubleshooting

If you encounter any issues or bugs while using `fry`, please [open an issue](https://gitlab.com/fatscript/fry/-/issues).

## Contributing

It will be a pleasure to receive your input.

Before you start, please see [CONTRIBUTING.md](CONTRIBUTING.md).

### Roadmap

We are actively developing the `fatscript/fry` project. To stay updated on next planned enhancements, please refer to our [issue tracker](https://gitlab.com/fatscript/fry/-/issues?label_name=enhancement).

### Donations

Did you find `fry` useful and would like to say thanks?

[Buy me a coffee](https://www.buymeacoffee.com/aprates)

## License

[GPLv3](LICENSE) © 2022-2025 Antonio Prates.

## Acknowledgments

"If I have seen further than others, it is by standing upon the shoulders of giants."
- Isaac Newton

`fatscript/fry` also may include or use the following open-source projects:

- [cURL/libcurl](https://curl.se/) (MIT/X license)
- [OpenSSL](https://www.openssl.org/) (Apache-2.0 license)
- [Linenoise UTF-8](https://github.com/fatscript/linenoise) (BSD-like license)
- [GNU Readline](https://tiswww.case.edu/php/chet/readline/rltop.html) (GPL license)
- [Emscripten](https://emscripten.org/) (MIT license)
- [Xterm.js](https://xtermjs.org/) (MIT license)
- [NCurses](https://invisible-island.net/ncurses/) (MIT license)
- [Hermit font](https://pcaro.es/hermit/) (OFL license)
- [HonKit](https://github.com/honkit/honkit) (Apache-2.0 license)

## Official page

[FatScript.org](https://fatscript.org)
