#!/usr/bin/env bash

# @file install.sh
# @brief Single step build+install script
# @author Antonio Prates <hello@aprates.dev>
# @version 3.3.0
# @date 2024-09-04
#
# @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
# Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
# Licensed under the GNU General Public License v3.0.
# See LICENSE file in the project root for full license.

quickTestFry() {
    echo -n '   testing'
    for file in test/t*.fat
    do
        # skip these tests:
        if [[
                "$file" != "test/main.fat" && \
                "$file" != "test/t020.fat" && \
                "$file" != "test/t026.fat" && \
                "$file" != "test/t038.fat" && \
                "$file" != "test/t068.fat" && \
                "$file" != "test/t082.fat" && \
                "$file" != "test/t084.fat" && \
                "$file" != "test/t089.fat" && \
                "$file" != "test/t104.fat" && \
                "$file" != "test/t105.fat" && \
                "$file" != "test/t110.fat" && \
                "$file" != "test/t123.fat"    \
        ]]
        then
            ./bin/fry "$file" &>/dev/null  # quiet
            exitCode=$?
            
            if [ "$exitCode" != "0" ]
            then
                echo " failed: $file" && exit 1
            fi
            
            # show progress (make sure not skipped)
            if [[
                    "$file" == "test/t021.fat" || \
                    "$file" == "test/t042.fat" || \
                    "$file" == "test/t063.fat" || \
                    "$file" == "test/t085.fat" || \
                    "$file" == "test/t106.fat"    \
            ]]
            then
                echo -n '.'
            fi
        fi
    done
    echo $' \e[34mdone ✔ \e[0m'
}

installFry() {
    echo 'Installing:'
    echo -n '  '
    if command -v termux-setup-storage &>/dev/null  # Termux?
    then
        cp -fv ./bin/fry "$PREFIX/bin/"
    elif [ "$UID" == "0" ]  # root user?
    then
        mkdir -p /usr/local/bin/ && \
        cp -fv ./bin/fry /usr/local/bin/
    elif [ "$(uname -s)" == "Darwin" ]  # MacOS?
    then
        sudo mkdir -p /usr/local/bin/ && \
        sudo cp -fv ./bin/fry /usr/local/bin/
    else
        # ...assume Linux regular user
        mkdir -p "$HOME/.local/bin/" && \
        cp -fv ./bin/fry "$HOME/.local/bin"
    fi
}

./compile.sh "$@" && quickTestFry && installFry && \
echo "   v-$(fry -v) installed! run: fry --help"
