/**
 * @file huge.h
 * @brief HugeInt arithmetic library for numbers up to 4096 bits
 * @author Antonio Prates <hello@aprates.dev>
 * @version 3.1.0
 * @date 2024-07-25
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>

#define HUGE_INT_SIZE 128  // 128 * 32 bits = 4096 bits
#define HUGE_FRAG 8

typedef uint32_t HugeInt[HUGE_INT_SIZE];

/**
 * @brief Find the highest set bit.
 *
 * @param num HugeInt number to check
 * @return int bits count
 */
int minBitsToRepresent(const HugeInt num);

/**
 * @brief Converts HugeInt to double (with precision loss).
 *
 * @param num HugeInt number to convert
 * @return double
 */
double hugeToFloat(const HugeInt num);

/**
 * @brief Converts HugeInt to binary data.
 * @note Sets olen to -1 in case memory allocation fails.
 *
 * @param num HugeInt number to convert
 * @param olen output length
 * @return bytes (internally allocated, use free)
 */
char* hugeToBytes(const HugeInt num, int* olen);

/**
 * @brief Writes binary data into HugeInt.
 * @note Data size can't exceed HUGE_INT_SIZE * sizeof(uint32_t) bytes.
 *
 * @param chunk binary data
 * @param chunkLen data length
 * @param num output buffer (externally allocated and initialized to zero)
 */
void bytesToHuge(const unsigned char* chunk, int chunkLen, HugeInt num);

/**
 * @brief Adds two HugeInt numbers.
 *
 * @param result Buffer to store the result of the addition
 * @param a First operand for the addition
 * @param b Second operand for the addition
 * @return true if there is no overflow, false otherwise
 */
bool hugeAdd(HugeInt result, const HugeInt a, const HugeInt b);

/**
 * @brief Subtracts the second HugeInt number from the first.
 *
 * @param result Buffer to store the result of the subtraction
 * @param a First operand (minuend)
 * @param b Second operand (subtrahend)
 * @return true if there is no underflow, false otherwise
 */
bool hugeSub(HugeInt result, const HugeInt a, const HugeInt b);

/**
 * @brief Multiplies two HugeInt numbers.
 *
 * @param result Buffer to store the result of the multiplication
 * @param a First operand for the multiplication
 * @param b Second operand for the multiplication
 * @return true if there is no overflow, false otherwise
 */
bool hugeMul(HugeInt result, const HugeInt a, const HugeInt b);

/**
 * @brief Compares two HugeInt numbers for equality.
 *
 * @param a First HugeInt number to compare
 * @param b Second HugeInt number to compare
 * @return true if the numbers are equal, false otherwise
 */
bool hugeEq(const HugeInt a, const HugeInt b);

/**
 * @brief Sets a HugeInt number from a hexadecimal string.
 *
 * @param num HugeInt number to set
 * @param hexString Hexadecimal string to parse
 * @return true if the string is successfully parsed, false otherwise
 */
bool hugeSet(HugeInt num, const char* hexString);

/**
 * @brief Checks if the first HugeInt number is less than the second.
 *
 * @param a First HugeInt number to compare
 * @param b Second HugeInt number to compare
 * @return true if a is less than b, false otherwise
 */
bool hugeIsLess(const HugeInt a, const HugeInt b);

/**
 * @brief Calculates the modulus of a HugeInt number.
 *        (check your input, a modulus == 0 may lead to infinite loop)
 *
 * @param result Buffer to store the result of the modulus operation
 * @param a HugeInt number to be reduced
 * @param modulus The modulus value
 */
void hugeMod(HugeInt result, const HugeInt a, const HugeInt modulus);

/**
 * @brief Divides a HugeInt Number.
 *
 * @param result HugeInt quotient
 * @param dividend HugeInt number
 * @param divisor HugeInt number
 */
void hugeDiv(HugeInt result, const HugeInt dividend, const HugeInt divisor);

/**
 * @brief Performs result = base^exponent.
 *        (check your input, a exponent == 0 may lead to overflow)
 *
 * @param result Buffer to store the result of the exponentiation
 * @param base Base value for exponentiation
 * @param exponent Exponent value
 * @return true if there is no overflow, false otherwise
 */

bool hugeExp(HugeInt result, const HugeInt base, const HugeInt exponent);

/**
 * @brief Performs modular exponentiation on HugeInt numbers.
 *
 * @param result Buffer to store the result of the exponentiation
 * @param base Base value for exponentiation
 * @param exponent Exponent value
 * @param modulus Modulus value for the operation
 * @return true if there is no overflow, false otherwise
 */
bool hugeModExpImpl(HugeInt result, const HugeInt base, const HugeInt exponent,
                    const HugeInt modulus);
