/**
 * @file sdk.c
 * @brief fry development toolkit
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-22
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "sdk.h"

#include "../runtime/embed/commands.h"  // EbCmd definition
#include "../syntax/lexer.h"            // unparse definitions

char *bundleKey = NULL;
bool crashOnError = true;
bool hasFoundErrors = false;
bool configOnce = false;
bool cursesMode = false;
bool isAstOnly = false;
Scope *globalScope = NULL;
char *currentLocale = NULL;
char *basePath = NULL;
bool isUtf8Mode = false;
int stackDepth = DEF_DEPTH;
int maxStackUsed = 0;

Type *metaSpace[META_HASH] = {0};
Type *ckTypeType = NULL;
Type *checkVoid = NULL;
Type *checkBoolean = NULL;
Type *checkNumber = NULL;
Type *checkHugeInt = NULL;
Type *checkChunk = NULL;
Type *checkText = NULL;
Type *checkList = NULL;
Type *checkScope = NULL;
Type *checkMethod = NULL;
Type *checkError = NULL;

Type *ckEpoch = NULL;
Type *ckExitCode = NULL;

Type *ckCPointer = NULL;

Type *checkProcedure = NULL;

Type *ckAssignError = NULL;
Type *ckAsyncError = NULL;
Type *ckCallError = NULL;
Type *ckFileError = NULL;
Type *ckIndexError = NULL;
Type *ckKeyError = NULL;
Type *ckSyntaxError = NULL;
Type *ckTypeError = NULL;
Type *ckValueError = NULL;

Type *checkStrict = NULL;

// initialize boolean singletons for runtime use
static Node truthNode = {.type = FatBoolean, .src = SRC_AUX, .num.b = true};
static Node falseNode = {.type = FatBoolean, .src = SRC_AUX, .num.b = false};
Node *trueSingleton = &truthNode;
Node *falseSingleton = &falseNode;

Scope **visited[SCOPE_HASH] = {0};
int visitCap[SCOPE_HASH];
static char *visitingFunc = NULL;

DLL dllSpace[MAX_DLLS] = {0};

pthread_t mainThreadId;

#ifdef SSL_SUPPORT
SSL_CTX *sslClientCtx = NULL;
SSL_CTX *sslServerCtx = NULL;
#endif

const char *getFryV(void) {
  static const char *version = FRY_VERSION;
  return *version == 'v' ? &version[1] : version;
}

void printVersion(VersionMode mode) {
  if (mode == VerMinimal) {
    puts(getFryV());
    return;
  }

  if (mode == VerDebugger) {
    stdoutLn("FRY " FRY_VERSION "-" PLATFORM "-" ARCHITECTURE
             " - FatScript Debugger\n",
             CL_GRA);
    return;
  }

  puts("FRY " FRY_VERSION "-" PLATFORM "-" ARCHITECTURE
       " - FatScript Interpreter\n");

  // ...and maybe additional info bellow:
  if (mode == VerWarranty) {
    puts(
      "Copyright (c) 2022-2024, Antonio Prates.\n"
      "fatscript/fry project <https://gitlab.com/fatscript/fry>.\n\n"
      "This program is free software: you can redistribute it and/or "
      "modify"
      "\nit under the terms of the GNU General Public License as "
      "published "
      "by\nthe Free Software Foundation, version 3.\n\n"
      "This program is distributed in the hope that it will be useful, but"
      "\nWITHOUT ANY WARRANTY; without even the implied warranty of\n"
      "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU\n"
      "General Public License for more details.\n\n"
      "You should have received a copy of the GNU General Public License\n"
      "along with this program. If not, see "
      "<http://www.gnu.org/licenses/>.\n"
#ifndef __EMSCRIPTEN__
#ifndef USE_READLINE
      "\n"
      "Additional Notices:\n"
      "This program includes a modified version of the linenoise library,\n"
      "which is covered under its own BSD-style license:\n"
      "<raw.githubusercontent.com/fatscript/linenoise/utf8-support/"
      "LICENSE>\n"
#endif
#endif
    );
  }
}

static inline char *handleInfinity(char *buff, const double f) {
  const char *inf = f > 0 ? "infinity" : "-infinity";
  const int ret = buff ? snprintf(buff, NUMBER_MAX_LENGTH + 1, "%s", inf)
                       : asprintf(&buff, "%s", inf);
  if (ret == -1) {
    fatalOut(__FILE__, __func__, MSG_F_F_N);
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }
  return buff;
}

char *prettyNumber(char *buff, const double f) {
  if (isinf(f)) {
    return handleInfinity(buff, f);
  }

  char *result = ofFloat(buff, f);

  if (f > NUMBER_INT_ONLY) {
    // Replace random mantissa with zeros
    char *mantissa = &result[NUMBER_DIG_ONLY];
    for (int i = 0; mantissa[i]; i++) {
      mantissa[i] = '0';
    }
    return result;
  }

  int nines = 0;
  char lastChar = '\0';
  int repeatCount = 0;
  for (size_t i = strlen(result) - 1, dec = 0; i > 0; i--, dec++) {
    char digit = result[i];

    // Exit on integer part
    if (digit == '.') {
      break;
    }

    if (digit == lastChar) {
      repeatCount++;
      if (repeatCount > 6) {
        result[i + 7] = '\0';  // trim with 7 repetitions
        if (digit > '4' && digit < '9') {
          result[i + 6] += 1;  // round up least significative digit
        }
      }
    } else {
      lastChar = digit;
      repeatCount = 1;
    }

    if (digit == '9') {
      nines++;
    } else if (nines > 5) {
      result[i + 1] = '\0';  // trim at previous digit
      result[i] += 1;        // round up current digit

      if (result[i] == '9') {
        nines++;
        lastChar = '9';
        repeatCount = nines;
      } else {
        nines = 0;
        lastChar = result[i];
        repeatCount = 1;
      }

    } else {
      nines = 0;
    }
  }

  char *decimalPoint = strchr(result, '.');
  if (!decimalPoint) {
    return result;
  }

  // Ensure we don't return more than NUMBER_MAX_DEC digits
  decimalPoint[NUMBER_MAX_DEC + 1] = '\0';

  // Ensure no trailing zeroes after decimal point
  char *end = decimalPoint + strlen(decimalPoint) - 1;
  while (end > decimalPoint && *end == '0') {
    *end = '\0';
    end--;
  }
  if (end == decimalPoint) {
    *end = '\0';
  }
  return result;
}

bool hasErrorHandler(Context *ctx) {
  for (int i = atomic_load(&ctx->top); i; i--) {
    if (ctx->stack[i].trap) {
      return true;
    }
  }
  return false;
}

noreturn void crashErrorHandler(char *msg, Context *ctx) {
  logError(__FILE__, __func__, msg);
  logStack(ctx, STACK_TRACE);
  msg = "an error ocurred while handling a failure";
  fatalOut(__FILE__, __func__, msg);
  exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
}

noreturn void crashFromError(Node *err, Context *ctx) {
  endCursesMode();
  stderrStartLine(CL_RED);
  fprintf(stderr, MRG_ERR "%s: %s, at %s", err->ck->name, err->val, err->src);
  stderrEndLine();
  logStack(ctx, STACK_TRACE);
  exitFry(EXIT_FAILURE);
}

Node *createError(char *msg, bool copy, Type *ck, Context *ctx) {
  assert(msg != NULL);
  assert(ctx != NULL);

  char *src = ctx->cur->node ? cpSrc(ctx->cur->node) : SRC_RUN;

  if (debugLogs) {
    logDebug2(__FILE__, __func__, src, msg);
  }

  Node *error = createNode(FatError, src, ctx);
  error->ck = ck ? ck : checkError;
  error->val = copy ? strDup(msg) : msg;

  if (ctx->isHandlingFailure) {
    crashErrorHandler(msg, ctx);
  }

  if (hasErrorHandler(ctx)) {
    if (debugLogs) {
      logDebug(__FILE__, __func__, "failure event registered");
    }
    lockResource(&ctx->lock);
    UNLOCK_NODE(ctx->failureEvent);
    ctx->failureEvent = error;
    unlockResource(&ctx->lock);
  } else if (crashOnError && pthread_equal(pthread_self(), mainThreadId)) {
    crashFromError(error, ctx);
  }

  return error;
}

long adaptiveSkipMod(long index) {
  return index <= 16     ? index % SKIP_LIST_MIN
         : index <= 32   ? index % 16
         : index <= 64   ? index % 32
         : index <= 128  ? index % 64
         : index <= 256  ? index % 128
         : index <= 512  ? index % 256
         : index <= 1024 ? index % 512
                         : index % SKIP_LIST_MAX;
}

static char *prettyChunk(const unsigned char *data, size_t length) {
  static const char digits[] = "0123456789";
  char *buffer = FRY_ALLOC(256);
  int index = 0;
  buffer[index++] = '[';
  buffer[index++] = ' ';
  for (size_t i = 0; i < length; i++) {
    // Check if buffer can hold more hex characters or apply ellipsis
    if (index + 5 > 255) {
      buffer[index - 2] = '.';
      buffer[index - 1] = '.';
      buffer[index++] = '.';
      break;
    }
    // Append the decimal digits to the buffer
    int value = data[i];
    int hundreds = value / 100;
    int tens = (value / 10) % 10;
    int ones = value % 10;
    if (hundreds) {
      buffer[index++] = digits[hundreds];
      buffer[index++] = digits[tens];
    } else if (tens) {
      buffer[index++] = digits[tens];
    }
    buffer[index++] = digits[ones];
    buffer[index++] = ',';
    buffer[index++] = ' ';
  }

  if (index == 2) {
    buffer[index - 1] = ']';  // close bracket
  } else if (index > 1 && buffer[index - 1] == ' ') {
    buffer[index - 2] = ' ';  // erase comma
    buffer[index - 1] = ']';  // close bracket
  }
  buffer[index] = '\0';  // ensure null-terminated

  return buffer;
}

char *toString(const Node *node) {
  NodeType type = node ? node->type : FatVoid;

  switch (type) {
    case FatVoid:
      return strDup("null");

    case FatBoolean:
      return strDup(ofBool(node->num.b));

    case FatNumber:
      return prettyNumber(NULL, node->num.f);

    case FatChunk:
      return prettyChunk((Byte *)node->val, node->num.s);

    case FatHugeInt:
      return unparseHugeInt(*node->num.h, false);

    case FatText:
    case FatTemp:
      return copyChunk(node->val, node->num.s);

    case FatError:
      return join3(node->ck ? node->ck->name : "Error", ": ",
                   node->val ? node->val : "unkown");

    case FatScope:
      if (node->scp && node->scp->ck) {
        return strDup(node->scp->ck->name);
      }
      FALL_THROUGH;

    default:
      return strDup(fatType(node->type));
  }
}

char *joinListItems(Scope *list, const char *sep) {
  if (!list || !list->entries) {
    return strDup("");  // return empty string for no contents
  }

  char **strs = FRY_ALLOC((list->size + 1) * sizeof(char *));

  long i = 0;
  for (Entry *entry = list->entries; entry; entry = entry->next) {
    strs[i++] = toString(entry->data);
  }
  strs[i] = NULL;  // null-terminate list

  char *result = joinSep(strs, sep);
  freeStrList(strs);  // clean up auxiliary list of strings
  return result;
}

/**
 * Clean up auxiliary list of strings for JSON concatenation
 */
static void freeJsonTokenList(char **strs, long tokens) {
  const long lastDynTok = tokens - 1;
  for (long i = 1; i < lastDynTok; i += 2) {
    free(strs[i]);
  }
  free(strs);
}

static char *toJsonRec(const Node *node, bool strict);

static char *toJsonList(Scope *list, bool strict) {
  if (!list || list->size <= 0 || !isFirstVisit(list)) {
    return strDup("[]");
  }

  long tokens = list->size * 2 + 2;
  char **strs = FRY_ALLOC(tokens * sizeof(char *));

  long i = 0;
  strs[i++] = "[";
  for (Entry *entry = list->entries; entry; entry = entry->next) {
    strs[i++] = toJsonRec(entry->data, strict);
    strs[i++] = ",";
  }
  strs[i - 1] = "]";
  strs[i] = NULL;  // null-terminate list

  char *result = mkString(strs);
  freeJsonTokenList(strs, tokens);
  unVisit(list);
  return result;
}

static char *toJsonScope(Scope *scope, bool strict) {
  if (!scope || scope->size <= 0 || !isFirstVisit(scope)) {
    return strDup("{}");
  }

  long tokens = scope->size * 4 + 2;
  char **strs = FRY_ALLOC(tokens * sizeof(char *));

  long i = 0;
  strs[i++] = "{";
  for (Entry *entry = scope->entries; entry; entry = entry->next) {
    strs[i++] = unparseText(entry->key.s, true);
    strs[i++] = ":";
    strs[i++] = toJsonRec(entry->data, strict);
    strs[i++] = ",";
  }
  strs[i - 1] = "}";
  strs[i] = NULL;  // null-terminate list

  char *result = mkString(strs);
  freeJsonTokenList(strs, tokens);
  unVisit(scope);
  return result;
}

static inline char *toJsonError(const Node *error) {
  auto_str aux = toString(error);
  char *result = unparseText(aux, true);
  return result;
}

static inline char *encodeChunkToB64(const Node *data) {
  char *encoded = FRY_ALLOC(b64EncodeLength(data->num.s) + 1);
  encoded[b64Encode(data->val, data->num.s, encoded)] = '\0';
  return encoded;
}

static char *toJsonRec(const Node *node, bool strict) {
  NodeType type = node ? node->type : FatVoid;

  switch (type) {
    case FatVoid:
      return strDup("null");

    case FatBoolean:
      return strDup(ofBool(node->num.b));

    case FatNumber:
      return prettyNumber(NULL, node->num.f);

    case FatText:
      return unparseText(node->val, true);

    case FatList:
      return toJsonList(node->scp, strict);

    case FatScope:
      return toJsonScope(node->scp, strict);

    case FatError:
      return toJsonError(node);

    default:
      if (strict) {
        return strDup("null");
      }
      switch (type) {
        case FatHugeInt:
          return unparseHugeInt(*node->num.h, true);

        case FatChunk:
          return join3("(b64->$fromBase64)('", encodeChunkToB64(node), "')");

        default:
          return strDup(fatType(node->type));
      }
  }
}

char *toJson(const Node *node, bool strict) {
  if (!initVisit(__func__)) {
    fatalOut(__FILE__, __func__, MSG_U_REC);
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }
  char *result = toJsonRec(node, strict);
  endVisit();
  return result;
}

char *nodeTypeToString(Node *node) {
  if (!node) {
    return strDup(fatType(FatVoid));
  }

  const char *type = node->scp && node->scp->ck ? node->scp->ck->name
                     : node->ck                 ? node->ck->name
                                                : fatType(node->type);

  switch (node->type) {
    case FatProcedure:
    case FatMethod:
      type = node->ck ? node->ck->name : "Any";  // return type
      return join2(node->type == FatMethod ? "Method/" : "Procedure/", type);

    case FatList:
      type = node->ck ? node->ck->name : "List";
      if (node->scp && node->scp->ck) {
        const char *contentType = node->scp->ck->name;
        return join3(type, "/", contentType);
      }
      return join2(type, "/Empty");

    default:
      return strDup(type);
  }
}

void initLayer(Scope *layer) {
  assert(layer != NULL);

  // Init Scope
  if (pthread_mutex_init(&layer->lock, &recursiveMutex)) {
    fatalOut(__FILE__, __func__, MSG_F_MUTEX);
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }
  atomic_init(&layer->refs, 0);
  layer->isLayer = true;
}

// Define a function pointer type for freeing entries
typedef void (*FreeEntryFunc)(Entry *);

void wipeScope(Scope *scope, const Entry *keep, bool willReuse) {
  assert(scope != NULL);

  // Determine the appropriate free function to use
  FreeEntryFunc freeFunc = scope->isList ? freeListEntry : freeScopeEntry;

  lockResource(&scope->lock);

  Entry *garbage = scope->entries;
  while (garbage) {
    Entry *next = garbage->next;
    if (garbage != keep) {
      freeFunc(garbage);
    } else {
      UNLOCK_NODE(garbage->data);
    }
    garbage = next;
  }

  if (willReuse) {
    assert(scope->isLayer);                  // cannot reuse non-layer
    assert(atomic_load(&scope->refs) == 0);  // cannot reference a layer

    scope->entries = NULL;
    scope->quick1 = NULL;
    scope->quick2 = NULL;
    scope->cached = NULL;
    scope->size = 0;
    scope->blanks = 0;
    unlockResource(&scope->lock);
  } else {
    unlockResource(&scope->lock);
    pthread_mutex_destroy(&scope->lock);
  }
}

Node *runtimeNumber(double num, Context *ctx) {
  Node *result = createNode(FatNumber, SRC_RUN, ctx);
  result->num.f = num;
  result->ck = checkNumber;
  return result;
}

Node *runtimeHuge(HugeInt num, Context *ctx) {
  Node *result = createNode(FatHugeInt, SRC_RUN, ctx);
  result->num.h = copyHuge(num);
  result->ck = checkHugeInt;
  return result;
}

Node *runtimeChunk(char *val, size_t len, Context *ctx) {
  Node *result = createNode(FatChunk, SRC_RUN, ctx);
  result->num.s = len;
  result->val = val;
  result->ck = checkChunk;
  return result;
}

Node *runtimeText(char *val, size_t len, Context *ctx) {
  assert(val != NULL);

  Node *result = createNode(FatText, SRC_RUN, ctx);
  result->num.s = len;
  result->val = val;
  result->ck = checkText;
  return result;
}

Node *runtimeTextDup(const char *val, Context *ctx) {
  if (!val) {
    return NULL;
  }

  size_t len = strlen(val);

  Node *result = createNode(FatText, SRC_RUN, ctx);
  result->num.s = len;
  result->val = copyChunk(val, len);
  result->ck = checkText;
  return result;
}

Node *runtimeCollection(Scope *scope, Context *ctx) {
  if (!scope) {
    return NULL;
  }

  Node *result = NULL;
  if (scope->isList) {
    result = createNode(FatList, SRC_RUN, ctx);
    result->ck = checkList;
  } else {
    result = createNode(FatScope, SRC_RUN, ctx);
    result->ck = checkScope;
  }
  bindScope(result, scope);
  return result;
}

static bool isHugeZero(HugeInt num) {
  const HugeInt hugeZero = {0};
  return hugeEq(num, hugeZero);
}

bool booleanOf(Node *node) {
  if (!node) {
    return false;
  }

  switch (node->type) {
    case FatVoid:
      return false;

    case FatBoolean:
      return node->num.b;

    case FatNumber:
      return !floatEq(node->num.f, 0);

    case FatHugeInt:
      return !isHugeZero(*node->num.h);

    case FatChunk:
    case FatText:
      return node->num.s;

    case FatProcedure:
    case FatMethod:
    case FatType:
      return true;

    case FatList:
      return node->scp && node->scp->entries;

    case FatScope:
      return node->scp && node->scp->size - node->scp->blanks;

    default:
      return false;
  }
}

static inline bool nodeValEq(Node *a, Node *b) {
  switch (a->type) {
    case FatBoolean:
      return a->num.b == b->num.b;

    case FatNumber:
      return floatEq(a->num.f, b->num.f);

    case FatHugeInt:
      return hugeEq(*a->num.h, *b->num.h);

    case FatText:
    case FatTemp:
      if (a->num.s != b->num.s) {
        return false;
      }
      if (a->num.s == 0) {
        return true;  // nothing to compare
      }
      return FAST_STR_EQ(a->val, b->val);

    case FatChunk:
      if (a->num.s != b->num.s) {
        return false;
      }
      return memcmp(a->val, b->val, a->num.s) == 0;

    default:
      return true;
  }
}

static bool nodeEqRec(Node *a, Node *b, int level) {
  // Perform pointer shallow check
  if (a == b) {
    return true;
  }
  if (!a || !b) {
    return false;
  }

  // Perform type check (if comparing with type)
  if (a->type != b->type) {
    if (a->type == FatType && checkType(a->ck, b)) {
      return true;
    }
    if (b->type == FatType && checkType(b->ck, a)) {
      return true;
    }
    return false;
  }

  // Perform inexpensive data check
  if (!nodeValEq(a, b)) {
    return false;
  }

  // Limit deeply nested comparison
  if (level >= MAX_EQ_DEPTH) {
    if (debugLogs) {
      logDebug(__FILE__, __func__, "depth for recursive comparison exceeded");
    }
    return false;
  }

  // Perform recursive deep check
  return nodeEqRec(a->head, b->head, level) &&
         nodeEqRec(a->body, b->body, level) &&
         nodeEqRec(a->tail, b->tail, level) && scopeEq(a->scp, b->scp, level);
}

bool nodeEq(Node *a, Node *b) { return nodeEqRec(a, b, 0); }

bool scopeEq(Scope *a, Scope *b, int level) {
  // Perform pointer shallow check
  if (a == b) {
    return true;
  }
  if (!a || !b) {
    return false;
  }
  if (a->entries == b->entries) {
    return true;
  }

  // Perform inexpensive type check
  if (a->isList != b->isList) {
    return false;
  }

  Entry *ea = a->entries;
  Entry *eb = b->entries;
  if (a->isList) {
    // Perform inexpensive size check (List)
    if (a->size != b->size) {
      return false;
    }
    // Perform recursive deep check (List)
    for (; ea && eb; ea = ea->next, eb = eb->next) {
      // Do actual value comparison
      if (!nodeEqRec(ea->data, eb->data, level + 1)) {
        return false;
      }
    }
  } else {
    // Perform inexpensive size check (Scope)
    if ((a->size - a->blanks) != (b->size - b->blanks)) {
      return false;
    }
    // Perform recursive deep check (Scope)
    for (;; ea = ea->next, eb = eb->next) {
      // Skip null values in scopes
      while (ea && !ea->data) {
        ea = ea->next;
      }
      while (eb && !eb->data) {
        eb = eb->next;
      }
      // End of scope reached?
      if (!ea || !eb) {
        break;
      }
      // Perform key (property name) check
      if (!FAST_STR_EQ(ea->key.s, eb->key.s)) {
        return false;
      }
      // Do actual value comparison
      if (!nodeEqRec(ea->data, eb->data, level + 1)) {
        return false;
      }
    }
  }
  return !ea && !eb;  // both sides ended together? (failsafe)
}

Type *createComposite(const char *name, char **types) {
  Type *composite = NULL;
  if (name) {
    // Prevent recursive composition
    for (int i = 0; types[i]; i++) {
      if (FAST_STR_EQ(types[i], name)) {
        logAlert(__func__, "recursive mix", name);
        return NULL;
      }
    }
    composite = setType(name, NULL);
  } else {
    auto_str auxName = joinSep(types, "/");
    composite = setType(auxName, NULL);
  }

  // If existing, validate it's identical, or fail creation
  size_t count = listCount((void **)types);
  if (composite->alias) {
    if (strchr(composite->name, '/')) {
      return composite;  // bypass check hack
    }

    bool isSame = true;
    if (!composite->isComposite || composite->alias != getType(types[0]) ||
        listCount((void **)composite->include) != count - 1) {
      isSame = false;
    } else {
      for (size_t i = 1; i < count; i++) {
        if (composite->include[i - 1] != getType(types[i])) {
          isSame = false;
        }
      }
    }

    if (!isSame) {
      logAlert(__func__, MSG_D_DEF, composite->name);
      return NULL;
    }

    return composite;
  }

  // Allocate memory for composite definition
  composite->include = FRY_ALLOC(sizeof(Type *) * (count));

  // Store the composite mix
  composite->isComposite = true;
  composite->alias = setType(types[0], NULL);
  for (size_t i = 1; i < count; i++) {
    composite->include[i - 1] = setType(types[i], NULL);
  }
  composite->include[count - 1] = NULL;

  return composite;
}

Type *resolveAlias(Type *ck) {
  Type *base = ck;
  for (; ck; ck = ck->alias) {
    base = ck;
  }
  return base;
}

bool isAlias(const Type *base, Type *alias) {
  for (; alias; alias = alias->alias) {
    if (alias == base) {
      return true;
    }
  }
  return false;
}

/**
 * Compares types resolving aliases
 */
static inline bool matchTypes(Type *a, Type *b) {
  a = resolveAlias(a);
  b = resolveAlias(b);
  if (a && b) {
    return a == b;
  }
  return true;  // FatAny implied on a or b
}

static bool checkTypeMethod(const Type *ck, Node *node) {
  if (!ck->isComposite) {
    return ck == checkMethod || ck == checkProcedure;
  }

  if (ck->alias != checkMethod && ck->alias != checkProcedure) {
    return false;
  }

  if (!node->ck) {
    return false;  // no annotation and no type inference
  }

  if (!node->ck->isComposite) {
    return ck->include[0] == node->ck && ck->include[1] == NULL;
  }

  if (ck->include[0]->isComposite && ck->include[0] == node->ck) {
    return true;  // fnx: Method/Matrix = fn, where fn = (x): Matrix -> x
  }

  if (ck->include[0] != node->ck->alias) {
    return false;
  }

  for (int i = 1; ck->include[i] && node->ck->include[i - 1]; i++) {
    if (ck->include[i] != node->ck->include[i - 1]) {
      return false;
    }
  }

  return true;
}

static bool checkTypeList(const Type *ck, Node *node) {
  if (!ck->isComposite) {
    return ck == checkList;
  }

  if (ck->alias != checkList) {
    return false;
  }

  Node *head = NULL;
  for (int i = 0; ck->include[i]; i++, node = head) {
    Scope *list = node->scp && node->scp->isList ? node->scp : NULL;

    if (list) {
      head = list->entries ? list->entries->data : NULL;

      if (head) {
        if (ck->include[i]->isComposite) {
          if (ck->include[i + 1]) {
            logAlert(__func__, MSG_U_D_T_C, ck->name);
            break;  // limitation: cannot check List/Composite/Other
          }
          // Rationale: if dev is making use of List/Composite he knows what he
          // is doing and expects the type system to add safety even if it adds
          // a significant overhead. So we traverse all items to verify types.
          for (Entry *entry = list->entries; entry; entry = entry->next) {
            if (!checkAlias(ck->include[i], entry->data)) {
              return false;
            }
          }
          break;
        }
        if (!checkAlias(ck->include[i], head)) {
          return false;
        }
      } else {
        break;  // can't check type on empty list
      }
    } else {
      // Rationale: Lists are "guaranteed" to be of the "same" type at the top
      // level, so we can only check one level of depth. Otherwise, we would
      // need to traverse the list at runtime to be certain, which is not viable
      // without overhead, and likely not desirable. So we warn and bail out...
      logAlert(__func__, MSG_UNSUP MSG_U_D_T_C, ck->name);
      break;  // limitation: only checks lists of lists...of X
    }
  }

  return true;
}

static bool checkTypeScope(const Type *ck, const Node *node) {
  Scope *scope = node->scp;

  if (ck->isComposite) {
    if (!checkTypeScope(ck->alias, node)) {
      return false;
    }

    const Type *sub = ck->include[0];

    lockResource(&scope->lock);
    for (Entry *entry = scope->entries; entry; entry = entry->next) {
      if (!checkAlias(sub, entry->data)) {
        unlockResource(&scope->lock);
        return false;
      }
    }
    unlockResource(&scope->lock);

    // Limitation: only checks at depth 0
    if (ck->include[1]) {
      logAlert(__func__, MSG_U_D_T_C, ck->name);
    }

    return true;
  }

  if (isAlias(ck, scope->ck)) {
    return true;
  }

  if (ck == checkScope) {
    return true;
  }

  if (scope->ck && scope->ck->include) {
    Type **incl = scope->ck->include;

    // node has only one include and it is ck?
    if (incl[0] == ck && !incl[1]) {
      return true;
    }

    // base types are a match?
    if (ck->include) {
      for (int i = 0; ck->include[i] || incl[i]; i++) {
        if (ck->include[i] != incl[i]) {
          return false;
        }
      }
      return true;
    }
  }

  return false;
}

bool checkType(const Type *ck, Node *node) {
  if (!ck || !node || node->type == FatVoid) {
    return true;  // Any implied
  }

  if (node->type == FatType) {
    Type *ckPtr = (Type *)ck;
    return ck == ckTypeType || node->ck == ckTypeType ||
           matchTypes(ckPtr, node->ck);
  }

  if (node->type == FatMethod) {
    return ck == checkMethod;
  }

  if (node->type == FatProcedure) {
    return ck == checkProcedure;
  }

  if (node->type == FatList) {
    return ck == checkList;
  }

  if (node->type == FatScope && node->scp && node->scp->ck) {
    return ck == node->scp->ck;
  }

  return ck == node->ck;
}

bool checkAlias(const Type *ck, Node *node) {
  if (!ck || !node || node->type == FatVoid) {
    return true;  // Any implied
  }

  if (node->type == FatType) {
    return ck == ckTypeType;
  }

  if (node->type == FatMethod || node->type == FatProcedure) {
    return checkTypeMethod(ck, node);
  }

  if (node->type == FatList) {
    return checkTypeList(ck, node);
  }

  if (node->type == FatScope && node->scp) {
    return checkTypeScope(ck, node);
  }

  return isAlias(ck, node->ck);
}

int getMethodArity(Node *method) {
  int arity = 0;
  for (Node *arg = method->head; arg; arg = arg->seq) {
    arity++;
  }
  return arity;
}

char *cpSrc(Node *node) {
  return *node->src == '$' ? node->src : strDup(node->src);
}

int initDLL(const char *name) {
  for (int i = 0; i < MAX_DLLS; i++) {
    if (!dllSpace[i].name) {
      void *handle = dlopen(name, RTLD_LAZY);
      if (!handle) {
        return -1;  // use dlerror() to read the error
      }

      dllSpace[i].name = strDup(name);
      dllSpace[i].handle = handle;

      if (debugLogs) {
        logDebug(__func__, name, "loaded");
      }
      return i;
    }

    if (FAST_STR_EQ(dllSpace[i].name, name)) {
      return i;
    }
  }

  return -2;  // MSG_BMO
}

DLL *getDLL(int index) {
  if (index >= 0 && index < MAX_DLLS && dllSpace[index].name) {
    return &dllSpace[index];
  }
  return NULL;
}

static void cleanupDLLs(void) {
  for (int i = 0; i < MAX_DLLS; i++) {
    if (!dllSpace[i].name) {
      break;
    }

    if (debugLogs) {
      logDebug(__func__, dllSpace[i].name, "unloaded");
    }
    dlclose(dllSpace[i].handle);
    free(dllSpace[i].name);
  }
}

void endCursesMode(void) {
  if (cursesMode) {
    cursesMode = false;
    endwin();
  }
}

static void exitHooks(const char *ref) {
#ifndef DEBUG
  (void)ref;
#endif

  // Only the main thread should run exitHooks
  if (!pthread_equal(pthread_self(), mainThreadId)) {
    abort();  // should never happen!
  }

#ifndef __EMSCRIPTEN__
  // Iterate through all registered workers and initiate cancellation
  int unfinished = atomic_load(&activeThreads);
  if (unfinished > 1) {
    fprintf(stderr, MRG_END "terminating %d async worker%s...", unfinished,
            unfinished > 1 ? "s" : "");
    for (int hash = 0; hash < SCOPE_HASH; hash++) {
      for (int i = 0; i < scpRegistryCap[hash]; i++) {
        Worker *worker = scopesRegistry[hash][i]->async;
        if (worker && worker->ctx && !atomic_load(&worker->hasJoined)) {
          int killError = pthread_kill(worker->ctx->id, SIGUSR1);
          if (killError) {
            logError(__FILE__, __func__, strerror(killError));
            continue;
          }
          int joinError = pthread_join(worker->ctx->id, NULL);
          if (joinError) {
            logError(__FILE__, __func__, strerror(joinError));
            continue;
          }
          atomic_fetch_sub(&activeThreads, 1);
        }
      }
    }
    unfinished = atomic_load(&activeThreads) - 1;
    if (unfinished > 0) {
      fprintf(stderr, " %d worker%s did not respond!\n\n", unfinished,
              unfinished > 1 ? "s" : "");
    } else {
      fputs(" done!\n\n", stderr);
    }
  }
#endif

#ifdef DEBUG
  if (traceLogs) {
    logDebug2(__FILE__, __func__, ref, "cleaning up...");
  }
  // this cleanup is useful for tracking memory leaks, don't remove
  if (globalScope) {
    wipeScope(globalScope, NULL, false);
    resetSession();  // collect all nodes
    free(globalScope);
    globalScope = NULL;
  }
  Type *next = NULL;
  for (int hash = 0; hash < META_HASH; hash++) {
    for (Type *ck = metaSpace[hash]; ck; ck = next) {
      next = ck->next;
      logTrace2(__FILE__, __func__, "dispose", ck->name);
      free((char *)ck->name);
      if (ck->include) {
        free(ck->include);
      }
      if (ck->alias && ck->def) {
        free(ck->def->val);
        free(ck->def);
      }
      free(ck);
    }
  }
  for (int hash = 0; hash < SCOPE_HASH; hash++) {
    free(visited[hash]);
  }
#endif

  if (configOnce) {
    curl_global_cleanup();
    cleanMemoryLock();
#ifdef __EMSCRIPTEN__
    Mix_CloseAudio();
    SDL_Quit();
#endif
#ifdef SSL_SUPPORT
    if (sslClientCtx) {
      SSL_CTX_free(sslClientCtx);
    }
    if (sslServerCtx) {
      SSL_CTX_free(sslServerCtx);
    }
    EVP_cleanup();
    ERR_free_strings();
#endif
  }

  cleanupDLLs();
}

noreturn void exitFromSignal(ExitCode sig) {
  // Only the main thread should receive signals
  if (!pthread_equal(pthread_self(), mainThreadId)) {
    abort();
  }

  // Ignore new termination signals while shutting down
  signal(SIGINT, SIG_IGN);
  signal(SIGHUP, SIG_IGN);
  signal(SIGTERM, SIG_IGN);

  endCursesMode();

  // Not entirely safe on signal handler, but adds a nice touch :P
  long count = atomic_load(&activeMemory);
  char *approx = "";
  char *unit = "";
  if (count > 999) {
    approx = "~";
    count = ceil((double)count / 1000);
    unit = "k";
    if (count > 999) {
      count = ceil((double)count / 1000);
      unit = "m";
    }
  }
  stderrEndLine();
  fputs(MRG_END "pid ", stderr);
  fprintf(stderr, "%u halted with ", (unsigned int)getpid());
  fprintf(stderr, "%s%ld%s active nodes and ", approx, count, unit);
  count = (long)trackedScopes;
  approx = "";
  unit = "";
  if (count > 999) {
    approx = "~";
    count = ceil((double)count / 1000);
    unit = "k";
    if (count > 999) {
      count = ceil((double)count / 1000);
      unit = "m";
    }
  }
  fprintf(stderr, "%s%ld%s active scopes!\n\n", approx, count, unit);
  // End of unsafe section

  exitHooks(__func__);
  exit(sig);
}

noreturn void exitFry(ExitCode code) {
  if (!pthread_equal(pthread_self(), mainThreadId)) {
    kill(getpid(), SIGTERM);  // uses exitFromSignal on main thread
    pthread_exit(NULL);
  }
  endCursesMode();
  exitHooks(__func__);
  exit(abs(code % 256));  // ensure exit code between 0 and 255 (POSIX)
}

static bool isLocaleUTF8(const char *locale) {
  return strcasestr(locale, "UTF-8") || strcasestr(locale, "UTF8");
}

bool setFryLocale(const char *locale) {
  free(currentLocale);

  currentLocale = strDup(setlocale(LC_ALL, locale));

  if (currentLocale) {
    isUtf8Mode = isLocaleUTF8(currentLocale);
    return true;
  }

  // else setlocale failed, thus returned null, fallback to system default
  currentLocale = strDup(setlocale(LC_ALL, NULL));
  isUtf8Mode = currentLocale && isLocaleUTF8(currentLocale);
  return false;
}

bool canExpandPath(const char *path) {
  size_t len = strlen(path);
  return len > 2 && strcmp(&path[len - 2], "/_") == 0;
}

static inline void removeFromList(char **paths) {
  free(paths[0]);
  for (int i = 0; paths[i]; i++) {
    paths[i] = paths[i + 1];
  }
}

char *getBasePath(const char *filepath) {
  const char *lastSlash = strrchr(filepath, '/');
  if (lastSlash) {
    return copyChunk(filepath, lastSlash - filepath + 1);
  }
  return NULL;
}

char *getSourceForPath(const char *path) {
  // Return the source for a file
  auto_str importPath = join3(basePath ? basePath : "", path, ".fat");
  bool isValid = existsFile(importPath);
  if (!isValid) {
    importPath[strlen(importPath) - 4] = '\0';  //  try without ".fat"
  }
  if (isValid || existsFile(importPath)) {
    if (debugLogs) {
      logDebug2(__FILE__, __func__, "importing", importPath);
    }
    char *sourceFromFile = NULL;
    if (readFile(&sourceFromFile, importPath, "r") == -1) {
      fatalOut(importPath, __func__, "failed to read");
      exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
    }
    return sourceFromFile;
  }

  // Otherwise check if this is a directory import
  if (!canExpandPath(path)) {
    if (debugLogs) {
      logDebug2(__FILE__, __func__, "can't resolve", path);
    }
    return NULL;
  }

  // Generate a list of imports for the directory
  char *source = NULL;
  auto_str trimmed = copyChunk(path, strlen(path) - 2);  // remove "/_"
  auto_str dirPath = join3(basePath ? basePath : "", trimmed, "/");
  if (isDir(dirPath)) {
    if (debugLogs) {
      logDebug2(__FILE__, __func__, "expanding", dirPath);
    }

    char **paths = listDir(dirPath);
    if (paths && paths[0]) {
      for (size_t i = 0; paths[i]; i++) {
        size_t len = strlen(paths[i]);
        // Exclude directories
        if (len > 1 && paths[i][len - 1] == '/') {
          removeFromList(&paths[i--]);
          continue;
        }
        // Check file extension...
        if (strchr(paths[i], '.')) {
          // If '.fat', trim and keep include
          if (len > 4 && strcmp(&paths[i][len - 4], ".fat") == 0) {
            paths[i][len - 4] = '\0';
            continue;
          }
          // Exclude files with other extensions
          removeFromList(&paths[i--]);
        }
        // No file extension, keep as include
      }

      // Build synthetic import list
      auto_str statementBase = join3("\n_ <- ", trimmed, "/");
      auto_str aux = joinSep(paths, statementBase);
      source = join3(&statementBase[1], aux, "\n");

      freeStrList(paths);
    } else {
      if (debugLogs) {
        logDebug2(__FILE__, __func__, "nothing in dir", dirPath);
      }
      source = strDup("");  // don't throw an error
    }
  } else if (debugLogs) {
    logDebug2(__FILE__, __func__, "not a folder", dirPath);
  }

  return source;
}

bool initVisit(const char *func) {
  lockResource(&memoryLock);
  if (visitingFunc) {
#ifdef DEBUG
    if (strcmp(func, "markAndSweep") != 0) {
      auto_str msg = join2("called while" GUIDE, visitingFunc);
      logAlert(__func__, func, msg);
    }
#endif
    unlockResource(&memoryLock);
    return false;  // not ok to proceed!
  }

  visitingFunc = (char *)func;
  memset(visitCap, 0, sizeof(visitCap));
  return true;
}

void endVisit(void) {
  // We don't free buffers of visited, but reuse them on following iterations
  visitingFunc = NULL;
  unlockResource(&memoryLock);
}

bool isFirstVisit(Scope *scope) {
  int i = 0;
  int hash = (int)((uintptr_t)scope % SCOPE_HASH);

  for (; i < visitCap[hash]; i++) {
    if (visited[hash][i] == scope) {
      return false;  // already visited
    }
  }

  // Not yet visited, register visit
  if (visitCap[hash] % SCOPE_BLOCK == 0) {
    if (visited[hash]) {
      visited[hash] = FRY_REALLOC(
        visited[hash], sizeof(Scope *) * (visitCap[hash] + SCOPE_BLOCK));
    } else {
      visited[hash] = FRY_ALLOC(sizeof(Scope *) * (SCOPE_BLOCK));
    }
  }
  visited[hash][i] = scope;
  visitCap[hash]++;
  return true;
}

void unVisit(Scope *scope) {
  int hash = (int)((uintptr_t)scope % SCOPE_HASH);
  for (int i = 0; i < visitCap[hash]; i++) {
    if (visited[hash][i] == scope) {
      // Shift elements up to fill the gap
      memmove(&visited[hash][i], &visited[hash][i + 1],
              sizeof(Scope *) * (visitCap[hash] - i - 1));

      // Decrement the visit capacity
      visitCap[hash]--;
      return;
    }
  }
#ifdef DEBUG
  // Should never happen (misuse)
  if (debugLogs) {
    logDebug(__FILE__, __func__, "scope not found");
  }
#endif
}

void printFgColor(int fgCode, FILE *stream) {
  char buffer[24] = {'\0'};
  if (fgCode < -1) {
    fgCode += 16777217;  // reveal
    unsigned int red = (fgCode >> 16) & 0xFF;
    unsigned int green = (fgCode >> 8) & 0xFF;
    unsigned int blue = fgCode & 0xFF;
    snprintf(buffer, sizeof(buffer), "\033[38;2;%u;%u;%um", red, green, blue);
  } else if (fgCode < 0) {
    // ignore  (-1)
  } else if (fgCode < 8) {
    snprintf(buffer, sizeof(buffer), "\033[3%dm", fgCode);
  } else if (fgCode < 16) {
    snprintf(buffer, sizeof(buffer), "\033[9%dm", fgCode - 8);
  } else if (fgCode < 256) {
    snprintf(buffer, sizeof(buffer), "\033[38:5:%dm", fgCode);
  }  // else also ignore
  directPrint(buffer, stream);
}

void printBgColor(int bgCode, FILE *stream) {
  char buffer[24] = {'\0'};
  if (bgCode < -1) {
    bgCode += 16777217;  // reveal
    unsigned int red = (bgCode >> 16) & 0xFF;
    unsigned int green = (bgCode >> 8) & 0xFF;
    unsigned int blue = bgCode & 0xFF;
    snprintf(buffer, sizeof(buffer), "\033[48;2;%u;%u;%um", red, green, blue);
  } else if (bgCode < 0) {
    // ignore (-1)
  } else if (bgCode < 8) {
    snprintf(buffer, sizeof(buffer), "\033[4%dm", bgCode);
  } else if (bgCode < 16) {
    snprintf(buffer, sizeof(buffer), "\033[10%dm", bgCode - 8);
  } else if (bgCode < 256) {
    snprintf(buffer, sizeof(buffer), "\033[48:5:%dm", bgCode);
  }  // else also ignore
  directPrint(buffer, stream);
}

char *getFryMeta(void) {
  char *list[512] = {0};
  size_t pos = 0;

  asprintf(&list[pos++], "{\n  fryVersion    = '%s'\n", FRY_VERSION);
  asprintf(&list[pos++], "  platform      = '%s'\n", PLATFORM);
  asprintf(&list[pos++], "  arch          = '%s'\n", ARCHITECTURE);

#ifdef __EMSCRIPTEN__
  asprintf(&list[pos++], "  buildMode     = 'web'\n");
#elif DEBUG
  asprintf(&list[pos++], "  buildMode     = 'debug'\n");
#elif TURBO
  asprintf(&list[pos++], "  buildMode     = 'turbo'\n");
#else
  asprintf(&list[pos++], "  buildMode     = 'default'\n");
#endif

  asprintf(&list[pos++], "  buildDate     = '%s'\n", __DATE__);
#ifdef USE_NCURSES
  asprintf(&list[pos++], "  tuiBackend    = 'ncurses'\n");
#else
  asprintf(&list[pos++], "  tuiBackend    = 'fatcurses'\n");
#endif

#ifdef USE_READLINE
  asprintf(&list[pos++], "  editBackend   = 'readline'\n");
#elif __EMSCRIPTEN__
  asprintf(&list[pos++], "  editBackend   = 'webenv'\n");
#else
  asprintf(&list[pos++], "  editBackend   = 'linenoise'\n");
#endif

  asprintf(&list[pos++], "  rcFile        = '%s'\n", RC_FILE);
  asprintf(&list[pos++], "  replFile      = '%s'\n", REPL_FILE);
  asprintf(&list[pos++], "  dateFmt       = '%s'\n", DEFAULT_DATE_FMT);
  asprintf(&list[pos++], "  maxToInt      = %.0f\n", MAX_TO_INT);
  asprintf(&list[pos++], "  defaultMem    = %ld\n", DEFAULT_MEM);
  asprintf(&list[pos++], "  memoryLimit   = %ld\n", memoryLimit);
  asprintf(&list[pos++], "  minDepth      = %d\n", MIN_DEPTH);
  asprintf(&list[pos++], "  defDepth      = %d\n", DEF_DEPTH);
  asprintf(&list[pos++], "  maxDepth      = %d\n", MAX_DEPTH);
  asprintf(&list[pos++], "  stackDepth    = %d\n", stackDepth);
  asprintf(&list[pos++], "  stackTrace    = %d\n", STACK_TRACE);
  asprintf(&list[pos++], "  minQuickRef   = %d\n", MIN_QUICK_REF);
  asprintf(&list[pos++], "  skipSize      = %d\n", SKIP_SIZE);
  asprintf(&list[pos++], "  skipListMin   = %d\n", SKIP_LIST_MIN);
  asprintf(&list[pos++], "  skipListMax   = %d\n", SKIP_LIST_MAX);
  asprintf(&list[pos++], "  metaHash      = %d\n", META_HASH);
  asprintf(&list[pos++], "  scopeHash     = %d\n", SCOPE_HASH);
  asprintf(&list[pos++], "  findScpDedup  = %d\n", FIND_SCP_DEDUP);
  asprintf(&list[pos++], "  gcPremonition = %d\n", GC_PREMONITION);
  asprintf(&list[pos++], "  gcThreadLag   = %d\n", GC_THREAD_LAG);
  asprintf(&list[pos++], "  maxContextMem = %d\n", MAX_CONTEXT_MEM);
  asprintf(&list[pos++], "  typeMax       = %d\n", TYPE_MAX);
  asprintf(&list[pos++], "  indent        = %d\n", INDENT);
  asprintf(&list[pos++], "  argsStyleMax  = %d\n", ARG_STYLE_MAX);
  asprintf(&list[pos++], "  localStyleMax = %d\n", LOCAL_STYLE_MAX);
  asprintf(&list[pos++], "  initDirLen    = %d\n", INIT_DIR_LEN);
  asprintf(&list[pos++], "  dirListMax    = infinity\n");
  asprintf(&list[pos++], "  initBuffLen   = %d\n", INIT_B_LEN);
  asprintf(&list[pos++], "  buffLen       = %d\n", BUFF_LEN);
  asprintf(&list[pos++], "  entryNameMax  = %d\n", ENTRY_NAME_MAX);
  asprintf(&list[pos++], "  bunStack      = %d\n", BUN_STACK);
  asprintf(&list[pos++], "  importMax     = %d\n", IMPORT_MAX);
  asprintf(&list[pos++], "  derivedSize   = %d\n", DERIVED_SIZE);
  asprintf(&list[pos++], "  sizeOfToken   = %d\n", (int)sizeof(Token));
  asprintf(&list[pos++], "  sizeOfReader  = %d\n", (int)sizeof(Reader));
  asprintf(&list[pos++], "  sizeOfNode    = %d\n", (int)sizeof(Node));
  asprintf(&list[pos++], "  sizeOfMeta    = %d\n", (int)sizeof(Memory));
  asprintf(&list[pos++], "  sizeOfScope   = %d\n", (int)sizeof(Scope));
  asprintf(&list[pos++], "  sizeOfEntry   = %d\n", (int)sizeof(Entry));
  asprintf(&list[pos++], "  sizeOfContext = %d\n", (int)sizeof(Context));
  asprintf(&list[pos++], "  sizeOfWorker  = %d\n", (int)sizeof(Worker));
  asprintf(&list[pos++], "  sizeOfHugeInt = %d\n", (int)sizeof(HugeInt));
  asprintf(&list[pos++], "  isAstOnly     = %s\n", ofBool(isAstOnly));
  asprintf(&list[pos++], "  debugLogs     = %s\n", ofBool(debugLogs));
  asprintf(&list[pos++], "  crashOnError  = %s\n", ofBool(crashOnError));
  if (configOnce) {
    asprintf(&list[pos++], "  randomSeedA   = 0x%" PRIx64 "\n", xorshiftStateA);
    asprintf(&list[pos++], "  randomSeedB   = 0x%" PRIx64 "\n", xorshiftStateB);
    asprintf(&list[pos++], "  randomSeedC   = 0x%" PRIx64 "\n", xorshiftStateC);
  }

#ifdef FFI_SUPPORT
  asprintf(&list[pos++], "  ffiSupport    = true\n");
  asprintf(&list[pos++], "  maxDlls       = %d\n", MAX_DLLS);
#else
  asprintf(&list[pos++], "  ffiSupport    = false\n");
  asprintf(&list[pos++], "  maxDlls       = 0\n");
#endif

  asprintf(&list[pos++], "  strictProcs   = true\n");

#ifdef SSL_SUPPORT
  asprintf(&list[pos++], "  sslSupport    = true\n");
  asprintf(&list[pos++], "  sslVersion    = 0x%lx\n", OPENSSL_VERSION_NUMBER);
#else
  asprintf(&list[pos++], "  sslSupport    = false\n");
  asprintf(&list[pos++], "  sslVersion    = 0\n");
#endif

  asprintf(&list[pos++], "  ebCmdSupport  = [");

  EbCmd lastCmd = EbStorableErase;

  for (EbCmd i = 1; i <= lastCmd; i++) {
    const char *cmd = ebCmd(i);
    asprintf(&list[pos++], "'%s'", cmd);
    if (i == lastCmd) {
      asprintf(&list[pos++], "]\n");
    } else {
      asprintf(&list[pos++], ",");
    }
  }
  asprintf(&list[pos++], "}\n");

  // Sanity check with leeway for configOnce and debug comment
  int totalItems = pos + 5;
  int totalSize = sizeof(list) / sizeof(char *);
  if (totalItems >= totalSize) {
    fatalOut(__FILE__, __func__, MSG_BMO);
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }

#ifdef DEBUG
  // Add debug comment
  asprintf(&list[pos++], "#%d/%d\n", totalItems, totalSize);
#endif

  char *result = mkString(list);

  // Clean up
  freeEachStrInList(list);

  return result;
}
