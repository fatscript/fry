/**
 * @file dependencies.h
 * @brief All the good stuff
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-11
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license information.
 */

#pragma once

#define _GNU_SOURCE

// C standard libraries
#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <inttypes.h>
#include <limits.h>
#include <locale.h>
#include <math.h>
#include <signal.h>
#include <stdarg.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdnoreturn.h>
#include <string.h>
#include <strings.h>
#include <time.h>

// POSIX libraries
#include <arpa/inet.h>
#include <dirent.h>
#include <dlfcn.h>
#include <fcntl.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <net/if_arp.h>
#include <netdb.h>
#include <netinet/in.h>
#include <poll.h>
#include <pthread.h>
#include <regex.h>
#include <sys/ioctl.h>
#include <sys/resource.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <termios.h>
#include <unistd.h>

// Network libraries (GNU and MacOS)
#ifndef __APPLE__
#include <netpacket/packet.h>
#else
#include <net/if_dl.h>
#include <net/if_types.h>
#endif

// Other libraries
#ifdef FFI_SUPPORT
#ifdef __APPLE__
#include <ffi/ffi.h>
#else
#include <ffi.h>
#endif
#endif
#ifndef __EMSCRIPTEN__
#include <curl/curl.h>
#ifdef USE_READLINE
#include <readline/history.h>
#include <readline/readline.h>
#else  // use linenoise instead
#include "encodings/utf8.h"
#include "linenoise.h"
#define add_history linenoiseHistoryAdd
#define readline linenoise
#endif
#define directPrint fputs
#ifdef SSL_SUPPORT
#define OPENSSL_VERSION_WITH_NEW_TYPES 0x30000000L
#include <openssl/err.h>
#include <openssl/evp.h>
#include <openssl/rand.h>
#include <openssl/ssl.h>
#endif
#else  // __EMSCRIPTEN__
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>

#include "webenv.h"
#endif

// silence fallthrough on old compiler versions
#if defined(__GNUC__) && __GNUC__ >= 7 || \
  defined(__clang__) && __clang_major__ >= 12
#define FALL_THROUGH __attribute__((fallthrough))
#else
#define FALL_THROUGH ((void)0)
#endif /* __GNUC__ >= 7 */

// extras for curses
#define KEY_ESC 27
#define KEY_SPACE 32

// extra for fcntl
#define WRITE_FLAGS (O_WRONLY | O_CREAT | O_TRUNC)

// extra for sys
#define EXECUTABLE_FILE_PERMISSIONS \
  (S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH)
