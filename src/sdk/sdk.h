/**
 * @file sdk.h
 * @brief fry development toolkit
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-09
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#pragma once

#include "b64.h"
#include "crypto.h"
#include "dependencies.h"
#include "huge.h"
#include "logger.h"
#include "memory.h"
#include "parameters.h"
#include "structures.h"
#include "sugar.h"

#ifndef FRY_VERSION
#define FRY_VERSION "unversioned"
#endif

// Define platform string
#ifdef __EMSCRIPTEN__
#define PLATFORM "web"
#elif __APPLE__
#define PLATFORM "macos"
#elif __ANDROID__
#define PLATFORM "droid"
#elif __linux__
#define PLATFORM "linux"
#else
#define PLATFORM "unix"  // assuming Unix-like
#endif

// Define architecture string
#ifdef __x86_64__
#define ARCHITECTURE "x86_64"
#elif __ppc64__
#define ARCHITECTURE "ppc64"
#elif __aarch64__
#define ARCHITECTURE "aarch64"
#elif __ia64__
#define ARCHITECTURE "ia64"
#else
#define ARCHITECTURE "386"  // maybe!?
#endif

#ifndef USE_NCURSES
#include "fatcurses.h"
#else
#include <curses.h>  // dynamically linked
#endif

#define GUIDE " > "

// obfuscation / shutdown / interpreter options
extern char *bundleKey;
extern bool crashOnError;
extern bool hasFoundErrors;
extern bool configOnce;
extern bool cursesMode;
extern bool isAstOnly;
extern Scope *globalScope;
extern char *currentLocale;
extern char *basePath;
extern bool isUtf8Mode;  // maintained by setFryLocale
extern int stackDepth;
extern int maxStackUsed;  // benchmark info only

// meta space / type checks (native types) quick hacky refs
extern Type *metaSpace[META_HASH];
extern Type *ckTypeType;
extern Type *checkVoid;
extern Type *checkBoolean;
extern Type *checkNumber;
extern Type *checkHugeInt;
extern Type *checkChunk;
extern Type *checkText;
extern Type *checkList;
extern Type *checkScope;
extern Type *checkMethod;
extern Type *checkError;

// number sub-types
extern Type *ckEpoch;
extern Type *ckExitCode;

// chunk sub-type
extern Type *ckCPointer;

// method sub-type
extern Type *checkProcedure;

// error sub-types
extern Type *ckAssignError;  // assign a new value to an immutable entry
extern Type *ckAsyncError;   // asynchronous operation failure
extern Type *ckCallError;    // a call is made with insufficient arguments
extern Type *ckFileError;    // file operation failure
extern Type *ckIndexError;   // index is out of list/text bounds
extern Type *ckKeyError;     // the key (name) is not found in scope
extern Type *ckSyntaxError;  // syntax or code structure error
extern Type *ckTypeError;    // mismatch of method call or return
extern Type *ckValueError;   // type is right, but content is valid

// strict type
extern Type *checkStrict;

// runtime node deduplication (see also cow in scopes.c)
extern Node *trueSingleton;
extern Node *falseSingleton;
#define RUNTIME_BOOLEAN(val) ((val) ? trueSingleton : falseSingleton)

// traverse scopes recursively once
extern Scope **visited[SCOPE_HASH];
extern int visitCap[SCOPE_HASH];

// dynamic link libraries
extern DLL dllSpace[MAX_DLLS];

extern pthread_t mainThreadId;

#ifdef SSL_SUPPORT
extern SSL_CTX *sslClientCtx;
extern SSL_CTX *sslServerCtx;
#endif

typedef enum { VerMinimal, VerDebugger, VerHeader, VerWarranty } VersionMode;

/**
 * @brief Check if node is defined and of given type (macro).
 *
 * @param n Node*
 * @param t NodeType
 * @return boolean
 */
#define IS_FAT_TYPE(n, t) ((n) && ((n)->type == (t)))

/**
 * @brief Check if node is of type error (macro).
 *
 * @param n Node* non-null
 * @return boolean
 */
#define IS_FAT_ERROR(n) ((n)->type == FatError)

/**
 * @brief Evaluates whether two strings x and y are equal (macro).
 * @note This is a proven zero-tradeoff optimization for strcmp.
 *
 * @param x char* non-null
 * @param y char* non-null
 * @return boolean
 */
#define FAST_STR_EQ(x, y) (*(x) == *(y) && strcmp((x), (y)) == 0)

/**
 * @brief Get the version code without prepended 'v'.
 *
 * @return Const
 */
const char *getFryV(void);

/**
 * @brief Print fry version with more or less details.
 *
 * @param mode VersionMode
 */
void printVersion(VersionMode mode);

/**
 * @brief Convert Float to string, discarding decimals if zeros.
 *
 * @param f Float (floating point number) to convert to string
 * @param buff char * of NUMBER_MAX_LENGTH + 1 (optional)
 * @return string (use free if no buff is provided)
 */
char *prettyNumber(char *buff, double f);

/**
 * @brief Checks if there is any error handler in context.
 *
 * @param ctx Context*
 * @return boolean
 */
bool hasErrorHandler(Context *ctx);

/**
 * @brief Print error, and crash from error handler.
 * @note Call this if an error is raised within an error handler.
 *
 * @param msg string (error message)
 * @param ctx Context*
 */
noreturn void crashErrorHandler(char *msg, Context *ctx);

/**
 * @brief Print error, context stack and call exitFry.
 *
 * @param err Node* (error)
 * @param ctx Context*
 */
noreturn void crashFromError(Node *err, Context *ctx);

/**
 * @brief Create an Error node from message.
 *
 * @param msg error message
 * @param copy should copy original string?
 * @param ck Type* erro sub-type (optional)
 * @param ctx Context*
 * @return Node*
 */
Node *createError(char *msg, bool copy, Type *ck, Context *ctx);

/**
 * @brief Gets the adaptive modulus (rest) for a given index.
 * Note: see also adaptiveSkipMax at lists.c
 *
 * @param index position in skip-list
 * @return long
 */
long adaptiveSkipMod(long index);

/**
 * @brief Stringify primitive value.
 *
 * @param node Node*
 * @return string (use free)
 */
char *toString(const Node *node);

/**
 * @brief Join nodes in a list as string with separator.
 *
 * @param list Scope*
 * @param sep string (separator)
 * @return string (use free)
 */
char *joinListItems(Scope *list, const char *sep);

/**
 * @brief Stringify node content as JSON.
 *
 * @param node Node*
 * @param strict bool (use interoperable format)
 * @return string (use free)
 */
char *toJson(const Node *node, bool strict);

/**
 * @brief Extracts type name of the node into a string.
 *
 * @param node Node*
 * @return string (use free)
 */
char *nodeTypeToString(Node *node);

/**
 * @brief Inits scope layer (pair with wipeScope).
 *
 * @param layer layered scope
 */
void initLayer(Scope *layer);

/**
 * @brief Frees up scope entries except for optional "keep" arg.
 * @note If willReuse is set, you have to manually destroy the scope mutex.
 *
 * @param scope to erase
 * @param keep free all but this entry (optional)
 * @param willReuse should keep the mutex?
 */
void wipeScope(Scope *scope, const Entry *keep, bool willReuse);

/**
 * @brief Create a runtime number node (shorthand).
 *
 * @param num number
 * @param ctx Context*
 * @return Node*
 */
Node *runtimeNumber(double num, Context *ctx);

/**
 * @brief Create a runtime huge int node (shorthand).
 *
 * @param num huge number
 * @param ctx Context*
 * @return Node*
 */
Node *runtimeHuge(HugeInt num, Context *ctx);

/**
 * @brief Create a runtime chunk node (shorthand).
 *
 * @param val binary data
 * @param len data size
 * @param ctx Context*
 * @return Node*
 */
Node *runtimeChunk(char *val, size_t len, Context *ctx);

/**
 * @brief Create a runtime text node (shorthand).
 *
 * @param val string
 * @param len string length
 * @param ctx Context*
 * @return Node*
 */
Node *runtimeText(char *val, size_t len, Context *ctx);

/**
 * @brief Duplicate string and create a runtime text node (shorthand).
 *
 * @param val string
 * @param ctx Context*
 * @return Node*
 */
Node *runtimeTextDup(const char *val, Context *ctx);

/**
 * @brief Create a runtime scope node (shorthand).
 *
 * @param scope Scope*
 * @param ctx Context*
 * @return Node*
 */
Node *runtimeCollection(Scope *scope, Context *ctx);

/**
 * @brief Evaluates node value as boolean.
 *
 * @param node to evaluate
 * @return boolean
 */
bool booleanOf(Node *node);

/**
 * @brief Check if two fat nodes are identical.
 *
 * @param a Node*
 * @param b Node*
 * @return boolean
 */
bool nodeEq(Node *a, Node *b);

/**
 * @brief Check if two scopes are identical.
 *
 * @param a Scope*
 * @param b Scope*
 * @param level int (start with 0)
 * @return boolean
 */
bool scopeEq(Scope *a, Scope *b, int level);

/**
 * @brief Create a composite type.
 *
 * @param name string
 * @param types list of strings
 * @return Type*
 */
Type *createComposite(const char *name, char **types);

/**
 * @brief Resolve original type form alias.
 *
 * @param ck alias
 * @return Type*
 */
Type *resolveAlias(Type *ck);

/**
 * @brief Check if type has the same base type (on alias chain).
 *
 * @param base type
 * @param alias type
 * @return boolean
 */

bool isAlias(const Type *base, Type *alias);

/**
 * @brief Check if value container is of type.
 *
 * @param ck type of entry/container
 * @param node value
 * @return are compatible
 */
bool checkType(const Type *ck, Node *node);

/**
 * @brief Check if value container is of type or alias.
 *
 * @param ck type of entry/container
 * @param node value
 * @return are compatible
 */
bool checkAlias(const Type *ck, Node *node);

/**
 * @brief Get the Method's arity.
 *
 * @param method Node* (FatMethod expected)
 * @return int
 */
int getMethodArity(Node *method);

/**
 * @brief Uses smart logic to duplicate (or not) source ref.
 *
 * @param node which a copy is being taken
 * @return string smart source name (see also: deleteNode)
 */
char *cpSrc(Node *node);

/**
 * @brief Loads DLL via dlopen, if not yet open.
 *
 * @param name DLL filename
 * @return int loaded DLL index
 */
int initDLL(const char *name);

/**
 * @brief Return the DLL struct pointer, if already loaded.
 *
 * @param index loaded DLL index
 * @return DLL* (returns NULL on failure)
 */
DLL *getDLL(int index);

/**
 * @brief Exits curses mode and sets control flag.
 */
void endCursesMode(void);

/**
 * @brief Catch signal, trap other signals and exit cleanly.
 *
 * @param sig signal code
 */
noreturn void exitFromSignal(ExitCode sig);

/**
 * @brief Call cleanup hooks and terminates with exit code.
 *
 * @param code exit code
 */
noreturn void exitFry(ExitCode code);

/**
 * @brief Set fry locale setting (or fallback to system default).
 *
 * @param locale string
 * @return boolean (false if using fallback)
 */
bool setFryLocale(const char *locale);

/**
 * @brief Is a folder import (using dot-underscore)
 *
 * @param path string
 * @return boolean
 */
bool canExpandPath(const char *path);

/**
 * @brief Extract and the import base path a given path (entry point)
 *
 * @param filepath string
 * @return string (use free, if non-null)
 */
char *getBasePath(const char *filepath);

/**
 * @brief Expand folder into synthetic source of imports, or read the file.
 *
 * @param path import path
 * @return string (null on error, use free)
 */
char *getSourceForPath(const char *path);

/**
 * @brief Prepare visited registry for traversing/visiting.
 * @note Use paired with endVisit.
 * @param func function reference
 * @return boolean (is it ok to proceed?)
 */
bool initVisit(const char *func);

/**
 * @brief Clean up visited registry.
 */
void endVisit(void);

/**
 * @brief Mark scope visited scopes to registry if first time.
 *
 * @param scope Scope* (possibly nested)
 * @return boolean (true if first time)
 */
bool isFirstVisit(Scope *scope);

/**
 * @brief Unmarks a scope as visited from registry.
 *
 * @param scope Scope* (already tagged with isFirstVisit)
 */
void unVisit(Scope *scope);

/**
 * @brief Prints the ANSI escape code to set the foreground color.
 *
 * @param fgCode color code for the foreground
 * @param stream output where the escape code should be written
 */
void printFgColor(int fgCode, FILE *stream);

/**
 * @brief Prints the ANSI escape code to set the background color.
 *
 * @param bgCode color code for the background
 * @param stream output where the escape code should be written
 */
void printBgColor(int bgCode, FILE *stream);

/**
 * @brief Return a string with fry metadata.
 *
 * @return string (use free)
 */
char *getFryMeta(void);
