/**
 * @file server.c
 * @brief Socket connection base
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-22
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "server.h"

#include "sdk.h"

__thread bool verifySSL = true;
__thread uint16_t listenedPort = 0;

#ifdef SSL_SUPPORT

#ifdef DEBUG
static void ssl_info_callback(const SSL *ssl, int where, int ret) {
  if (!debugLogs) {
    return;
  }
  const char *str;
  int w = where & ~SSL_ST_MASK;
  if (w & SSL_ST_CONNECT) {
    str = "SSL_connect";
  } else if (w & SSL_ST_ACCEPT) {
    str = "SSL_accept";
  } else {
    str = "undefined";
  }
  if (where & SSL_CB_LOOP) {
    fprintf(stderr, MRG_STR "%s:%s\n", str, SSL_state_string_long(ssl));
  } else if (where & SSL_CB_ALERT) {
    str = (where & SSL_CB_READ) ? "read" : "write";
    fprintf(stderr, MRG_STR "SSL3 alert %s:%s:%s\n", str,
            SSL_alert_type_string_long(ret), SSL_alert_desc_string_long(ret));
  } else if (where & SSL_CB_EXIT) {
    if (ret == 0) {
      fprintf(stderr, MRG_STR "%s:failed in %s\n", str,
              SSL_state_string_long(ssl));
    } else if (ret < 0) {
      fprintf(stderr, MRG_STR "%s:error in %s\n", str,
              SSL_state_string_long(ssl));
    }
  }
}
#endif

int initServerSSL(const char *certPath, const char *keyPath) {
  if (sslServerCtx) {
    return 1;  // SSL context already set
  }

  sslServerCtx = SSL_CTX_new(TLS_server_method());
  if (!sslServerCtx) {
    return 2;  // failed to create SSL context
  }

  SSL_CTX_use_certificate_file(sslServerCtx, certPath, SSL_FILETYPE_PEM);
  SSL_CTX_use_PrivateKey_file(sslServerCtx, keyPath, SSL_FILETYPE_PEM);
  if (!SSL_CTX_check_private_key(sslServerCtx)) {
    return 3;  // SSL failed key validation
  }

  logMarker("SSL/TLS enabled! Accepting secure connections only.");

#ifdef DEBUG
  SSL_CTX_set_info_callback(sslServerCtx, ssl_info_callback);
#endif

  return 0;  // success
}

int initClientSSL(void) {
  if (sslClientCtx) {
    return 1;  // SSL context already set
  }

  sslClientCtx = SSL_CTX_new(TLS_client_method());
  if (!sslClientCtx) {
    return 2;  // failed to create SSL context
  }

  // Load system default CA certificates
  if (!SSL_CTX_set_default_verify_paths(sslClientCtx)) {
    logAlert(__FILE__, __func__, ERR_reason_error_string(ERR_get_error()));
    return 3;  // failed to set default CA paths
  }

  if (verifySSL) {
    // Require certificate verification
    SSL_CTX_set_verify(sslClientCtx, SSL_VERIFY_PEER, NULL);
  } else {
    // Explicitly disable verification
    SSL_CTX_set_verify(sslClientCtx, SSL_VERIFY_NONE, NULL);
  }

#ifdef DEBUG
  SSL_CTX_set_info_callback(sslClientCtx, ssl_info_callback);
#endif

  return 0;  // success
}

SSL *handshakeSSL(int fd) {
  SSL *ssl = SSL_new(sslServerCtx);
  SSL_set_fd(ssl, fd);
  if (SSL_accept(ssl) <= 0) {
    unsigned long err;
    while ((err = ERR_get_error())) {
      fprintf(stderr, MRG_STR "SSL Error: %s\n", ERR_error_string(err, NULL));
    }
    logAlert(__FILE__, __func__, "failed SSL handshake");
    SSL_shutdown(ssl);
    SSL_free(ssl);
    close(fd);
    return NULL;  // failure
  }

  return ssl;  // success
}

#endif

int startListening(uint16_t port) {
#ifdef __APPLE__
  int serverFd = socket(AF_INET, SOCK_STREAM, 0);
#else
  int serverFd = socket(AF_INET, SOCK_STREAM | SOCK_CLOEXEC, 0);
#endif
  if (serverFd < 0) {
    fatalOut(__FILE__, __func__, "unable to create socket");
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }

  struct sockaddr_in server = {0};
  server.sin_family = AF_INET;
  server.sin_port = htons(port);
  server.sin_addr.s_addr = htonl(INADDR_ANY);
  socklen_t server_len = sizeof(server);

  if (bind(serverFd, (struct sockaddr *)&server, server_len) < 0) {
    fatalOut(__FILE__, __func__, "unable to bind socket");
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }

  if (listen(serverFd, SOMAXCONN) < 0) {
    fatalOut(__FILE__, __func__, "unable to listen");
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }

  // Get the port that was assigned
  if (port == 0) {
    if (getsockname(serverFd, (struct sockaddr *)&server, &server_len) == -1) {
      fatalOut(__FILE__, __func__, "unable to get socket address");
      exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
    }

    port = ntohs(server.sin_port);
  }

  listenedPort = port;
  return serverFd;
}

ssize_t sendMessage(int fd, void *ssl, const char *msg, size_t len) {
#ifndef SSL_SUPPORT
  (void)ssl;
#endif

  if (!len) {
    return 0;
  }

  ssize_t bytes = 0;
  size_t totalSent = 0;

  while (totalSent < len) {
#ifdef SSL_SUPPORT
    if (ssl) {
      bytes = SSL_write((SSL *)ssl, msg + totalSent, len - totalSent);
      if (bytes <= 0) {
        break;
      }
    } else {
#endif
      bytes = write(fd, msg + totalSent, len - totalSent);
      if (bytes <= 0) {
        break;
      }
#ifdef SSL_SUPPORT
    }
#endif
    totalSent += bytes;
  }

  if (totalSent < len) {
    return -1;
  }

  return (ssize_t)totalSent;
}

ssize_t receiveMessage(int fd, void *ssl, char *buff, size_t len) {
  ssize_t bytes = 0;
  errno = 0;

#ifdef SSL_SUPPORT
  if (ssl) {
    bytes = SSL_read((SSL *)ssl, buff, len);
    if (bytes < 0) {
      unsigned long err_code = 0;
      switch (SSL_get_error((SSL *)ssl, bytes)) {
        case SSL_ERROR_ZERO_RETURN:
          logAlert(__FILE__, __func__, "SSL connection closed by peer");
          break;
        case SSL_ERROR_WANT_READ:
        case SSL_ERROR_WANT_WRITE:
          logAlert(__FILE__, __func__, "SSL read did not complete");
          break;
        case SSL_ERROR_SYSCALL:
          logAlert(__FILE__, __func__, strerror(errno));
          break;
        default:
          err_code = ERR_get_error();
          if (err_code != 0) {
            char err_buf[256];
            ERR_error_string_n(err_code, err_buf, sizeof(err_buf));
            logAlert(__FILE__, __func__, err_buf);
          } else {
            logAlert(__FILE__, __func__, "SSL read unknown error");
          }
      }
    }
  } else {
#endif
    bytes = read(fd, buff, len);
    if (bytes < 0) {
      if (errno) {
        logAlert(__FILE__, __func__, strerror(errno));
      }
    }
#ifdef SSL_SUPPORT
  }
#else
  (void)ssl;
#endif

  return bytes;
}

int sockAcceptTimeout(int serverFd, struct sockaddr *addr, socklen_t *addrlen,
                      int timeoutMs) {
  struct pollfd fds[1] = {0};
  fds[0].fd = serverFd;
  fds[0].events = POLLIN;

  // Wait until the serverFd is readable (new connection) or until timeout
  int pollRes = poll(fds, 1, timeoutMs);
  if (pollRes < 0) {
    return -1;
  }

  if (pollRes == 0) {
    // Timed out; no new connection within timeoutMs
    errno = EAGAIN;
    return -1;
  }

  // The descriptor is ready, check for potential errors
  if (fds[0].revents & (POLLERR | POLLNVAL | POLLHUP)) {
    // The socket is in an error state, closed, or invalid
    errno = EBADF;
    return -1;
  }

  // If we get here, we can safely call accept()
#ifdef __APPLE__
  return accept(serverFd, addr, addrlen);
#else
  return accept4(serverFd, addr, addrlen, SOCK_CLOEXEC);
#endif
}

void closeSocket(int fd, void *ssl) {
  if (fd < 0) {
    return;  // prevent double free
  }
#ifdef SSL_SUPPORT
  if (ssl) {
    SSL_shutdown((SSL *)ssl);
    SSL_free((SSL *)ssl);
  }
#else
  (void)ssl;
#endif
  close(fd);
}
