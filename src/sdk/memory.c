/**
 * @file memory.c
 * @brief Memory management and garbage collector functions
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-20
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "sdk.h"

long memoryLimit = DEFAULT_MEM;
long maxAllocated = 0;
long lastGCUsage = INIT_GC_HEURISTIC;
atomic_long activeMemory;
bool useCollector = true;
bool memoryRedefined = false;
Memory *memRegistry = NULL;

Scope **scopesRegistry[SCOPE_HASH] = {0};
int scpRegistryCap[SCOPE_HASH] = {0};
int trackedScopes = 0;
int maxTrackedScopes = 0;
#ifdef DEBUG
long cacheHits = 0;
Node *parsedPrograms = NULL;
#endif

double gcTotalTimeMs = 0;
bool warnHighGC = false;
atomic_bool hasLoggedWorkerBlocked;

pthread_mutex_t memoryLock;
pthread_mutexattr_t recursiveMutex;

pthread_mutex_t atomicLock;

atomic_long threadCounter;
atomic_int activeThreads;
atomic_int maxThreadCount;

#ifndef DEBUG
static int dummy_mutex_noop(pthread_mutex_t *ignoredLock) {
  (void)ignoredLock;
  return 0;  // do nothing
}
#endif

int (*lockResource)(pthread_mutex_t *) = NULL;
int (*unlockResource)(pthread_mutex_t *) = NULL;

void initMemoryManagement(void) {
  atomic_init(&activeMemory, 0);
  atomic_init(&hasLoggedWorkerBlocked, false);
  atomic_init(&threadCounter, 0);
  atomic_init(&activeThreads, 1);
  atomic_init(&maxThreadCount, 1);

#ifndef DEBUG
  // Initially point to dummy function, see asyncStart at async.c
  lockResource = dummy_mutex_noop;
  unlockResource = dummy_mutex_noop;
#else
  // use real lock/unlock functions from the start, when built with debug flag
  // helps verify locking logic, but slows down single-threaded code by ~30%
  lockResource = pthread_mutex_lock;
  unlockResource = pthread_mutex_unlock;
#endif

  // Initialize the mutex attributes
  if (pthread_mutexattr_init(&recursiveMutex)) {
    fatalOut(__FILE__, __func__, "failed to initialize attr");
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }

  // Set the mutex type to recursive
  if (pthread_mutexattr_settype(&recursiveMutex, PTHREAD_MUTEX_RECURSIVE)) {
    fatalOut(__FILE__, __func__, "failed to set mutex type");
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }

  // Initialize global mutexes
  if (pthread_mutex_init(&memoryLock, &recursiveMutex)) {
    fatalOut(__FILE__, __func__, MSG_F_MUTEX);
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }
  if (pthread_mutex_init(&atomicLock, &recursiveMutex)) {
    fatalOut(__FILE__, __func__, MSG_F_MUTEX);
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }
}

void cleanMemoryLock(void) {
  pthread_mutex_destroy(&atomicLock);
  pthread_mutex_destroy(&memoryLock);
  pthread_mutexattr_destroy(&recursiveMutex);
}

// only call this function with global memory locked
static inline void addToGlobalMemory(Memory *memory) {
  memory->next = memRegistry;
  memRegistry = memory;
}

void moveLocalToGlobalMemory(Context *ctx) {
  lockResource(&memoryLock);
  Memory *memory = ctx->temp;
  while (memory) {
    Memory *next = memory->next;
    addToGlobalMemory(memory);
    memory = next;
  }
  ctx->tempCount = 0;
  ctx->temp = NULL;
  unlockResource(&memoryLock);
}

static inline void addToContextMemory(Memory *memory, Context *ctx) {
  memory->next = ctx->temp;
  ctx->temp = memory;
  ctx->tempCount++;

  if (ctx->tempCount == MAX_CONTEXT_MEM) {
    moveLocalToGlobalMemory(ctx);
  }
}

void createMemory(Node *node, Context *ctx) {
  Memory *memory = FRY_ALLOC(sizeof(Memory));

  // Initialize members
  memory->data = node;
  atomic_init(&memory->refs, 0);  // see: bind/unbind Scope methods
  memory->visited = false;
  memory->locked = true;

  node->meta = memory;

  if (ctx) {
    addToContextMemory(memory, ctx);  // contention-less
  } else {
    lockResource(&memoryLock);
    addToGlobalMemory(memory);
    unlockResource(&memoryLock);
  }

#ifdef DEBUG
  if (traceLogs) {
    traceMemory("created", node);
  }
#endif
}

/**
 * Frees node (and it's string value), if not null
 * (also if type definition, dereferences itself)
 */
static void deleteNode(Node *node) {
  if (node->type == FatType) {
    Type *type = getType(node->val);
    if (type && type->def == node) {
      if (debugLogs) {
        logDebug2(__FILE__, __func__, "type definition", type->name);
      }
      type->def = NULL;
    }
  } else if (node->type == FatHugeInt) {
    free(node->num.h);
  }

  unbindScope(node);
  free(node->val);

#ifdef DEBUG
  if (traceLogs) {
    traceMemory("removed", node);
  }
#endif
  if (*node->src != '$') {
    free(node->src);  // see also: cpSrc
  }

  free(node);
}

static void removeMemory(Memory *memory, Memory *prev) {
  assert(memRegistry != NULL);

  // Delete the actual node value
  Node *node = memory->data;
  if (node) {
    deleteNode(node);
  }

  // Remove metadata from registry
  if (prev) {
    prev->next = memory->next;
  } else {
    memRegistry = memory->next;
  }
  atomic_fetch_sub(&activeMemory, 1);
  free(memory);
}

static void visitNode(Node *node, bool follow);

static void visitScope(Scope *scope);

static void visitStack(Context *ctx) {
  assert(ctx != NULL);

  lockResource(&ctx->lock);

  visitNode(ctx->failureEvent, false);
  visitNode(ctx->unclaimed, false);

  Scope *prev[FIND_SCP_DEDUP + 1] = {0};  // null-terminated
  const Frame *stack = ctx->stack;

  for (int i = atomic_load(&ctx->top); i; i--) {
    visitNode(stack[i].node, false);
    visitNode(stack[i].trap, false);

    Scope *scope = stack[i].scp;
    if (scope) {
      // Check if scope is in previous scopes (dedup)
      bool alreadyChecked = false;
      for (int j = 0; prev[j]; j++) {
        if (scope == prev[j]) {
          alreadyChecked = true;
          break;
        }
      }
      if (!alreadyChecked) {
        visitScope(scope);
        // Shift previous and add current scope to the start
        memmove(&prev[1], &prev[0], sizeof(Scope *) * (FIND_SCP_DEDUP - 1));
        prev[0] = scope;
      }
    }
  }

  unlockResource(&ctx->lock);
}

static void visitScope(Scope *scope) {
  if (!isFirstVisit(scope)) {
    return;
  }

  lockResource(&scope->lock);
  for (Entry *entry = scope->entries; entry; entry = entry->next) {
    visitNode(entry->data, false);
  }
  unlockResource(&scope->lock);

  if (scope->async) {
    visitStack(scope->async->ctx);
  }
}

static void visitNode(Node *node, bool follow) {
  if (!node || !node->meta || node->meta->visited) {
    return;
  }
  node->meta->visited = true;
  visitNode(node->head, true);
  visitNode(node->body, true);
  visitNode(node->tail, true);
  if (node->scp) {
    visitScope(node->scp);
  }
  if (follow) {
    for (node = node->seq; node; node = node->seq) {
      visitNode(node, false);
    }
  }
}

static void logGcElapsedTime(const char *func, double elapsedMs) {
  const double act = (double)atomic_load(&activeMemory);
  const double lim = (double)memoryLimit;

  stderrStartLine(CL_MGT);
  fprintf(stderr, MRG_STR LB_CLOCK ": %s took %.0f ms ", func, elapsedMs);
  fprintf(stderr, "(kept: %d%% nodes / ", (int)(act * 100.0 / lim));
  printHumanizedCount(trackedScopes, true, stderr);
  fputs(" scopes)", stderr);
  stderrEndLine();
}

static void logGcInfo(const char *func, double elapsedMs) {
  if (pthread_equal(pthread_self(), mainThreadId)) {
    logGcElapsedTime(func, elapsedMs);
    atomic_store(&hasLoggedWorkerBlocked, false);
  } else if (!atomic_exchange(&hasLoggedWorkerBlocked, true)) {
    stderrStartLine(CL_MGT);
    fprintf(stderr, MRG_STR LB_CLOCK ": %d async workers blocked by GC...",
            atomic_load(&activeThreads) - 1);
    stderrEndLine();
  }
}

void microGC(Node *keep, Memory *until, Context *ctx) {
  Memory *next = NULL;
  Memory *prev = NULL;
  for (Memory *mem = ctx->temp; mem && mem != until; mem = next) {
    next = mem->next;
    Node *node = mem->data;

    // Note: we need to gard methods because it could be a failure handler
    bool isProtected = node == keep || node->type == FatMethod ||
                       node == ctx->failureEvent || node == ctx->unclaimed ||
                       atomic_load(&mem->refs);

    // If protected, continue to next memory reference...
    if (isProtected) {
      prev = mem;
      continue;
    }

    // Otherwise, free node!
    atomic_fetch_sub(&activeMemory, 1);
    ctx->tempCount--;
    if (prev) {
      prev->next = next;
    } else {
      ctx->temp = next;
    }
    deleteNode(node);
    free(mem);
  }
}

/**
 * @brief Perform a garbage collection recursion through all nodes
 *        still accessible from global scope or bound to context
 *        (partial step / incremental)
 *
 * @param ctx Context*
 * @return elapsed time in milliseconds
 */
static double markAndSweep(Context *ctx) {
  if (!useCollector || !ctx) {
    return 0;
  }

  // Prepare for GC, move thread registered nodes (local) to global metaspace
  moveLocalToGlobalMemory(ctx);

  // Allow GC to run only on main thread
  if (!pthread_equal(pthread_self(), mainThreadId)) {
    msSleep(GC_THREAD_LAG);  // hope for main thread to run GC asap
    return GC_THREAD_LAG;
  }

  if (!initVisit(__func__)) {
    return 0;  // prevent GC while some other traversal is taking place
  }

#ifdef DEBUG
  if (debugLogs) {
    logDebug(__FILE__, __func__, "start");
  }
#endif

  clock_t tack = clock();

  if (atomic_load(&activeMemory) > memoryLimit - (GC_PREMONITION * 2 / 3)) {
    warnHighGC = true;
  }

  // Unmark node references
  Memory *mem = memRegistry;
  for (; mem; mem = mem->next) {
    mem->visited = false;
  }

  // Mark stack references
  visitStack(ctx);

  // Mark metaSpace references
  for (int hash = 0; hash < META_HASH; hash++) {
    for (Type *ck = metaSpace[hash]; ck; ck = ck->next) {
      visitNode(ck->def, false);
    }
  }

  // Collect "lost" scopes
  for (int hash = 0; hash < SCOPE_HASH; hash++) {
    for (int i = 0; i < scpRegistryCap[hash]; i++) {
      if (isFirstVisit(scopesRegistry[hash][i])) {
        collectScope(scopesRegistry[hash][i]);
      }
    }
  }

  // Collect "orphan" nodes (2nd pass)
  Memory *prev = NULL;
  Memory *next = NULL;
  for (mem = memRegistry; mem; mem = next) {
    next = mem->next;  // mem is freed during delete node
    if (!mem->visited && !mem->locked && !atomic_load(&mem->refs)) {
      removeMemory(mem, prev);
    } else {
      prev = mem;
    }
  }

  endVisit();

#ifdef DEBUG
  if (debugLogs) {
    logDebug(__FILE__, __func__, "end");
  }
#endif

  // Track time spent with garbage collector
  double elapsedMs = (double)(clock() - tack) * 1000 / CLOCKS_PER_SEC;
  gcTotalTimeMs += elapsedMs;
  return elapsedMs;
}

double fullGC(Context *ctx) {
  double totalTime = 0;
  long activeNodes = atomic_load(&activeMemory);

  for (long prevCount = 0, cycles = GC_MAX_PASSES;
       activeNodes != prevCount && cycles;
       activeNodes = atomic_load(&activeMemory), cycles--) {
    prevCount = activeNodes;
    totalTime += markAndSweep(ctx);
  }

  if (pthread_equal(pthread_self(), mainThreadId)) {
    lastGCUsage = activeNodes;  // stable number of active nodes
  }

  if (statsLogs) {
    logGcInfo(__func__, totalTime);
  }
  return totalTime;
}

double quickGC(Context *ctx) {
  double totalTime = markAndSweep(ctx);
  if (statsLogs) {
    logGcInfo(__func__, totalTime);
  }
  return totalTime;
}

void addMemory(Node *node, Context *ctx) {
  long activeCount = atomic_fetch_add(&activeMemory, 1);

  // Premonition: run quick GC if approaching the hard limit
  // Growth Factor: run full GC if grown "too much" since last GC
  if (activeCount > memoryLimit - GC_PREMONITION) {
    quickGC(ctx);
    activeCount = atomic_load(&activeMemory);
  } else if (activeCount > lastGCUsage * GC_HEURISTIC_FACT) {
    fullGC(ctx);
    activeCount = atomic_load(&activeMemory);
  }

  // Hard Cap: crash if usage still exceeds the hard limit after GC
  if (activeCount > memoryLimit) {
    fatalOut(__FILE__, __func__, "limit reached");
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }

  // Update stats
  if (activeCount > maxAllocated) {
    maxAllocated = activeCount;
  }

  if (ctx) {
    createMemory(node, ctx);  // don't track source level nodes (with no ctx)
  }
}

void trackScope(Scope *scope) {
  if (!scope || scope->isTracked) {
    return;
  }

  if (scope == globalScope || scope->isLayer) {
    return;
  }

  int hash = (int)((uintptr_t)scope % SCOPE_HASH);
  lockResource(&memoryLock);
  if (scpRegistryCap[hash] % SCOPE_BLOCK == 0) {
    if (scpRegistryCap[hash] == 0) {
      scopesRegistry[hash] = FRY_ALLOC(sizeof(Scope *) * SCOPE_BLOCK);
    } else {
      scopesRegistry[hash] =
        FRY_REALLOC(scopesRegistry[hash],
                    sizeof(Scope *) * (scpRegistryCap[hash] + SCOPE_BLOCK));
    }
  }
  scopesRegistry[hash][scpRegistryCap[hash]++] = scope;
  trackedScopes++;
  unlockResource(&memoryLock);

#ifdef DEBUG
  if (traceLogs) {
    traceScope("tracked", scope);
  }
#endif
  if (trackedScopes > maxTrackedScopes) {
    maxTrackedScopes = trackedScopes;
  }
  scope->isTracked = true;
}

void bindScope(Node *node, Scope *scope) {
  if (!scope) {
    return;
  }

  node->scp = scope;
  atomic_fetch_add(&scope->refs, 1);
  trackScope(scope);
}

void unbindScope(Node *node) {
  if (!node->scp) {
    return;
  }

  atomic_fetch_sub(&node->scp->refs, 1);
  node->scp = NULL;
}

void collectScope(Scope *scope) {
  if (!useCollector || scope == globalScope || scope->isLayer) {
    return;
  }

  if (atomic_load(&scope->refs)) {
    return;  // can't collect scope still referenced
  }

  if (scope->async && !atomic_load(&scope->async->hasJoined)) {
    return;  // can't collect an active async scope
  }

  int hash = (int)((uintptr_t)scope % SCOPE_HASH);
  for (int i = 0; i < scpRegistryCap[hash]; i++) {
    if (scopesRegistry[hash][i] == scope) {
      lockResource(&memoryLock);
      scpRegistryCap[hash]--;
      if (i < scpRegistryCap[hash]) {
        memmove(&scopesRegistry[hash][i], &scopesRegistry[hash][i + 1],
                (scpRegistryCap[hash] - i) * sizeof(Scope *));
      }
      trackedScopes--;

      // Check if the bucket should be scaled down
      if ((scpRegistryCap[hash] % SCOPE_BLOCK == 0)) {
        if (scpRegistryCap[hash]) {
          int size =
            (((scpRegistryCap[hash] - 1) / SCOPE_BLOCK) + 1) * SCOPE_BLOCK;
          scopesRegistry[hash] =
            FRY_REALLOC(scopesRegistry[hash], size * sizeof(Scope *));
        } else {
          free(scopesRegistry[hash]);
        }
      }
      unlockResource(&memoryLock);

      if (scope->async) {
        freeContext(scope->async->ctx);
        free(scope->async);
        scope->async = NULL;
      }
      wipeScope(scope, NULL, false);
#ifdef DEBUG
      if (traceLogs) {
        traceScope("untracked", scope);
      }
#endif
      free(scope);
      return;
    }
  }
}

static void stackOverflow(Context *ctx) {
  endCursesMode();
  stderrStartLine(CL_RED);
  fputs(MRG_BLT LB_ERROR, stderr);
  fprintf(stderr, ": call stack depth limit of %d exhausted", stackDepth);
  stderrEndLine();
  logStack(ctx, STACK_TRACE);
  fatalOut(__FILE__, __func__, "execution halted");
  exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
}

void pushStack(Context *ctx, const char *func, Node *node, Scope *scp) {
  assert(ctx != NULL);

  lockResource(&ctx->lock);

  // Check for stack size and expand stack if needed
  int ctxTop = atomic_fetch_add(&ctx->top, 1) + 1;
  if (ctxTop >= stackDepth) {
    stackOverflow(ctx);
  }

  // Maintain metrics info
  if (ctxTop > maxStackUsed) {
    maxStackUsed = ctxTop;
  }

  // Update stack frame references
  Frame *cur = ctx->cur = &ctx->stack[ctxTop];
  cur->node = node;
  cur->scp = scp;
  cur->trap = NULL;
  cur->func = func;

  unlockResource(&ctx->lock);
#ifdef DEBUG
  if (traceLogs) {
    traceRun(func, cur);
  }
#endif
}

void popStack(Context *ctx, int n) {
  assert(n >= 0);  // can't pop a negative value

  lockResource(&ctx->lock);
  int ctxTop = atomic_fetch_sub(&ctx->top, n) - n;
  ctx->cur = &ctx->stack[ctxTop];
  unlockResource(&ctx->lock);

  assert(ctxTop >= 0);  // stack underflow
}

void resetSession(void) {
  lockResource(&memoryLock);
  while (memRegistry) {
    removeMemory(memRegistry, NULL);
  }
  unlockResource(&memoryLock);
}

long safeValue(double value, long max, const char *ref) {
  if (MIN_DEPTH <= value && value <= max) {
    return (long)value;
  }
  if (value < MIN_DEPTH) {
    value = MIN_DEPTH;
  } else if (value > max) {
    value = max;
  }
  char num[NUMBER_MAX_LENGTH + 1];
  prettyNumber(num, value);
  auto_str msg = join4("auto-set to ", num, " ", ref);
  logAlert(__FILE__, __func__, msg);
  return value;
}

double realBytesUsage(void) {
  struct rusage usage = {0};
  getrusage(RUSAGE_SELF, &usage);
#ifndef __APPLE__
  return (double)usage.ru_maxrss * 1024.0;
#else
  return (double)usage.ru_maxrss;
#endif
}

static void trackSource(Node *node, bool follow, Context *ctx) {
  // see extractSwitchOrCases for why we need to check if meta
  // is defined (avoid meta pointing to same node duplicates)
  if (!node || node->meta || node == trueSingleton || node == falseSingleton) {
    return;
  }

  createMemory(node, ctx);
  node->meta->locked = false;  // UNLOCK_NODE (inlined)

  trackSource(node->head, true, ctx);
  trackSource(node->body, true, ctx);
  trackSource(node->tail, true, ctx);
  if (follow) {
    for (node = node->seq; node; node = node->seq) {
      trackSource(node, false, ctx);
    }
  }
}

void trackParsed(Node *program, Context *ctx) {
  if (!program) {
    return;
  }

  trackSource(program, true, ctx);
#ifdef DEBUG
  lockResource(&memoryLock);
  // Unlink node from programs linked list (used for leak check)
  if (program == parsedPrograms) {
    parsedPrograms = program->programs;
    unlockResource(&memoryLock);
    return;
  }

  Node *prev = NULL;
  Node *aux = parsedPrograms;
  while (aux && aux != program) {
    prev = aux;
    aux = aux->programs;
  }
  if (!aux) {
    fatalOut(__FILE__, __func__, "twice on same program");
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }

  prev->programs = program->programs;
  unlockResource(&memoryLock);
#endif
}
