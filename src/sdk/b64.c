/**
 * @file b64.c
 * @brief Base-64 encoding/decoding implementation.
 *
 * This is based on the libb64 project, in the public domain.
 * For details, see http://sourceforge.net/projects/libb64
 *
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.0.0
 * @date 2023-12-15
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2023, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "b64.h"

typedef enum { EncStepA, EncStepB, EncStepC } B64EncodeStep;

typedef struct {
  B64EncodeStep step;
  char current;
} B64EncodeState;

const char* b64encoding =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

bool isB64Encoded(const char* data) {
  for (size_t i = 0; data[i]; i++) {
    if (!(('A' <= data[i] && data[i] <= 'Z') ||
          ('a' <= data[i] && data[i] <= 'z') ||
          ('0' <= data[i] && data[i] <= '9') ||
          (data[i] == '+' || data[i] == '/') || data[i] == '=')) {
      return false;
    }
  }
  return true;
}

/**
 * Encodes a single 6-bit value into its corresponding Base64 character
 */
static char b64EncodeValue(signed char value) {
  if (value > 63) {
    return '=';
  }
  return b64encoding[(int)value];
}
static size_t b64EncodeBlock(const char* input, const size_t inLen,
                             char* output, B64EncodeState* state) {
  const char* current = input;
  const char* const inputEnd = input + inLen;
  char* codeChar = output;
  char result = state->current;
  char fragment = 0;

  while (true) {
    // step A
    if (current == inputEnd) {
      state->current = result;
      state->step = EncStepA;
      return (size_t)(codeChar - output);
    }

    fragment = *current++;
    result = (fragment & 0x0fc) >> 2;
    *codeChar++ = b64EncodeValue(result);
    result = (fragment & 0x003) << 4;

    // step B
    if (current == inputEnd) {
      state->current = result;
      state->step = EncStepB;
      return (size_t)(codeChar - output);
    }

    fragment = *current++;
    result |= (fragment & 0x0f0) >> 4;
    *codeChar++ = b64EncodeValue(result);
    result = (fragment & 0x00f) << 2;

    // step C
    if (current == inputEnd) {
      state->current = result;
      state->step = EncStepC;
      return (size_t)(codeChar - output);
    }

    fragment = *current++;
    result |= (fragment & 0x0c0) >> 6;
    *codeChar++ = b64EncodeValue(result);

    result = (fragment & 0x03f) >> 0;
    *codeChar++ = b64EncodeValue(result);
  }

  return 0;  // should not reach here
}

static size_t b64EncodeBlockEnd(char* output, B64EncodeState* state) {
  char* codeChar = output;

  switch (state->step) {
    case EncStepB:
      *codeChar++ = b64EncodeValue(state->current);
      *codeChar++ = '=';
      *codeChar++ = '=';
      break;

    case EncStepC:
      *codeChar++ = b64EncodeValue(state->current);
      *codeChar++ = '=';
      break;

    case EncStepA:
      break;
  }

  return (size_t)(codeChar - output);
}

size_t b64EncodeLength(size_t len) { return ((len + 2) / 3) * 4; }

size_t b64Encode(const char* input, const size_t inLen, char* output) {
  B64EncodeState encodeState = {0};
  size_t written = b64EncodeBlock(input, inLen, output, &encodeState);
  written += b64EncodeBlockEnd(output + written, &encodeState);
  return written;
}

static int b64DecodeValue(signed char value) {
  static const signed char decoding[] = {
      62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1,
      -1, -1, -2, -1, -1, -1, 0,  1,  2,  3,  4,  5,  6,  7,  8,  9,
      10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
      -1, -1, -1, -1, -1, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35,
      36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51};

  if (value < (signed char)'+') {
    return -1;
  }

  value -= (signed char)'+';

  if (value >= (signed char)(sizeof(decoding) / sizeof(*decoding))) {
    return -1;
  }

  return decoding[value];
}

size_t b64DecodeLength(size_t encodedLen) { return encodedLen / 4 * 3 + 2; }

size_t b64Decode(const char* input, const size_t inLen, char* output) {
  const char* codeChar = input;
  char* current = output;
  int fragment = 0;

  while (true) {
    // step A
    do {
      if (codeChar == input + inLen) {
        return (size_t)(current - output);
      }
      fragment = b64DecodeValue(*codeChar++);
    } while (fragment < 0);
    *current = (fragment & 0x03f) << 2;

    // step B
    do {
      if (codeChar == input + inLen) {
        return (size_t)(current - output);
      }
      fragment = b64DecodeValue(*codeChar++);
    } while (fragment < 0);
    *current++ |= (fragment & 0x030) >> 4;
    *current = (fragment & 0x00f) << 4;

    // step C
    do {
      if (codeChar == input + inLen) {
        return (size_t)(current - output);
      }
      fragment = b64DecodeValue(*codeChar++);
    } while (fragment < 0);
    *current++ |= (fragment & 0x03c) >> 2;
    *current = (fragment & 0x003) << 6;

    // step D
    do {
      if (codeChar == input + inLen) {
        return (size_t)(current - output);
      }
      fragment = b64DecodeValue(*codeChar++);
    } while (fragment < 0);
    *current++ |= (fragment & 0x03f);
  }

  return 0;  // should never reach here
}
