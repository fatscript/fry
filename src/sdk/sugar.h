/**
 * @file sugar.h
 * @brief Some syntactic sugar to make life easier and code look better
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-12
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#pragma once

#include "dependencies.h"

// just for semantics
typedef int ExitCode;
typedef unsigned char Byte;

// fry will attempt to parse up to a N chars for numbers,
// which allows capturing even the biggest IEEE 754 number
#define NUMBER_MAX_LENGTH 309

// only consider integer part for number greater than this,
// decimal part is pretty much rubbish anyway at this point
#define NUMBER_INT_ONLY 1.0e+16f
#define NUMBER_DIG_ONLY 16
#define NUMBER_MAX_DEC 11

// string auto cleanup macro (once pointer goes out of scope)
#define auto_str char* __attribute__((cleanup(autoCleanStr)))

// unified allocator
#ifdef TURBO
// In the default build, explicit checks for out-of-memory (OOM)
// are performed to handle allocation failures. This approach is
// taken out of extra caution and to ensure better compatibility
// across various platforms. Typically, OOM situations are likely
// to result in unrecoverable application crashes.
// When TURBO mode is enabled, these checks are bypassed to reduce
// overhead associated with memory allocation failure handling,
// under the assumption that OOM would cause the application to
// crash regardless.
#define FRY_ALLOC(size) malloc(size)               // no check
#define FRY_CALLOC(size) calloc(1, size)           // no check
#define FRY_REALLOC(ptr, size) realloc(ptr, size)  // no check
#elif defined(__GNUC__) && !defined(DEBUG)
// GNUC specific, non-debug, optimized inline code
#define FRY_ALLOC(size)                      \
  ({                                         \
    void* ptr = malloc(size);                \
    if (!ptr) {                              \
      fatalOut(__FILE__, __func__, MSG_OOM); \
      exit(EXIT_FAILURE);                    \
    }                                        \
    ptr;                                     \
  })
#define FRY_CALLOC(size)                     \
  ({                                         \
    void* ptr = calloc(1, size);             \
    if (!ptr) {                              \
      fatalOut(__FILE__, __func__, MSG_OOM); \
      exit(EXIT_FAILURE);                    \
    }                                        \
    ptr;                                     \
  })
#define FRY_REALLOC(ptr, size)               \
  ({                                         \
    void* newPtr = realloc(ptr, size);       \
    if (!newPtr) {                           \
      fatalOut(__FILE__, __func__, MSG_OOM); \
      exit(EXIT_FAILURE);                    \
    }                                        \
    newPtr;                                  \
  })
#else
// Fallback for other compilers or debug mode
#define FRY_ALLOC(size) safeAlloc(size, __FILE__, __func__)
#define FRY_CALLOC(size) safeCalloc(size, __FILE__, __func__)
#define FRY_REALLOC(ptr, size) safeRealloc(ptr, size, __FILE__, __func__)
void* safeAlloc(size_t size, const char* file, const char* func);
void* safeCalloc(size_t size, const char* file, const char* func);
void* safeRealloc(void* ptr, size_t size, const char* file, const char* func);
#endif

// used by xorshift method
extern __thread uint64_t xorshiftStateA;
extern __thread uint64_t xorshiftStateB;
extern __thread uint64_t xorshiftStateC;

// used by regex
extern __thread regex_t regex;
extern __thread char* rePattern;

typedef struct Utf8conversion {
  char lowercase[3];
  char uppercase[3];
} Utf8conversion;

/**
 * Implements thread sleep using timespec.
 *
 *  @param ms milliseconds to sleep
 */
void msSleep(long ms);

/**
 * @brief Safe string equality tester, with boolean outcome.
 * @note Checks if values are null before calling strcmp.
 *
 * @param a string
 * @param b string
 * @return boolean (true is equal)
 */
bool strEq(const char* a, const char* b);

/**
 * @brief Convert a character to string.
 *
 * @param c char
 * @return string (use free)
 */
char* ofChar(char c);

/**
 * @brief Convert a number to a new string.
 *
 * @param n long
 * @return string (use free)
 */
char* ofInt(long n);

/**
 * @brief Convert Float to a string (allocates new if no buffer).
 *
 * @param f Float to convert to string
 * @param buff char* buffer of NUMBER_MAX_LENGTH + 1 (optional)
 * @return string (use free if no buffer is provided)
 */
char* ofFloat(char* buff, double f);

/**
 * @brief Compare floating point using epsilon, with boolean outcome.
 *
 * @param a Float
 * @param b Float
 * @return boolean
 */
bool floatEq(double a, double b);

/**
 * @brief Convert a boolean to string.
 *
 * @param b bool
 * @return string (don't free / hardcoded)
 */
const char* ofBool(bool b);

/**
 * @brief Find index of char in text.
 *
 * @param text string
 * @param ch char
 * @return long (index of char if present, or -1 otherwise)
 */
long indexOf(const char* text, char ch);

/**
 * @brief Copy null terminated string into new string (strdup equivalent).
 *
 * @param text to copy
 * @return string (use free)
 */
char* strDup(const char* text);

/**
 * @brief Trim blanks and copy result into new string.
 *
 * @param text to copy and trim
 * @param len size_t of text
 * @return string (use free)
 */
char* strTrim(const char* text, size_t len);

/**
 * @brief Copy selection of chars (chunk) into new string.
 * @note For safety reasons a null-termination is ensured.
 *
 * @param chunk to copy from start
 * @param len size_t of fragment
 * @return string
 */
char* copyChunk(const char* chunk, size_t len);

/**
 * @brief mkString shorthand, joins 2 strings in a new string.
 *
 * @param s1 string
 * @param s2 string
 * @return string (use free)
 */
char* join2(const char* s1, const char* s2);

/**
 * @brief mkString shorthand, joins 3 strings in a new string.
 *
 * @param s1 string
 * @param s2 string
 * @param s3 string
 * @return string (use free)
 */
char* join3(const char* s1, const char* s2, const char* s3);

/**
 * @brief mkString shorthand, joins 4 strings in a new string.
 *
 * @param s1 string
 * @param s2 string
 * @param s3 string
 * @param s4 string
 * @return string (use free)
 */
char* join4(const char* s1, const char* s2, const char* s3, const char* s4);

/**
 * @brief mkString shorthand, joins 5 strings in a new string.
 *
 * @param s1 string
 * @param s2 string
 * @param s3 string
 * @param s4 string
 * @param s5 string
 * @return string (use free)
 */
char* join5(const char* s1, const char* s2, const char* s3, const char* s4,
            const char* s5);

/**
 * @brief mkString shorthand, joins 6 strings in a new string.
 *
 * @param s1 string
 * @param s2 string
 * @param s3 string
 * @param s4 string
 * @param s5 string
 * @param s6 string
 * @return string (use free)
 */
char* join6(const char* s1, const char* s2, const char* s3, const char* s4,
            const char* s5, const char* s6);

/**
 * @brief Join strings, with no separator.
 *
 * @param strs string list (null terminated)
 * @return string (use free)
 */
char* mkString(char** strs);

/**
 * @brief Join strings, with a separator.
 *
 * @param strs string list (null terminated)
 * @param sep string (separator)
 * @return string (use free)
 */
char* joinSep(char** strs, const char* sep);

/**
 * @brief Split string text by separator sep into a list of strings.
 *
 * @param text string
 * @param sep string (separator)
 * @return string list (use free on each item)
 */
char** splitSep(const char* text, const char* sep);

/**
 * @brief Counts items in list of pointers, provided list is null-terminated.
 *
 * @param ptrs void**
 * @return size_t
 */

size_t listCount(void** ptrs);

/**
 * @brief Counts number of UTF-8 chars in string (not bytes).
 *
 * @param str string
 * @return size_t
 */
size_t utf8len(const char* str);

/**
 * @brief Moves string pointer to the next UTF-8 char.
 *
 * @param str string
 * @return string (pointer on same string)
 */
char* utf8next(const char* str);

/**
 * @brief Copies the full UTF-8 char at string head position.
 *
 * @param str string
 * @return string
 */
char* utf8char(const char* str);

/**
 * @brief Gets number of bytes of UTF-8 char at string head position.
 * @note Null-byte '\0' is considered a valid UTF-8 char and returns 1.
 *
 * @param str string
 * @return int
 */
int utf8bytes(const char* str);

/**
 * @brief Quick dirty UTF-8 toupper for latin chars.
 *
 * @param str string
 */
void utf8upper(char* str);

/**
 * @brief Quick dirty UTF-8 tolower for latin chars.
 *
 * @param str string
 */
void utf8lower(char* str);

/**
 * @brief Find substring like strstr, UTF-8-aware drop-in replacement.
 *
 * @param haystack string to be searched
 * @param needle substring to search for
 * @return first occurrence of 'needle' within 'haystack' or NULL if not found
 */
char* utf8strstr(const char* haystack, const char* needle);

/**
 * @brief Free each string in list until null is found.
 *
 * @param strs string list
 */
void freeEachStrInList(char** strs);

/**
 * @brief Frees all strings of a null terminated list of strings, if not null.
 *
 * @param strs string list
 */
void freeStrList(char** strs);

/**
 * @brief GCC cleanup function for C strings.
 *
 * @param str string pointer
 */
void autoCleanStr(char** str);

/**
 * @brief Check if word is substring at start of text.
 *
 * @param text string
 * @param word string
 * @return boolean
 */
bool startsWith(const char* text, const char* word);

/**
 * @brief Count occurrences of word (substring) in provided text.
 *
 * @param text string
 * @param word string
 * @return size_t
 */
size_t countWord(const char* text, const char* word);

/**
 * @brief Regular expression matcher using the POSIX regex library.
 *
 * @param pattern string
 * @param text string
 * @return boolean
 */
bool matchRegex(const char* pattern, const char* text);

/**
 * @brief Executes a regex pattern against a specified text, returning groups.
 *
 * @param pattern string
 * @param text string
 * @param count int* (indicates the size of returned group pointers vector)
 * @return char** (matches for each capturing group, use free)
 */
char** regexGroups(const char* pattern, const char* text, int* count);

/**
 * @brief Replace word (substring) in text (first occurrence).
 *
 * @param text string (original)
 * @param oldWord string (to be replaced)
 * @param newWord string (replacement)
 * @return string (use free)
 */
char* replaceFirst(const char* text, const char* oldWord, const char* newWord);

/**
 * @brief Replace word (substring) in text (all occurrences).
 *
 * @param text string (original)
 * @param oldWord string (to be replaced)
 * @param newWord string (replacement)
 * @return string (use free)
 */
char* replaceAll(const char* text, const char* oldWord, const char* newWord);

/**
 * @brief Write a string buffer to a filepath.
 * @note Use strerror(errno) to read the error.
 *
 * @param filepath string
 * @param buffer string (data)
 * @param len size of buffer
 * @param mode string (e.g. "a" for append, "w" for write, etc.)
 * @return boolean success
 */
bool writeFile(const char* filepath, const char* buffer, size_t len,
               const char* mode);

/**
 * @brief Check if a file exists on a given filepath.
 *
 * @param filepath string
 * @return boolean
 */
bool existsFile(const char* filepath);

/**
 * @brief Read text file to a string buffer.
 * @note Use strerror(errno) to read the error.
 *
 * @param result buffer allocated by this function (use free)
 * @param filepath string
 * @param mode string (e.g. "r" for read, "rb" for read binary)
 * @return bytes read, or -1 for failure
 */
ssize_t readFile(char** result, const char* filepath, const char* mode);

/**
 * @brief Check if a given filepath is a directory.
 *
 * @param filepath string
 * @return boolean
 */
bool isDir(const char* filepath);

/**
 * @brief Get list of filepaths inside a dir path.
 * @note Use strerror(errno) to read the error.
 *
 * @param filepath string
 * @return string list or NULL if failed (use free on each item)
 */
char** listDir(const char* filepath);

/**
 * @brief Generate a quick 32-bit hash for a "long" string.
 *
 * @param text string
 * @param len size_t
 * @return uint32_t hash
 */
uint32_t jesteressHash32(const char* text, size_t len);

/**
 * @brief Initializes the random number generator for the thread.
 */
void initXorshiftStates(void);

/**
 * @brief Random number generator, xorshift based, better quality.
 * @note Uses 3x 64-bit resolution states for increased randomness.
 *
 * @return uint64_t pseudo-random
 */
uint64_t rnGen(void);

/**
 * @brief Get the system time.
 * @note Use type CLOCK_REALTIME for UTC epoch time.
 *
 * @param clockType clockid_t
 * @return Float
 */
double getCurrentMs(clockid_t clockType);
