/**
 * @file webenv.c
 * @brief Glue code for Emscripten (replacement for curl and readline)
 * @author Antonio Prates <hello@aprates.dev>
 * @version 3.1.2
 * @date 2024-08-10
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "webenv.h"

#include <ctype.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

// these functions are implemented elsewhere, see: sdk.c
void printFgColor(int fgCode, FILE* stream);
void printBgColor(int bgCode, FILE* stream);

#ifdef __EMSCRIPTEN__
int evalCount = 0;
#endif

bool hasAudioSupport = true;

// curl

struct curl_slist* curl_slist_append(struct curl_slist* list,
                                     const char* data) {
  (void)list;
  (void)data;
  return NULL;
}

void curl_slist_free_all(struct curl_slist* list) { (void)list; }

CURLcode curl_global_init(long flags) {
  (void)flags;
  return EXIT_SUCCESS;
}

void curl_global_cleanup(void) {}

CURL* curl_easy_init(void) {
  CURL* curl = calloc(1, sizeof(EmscriptenCURL));
  if (!curl) {
    fputs("** OOM at curl_easy_init **\n", stderr);
    exit(EXIT_FAILURE);
  };
  return curl;
}

void curl_easy_cleanup(CURL* curl) { free(curl); }

#ifdef __EMSCRIPTEN__
// fetchResult value meanings (0 for no result yet)
static int fetchResult = 0;

// the http status of response
static int fetchStatus = 0;

// body callback function pointer
static size_t (*bodyFn)(char* buff, size_t size, size_t count, void* p) = NULL;

// the pointer to bodyFn data
void* bodyData = NULL;

// headers callback function pointer
static size_t (*headerFn)(char* buff, size_t size, size_t count,
                          void* p) = NULL;

// the pointer to headerFn data
void* headersData = NULL;

static void fetchSucceeded(emscripten_fetch_t* fetch) {
  fetchResult = CURLE_OK;
  fetchStatus = fetch->status;

  size_t size = 1;  // size is 1 for char-based buffers
  bodyFn((char*)fetch->data, size, fetch->numBytes, bodyData);

  if (headerFn) {
    // Get the headers length
    size_t headersLen = emscripten_fetch_get_response_headers_length(fetch);

    char* headersBuff = malloc(headersLen + 1);
    if (!headersBuff) {
      fputs("** OOM at fetchSucceeded **\n", stderr);
      exit(EXIT_FAILURE);
    }

    // Fetch and unpack headers
    emscripten_fetch_get_response_headers(fetch, headersBuff, headersLen + 1);
    char** headers = emscripten_fetch_unpack_response_headers(headersBuff);

    // Iterate over the unpacked headers and invoke the callback
    for (int i = 0; headers[i] && headers[i + 1]; i += 2) {
      char header[1024] = {'\0'};
      snprintf(header, sizeof(header), "%s:%s", headers[i], headers[i + 1]);

      // Invoke the callback
      size_t count = strlen(header);
      headerFn(header, size, count, headersData);
    }

    // Clean up
    emscripten_fetch_free_unpacked_response_headers(headers);
    free(headers);
  }

  emscripten_fetch_close(fetch);
}

static void fetchFailed(emscripten_fetch_t* fetch) {
  fetchResult = CURLE_ERR;
  emscripten_fetch_close(fetch);
}

#endif

CURLcode curl_easy_getinfo(CURL* curl, CURLINFO info, ...) {
  (void)curl;

#ifdef __EMSCRIPTEN__
  va_list argp;
  va_start(argp, info);
  if (info == CURLINFO_RESPONSE_CODE) {
    int* pStatusCode = va_arg(argp, int*);
    if (pStatusCode) {
      *pStatusCode = (long)fetchStatus;
    }
  }
  // ... handle other infos

  va_end(argp);
#else
  (void)info;
#endif

  return CURLE_OK;
}

CURLcode curl_easy_setopt(CURL* curl, CURLoption option, ...) {
  va_list arg;
  va_start(arg, option);

  EmscriptenCURL* session = (EmscriptenCURL*)curl;
  switch (option) {
    case CURLOPT_URL:
      session->url = va_arg(arg, char*);
      break;
    case CURLOPT_CUSTOMREQUEST:
      session->method = va_arg(arg, char*);
      break;
    case CURLOPT_POSTFIELDS:
      session->postFields = va_arg(arg, char*);
      break;
    case CURLOPT_POSTFIELDSIZE:
      session->postSize = va_arg(arg, size_t);
      break;
#ifdef __EMSCRIPTEN__
    case CURLOPT_WRITEFUNCTION:
      bodyFn = va_arg(arg, size_t(*)(char*, size_t, size_t, void*));
      break;
    case CURLOPT_WRITEDATA:
      bodyData = va_arg(arg, void*);
      break;
    case CURLOPT_HEADERFUNCTION:
      headerFn = va_arg(arg, size_t(*)(char*, size_t, size_t, void*));
      break;
    case CURLOPT_HEADERDATA:
      headersData = va_arg(arg, void*);
      break;
#endif
      // case CURLOPT_HTTPHEADER:
      // https://github.com/emscripten-core/emscripten/issues/20150
      // ... handle other options
    default:
      break;
  }

  va_end(arg);
  return CURLE_OK;
}

CURLcode curl_easy_perform(CURL* curl) {
#ifdef __EMSCRIPTEN__
  EmscriptenCURL* session = (EmscriptenCURL*)curl;

  emscripten_fetch_attr_t attr;
  emscripten_fetch_attr_init(&attr);
  strcpy(attr.requestMethod, session->method);
  attr.attributes = EMSCRIPTEN_FETCH_LOAD_TO_MEMORY;
  attr.onsuccess = fetchSucceeded;
  attr.onerror = fetchFailed;

  // If there's a body, set it
  if (session->postFields) {
    attr.requestData = session->postFields;
    attr.requestDataSize = session->postSize;
  }

  // Perform
  fetchResult = 0;
  evalCount = 0;
  emscripten_fetch(&attr, session->url);
  while (!fetchResult) {
    emscripten_sleep(JS_IO_LAG);
  }

  return fetchResult;

#else
  (void)curl;
  return CURLE_OK;
#endif
}

const char* curl_easy_strerror(CURLcode code) {
  (void)code;
  return "Couldn't connect to server";
}

// readline

bool rl_inhibit_completion = false;

char* readline(const char* prompt) {
  if (prompt) {
    directPrint(prompt, stdout);
  }

  while (!hasNewline()) {
#ifdef __EMSCRIPTEN__
    evalCount = 0;
    emscripten_sleep(JS_IO_LAG);
#endif
  }

  return getInputBuffer();
}

void add_history(const char* input) { (void)input; }
