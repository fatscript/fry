/**
 * @file memory.h
 * @brief Memory management and garbage collector functions
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-20
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#pragma once

#include "structures.h"
#include "sugar.h"

// memory accountancy
#define MEM_MAX 2147483647L  // min value of LONG_MAX on "any" platform

// memory options / state
extern long memoryLimit;          // runtime maximum allowance config
extern long maxAllocated;         // actual maximum reached (runtime)
extern long lastGCUsage;          // stable memory after full GC
extern atomic_long activeMemory;  // current maximum during runtime
extern bool useCollector;         // should perform garbage collection
extern bool memoryRedefined;      // memoryLimit was changed
extern Memory *memRegistry;       // pointer to meta-space head

// scopes meta-space tracking
extern Scope **scopesRegistry[SCOPE_HASH];
extern int scpRegistryCap[SCOPE_HASH];
extern int trackedScopes;
extern int maxTrackedScopes;
#ifdef DEBUG
extern long cacheHits;
extern Node *parsedPrograms;  // allows for memory leak check with valgrind
#endif

// gc stats
extern double gcTotalTimeMs;
extern bool warnHighGC;

// global memory lock
extern pthread_mutex_t memoryLock;
extern pthread_mutexattr_t recursiveMutex;

// global atomic operations lock, see async.c
extern pthread_mutex_t atomicLock;

// Global atomic thread counter
extern atomic_long threadCounter;
extern atomic_int activeThreads;
extern atomic_int maxThreadCount;

extern int (*lockResource)(pthread_mutex_t *);
extern int (*unlockResource)(pthread_mutex_t *);

void initMemoryManagement(void);

void cleanMemoryLock(void);

/**
 * @brief Leaves node GC ready ("unlocks" node for GC).
 *
 * @param n Node*
 */
#define UNLOCK_NODE(n) ((n) && (n)->meta && ((n)->meta->locked = false))

/**
 * @brief Register nodes from the context cache in global memory.
 *
 * @param ctx Context*
 */
void moveLocalToGlobalMemory(Context *ctx);

/**
 * @brief Create a Memory meta for a node.
 *
 * @param node Node*
 * @param ctx Context*
 */
void createMemory(Node *node, Context *ctx);

/**
 * @brief Perform micro garbage collection for nodes allocated in local context.
 *
 * @param keep Node* usually what is being returned by the current context
 * @param until Memory* previous reference that was in local context (stop GC)
 * @param ctx Context*
 */
void microGC(Node *keep, Memory *until, Context *ctx);

/**
 * @brief Perform a complete garbage collection cycle (full GC).
 *
 * @param ctx Context*
 * @return elapsed time in milliseconds
 */
double fullGC(Context *ctx);

/**
 * @brief Perform a single garbage collection iteration step.
 *
 * @param ctx Context*
 * @return elapsed time in milliseconds
 */
double quickGC(Context *ctx);

/**
 * @brief Add reference of node to memory registry (primitive).
 *
 * @param node Node*
 * @param ctx Context*
 */
void addMemory(Node *node, Context *ctx);

/**
 * @brief Add scope to scopesRegistry hash table.
 *
 * @param scope Scope*
 */
void trackScope(Scope *scope);

/**
 * @brief Safely binds a scope to a node, adding GC controls.
 *
 * @param node node representing collection (Scope/List)
 * @param scope actual struct Scope*
 */
void bindScope(Node *node, Scope *scope);

/**
 * @brief Safely unbinds a scope from a node, performing GC controls.
 *
 * @param node node representing collection (Scope/List)
 */
void unbindScope(Node *node);

/**
 * @brief Dereferences members of a scope.
 *
 * @param scope Scope*
 */
void collectScope(Scope *scope);

/**
 * @brief Add frame to top of Stack.
 *
 * @param ctx Context*
 * @param func function reference
 * @param node Node*
 * @param scp Scope*
 */
void pushStack(Context *ctx, const char *func, Node *node, Scope *scp);

/**
 * @brief Remove n top frames of context stack (batched).
 *
 * @param ctx Context*
 * @param n number of frames to pop at once
 */
void popStack(Context *ctx, int n);

/**
 * @brief Deletes all loaded nodes registered into memory.
 */
void resetSession(void);

/**
 * @brief Return number within [MIN_DEPTH-max] range.
 *
 * @param value Float
 * @param max long
 * @param ref string
 * @return long
 */
long safeValue(double value, long max, const char *ref);

/**
 * @brief Get real memory usage (via ru_maxrss)
 *
 * @return double (in bytes)
 */
double realBytesUsage(void);

/**
 * @brief Allow the parsed source to be freed by tagging with GC meta.
 *
 * @param program Node*
 * @param ctx Context*
 */
void trackParsed(Node *program, Context *ctx);
