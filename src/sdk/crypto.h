/**
 * @file crypto.h
 * @brief A very simple solution to obfuscate data with bitwise-XOR.
 *
 * Based on work I've done together with team mate Joao Condack back in
 * mid-2005, at PUC-Rio University. Now with standard base-64 encoding.
 *
 * Used as back-end for the FatScript enigma library.
 *
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.0.0
 * @date 2025-01-04
 * @note This schema is not cryptographically safe!
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#pragma once

#include "sugar.h"

/**
 * @brief Base64 encode bitwise-XOR key over message.
 *
 * @param key secret (if NULL, defaults to defaultKey)
 * @param text to be encrypted (must not be NULL)
 * @return string (use free), NULL on error
 */
char* xorEncode(const char* key, const char* text);

/**
 * @brief Base64 decode bitwise-XOR key over message.
 *
 * @param key secret (if NULL, defaults to defaultKey)
 * @param data to be dencrypted (must not be NULL)
 * @return string (use free)
 */
char* xorDecode(const char* key, const char* data);

/**
 * @brief Generates a random text from url-safe characters.
 *
 * @param len size_t
 * @return string (use free)
 */
char* generateKey(size_t len);

/**
 * @brief Generates a random UUID (version 4 RFC 4122 format).
 *
 * @return string (use free)
 */
char* generateUuid(void);

/**
 * @brief Function to derive a key using the base64 alphabet.
 * @note Salt must be handled externally and already mixed into secret.
 *
 * @param secret password of pass-phrase (must not be NULL)
 * @param len size of secret
 * @param iter number of iterations
 * @return string (use free)
 */
char* deriveKey(const char* secret, size_t len, int iter);
