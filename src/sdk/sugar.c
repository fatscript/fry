/**
 * @file sugar.c
 * @brief Some syntactic sugar to make life easier and code look better
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-16
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "sugar.h"

#include "logger.h"
#include "memory.h"
#include "parameters.h"

// must be seeded with a nonzero value
__thread uint64_t xorshiftStateA = 0;
__thread uint64_t xorshiftStateB = 0;
__thread uint64_t xorshiftStateC = 0;

__thread regex_t regex;
__thread char* rePattern = NULL;

// this hacky list doesn't cover every accented character, just 2 byte common
Utf8conversion utf8table[] = {
  {"á", "Á"}, {"à", "À"}, {"â", "Â"}, {"ä", "Ä"}, {"ã", "Ã"}, {"å", "Å"},
  {"æ", "Æ"}, {"ç", "Ç"}, {"é", "É"}, {"è", "È"}, {"ê", "Ê"}, {"ë", "Ë"},
  {"í", "Í"}, {"ì", "Ì"}, {"î", "Î"}, {"ï", "Ï"}, {"ð", "Ð"}, {"ñ", "Ñ"},
  {"ó", "Ó"}, {"ò", "Ò"}, {"ô", "Ô"}, {"ö", "Ö"}, {"õ", "Õ"}, {"ø", "Ø"},
  {"œ", "Œ"}, {"š", "Š"}, {"ú", "Ú"}, {"ù", "Ù"}, {"û", "Û"}, {"ü", "Ü"},
  {"ý", "Ý"}, {"ÿ", "Ÿ"}, {"ž", "Ž"}, {"ř", "Ř"}, {"č", "Č"}, {"ě", "Ě"},
  {"ś", "Ś"}, {"ł", "Ł"}, {"ź", "Ź"}, {"ć", "Ć"}, {"ń", "Ń"}, {"ę", "Ę"},
  {"ą", "Ą"}, {"ů", "Ů"}, {"đ", "Đ"}, {"ľ", "Ľ"}, {"ť", "Ť"}, {"ď", "Ď"},
  {"ğ", "Ğ"}, {"ş", "Ş"}, {"ő", "Ő"}, {"ű", "Ű"}, {"ň", "Ň"}
  // add more entries as needed...
};

#ifndef TURBO
#if !defined(__GNUC__) || defined(DEBUG)

void* safeAlloc(size_t size, const char* file, const char* func) {
  void* ptr = malloc(size);
  if (!ptr) {
    fatalOut(file, func, MSG_OOM);
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }
  return ptr;
}

void* safeCalloc(size_t size, const char* file, const char* func) {
  void* ptr = calloc(1, size);
  if (!ptr) {
    fatalOut(file, func, MSG_OOM);
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }
  return ptr;
}

void* safeRealloc(void* ptr, size_t size, const char* file, const char* func) {
  void* newPtr = realloc(ptr, size);
  if (!newPtr) {
    fatalOut(file, func, MSG_OOM);
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }
  return newPtr;
}

#endif
#endif

void msSleep(const long ms) {
  struct timespec rem = {0};

  struct timespec req = {
    .tv_sec = ms / 1000,               // millis to seconds
    .tv_nsec = (ms % 1000) * 1000000L  // remaining millis to nanoseconds
  };

  while (nanosleep(&req, &rem) == -1 && errno == EINTR) {
    // If interrupted by a signal, continue sleeping for the remaining time
    req = rem;
  }
}

bool strEq(const char* a, const char* b) {
  if (a == b) {
    return true;
  }
  if (!a || !b) {
    return false;
  }
  return strcmp(a, b) == 0;
}

char* ofChar(const char c) {
  char* result = NULL;
  if (asprintf(&result, "%c", c) == -1) {
    fatalOut(__FILE__, __func__, MSG_OOM);
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }
  return result;
}

char* ofInt(const long n) {
  char* result = NULL;
  if (asprintf(&result, "%ld", n) == -1) {
    fatalOut(__FILE__, __func__, MSG_OOM);
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }
  return result;
}

// see also: prettyNumber at sdk.c
char* ofFloat(char* buff, const double f) {
  int ret = 0;
  if (f > NUMBER_INT_ONLY) {
    if (buff) {
      ret = snprintf(buff, NUMBER_MAX_LENGTH + 1, "%.0f", f);
    } else {
      ret = asprintf(&buff, "%.0f", f);
    }
  } else {
    if (buff) {
      ret = snprintf(buff, NUMBER_MAX_LENGTH + 1, "%.11f", f);
    } else {
      ret = asprintf(&buff, "%.11f", f);
    }
  }
  if (ret == -1) {
    fatalOut(__FILE__, __func__, MSG_F_F_N);
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }
  return buff;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfloat-equal"

// inspired by: https://embeddeduse.com/2019/08/26/qt-compare-two-floats/
bool floatEq(const double a, const double b) {
  // safer than FLT_EPSILON and good enough for FatScript
  static const double epsilon = 1.0e-06;

  if (a == b) {
    return true;  // quick succeed
  }

  double diff = fabs(a - b);
  return diff <= epsilon;
}

const char* ofBool(bool b) {
  if (b) {
    return "true";
  }
  return "false";
}

#pragma GCC diagnostic pop

long indexOf(const char* text, const char ch) {
  const char* found = strchr(text, ch);
  if (found) {
    return found - text;
  }
  return -1;
}

char* strDup(const char* text) {
  if (!text) {
    return NULL;
  }

  const size_t size = strlen(text) + 1;  // '\0' included
  char* result = FRY_ALLOC(size);
  return memcpy(result, text, size);
}

char* strTrim(const char* text, size_t len) {
  if (!text || !len || !text[0]) {
    return strDup("");
  }

  size_t trimIndex = 0;
  for (; isspace(text[trimIndex]); trimIndex++) {
    ;  // skip whitespace at the beginning
  }

  if (len == trimIndex) {
    return strDup("");
  }

  len -= trimIndex;
  char* trimmed = copyChunk(&text[trimIndex], len);

  for (trimIndex = len - 1; isspace(trimmed[trimIndex]); trimIndex--) {
    ;  // search from last to first for non-whitespace
  }

  // trimIndex holds last non-blank index, null-terminate the string
  len = trimIndex + 1;
  trimmed[len] = '\0';

  return FRY_REALLOC(trimmed, len + 1);
}

char* copyChunk(const char* chunk, size_t len) {
  char* result = FRY_ALLOC(len + 1);
  memcpy(result, chunk, len);
  result[len] = '\0';
  return result;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-qual"  // drop const qualifier

// alternative implementation of strcat (returns pointer to end)
// intent: traverse each string being concatenated once and only once
static char* quickCat(char* dest, const char* src) {
  char* ptr = (char*)src;
  while ((*dest++ = *ptr++)) {
    continue;
  }
  return --dest;
}

char* join2(const char* s1, const char* s2) {
  const char* list[3] = {s1, s2, NULL};
  return mkString((char**)list);
}

char* join3(const char* s1, const char* s2, const char* s3) {
  const char* list[4] = {s1, s2, s3, NULL};
  return mkString((char**)list);
}

char* join4(const char* s1, const char* s2, const char* s3, const char* s4) {
  const char* list[5] = {s1, s2, s3, s4, NULL};
  return mkString((char**)list);
}

char* join5(const char* s1, const char* s2, const char* s3, const char* s4,
            const char* s5) {
  const char* list[6] = {s1, s2, s3, s4, s5, NULL};
  return mkString((char**)list);
}

char* join6(const char* s1, const char* s2, const char* s3, const char* s4,
            const char* s5, const char* s6) {
  const char* list[7] = {s1, s2, s3, s4, s5, s6, NULL};
  return mkString((char**)list);
}

#pragma GCC diagnostic pop

char* mkString(char** strs) {
  size_t resultSize = 1;
  for (char** ptr = strs; *ptr; ptr++) {
    resultSize += strlen(*ptr);
  }

  char* result = FRY_ALLOC(resultSize);
  result[0] = '\0';

  char* end = result;
  for (char** ptr = strs; *ptr; ptr++) {
    end = quickCat(end, *ptr);
  }

  return result;
}

char* joinSep(char** strs, const char* sep) {
  if (!sep || !*sep) {
    return mkString(strs);
  }

  // Calculate the total length required
  size_t strsLen = 0;
  size_t sepLen = strlen(sep);
  for (size_t i = 0; strs[i]; i++) {
    if (i > 0) {
      strsLen += sepLen;  // add separator length for all but first string
    }
    strsLen += strlen(strs[i]);
  }

  char* result = FRY_ALLOC(strsLen + 1);
  result[0] = '\0';

  char* p = result;
  for (size_t i = 0; strs[i]; i++) {
    p = quickCat(p, strs[i]);
    if (strs[i + 1]) {
      p = quickCat(p, sep);
    }
  }

  return result;
}

char** splitSep(const char* text, const char* sep) {
  size_t occur = countWord(text, sep);
  char** list = FRY_ALLOC((occur + 2) * sizeof(char*));

  size_t i = 0;
  if (occur) {  // break into pieces (actual work)
    size_t sepLen = strlen(sep);
    for (char* frag = utf8strstr(text, sep); frag;
         frag = utf8strstr(text, sep)) {
      size_t fragLen = frag - text;
      list[i++] = copyChunk(text, fragLen);
      text += fragLen + sepLen;
    }
  }

  list[i++] = strDup(text);
  list[i] = NULL;

  return list;
}

size_t listCount(void** ptrs) {
  size_t i = 0;
  while (ptrs[i]) {
    i++;
  }
  return i;
}

size_t utf8len(const char* str) {
  size_t len = 0;
  for (char* p = (char*)str; *p; p++) {
    if ((*p & 0xC0) != 0x80) {
      len++;
    }
  }
  return len;
}

char* utf8next(const char* str) {
  if (!str || !*str) {
    return NULL;
  }

  do {
    str++;
  } while ((*str & 0xC0) == 0x80);

  return (char*)str;
}

char* utf8char(const char* str) {
  if (!str) {
    return NULL;
  }

  return copyChunk(str, utf8next(str) - str);
}

int utf8bytes(const char* str) {
  Byte c = (Byte)*str;
  return (c & 0x80) == 0      ? 1   // ASCII
         : (c & 0xE0) == 0xC0 ? 2   // 110xxxxx
         : (c & 0xF0) == 0xE0 ? 3   // 1110xxxx
         : (c & 0xF8) == 0xF0 ? 4   // 11110xxx
                              : 0;  // Invalid UTF-8
}

void utf8upper(char* str) {
  while (*str != '\0') {
    int len = utf8bytes(str);
    switch (len) {
      case 0:
        return;

      case 1:  // ASCII
        *str = (char)toupper(*str);
        break;

      case 2:  // handle two byte UTF-8 chars
        for (size_t j = 0; j < sizeof(utf8table) / sizeof(utf8table[0]); j++) {
          if (strncmp(str, utf8table[j].lowercase, 2) == 0) {
            strncpy(str, utf8table[j].uppercase, 2);
          }
        }
        break;

      default:
        break;  // otherwise, simply move forward
    }
    str += len;
  }
}

void utf8lower(char* str) {
  while (*str != '\0') {
    int len = utf8bytes(str);
    switch (len) {
      case 0:
        return;

      case 1:  // ASCII
        *str = (char)tolower(*str);
        break;

      case 2:  // handle two byte UTF-8 chars
        for (size_t j = 0; j < sizeof(utf8table) / sizeof(utf8table[0]); j++) {
          if (strncmp(str, utf8table[j].uppercase, 2) == 0) {
            strncpy(str, utf8table[j].lowercase, 2);
          }
        }
        break;

      default:
        break;  // otherwise, simply move forward
    }
    str += len;
  }
}

char* utf8strstr(const char* haystack, const char* needle) {
  if (!needle || !*needle) {
    return (char*)haystack;
  }

  size_t needleLen = strlen(needle);
  for (char* h = (char*)haystack; h; h = utf8next(h)) {
    if (strncmp(h, needle, needleLen) == 0) {
      return h;
    }
  }

  return NULL;
}

void freeEachStrInList(char** strs) {
  while (*strs) {
    free(*strs);
    strs++;
  }
}

void freeStrList(char** strs) {
  freeEachStrInList(strs);
  free(strs);
}

void autoCleanStr(char** str) { free(*str); }

bool startsWith(const char* text, const char* word) {
  if (!text || !word) {
    return false;
  }
  return strncmp(text, word, strlen(word)) == 0;
}

size_t countWord(const char* text, const char* word) {
  if (!text || !word || !*word) {
    return 0;
  }

  size_t occur = 0;
  size_t wordLen = strlen(word);
  for (text = utf8strstr(text, word); text; occur++) {
    text += wordLen;
    text = utf8strstr(text, word);
  }

  return occur;
}

static bool compileRegex(const char* pattern) {
  if (!rePattern || strcmp(pattern, rePattern) != 0) {
    // Ensure a previous pattern was compiled before free
    if (rePattern) {
      regfree(&regex);
      free(rePattern);
    }

    // Compile the regular expression pattern to cache
    rePattern = strDup(pattern);
    const int status = regcomp(&regex, rePattern, REG_EXTENDED);

    // Handle RE compilation error
    if (status != 0) {
      char description[80] = {0};
      regerror(status, &regex, description, sizeof(description));
      logError("bad regex", description, rePattern);
      free(rePattern);
      rePattern = NULL;
      return false;
    }
  }  // ..else, reuse cached RE
  return true;
}

bool matchRegex(const char* pattern, const char* text) {
  if (!compileRegex(pattern)) {
    return false;
  }

  // Match the string against the pattern
  return regexec(&regex, text, 0, NULL, 0) == 0;
}

static int countCapturingGroups(const char* pattern) {
  int groups = 0;
  bool brackets = false;
  for (int i = 0; pattern[i]; i++) {
    switch (pattern[i]) {
      case '[':
        brackets = true;
        break;
      case ']':
        brackets = false;
        break;
      case '(':
        if (!brackets && pattern[i + 1] && (i == 0 || pattern[i - 1] != '\\') &&
            !(pattern[i + 1] == '?' &&
              (pattern[i + 2] == ':' || pattern[i + 2] == '=' ||
               pattern[i + 2] == '!'))) {
          groups++;
        }
        break;
      default:
        break;
    }
  }
  return groups;
}

char** regexGroups(const char* pattern, const char* text, int* count) {
  if (!compileRegex(pattern)) {
    return NULL;
  }

  int groupCount = countCapturingGroups(pattern) + 1;
  regmatch_t* pmatch = FRY_ALLOC(sizeof(regmatch_t) * (groupCount));
  char** groups = FRY_CALLOC(sizeof(char*) * (groupCount));
  *count = groupCount;

  if (regexec(&regex, text, groupCount, pmatch, 0) == 0) {
    for (int i = 0; i < groupCount; i++) {
      if (pmatch[i].rm_so != -1) {
        int start = pmatch[i].rm_so;
        int end = pmatch[i].rm_eo;
        int size = end - start;
        groups[i] = copyChunk(text + start, size);
      }
    }
  }

  free(pmatch);
  return groups;
}

char* replaceFirst(const char* text, const char* oldWord, const char* newWord) {
  if (!text || !oldWord || !*oldWord) {
    return strDup(text);
  }

  const char* occur = utf8strstr(text, oldWord);
  if (!occur) {
    return strDup(text);
  }

  const size_t textLen = strlen(text);
  const size_t oldWordLen = strlen(oldWord);
  if (textLen == oldWordLen) {
    return strDup(newWord);
  }

  const size_t resultSize = textLen + strlen(newWord) - oldWordLen + 1;

  char* result = FRY_ALLOC(resultSize);

  const size_t fragLen = occur - text;
  memcpy(result, text, fragLen);
  char* p = quickCat(&result[fragLen], newWord);
  quickCat(p, &text[fragLen + oldWordLen]);

  return result;
}

char* replaceAll(const char* text, const char* oldWord, const char* newWord) {
  size_t occur = countWord(text, oldWord);
  if (!occur) {
    return strDup(text);
  }

  const size_t oldWordLen = strlen(oldWord);
  const size_t newWordLen = strlen(newWord);
  const size_t resultSize =
    strlen(text) + occur * (newWordLen - oldWordLen) + 1;

  char* result = FRY_ALLOC(resultSize);

  size_t len = 0;
  while (*text) {
    if (strncmp(text, oldWord, oldWordLen) == 0) {
      memcpy(&result[len], newWord, newWordLen);
      len += newWordLen;
      text += oldWordLen;
    } else {
      const char* next = utf8next(text);
      const size_t charLen = next - text;
      memcpy(&result[len], text, charLen);  // copy the UTF-8 character
      len += charLen;
      text = next;
    }
  }

  result[len] = '\0';

  return result;
}

bool writeFile(const char* filepath, const char* buffer, size_t len,
               const char* mode) {
  FILE* file = fopen(filepath, mode);
  if (!file) {
    return false;
  }

  if (!buffer || !len) {
    fclose(file);
    return true;  // nothing to write is already a success
  }

  // Write the specified length of buffer to file
  if (fwrite(buffer, 1, len, file) != len) {
    int savedErrno = errno;  // save errno before fclose
    fclose(file);
    if (!savedErrno) {
      errno = EIO;
    } else {
      errno = savedErrno;
    }
    return false;
  }

  if (fclose(file) == EOF) {
    if (!errno) {
      errno = EIO;
    }
    return false;
  }

  return true;
}

bool existsFile(const char* filepath) { return access(filepath, F_OK) == 0; }

static inline bool isRegularFile(const char* filepath) {
  struct stat fileInfo = {0};
  if (stat(filepath, &fileInfo)) {
    return false;
  }
  return S_ISREG(fileInfo.st_mode);
}

ssize_t readFile(char** result, const char* filepath, const char* mode) {
  if (!isRegularFile(filepath)) {
    errno = EINVAL;  // not a regular file
    return -1;
  }

  FILE* file = fopen(filepath, mode);
  if (!file) {
    return -1;
  }

  // Get file size
  struct stat fileInfo = {0};
  if (fstat(fileno(file), &fileInfo)) {
    int savedErrno = errno;  // save errno before fclose
    fclose(file);
    if (!savedErrno) {
      errno = EIO;
    } else {
      errno = savedErrno;
    }
    return -1;
  }
  if (fileInfo.st_size < 0) {
    fclose(file);
    errno = EIO;  // something wrong (unknown)
    return -1;
  }

  size_t size = (size_t)fileInfo.st_size;

  // Allocate memory for file contents
  char* buffer = FRY_ALLOC(size + 1);

  // Read file contents and close it (naive approach)
  size_t bytesRead = fread(buffer, 1, size, file);
  fclose(file);

#ifdef DEBUG
  if (traceLogs) {
    auto_str num = ofInt(bytesRead);
    logTrace2(filepath, __func__, "bytes read", num);
  }
#endif

  if (bytesRead != size) {
    free(buffer);
    errno = EIO;  // something wrong (unknown)
    return -1;
  }

  buffer[size] = '\0';  // fread does not null-terminate strings
  *result = buffer;
  return (ssize_t)size;
}

bool isDir(const char* filepath) {
  struct stat fileInfo = {0};
  if (stat(filepath, &fileInfo)) {
    return false;
  }
  return S_ISDIR(fileInfo.st_mode);
}

char** listDir(const char* filepath) {
  // Get dir handler
  DIR* dir = opendir(filepath);
  if (!dir) {
    return NULL;  // use strerror(errno) to read the error
  }

  // Pre-allocate initial "buffer" space for paths list
  size_t size = 0;
  size_t capacity = INIT_DIR_LEN;
  char** list = FRY_ALLOC((capacity + 1) * sizeof(char*));

  // Set base path into buffer and get pointer to end as prefixed
  size_t len = strlen(filepath);
  if (len >= PATH_MAX - 1) {
    fatalOut(__FILE__, __func__, MSG_BMO " (a)");
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }
  char buffer[PATH_MAX] = {'\0'};  // assign '\0' to buffer[0]
  char* prefixed = quickCat(buffer, filepath);
  if (filepath[len - 1] != '/') {
    prefixed = quickCat(prefixed, "/");
    len++;
  }

  // Traverse directory collecting path items
  struct dirent* entry = NULL;
  while ((entry = readdir(dir))) {
    if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
      if (size >= capacity) {
        capacity *= 2;  // double the capacity
        list = FRY_REALLOC(list, (capacity + 1) * sizeof(char*));
      }

      if (len + strlen(entry->d_name) >= PATH_MAX) {
        fatalOut(__FILE__, __func__, MSG_BMO " (b)");
        exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
      }
      quickCat(prefixed, entry->d_name);
      list[size++] = join2(entry->d_name, isDir(buffer) ? "/" : "");
    }
  }
  list[size] = NULL;

  if (closedir(dir) != 0) {
    logAlert(filepath, __func__, "can't close directory");
  }

  return FRY_REALLOC(list, (size + 1) * sizeof(char*));
}

// Any hash function, even a broken one, will be fine for detecting
// accidental file corruption. A 32-bit hash code should be enough
// to protect against bit errors in relatively small files <100kb:
// http://www.backplane.com/matt/crc64.html
// Apparently FNV1A_Jesteress is the fastest and "good enough" hash
// algorithm for "long" strings. For more details, please lookup
// FNV1A_Hash_Jesteress at: http://www.sanmayce.com/Fastest_Hash/
static inline uint32_t rol32(uint32_t x, uint32_t bits) {
  return x << bits | x >> (32 - bits);
}
uint32_t jesteressHash32(const char* text, size_t len) {
  static const size_t u = sizeof(uint16_t);
  static const size_t w = sizeof(uint32_t);
  static const size_t dw = sizeof(uint64_t);
  static const uint32_t yp = 709607;
  const char* p = text;

  uint32_t h = 2166136261;  // hash (init)

  for (; len >= dw; len -= dw, p += dw) {
    h = (h ^ (rol32(*(uint32_t*)p, 5) ^ *(uint32_t*)(p + 4))) * yp;
  }

  if (len & w) {
    h = (h ^ *(uint32_t*)p) * yp;
    p += w;
  }

  if (len & u) {
    h = (h ^ *(uint16_t*)p) * yp;
    p += u;
  }

  if (len & 1) {
    h = (h ^ *p) * yp;
  }

  return h ^ (h >> 16);
}

// Algorithm "xor" from p. 4 of Marsaglia, "Xorshift RNGs"
static inline uint64_t xorshift64(uint64_t state) {
  state ^= state << 13;
  state ^= state >> 7;
  state ^= state << 17;
  return state;
}

void initXorshiftStates(void) {
  // Seed: mix in some entropy from different sources into xorshift states
  uint64_t a = atomic_fetch_add(&threadCounter, 1);
  uint64_t b = time(NULL);
#ifndef __EMSCRIPTEN__
  uint64_t c = getpid();
  uint64_t d = clock();
#else
  uint64_t c = EM_ASM_INT_V({ return Date.now(); });
  uint64_t d = EM_ASM_INT_V({ return Math.floor(Math.random() * 65536); });
#endif
  xorshiftStateA = (123456789 + a) ^ b;
  xorshiftStateB = (987654321 + a) ^ (c << 16);
  xorshiftStateC = (456789012 + a) ^ (d << 16);

  // Warm-up: randomize initialization, attenuate seed correlations or traits
  rnGen();
  rnGen();
  rnGen();
  rnGen();

#ifdef DEBUG
  if (traceLogs) {
    stderrStartLine(CL_BLU);
    fprintf(stderr, MRG_STR LB_DEBUG ": %11.11s >     stateA > %" PRIu64,
            __func__, xorshiftStateA);
    stderrEndLine();
    stderrStartLine(CL_BLU);
    fprintf(stderr, MRG_STR LB_DEBUG ": %11.11s >     stateB > %" PRIu64,
            __func__, xorshiftStateB);
    stderrEndLine();
    stderrStartLine(CL_BLU);
    fprintf(stderr, MRG_STR LB_DEBUG ": %11.11s >     stateC > %" PRIu64,
            __func__, xorshiftStateC);
    stderrEndLine();
  }
#endif
}

uint64_t rnGen(void) {
  xorshiftStateA = xorshift64(xorshiftStateA);
  xorshiftStateB = xorshift64(xorshiftStateB);
  xorshiftStateC = xorshift64(xorshiftStateC);
  return xorshiftStateA ^ xorshiftStateB ^ xorshiftStateC;
}

double getCurrentMs(clockid_t clockType) {
  struct timespec t = {0};

  if (clock_gettime(clockType, &t) == -1) {
    logError(__FILE__, __func__, strerror(errno));
    return 0;
  }

  return (double)t.tv_sec * 1000 + (double)t.tv_nsec * 0.000001;
}
