/**
 * @file server.h
 * @brief Socket connection base
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-11
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#pragma once

#include "sdk.h"

#define WAIT_LISTEN_RETRY 50  // wait for 50 ms before retry
#define MAX_LISTEN_RETRY 10   // limit to 10 retries, then crash

extern __thread bool verifySSL;
extern __thread uint16_t listenedPort;

#ifdef SSL_SUPPORT

/**
 * @brief Configure SSL context (load certificates).
 *
 * @param certPath file path
 * @param keyPath file path
 * @return int (non-zero for failures)
 */
int initServerSSL(const char *certPath, const char *keyPath);

/**
 * @brief Configure SSL context (client function).
 *
 * @return int (2 for hard failure)
 */
int initClientSSL(void);

/**
 * @brief Do the SSL handshake (use after accept).
 * @note This function assumes sslServerCtx is set.
 *
 * @param fd socket file descriptor
 * @return SSL*
 */
SSL *handshakeSSL(int fd);

#endif

/**
 * @brief Bind to port and start listening for connections.
 *
 * @param port number
 * @return int (socket file descriptor)
 */
int startListening(uint16_t port);

/**
 * @brief Send a message over socket connection.
 *
 * @param fd socket file descriptor
 * @param ssl session SSL* (optional)
 * @param msg payload
 * @param len payload size
 * @return bytes sent (negative on failure)
 */
ssize_t sendMessage(int fd, void *ssl, const char *msg, size_t len);

/**
 * @brief Receive a message over socket connection.
 *
 * @param fd socket file descriptor
 * @param ssl session SSL* (optional)
 * @param buff buffer
 * @param len buffer size
 * @return bytes received (negative on failure)
 */
ssize_t receiveMessage(int fd, void *ssl, char *buff, size_t len);

/**
 * Attempt to accept a connection within timeoutMs milliseconds.
 *
 * @param serverFd socket file descriptor
 * @param addr pointer to sockaddr_storage with client address
 * @param addrlen pointer to socklen_t containing size of `addr`
 * @param timeoutMs timeout in milliseconds (0 for non-blocking, -1 for block)
 *
 * @return
 *   - on success: new client file descriptor (>= 0)
 *   - on timeout: -1 and sets errno = EAGAIN
 *   - on error: -1
 */
int sockAcceptTimeout(int serverFd, struct sockaddr *addr, socklen_t *addrlen,
                      int timeoutMs);

/**
 * @brief Terminate the socket connection.
 *
 * @param fd socket file descriptor
 * @param ssl session SSL* (optional)
 */
void closeSocket(int fd, void *ssl);
