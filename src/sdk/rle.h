/**
 * @file rle.h
 * @brief Flat run-length-encoded format
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.1.1
 * @date 2024-02-05
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#pragma once

#include "logger.h"

#define RLE_CHUNK_SIZE 512  // 512~2048 seems to be a sweet spot
#define RLE_ENCODE 32       // must be a power of 2, but can't exceed 128

/**
 * @brief Compress binary input data using RLE schema.
 *
 * @param input binary chunk
 * @param size of input (bytes)
 * @param compSize size of compressed output (bytes)
 * @return char* (compressed, use free)
 */
char *rleCompress(const char *input, size_t size, size_t *compSize);

/**
 * @brief Decompress RLE data into original format.
 * @note On failure, NULL is returned and *decoSize is set to non-zero value.
 *
 * @param compressed RLE data
 * @param size of compressed (bytes)
 * @param decoSize size of decompressed output (bytes)
 * @return char* (decompressed, use free)
 */
char *rleDecompress(const char *compressed, size_t size, size_t *decoSize);
