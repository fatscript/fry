/**
 * @file crypto.c
 * @brief A very simple solution to obfuscate data with bitwise-XOR.
 *
 * Based on work I've done together with team mate Joao Condack back in
 * mid-2005, at PUC-Rio University. Now with standard base-64 encoding.
 *
 * Used as back-end for the FatScript enigma library.
 *
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-16
 * @note This schema is not cryptographically safe!
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "crypto.h"

#include "b64.h"
#include "logger.h"

// Default key used if no custom key is provided
static const char* defaultKey = "1m2Tz82bWk1kbc4j563ec94f9cb4e44a";

// Hexadecimal characters for base64 encoding
static const char* hexCodes = "0123456789abcdef";

char* xorEncode(const char* key, const char* text) {
  if (!text) {
    return NULL;
  }

  // Compute hash
  size_t textLen = strlen(text);
  uint32_t hash = jesteressHash32(text, textLen);

  // Allocate buffer encrypted copy
  size_t inputLen = sizeof(hash) + textLen;
  auto_str encrypted = FRY_ALLOC(inputLen);

  // Copy hash and text into the buffer
  memcpy(encrypted, &hash, sizeof(hash));
  memcpy(encrypted + sizeof(hash), text, textLen);

  // Apply XOR encryption
  const char* k = key && *key ? key : defaultKey;
  size_t keyLen = strlen(k);
  for (size_t i = 0; i < inputLen; i++) {
    encrypted[i] ^= k[i % keyLen];
  }

  // Allocate buffer for encoded output
  size_t encodeLen = b64EncodeLength(inputLen);
  char* encoded = FRY_ALLOC(encodeLen + 1);

  // Base64 encode the encrypted binary data
  size_t written = b64Encode(encrypted, inputLen, encoded);
  encoded[written] = '\0';

  return encoded;
}

char* xorDecode(const char* key, const char* data) {
  if (!data) {
    return NULL;
  }

  // Allocate buffer for decoded output
  size_t dataLen = strlen(data);
  size_t decodeLen = b64DecodeLength(dataLen);
  char* decoded = FRY_ALLOC(decodeLen + 1);

  // Base64 decode
  size_t len = b64Decode(data, dataLen, decoded);
  decoded[len] = '\0';

  // Apply XOR decryption
  const char* k = key && *key ? key : defaultKey;
  size_t keyLen = strlen(k);
  for (size_t i = 0; i < len; i++) {
    decoded[i] ^= k[i % keyLen];
  }

  // Extract hash
  uint32_t extractedHash = 0;
  if (len < sizeof(extractedHash)) {
    free(decoded);
    return NULL;  // bad data
  }
  memcpy(&extractedHash, decoded, sizeof(extractedHash));

  // Validate outcome
  size_t textLen = len - sizeof(extractedHash);
  uint32_t hash = jesteressHash32(decoded + sizeof(extractedHash), textLen);
  if (extractedHash != hash) {
    free(decoded);
    return NULL;  // hash mismatch, decryption failed
  }

  // Shift the decrypted text to the beginning of the buffer
  memmove(decoded, decoded + sizeof(extractedHash), textLen);
  decoded[textLen] = '\0';  // null-terminate the shifted text

  return FRY_REALLOC(decoded, textLen + 1);
}

char* generateKey(size_t len) {
  char* key = FRY_ALLOC(len + 1);
  for (size_t i = 0; i < len; i++) {
    key[i] = b64encoding[rnGen() % 64];  // base64 alphabet
  }
  key[len] = '\0';

  return key;
}

char* generateUuid(void) {
  // Allocate memory for UUID string
  char* uuid = FRY_CALLOC(37);

  // Set version number
  uuid[14] = '4';

  // Set RFC 4122 variant bits
  uuid[19] = hexCodes[(rnGen() % 4) + 8];

  // Set dashes in appropriate positions
  uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';

  // Generate random hex codes for the other positions
  for (int i = 0; i < 36; i++) {
    if (!uuid[i]) {
      uuid[i] = hexCodes[rnGen() % 16];
    }
  }

  return uuid;
}

char* deriveKey(const char* secret, size_t len, int iter) {
  uint32_t hash = jesteressHash32(secret, len);
  for (int i = 1; i < iter; i++) {
    hash = jesteressHash32((char*)&hash, sizeof(hash));
  }

  char output[DERIVED_SIZE] = {'\0'};
  size_t outputIndex = 0;
  while (outputIndex < DERIVED_SIZE) {
    char derived[11] = {'\0'};  // uint32_t max is 2^32 which is 10 digits long
    snprintf(derived, sizeof(derived), "%" PRIu32, hash);
    hash = jesteressHash32(derived, strlen(derived));
    for (int i = 0; i < 4; i++) {
      // Map each part of the hash to a character in the base64 alphabet
      output[outputIndex++] = b64encoding[(hash >> (6 * i)) & 0x3F];
    }
  }

  return copyChunk(output, DERIVED_SIZE);
}
