/**
 * @file structures.c
 * @brief fry interpreter internal data structures
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-16
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "memory.h"
#include "sdk.h"

const char *tokType(TokType type) {
  switch (type) {
    case TkEOF:
      return "eof";
    case TkPunct:
      return "punctuation";
    case TkKey:
      return "keyword";
    case TkId:
      return "identifier";
    case TkName:
      return "typename";
    case TkUnder:
      return "underscore";
    case TkOp:
      return "operator";
    case TkNumb:
      return "number";
    case TkHuge:
      return "huge";
    case TkText:
      return "text";
    case TkTemp:
      return "template";
    case TkRaw:
      return "raw";
    case TkComm:
      return "comment";
    case TkEmbed:
      return "embedded";
    default:
      return "unknown";
  }
}

const char *fatType(NodeType type) {
  switch (type) {
    case FatInvalid:
      return "Invalid";
    case FatVoid:
      return "Void";
    case FatAny:
      return "Any";
    case FatBoolean:
      return "Boolean";
    case FatNumber:
      return "Number";
    case FatHugeInt:
      return "HugeInt";
    case FatChunk:
      return "Chunk";
    case FatText:
      return "Text";
    case FatTemp:
      return "Template";
    case FatEntry:
      return "Entry";
    case FatList:
      return "List";
    case FatScope:
      return "Scope";
    case FatMethod:
      return "Method";
    case FatProcedure:
      return "Procedure";
    case FatError:
      return "Error";
    case FatType:
      return "Type";
    case FatBlock:
      return "Block";
    case FatDynamic:
      return "Dynamic";
    case FatCall:
      return "Call";
    case FatAssign:
      return "Assign";
    case FatUnary:
      return "Unary";
    case FatExpr:
      return "Expr";
    case FatCase:
      return "Case";
    case FatSwitch:
      return "Switch";
    case FatLoop:
      return "Loop";
    case FatEmbed:
      return "Embedded";
    default:
      return "Unknown";
  }
}

const char *opType(OpType op) {
  switch (op) {
    case OpAssign:
      return "assign";
    case OpOr:
      return "or";
    case OpAnd:
      return "and";
    case OpNot:
      return "not";
    case OpBool:
      return "bool-of";
    case OpLess:
      return "less";
    case OpMore:
      return "more";
    case OpLessEq:
      return "less-eq";
    case OpMoreEq:
      return "more-eq";
    case OpEqual:
      return "equal";
    case OpNotEq:
      return "not-eq";
    case OpPlus:
      return "plus";
    case OpIncrement:
      return "increment";
    case OpMinus:
      return "minus";
    case OpDecrement:
      return "decrement";
    case OpMultiply:
      return "multiply";
    case OpMulBy:
      return "multiply-by";
    case OpPow:
      return "power";
    case OpPowBy:
      return "power-by";
    case OpSlash:
      return "slash";
    case OpDivBy:
      return "divide-by";
    case OpPercent:
      return "percent";
    case OpModBy:
      return "modulus-by";
    case OpProcedure:
      return "procedure";
    case OpLambda:
      return "lambda";
    case OpImport:
      return "import";
    case OpCase:
      return "case";
    case OpSwitch:
      return "switch";
    case OpTap:
      return "tap";
    case OpIf:
      return "if";
    case OpDot:
      return "dot";
    case OpRange:
      return "range";
    case OpHORange:
      return "ho-range";  // half-open range
    case OpIfDot:
      return "if-dot";
    case OpCoalesce:
      return "coalesce";
    case OpCoAssign:
      return "co-assign";  // coalescing assign
    case OpMissing:
      return "missing";  // (unimplemented)
    case OpLoop:
      return "loop";
    case OpMutable:
      return "mutable";
    default:
      return "invalid";
  }
}

OpType getOpType(const char *op) {
  return !strcmp(op, "=")       ? OpAssign
         : !strcmp(op, "|")     ? OpOr
         : !strcmp(op, "&")     ? OpAnd
         : !strcmp(op, "!")     ? OpNot
         : !strcmp(op, "!!")    ? OpBool
         : !strcmp(op, "<")     ? OpLess
         : !strcmp(op, ">")     ? OpMore
         : !strcmp(op, "<=")    ? OpLessEq
         : !strcmp(op, ">=")    ? OpMoreEq
         : !strcmp(op, "==")    ? OpEqual
         : !strcmp(op, "!=")    ? OpNotEq
         : !strcmp(op, "+")     ? OpPlus
         : !strcmp(op, "+=")    ? OpIncrement
         : !strcmp(op, "-")     ? OpMinus
         : !strcmp(op, "-=")    ? OpDecrement
         : !strcmp(op, "*")     ? OpMultiply
         : !strcmp(op, "*=")    ? OpMulBy
         : !strcmp(op, "**")    ? OpPow
         : !strcmp(op, "**=")   ? OpPowBy
         : !strcmp(op, "/")     ? OpSlash
         : !strcmp(op, "/=")    ? OpDivBy
         : !strcmp(op, "%")     ? OpPercent
         : !strcmp(op, "%=")    ? OpModBy
         : !strcmp(op, "<>")    ? OpProcedure
         : !strcmp(op, "->")    ? OpLambda
         : !strcmp(op, "<-")    ? OpImport
         : !strcmp(op, "=>")    ? OpCase
         : !strcmp(op, ">>")    ? OpSwitch
         : !strcmp(op, "<<")    ? OpTap
         : !strcmp(op, "?")     ? OpIf
         : !strcmp(op, ".")     ? OpDot
         : !strcmp(op, "..")    ? OpRange
         : !strcmp(op, "..<")   ? OpHORange
         : !strcmp(op, "?.")    ? OpIfDot
         : !strcmp(op, "??")    ? OpCoalesce
         : !strcmp(op, "?\?=")  ? OpCoAssign
         : !strcmp(op, "?\?\?") ? OpMissing
         : !strcmp(op, "@")     ? OpLoop
         : !strcmp(op, "~")     ? OpMutable
                                : OpInvalid;
}

Token *createToken(TokType type, char *src) {
  Token *token = FRY_ALLOC(sizeof(Token));
  token->type = type;
  token->num = 0;
  token->val = NULL;
  token->next = NULL;
  token->src = src;
  return token;
}

#ifdef DEBUG
char *ofToken(Token *tok) {
  if (!tok) {
    return strDup("null");
  }
  char num[NUMBER_MAX_LENGTH + 1];  // case TkNumb
  char *text = tok->val;
  const char *type = tokType(tok->type);
  switch (tok->type) {
    case TkPunct:
    case TkKey:
    case TkId:
    case TkOp:
    case TkText:
    case TkTemp:
    case TkRaw:
    case TkHuge:
      if (text[0] == '\n') {
        return join2(type, GUIDE "eol");
      } else {
        return join4(type, GUIDE "'", text, "'");
      }
    case TkNumb:
      text = join3(type, GUIDE, prettyNumber(num, tok->num));
      return text;
    default:
      return strDup(type);
  }
}
#endif

Reader *createReader(const char *name) {
  Reader *reader = FRY_CALLOC(sizeof(Reader));
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-qual"  // drops const qualifier
  reader->name = (char *)name;                // don't free...
#pragma GCC diagnostic pop
  reader->ln = 1;
  reader->col = 1;
  return reader;
}

void deleteReader(Reader *reader) {
  Token *tok = reader->head;

  while (tok) {
    Token *next = tok->next;
    free(tok);
    tok = next;
  }

  free(reader);
}

void freeReaderAll(Reader *reader) {
  Token *tok = reader->head;

  while (tok) {
    Token *next = tok->next;
    free(tok->val);
    free(tok->src);
    free(tok);
    tok = next;
  }

  free(reader);
}

char advanceChar(Reader *reader) {
  char ch = reader->source[reader->pos];

  if (ch == '\n') {
    reader->ln++;
    reader->col = 1;
  } else {
    reader->col++;
  }

  reader->pos++;
  return ch;
}

Token *advanceTok(const char *func, Reader *reader) {
#ifndef DEBUG
  (void)func;
#else
  if (!reader->current) {
    fatalOut(func, __func__, MSG_N_PTR);
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }
  if (!reader->current->next) {
    fatalOut(func, reader->current->src, MSG_U_EOF);
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }
#endif

  reader->current = reader->current->next;

#ifdef DEBUG
  if (traceLogs) {
    auto_str token = ofToken(reader->current);
    logTrace2(func, __func__, reader->current->src, token);
  }
#endif

  return reader->current;
}

void skipTok(const char *func, Reader *reader) {
  Token *tok = reader->current;
  if (tok) {
    free(tok->val);
    free(tok->src);
    if (tok->type) {
      advanceTok(func, reader);
    }

    // avoid double free, see formatter freeReaderAll
    tok->val = NULL;
    tok->src = NULL;
  }
}

void initQuickRefs(IntTuple *refs, long size) {
  refs->_1 = size / 3;
  refs->_2 = size * 2 / 3;
}

Entry *createScopeEntry(const char *key, Node *data) {
  Entry *entry = FRY_ALLOC(sizeof(Entry));
  entry->key.s = strDup(key);
  entry->data = data;
  if (data && data->meta) {
    atomic_fetch_add(&data->meta->refs, 1);
  }
  entry->next = entry->skip = NULL;
  return entry;
}

Entry *createListEntry(long index, Node *data) {
  Entry *entry = FRY_ALLOC(sizeof(Entry));
  entry->key.i = index;
  entry->data = data;
  if (data && data->meta) {
    atomic_fetch_add(&data->meta->refs, 1);
  }
  entry->next = entry->skip = NULL;
  return entry;
}

void freeScopeEntry(Entry *entry) {
  Node *data = entry->data;
  if (data && data->meta) {
    atomic_fetch_sub(&data->meta->refs, 1);
  }
  free(entry->key.s);
  free(entry);
}

void freeListEntry(Entry *entry) {
  Node *data = entry->data;
  if (data && data->meta) {
    atomic_fetch_sub(&data->meta->refs, 1);
  }
  free(entry);
}

/**
 * Generate a simple hash from string
 */
static uint32_t djb2Hash(const char *str) {
  uint32_t hash = 5381;
  int ch = 0;
  while ((ch = (Byte)*str++)) {
    hash = ((hash << 5) + hash) + ch;
  }
  return hash;
}

bool isReservedName(const char *name) {
  return strcmp(name, "Any") == 0 || strcmp(name, "Type") == 0;
}

Type *getType(const char *name) {
  assert(name != NULL);

  Type *ck = NULL;
  int hash = (int)(djb2Hash(name) % META_HASH);

#ifdef DEBUG
  if (traceLogs) {
    auto_str hashIndex = ofInt(hash);
    auto_str msg = join2("hashIndex" GUIDE, hashIndex);
    logDebug2(__FILE__, __func__, name, msg);
  }
#endif

  for (ck = metaSpace[hash]; ck; ck = ck->next) {
    if (strcmp(ck->name, name) == 0) {
      break;
    }
  }

#ifdef DEBUG
  if (traceLogs) {
    auto_str prtRef = ofPointer((void *)ck);
    logDebug2(__FILE__, __func__, name, prtRef);
  }
#endif

  return ck;
}

Type *setType(const char *name, Node *def) {
  assert(name != NULL);

  if (isReservedName(name)) {
    return NULL;
  }

  Type *ck = getType(name);
  if (ck) {
    if (def) {
      if (ck->def) {
        // deep check: is definition perfectly equivalent to previous?
        if (nodeEq(ck->def, def)) {
          bool eq = true;
          Node *a = ck->def->head;
          Node *b = def->head;
          for (; eq && a && b; a = a->seq, b = b->seq) {
            eq = nodeEq(a, b);
          }
          if (eq && !a && !b) {
            return ck;  // ignore
          }
        }

        // error out!
        char *msg = join5(name, "\n - ", ck->def->src, "\n - ", def->src);
        fatalOut(__func__, "type redefined", msg);
        exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
      }
      ck->def = def;  // set the type definition if not defined
    }

    return ck;
  }

  ck = FRY_CALLOC(sizeof(Type));

#ifdef DEBUG
  if (traceLogs) {
    auto_str prtRef = ofPointer((void *)ck);
    logDebug2(__FILE__, __func__, name, prtRef);
  }
#endif

  // configure
  size_t len = strlen(name);
  int hash = (int)(djb2Hash(name) % META_HASH);
  ck->name = copyChunk(name, len);
  ck->def = def;

  lockResource(&memoryLock);
  ck->next = metaSpace[hash];
  metaSpace[hash] = ck;
  unlockResource(&memoryLock);

  return ck;
}

Scope *createScope(void) {
  Scope *scope = FRY_CALLOC(sizeof(Scope));
  if (pthread_mutex_init(&scope->lock, &recursiveMutex)) {
    fatalOut(__FILE__, __func__, MSG_F_MUTEX);
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }
  atomic_init(&scope->refs, 0);
  return scope;
}

Scope *createList(void) {
  Scope *scope = createScope();
  scope->isList = true;
  return scope;
}

Scope *scopeInward(Scope *nextScope, Context *ctx) {
  assert(nextScope != NULL);
  assert(ctx != NULL);

  Scope *prevScope = ctx->enclosingO;
  ctx->enclosingO = ctx->enclosingI;
  ctx->enclosingI = nextScope;
  return prevScope;
}

void scopeOutward(Scope *prevScope, Context *ctx) {
  assert(ctx != NULL);

  ctx->enclosingI = ctx->enclosingO;
  ctx->enclosingO = prevScope;
}

Node *createNode(NodeType type, char *at, Context *ctx) {
  Node *node = FRY_CALLOC(sizeof(Node));
  node->type = type;
  node->src = at;
  addMemory(node, ctx);
  return node;
}

Context *createContext(bool isMain) {
  Context *ctx = FRY_CALLOC(sizeof(Context));
  ctx->stack = FRY_ALLOC(stackDepth * sizeof(Frame));
  if (pthread_mutex_init(&ctx->lock, &recursiveMutex)) {
    fatalOut(__FILE__, __func__, MSG_F_MUTEX);
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }
  atomic_init(&ctx->top, 0);
  ctx->isMain = isMain;
  atomic_init(&ctx->isCanceled, false);
  ctx->enclosingI = ctx->enclosingO = globalScope;
  return ctx;
}

void freeContext(Context *ctx) {
  moveLocalToGlobalMemory(ctx);
  free(ctx->stack);
  pthread_mutex_destroy(&ctx->lock);
  free(ctx);
}

HugeInt *copyHuge(HugeInt num) {
  HugeInt *copy = FRY_ALLOC(sizeof(HugeInt));
  memcpy(copy, num, sizeof(HugeInt));
  return copy;
}

/**
 * In some specific case, like immutable lists of primitives
 * it is safe to pass by reference, as both the list and values
 * cannot be changed afterwards.
 */
static bool canSkipCollectionCopy(Scope *coll, bool isMut) {
  if (!isMut && coll && coll->isList && coll->size > 0) {
    switch (coll->entries->data->type) {
      case FatVoid:
      case FatBoolean:
      case FatNumber:
      case FatHugeInt:
      case FatChunk:
      case FatText:
        return true;
      case FatList:
        return canSkipCollectionCopy(coll->entries->data->scp, false);
      default:
        break;
    }
  }
  return false;
}

Node *copyNode(Node *node, bool isDeep, Context *ctx) {
  if (!node) {
    return NULL;
  }

  Node *copy = FRY_ALLOC(sizeof(Node));

  memcpy(copy, node, sizeof(Node));
  copy->seq = NULL;
  copy->src = cpSrc(node);

  switch (node->type) {
    case FatChunk:
    case FatText:
    case FatTemp:
      copy->val = copyChunk(node->val, node->num.s);
      break;

    case FatHugeInt:
      copy->num.h = copyHuge(*node->num.h);
      break;

    default:
      copy->val = strDup(node->val);
  }

  if (isDeep) {
    copy->head = copyNode(node->head, true, ctx);
    copy->body = copyNode(node->body, true, ctx);
    copy->tail = copyNode(node->tail, true, ctx);
    if (node->scp) {
      Scope *collection = canSkipCollectionCopy(node->scp, node->op)
                            ? node->scp
                            : copyCollection(node->scp, true, ctx);
      bindScope(copy, collection);
    }
  } else {
    bindScope(copy, node->scp);
  }

  addMemory(copy, ctx);
  return copy;
}

Scope *copyCollection(Scope *coll, bool isDeep, Context *ctx) {
  if (!coll) {
    return NULL;
  }

  if (isDeep && !isFirstVisit(coll)) {
    return NULL;
  }

#ifdef DEBUG
  if (debugLogs) {
    auto_str size = ofInt(coll->size);
    logDebug2(__FILE__, __func__, "size", size);
  }
#endif

  Scope *copy = createScope();

  lockResource(&coll->lock);

  copy->ck = coll->ck;
  copy->size = coll->size;
  copy->blanks = coll->blanks;
  copy->isList = coll->isList;

  if (coll->entries) {
    Entry *source = coll->entries;

    if (coll->isList) {
      Node *data = isDeep ? copyNode(source->data, true, ctx) : source->data;
      Entry *dest = copy->entries = createListEntry(0, data);
      copy->quick1 = dest;  // points skip-list "next"
      copy->quick2 = dest;  // points "last"
      for (long i = 1; source->next; i++) {
        data =
          isDeep ? copyNode(source->next->data, true, ctx) : source->next->data;
        dest = createListEntry(i, data);

        if (!adaptiveSkipMod(i)) {
          copy->quick1->skip = dest;
          copy->quick1 = dest;  // point skip-list "next"
        }

        copy->quick2->next = dest;  // "last" points "next"
        copy->quick2 = dest;        // point new "last"
        source = source->next;
      }
    } else {  // is Scope
      Node *data = isDeep ? copyNode(source->data, true, ctx) : source->data;
      Entry *dest = copy->entries = createScopeEntry(source->key.s, data);
      if (copy->size < MIN_QUICK_REF) {
        while (source->next) {
          data = isDeep ? copyNode(source->next->data, true, ctx)
                        : source->next->data;
          dest->next = createScopeEntry(source->next->key.s, data);
          source = source->next;
          dest = dest->next;
        }
      } else {
        IntTuple refs;
        initQuickRefs(&refs, coll->size - 1);
        Entry *skipList = dest;
        for (long i = 1; source->next; i++) {
          data = isDeep ? copyNode(source->next->data, true, ctx)
                        : source->next->data;
          dest->next = createScopeEntry(source->next->key.s, data);
          source = source->next;
          dest = dest->next;

          if (refs._1 == i) {
            copy->quick1 = dest;
          } else if (refs._2 == i) {
            copy->quick2 = dest;
          }

          if (i % SKIP_SIZE == 0) {
            skipList = (skipList->skip = dest);
          }
        }
      }
    }
  }

  unlockResource(&coll->lock);

  return copy;
}

Node *swapEntryData(Scope *coll, Entry *entry, Node *newData) {
  if (newData->meta) {
    atomic_fetch_add(&newData->meta->refs, 1);
  }

  lockResource(&coll->lock);
  Node *oldData = entry->data;
  entry->data = newData;
  if (!oldData) {
    coll->blanks--;  // specifically needed by upsertNode, harmless for lists
  }
  unlockResource(&coll->lock);

  if (oldData && oldData->meta) {
    atomic_fetch_sub(&oldData->meta->refs, 1);
  }
  return newData;
}
