/**
 * @file fry.c
 * @brief Fry interpreter CLI front-end
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-16
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "runtime/interpreter.h"
#include "runtime/utils/help.h"
#include "runtime/utils/repl.h"
#include "syntax/formatter.h"
#include "syntax/lexer.h"
#include "syntax/parser.h"
#include "syntax/patterns.h"

// CLI option flags
bool obfuscate = false;
bool saveSession = false;

static void printHelp(void) {
  printVersion(VerHeader);
  puts(
    "Usage: fry [options] [file]...\n"
    "\n"
    "Option/args   Long option      Meaning\n"
    " -a           --ast            print abstract syntax tree only\n"
    " -b out in    --bundle         save bundle to outfile (implies -p)\n"
    " -c           --clock          time and benchmark logs (toggle)\n"
    " -d           --debug          enable debug logs (implies -c)\n"
    " -e           --error          continue on error (toggle)\n"
    " -f file...   --format         indent FatScript source files\n"
    " -h           --help           show this help and exit\n"
    " -i           --interactive    enable REPL with file execution\n"
    " -j           --jail           restrict FS, network and sys calls\n"
    " -k #[k]      --stack          set stack depth (frame count)\n"
    " -m           --meta           show info about this build\n"
    " -n #[k,m]    --nodes          set memory limit (node count)\n"
    " -o out in    --obfuscate      encode bundle (implies -b)\n"
    " -p           --probe          perform static analysis (dry run)\n"
    " -s           --save           store REPL session to " REPL_FILE
    "\n"
#ifdef DEBUG
    " -t           --trace          enable trace logs (implies -d)\n"
#endif
    " -v           --version        show version number and exit\n"
    " -w           --warranty       show disclaimer and exit\n"
    "\n"
    "Language documentation: <http://fatscript.org>");
}

/**
 * Init the global scope pointer with a blank scope.
 */
static void initGlobalScope(void) { globalScope = createScope(); }

/**
 * Init type pointers (avoid lookup for native types).
 */
static void initTypeSystem(void) {
  // Init reserved name 'Type' as a type-like
  ckTypeType = setType("Type", NULL);

  // Init main native types (placeholder only)
  checkVoid = setType(fatType(FatVoid), NULL);
  checkBoolean = setType(fatType(FatBoolean), NULL);
  checkNumber = setType(fatType(FatNumber), NULL);
  checkHugeInt = setType(fatType(FatHugeInt), NULL);
  checkChunk = setType(fatType(FatChunk), NULL);
  checkText = setType(fatType(FatText), NULL);
  checkList = setType(fatType(FatList), NULL);
  checkScope = setType(fatType(FatScope), NULL);
  checkMethod = setType(fatType(FatMethod), NULL);
  checkError = setType(fatType(FatError), NULL);

  // Init default number sub-types as aliases
  ckEpoch = createAlias("Epoch", checkNumber);
  ckExitCode = createAlias("ExitCode", checkNumber);
  createAlias("Millis", checkNumber);

  // Init chunk sub-type as alias
  ckCPointer = createAlias("CPointer", checkChunk);

  // Init default method sub-type (procedure) as alias
  checkProcedure = createAlias("Procedure", checkMethod);

  // Init default error sub-types as aliases
  ckAssignError = createAlias("AssignError", checkError);
  ckAsyncError = createAlias("AsyncError", checkError);
  ckCallError = createAlias("CallError", checkError);
  ckFileError = createAlias("FileError", checkError);
  ckIndexError = createAlias("IndexError", checkError);
  ckKeyError = createAlias("KeyError", checkError);
  ckSyntaxError = createAlias("SyntaxError", checkError);
  ckTypeError = createAlias("TypeError", checkError);
  ckValueError = createAlias("ValueError", checkError);

  // Init node deduplication pointers
  trueSingleton->ck = falseSingleton->ck = checkBoolean;

  // Define and load StrictType (hardcoded)
  static Scope emptyScp = {0};
  static Node strict = {.type = FatType, .val = "StrictType", .src = SRC_AUX};
  strict.scp = &emptyScp;
  strict.body = strict.tail = &strict;
  checkStrict = strict.tail->ck = strict.ck = setType("StrictType", &strict);
}

/**
 * Make better use of space on big screens.
 */
static inline void getWindowSize(void) {
#ifndef __EMSCRIPTEN__
  if (stdoutIsTTY) {
    struct winsize w = {0};
    if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &w) == 0) {
      if (w.ws_col > 80) {
        indMax = w.ws_col / 2 - 2;  // adjust indent max for AST and logs
      }
    }
  }
#else  // __EMSCRIPTEN__
  termCols = getTermCols();
  termRows = getTermRows();
  if (termCols > 80) {
    indMax = termCols / 2 - 2;  // adjust indent max for AST and logs
  }
#endif
}

/**
 * Execute start hooks and run commands from RC_FILE.
 */
static void initFry(void) {
  if (configOnce) {
    return;
  }
  configOnce = true;

#ifdef DEBUG
  if (traceLogs) {
    logDebug(__FILE__, __func__, "configure once...");
  }
#endif

  // Initialize memory resources
  initMemoryManagement();
  initGlobalScope();
  initTypeSystem();

  // Register handlers for termination signals
  signal(SIGINT, exitFromSignal);
  signal(SIGHUP, exitFromSignal);
  signal(SIGTERM, exitFromSignal);

  // Init local timezone
  struct tm date = {0};
  time_t utc = time(NULL);
  gmtime_r(&utc, &date);
  date.tm_isdst = -1;  // request mktime to lookup DST in timezone database
  time_t local = mktime(&date);
  timeOffset = difftime(utc, local);

#ifdef DEBUG
  if (traceLogs) {
    stderrStartLine(CL_BLU);
    fputs(MRG_STR LB_DEBUG, stderr);
    fprintf(stderr, ": %11.11s >   timezone > %+.0f s", __func__, timeOffset);
    stderrEndLine();
  }
#endif

#ifdef SSL_SUPPORT
  SSL_load_error_strings();
  SSL_library_init();
  OpenSSL_add_all_algorithms();
#endif

  // Seed random number generator (main thread)
  initXorshiftStates();

  // Magic sauce
  bundleKey = strDup("mVjJaYkjQwx2mvDzDmQuVqg6TwHnDXQz");

  // Prepare libcurl for use
  if (curl_global_init(CURL_GLOBAL_ALL) == EXIT_FAILURE) {
    fatalOut(__FILE__, __func__, "curl init failed");
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }

  // Try to set an UTF-8 locale
  if (!setFryLocale("C.UTF-8")) {
    setFryLocale("en_US.UTF-8");
  }

#ifdef USE_READLINE
  // Disable readline tab completion
  rl_inhibit_completion = true;
#elif __EMSCRIPTEN__
  // Configure SDL2 for audio support
  if (SDL_Init(SDL_INIT_AUDIO) < 0) {
    logAlert(__FILE__, "SDL could not initialize", SDL_GetError());
    hasAudioSupport = false;
  }
  int const frequency = EM_ASM_INT_V({
    var context;
    try {
      context = new AudioContext();
    } catch (e) {
      context = new webkitAudioContext();
    }
    return context.sampleRate;
  });
  if (Mix_OpenAudio(frequency, MIX_DEFAULT_FORMAT, 2, 8192) < 0) {
    logAlert(__FILE__, "SDL_mixer could not initialize", Mix_GetError());
    hasAudioSupport = false;
  }
#else
  if (isUtf8Mode) {
    linenoiseSetEncodingFunctions(linenoiseUtf8PrevCharLen,
                                  linenoiseUtf8NextCharLen,
                                  linenoiseUtf8ReadCode);
  }
#endif

  // Gather other env info
  getWindowSize();

  // Precook RC_FILE
  char *rcPath = basePath ? join2(basePath, RC_FILE) : NULL;
  if (!rcPath || !existsFile(rcPath)) {
    free(rcPath);
    rcPath = existsFile(RC_FILE) ? strDup(RC_FILE) : NULL;
  }
  if (rcPath) {
    if (debugLogs) {
      logDebug(__func__, "Precook", rcPath);
    }

    // Store and temporarily ignore CLI memory limit argument
    long cliLimit = memoryLimit;
    memoryLimit = DEFAULT_MEM;
    memoryRedefined = false;

    Reader *reader = tokenizeFile(rcPath);
    Node *program = parse(reader);

    Context *initCtx = createContext(true);
    pushStack(initCtx, __func__, program, globalScope);
    interpret(globalScope, program, initCtx);
    popStack(initCtx, 1);

    // Restore CLI memory limit if config didn't change it
    if (!memoryRedefined) {
      memoryLimit = cliLimit;
    }

    // Clean up if not explicitly asked not to
    if (!keepDotFry) {
      trackParsed(program, initCtx);
      wipeScope(globalScope, NULL, false);
      free(globalScope);
      resetSession();
      globalScope = createScope();
    }

    free(reader->source);
    deleteReader(reader);
    freeContext(initCtx);

    if (debugLogs) {
      logDebug(__func__, "Precook", "done");
    }
    free(rcPath);
  }

  if (isAstOnly) {
    stderrLn(MRG_BLT "AST mode enabled", CL_YEL);
  }
  if (debugLogs) {
    stderrLn(MRG_BLT "Debug logs enabled", CL_YEL);
  }
  if (isJailMode) {
    stderrLn(MRG_BLT "Jail mode enabled", CL_YEL);
  }
  if (interactive) {
    stderrLn(MRG_BLT "Interactive mode enabled", CL_YEL);
  }
#ifdef DEBUG
  if (traceLogs) {
    stderrLn(MRG_BLT "Trace logs enabled", CL_YEL);
  }
#endif
}

/**
 * Run interactive session (REPL).
 */
static ExitCode runRepl(void) {
  initFry();
  if (saveSession) {
    stderrLn(MRG_BLT "Saving inputs to " REPL_FILE, CL_YEL);
  }

  printVersion(VerHeader);
  stdoutLn("        ?        - end line with semicolon (;) to run", CL_GRN);
  stdoutLn("      ))))       - enter $help to access cheat sheets", CL_GRN);
  stdoutLn("    /´. .`\\      - type $warranty for disclaimer note", CL_GRN);
  stdoutLn("__oOO__-__OOo__  - use $exit or Ctrl+D to end session", CL_GRN);
  puts("");

  if (isParseOnly) {
    isParseOnly = false;  // ignore option
  } else if (!interactive) {
    crashOnError = !crashOnError;  // toggle on plain REPL mode (no file)
  }

  showHints = true;
  showResult = true;
  allowDecode = true;
  interactive = true;

  ExitCode exitCode = EXIT_SUCCESS;
  char buffer[BUFF_LEN] = {'\0'};
  if (saveSession) {
    const char *timestamp = getSessionHeader();
    if (!writeFile(REPL_FILE, timestamp, strnlen(timestamp, H_BUFF_MAX), "a")) {
      logError(REPL_FILE, "writeFile", strerror(errno));
    }
  }

  while (getInput(buffer)) {
    size_t len = strnlen(buffer, BUFF_LEN);

    if (getHelp(buffer)) {
      fputs("\n", stderr);
      buffer[0] = '\0';
      continue;
    }

    if (utf8strstr(buffer, "$warranty")) {
      printVersion(VerWarranty);
      buffer[0] = '\0';
      continue;
    }

    if (utf8strstr(buffer, "$exit")) {
      break;
    }

    if (len > 1 && (buffer[len - 2] == ';')) {
      if (saveSession) {
        buffer[len - 2] = '\n';
        if (!writeFile(REPL_FILE, buffer, len, "a")) {
          logError(REPL_FILE, "writeFile", strerror(errno));
        }
      }
      buffer[len - 2] = '\0';
      Reader *reader = createReader(__func__);
      reader->source = buffer;
      Node *program = parse(reader);
      if (program) {
        exitCode = fry(program, reader->name, true);
        fputs("\n", stderr);
      }
      deleteReader(reader);
      buffer[0] = '\0';
    }
  }

#ifndef __APPLE__
  if (stdoutIsTTY) {
    if (isUtf8Mode) {
      fputs(*buffer ? "\033[1A" MRG_NTH : "\033[1A" MRG_1ST, stdout);
    } else {
      fputs("\033[1A" MRG_INP, stdout);
    }
  }
#endif

  // Educate user about $exit embedded command existence
  puts(isUtf8Mode ? "$exit\n" MRG_RES MSG_BYE : "$exit\n" MRG_STR MSG_BYE);

  return exitCode;
}

/**
 * Run from source file.
 */
static ExitCode runFile(char *filepath) {
  if (!filepath) {
    return EXIT_FAILURE;
  }

  basePath = getBasePath(filepath);
  initFry();
  allowDecode = true;

  Reader *reader = tokenizeFile(filepath);
  Node *program = parse(reader);
  free(reader->source);
  deleteReader(reader);

  ExitCode exitCode = fry(program, filepath, false);
  return interactive ? runRepl() : exitCode;
}

static char *processBundling(char *inPath) {
  // Set base path into context
  basePath = getBasePath(inPath);

  // Strip import base path (it's joined back later)
  char *path = basePath ? strrchr(inPath, '/') + 1 : inPath;

  // Allow setting bundleKey/options via RC_FILE
  initFry();

  // Create the bundle from input file
  isParseOnly = true;
  crashOnError = !crashOnError;
  pushImportScope();  // create bundling context
  char *result = bundle(path);
  popImportScope();  // destroy bundling context

  return result;
}

/**
 * Create bundle from file, optionally encoded via obfuscate flag.
 */
static ExitCode createBundle(char *outPath, char *inPath) {
  if (!inPath) {
    return EXIT_FAILURE;
  }

  char *bun = processBundling(inPath);

  if (debugLogs) {
    logMarker("BUNDLE");
    fprintf(stderr, "%s\n", bun);
  }

  // Encode the bundle if obfuscate flag is set
  if (obfuscate) {
    auto_str salt = generateKey(SALT_SIZE);
    auto_str pepper = generateKey(rnGen() % PEPPER_MAX);
    char *temp = join5(salt, "\n", bun, "\n#", pepper);
    free(bun);
    bun = xorEncode(bundleKey, temp);
    free(temp);
    temp = join2("\n#" MARK_ENC, bun);
    free(bun);
    bun = temp;
  }

  // Add headers to final result
  auto_str head = join3("#!/usr/bin/env fry\n#ver=", getFryV(), "\n");
  auto_str result = join3(head, bun, "\n");
  free(bun);

  // Open file in write mode for executable output
  int fileHandle = open(outPath, WRITE_FLAGS, EXECUTABLE_FILE_PERMISSIONS);
  if (fileHandle == -1) {
    logError(__FILE__, __func__, join2("failed to open: ", outPath));
    return EXIT_FAILURE;
  }

  // Write the bundled result to the file
  size_t len = strlen(result);
  long bytesWritten = write(fileHandle, result, len);
  close(fileHandle);

  // Check all has been written as expected
  if (bytesWritten == -1 || (size_t)bytesWritten != len) {
    logError(__FILE__, __func__, join2("failed to write: ", outPath));
    return EXIT_FAILURE;
  }

  logMarker(join4("bundled: ", inPath, " -> ", outPath));
  return EXIT_SUCCESS;
}

static void printMeta(void) {
  auto_str fryMeta = getFryMeta();
  puts(fryMeta);
}

static double parseNumericArg(const char *arg, long max, const char *ref) {
  errno = 0;
  char *multiplier = "";
  double limit = strtod(arg, &multiplier);
  if (*multiplier == 'k') {
    limit *= 1000;
  } else if (*multiplier == 'm') {
    limit *= 1000000;
  } else if (*multiplier) {
    limit = 0;
  }
  return safeValue(limit, max, ref);
}

/**
 * Parse command line options and execute.
 */
ExitCode main(int argc, char *argv[]) {
  mainThreadId = pthread_self();
  initLogger();

  for (int i = 1; i < argc; i++) {
    // Run as filepath on non-flag argument
    if (*argv[i] != '-') {
      fryArgc = argc - i - 1;
      if (fryArgc > 0) {
        fryArgv = &argv[i + 1];
      }
      exitFry(runFile(argv[i]));
    }

    // Extract name to flag letter
    char flag = argv[i][1];
    if (flag == '-') {
      if (strcmp(&argv[i][2], "stack") == 0) {
        flag = 'k';
      } else {
        flag = argv[i][2];
      }
    }

    // Apply flag effect
    switch (flag) {
      case 'a':  // --ast
        isAstOnly = true;
        break;

      case 'o':  // --obfuscate
        obfuscate = true;
        FALL_THROUGH;
      case 'b':  // --bundle
        if (argc - i != 3) {
          fatalOut(__FILE__, __func__, "--bundle" GUIDE MSG_M_PATH);
          exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
        }
        exitFry(createBundle(argv[i + 1], argv[i + 2]));

#ifdef DEBUG
      case 't':  // --trace
        traceLogs = true;
        FALL_THROUGH;
#endif
      case 'd':  // --debug
        debugLogs = true;
        FALL_THROUGH;
      case 'c':  // --clock
        statsLogs = !statsLogs;
        break;

      case 'p':  // --probe
        isParseOnly = showHints = true;
        FALL_THROUGH;
      case 'e':  // --error
        crashOnError = !crashOnError;
        break;

      case 'f':  // --format
        if (argc - i < 2) {
          fatalOut(__FILE__, __func__, "--format" GUIDE MSG_M_PATH);
          exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
        }
        for (i++; i < argc; i++) {
          format(argv[i], !isParseOnly);
        }
        return EXIT_SUCCESS;

      case 'h':  // --help
        printHelp();
        return EXIT_SUCCESS;

      case 'i':  // --interactive
        interactive = true;
        break;

      case 'j':  // --jail
        isJailMode = true;
        break;

      case 'k':  // --stack
        if (argc - i < 2) {
          fatalOut(__FILE__, __func__, "--stack" GUIDE MSG_M_PAR);
          exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
        }
        stackDepth = parseNumericArg(argv[i + 1], MAX_DEPTH, "stack frames");
        i++;
        break;

      case 'm':  // --meta
        printMeta();
        return EXIT_SUCCESS;

      case 'n':  // --nodes
        if (argc - i < 2) {
          fatalOut(__FILE__, __func__, "--nodes" GUIDE MSG_M_PAR);
          exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
        }
        memoryLimit = parseNumericArg(argv[i + 1], MEM_MAX, "memory nodes");
        i++;
        break;

      case 's':  // --save
        saveSession = true;
        break;

      case 'v':  // --version
        printVersion(VerMinimal);
        return EXIT_SUCCESS;

      case 'w':  // --warranty
        printVersion(VerWarranty);
        return EXIT_SUCCESS;

      default:  // just ignore invalid arg
        logAlert(__FILE__, MSG_I_OP, argv[i]);
        break;
    }
  }

  exitFry(runRepl());

  return EXIT_FAILURE;
}
