/**
 * @file parser.h
 * @brief Parses tokenized reader contents into Fat AST
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.6.0
 * @date 2024-05-26
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2023, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#pragma once

#include "../sdk/sdk.h"

// parser options
extern bool isParseOnly;  // perform static analysis only (probe)
extern bool showHints;    // enable extra static analysis outputs

/**
 * @brief Moves Reader to next meaningful token (non-blank, non-comment).
 *
 * @param reader Reader* to fast forward
 * @return TokType (TkEOF is zero, other types are non-zero)
 */
TokType hasContent(Reader *reader);

/**
 * @brief Parse reader source recursively into an abstract syntax tree.
 *
 * @param reader Reader*
 * @return Node*
 */
Node *parse(Reader *reader);
