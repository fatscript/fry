/**
 * @file lexer.c
 * @brief Builds a token reader from input
 * (inspired by https://lisperator.net/pltut/)
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-16
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "lexer.h"

#include "patterns.h"

bool allowDecode = false;

static inline char currentChar(const Reader *reader) {
  return reader->source[reader->pos];
}

/**
 * Create fat token (except number)
 */
static inline Token *setValue(TokType type, char *val, char *src) {
  Token *tok = createToken(type, src);
  tok->val = val;
  return tok;
}

/**
 * Create fat number token (only)
 */
static inline Token *setNumber(double num, char *src) {
  Token *tok = createToken(TkNumb, src);
  tok->num = num;
  return tok;
}

/**
 * Read number token and update pos to end
 */
static Token *readNumber(Reader *reader, char *src) {
  char *text = reader->source + reader->pos;
  size_t len = 0;

  // Handle special HugeInt type
  if (isHugeHex(text)) {
    text += 2;
    advanceChar(reader);  // consume '0'
    advanceChar(reader);  // consume 'x'
    while (isHex(text[len])) {
      advanceChar(reader);
      len++;
    }
    if (len > (size_t)(HUGE_INT_SIZE * HUGE_FRAG)) {
      errorOut(__func__, MSG_BMO, src);
      return setValue(TkHuge, strDup("0"), src);
    }
    return setValue(TkHuge, copyChunk(text, len), src);
  }

  // Handle common Number type
  char buffer[NUMBER_MAX_LENGTH + 1];
  bool hasDot = false;
  bool isExpo = false;
  for (; isNumeric(text[0], &hasDot, &isExpo);
       text = reader->source + reader->pos) {
    if (text[0] == '.' && !isDigit(text[1])) {
      break;
    }
    if (len < NUMBER_MAX_LENGTH) {
      buffer[len++] = advanceChar(reader);  // copy
    } else {
      if (len == NUMBER_MAX_LENGTH) {  // error once
        errorOut(__func__, MSG_BMO, src);
      }
      advanceChar(reader);  // skip rest
      len++;
    }
  }
  buffer[len > NUMBER_MAX_LENGTH ? 0 : len] = '\0';
  errno = 0;
  double num = strtod(buffer, NULL);
  if (errno) {
    errorOut(__func__, MSG_I_N_ARG, src);
  }
  return setNumber(num, src);
}

/**
 * Parse single octal character to numeric value.
 */
static inline Byte ofOctal(const char n) { return n - '0'; }

/**
 * Parse octal octet string into byte (octal literals 000 to 377)
 */
static inline char parseOctet(const char *nnn) {
  return (char)(ofOctal(nnn[0]) * 64 + ofOctal(nnn[1]) * 8 + ofOctal(nnn[2]));
}

static inline bool isOctal(const char ch) { return '0' <= ch && ch <= '7'; }

/**
 * Parse single hexadecimal character to numeric value.
 */
static inline Byte ofHex(const char n) {
  if (n >= 'a' && n <= 'f') {
    return 10 + (n - 'a');
  }
  if (n >= 'A' && n <= 'F') {
    return 10 + (n - 'A');
  }
  return n - '0';
}

/**
 * Parse hexadecimal octet string into byte (hexadecimal literals 00 to FF)
 */
static inline char parseHex(const char *nn) {
  return (char)(ofHex(nn[0]) * 16 + ofHex(nn[1]));
}

/**
 * Reads a quoted text token, handling escape sequences such as
 * '\n', '\t', '\xFF' (hex), '\000' (octal).
 */
static Token *readText(Reader *reader, char *src, bool isRaw) {
  char *buffer = FRY_ALLOC(INIT_B_LEN);
  size_t capacity = INIT_B_LEN;
  size_t len = 0;
  bool isEscaped = false;
  bool success = false;
  bool isTemplate = false;
  const char delimiter = isRaw ? MARK_RAW : MARK_TEM;

  advanceChar(reader);  // skip opening delimiter

  while (currentChar(reader)) {
    char ch = advanceChar(reader);

    if (ch == '\n') {
      errorOut(__func__, "line break in text", src);
    }

    if (len == capacity) {
      capacity *= 2;
      buffer = FRY_REALLOC(buffer, capacity);
    }

    if (isEscaped) {
      switch (ch) {
        case 'b':
          buffer[len++] = '\b';  // backspace
          break;
        case 'f':
          buffer[len++] = '\f';  // form feed
          break;
        case 'n':
          buffer[len++] = '\n';  // new line (line feed)
          break;
        case 'r':
          buffer[len++] = '\r';  // carriage return
          break;
        case 't':
          buffer[len++] = '\t';  // tab
          break;
        case 'e':
          buffer[len++] = '\033';  // escape
          break;
        default:
          if ('0' <= ch && ch <= '3') {  // maybe octal octet
            char *byte = reader->source + reader->pos - 1;
            if (isOctal(byte[1]) && isOctal(byte[2])) {
              advanceChar(reader);  // consume 2nd char
              advanceChar(reader);  // consume 3rd char
              buffer[len++] = parseOctet(byte);
            } else {
              buffer[len++] = ch;
            }
          } else if (ch == 'x' || ch == 'X') {  // maybe hexadecimal byte
            char *byte = reader->source + reader->pos;
            if (isHex(byte[0]) && isHex(byte[1])) {
              advanceChar(reader);  // consume 2nd char
              advanceChar(reader);  // consume 3rd char
              buffer[len++] = parseHex(byte);
            } else {
              buffer[len++] = ch;
            }
          } else {
            buffer[len++] = ch;
          }
      }
      isEscaped = false;
    } else if (ch == MARK_ESC && (isRaw || currentChar(reader) != '{')) {
      isEscaped = true;
    } else if (ch == delimiter) {
      success = true;
      break;
    } else {
      if (ch == '{' && !isRaw) {
        isTemplate = true;
      }
      buffer[len++] = ch;
    }
  }
  buffer[len] = '\0';
  buffer = FRY_REALLOC(buffer, len + 1);
  if (!success) {
    errorOut(__func__, MSG_U_EOF, src);
  }

  TokType type = isRaw ? TkRaw : isTemplate ? TkTemp : TkText;
  Token *result = setValue(type, buffer, src);
  result->num = (double)len;  // see also: fromCurrent at parser.c
  return result;
}

/**
 * Read punctuation token (read new lines as a single new line)
 */
static inline Token *readPunctuation(Reader *reader, char *src) {
  return setValue(TkPunct, ofChar(advanceChar(reader)), src);
}

/**
 * Decodes an obfuscated source if checksum is valid.
 * The decoded portion replaces the obfuscated in place.
 */
static void decodeSource(Reader *reader) {
  char *source = reader->source;
  const size_t pos = reader->pos;
  const char *blob = source + pos;
  const size_t len = strlen(blob);

  auto_str decoded = xorDecode(bundleKey, blob);
  if (!decoded) {
    fatalOut(reader->name, __func__, MSG_F_DEC " (a)");
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }

  size_t validationLen = readWhile(&isOneLine, decoded);
  if (validationLen != SALT_SIZE) {
    fatalOut(reader->name, __func__, MSG_F_DEC " (b)");
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }

  decoded[validationLen] = '\0';
  if (!isB64Encoded(decoded)) {
    fatalOut(reader->name, __func__, MSG_F_DEC " (c)");
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }

  // Replace in-place (decoded is just 2/3 of encoded size)
  source[pos] = '\0';
  strncat(source + pos, decoded + validationLen + 1, len - 1);  // ignore warn
}

static Token *readNext(Reader *reader);

/**
 * Read comment token (and do magic for special directives inside comments)
 */
static Token *readComment(Reader *reader, char *src) {
  char *source = reader->source + reader->pos + 1;  // look-ahead (#)

  // Check special directives inside comments
  if (startsWith(source, "ver=")) {
    // Alert about fry verion used for creating the bundle if not same
    source += 4;
    const char *verEnd = strstr(source, "\n");
    auto_str ver = verEnd ? copyChunk(source, verEnd - source) : strDup("?");
    if (strcmp(ver, getFryV()) != 0) {
      logAlert(reader->name, "bundled with another fry version", ver);
    }
  } else if (startsWith(source, MARK_ENC) && allowDecode) {
    // Decode if obfuscated source and passed checksum
    advanceChar(reader);  // consume ~
    advanceChar(reader);  // consume !
    advanceChar(reader);  // consume @
    advanceChar(reader);  // consume $
    decodeSource(reader);
    free(src);
    return readNext(reader);
  }

  return setValue(TkComm, readPattern(&isOneLine, reader), src);
}

/**
 * Read operator token
 */
static Token *readOperator(Reader *reader, char *src) {
  char buffer[4] = {0};  // up to 3 chars + null-terminator
  buffer[0] = advanceChar(reader);

  if (isOperator(currentChar(reader))) {
    buffer[1] = currentChar(reader);

    // First, check if the first two chars form a valid 2-character operator
    if (getOpType(buffer)) {
      advanceChar(reader);  // consume 2nd character

      // Then check for a possible 3-character operator
      if (isOperator(currentChar(reader))) {
        buffer[2] = currentChar(reader);
        OpType op = getOpType(buffer);
        if (op) {               // check if it's a valid 3-character operator
          advanceChar(reader);  // consume 3rd character
          if (op == OpMissing) {
            return setValue(TkKey, strDup(buffer), src);
          }
        } else {
          buffer[2] = '\0';  // revert back to 2-character operator
        }
      }
    } else {
      buffer[1] = '\0';  // revert back to 1-character operator
    }
  }

  return setValue(TkOp, strDup(buffer), src);
}

/*
 * Reads anything else... not space, not comment, not text, not punct or op
 */
static Token *readIdentifier(Reader *reader, char *src) {
  char *head = reader->source + reader->pos;

  size_t len = 0;
  char buffer[ENTRY_NAME_MAX];
  while (*head && isIdentifier(*head)) {
    buffer[len] = advanceChar(reader);
    head++;
    len++;
    if (len == ENTRY_NAME_MAX) {
      auto_str nbytes = ofInt(ENTRY_NAME_MAX);
      auto_str msg = join4("name cannot exceed ", nbytes, " bytes" GUIDE, src);
      fatalOut(__FILE__, __func__, msg);
      exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
    }
  }
  buffer[len] = '\0';

  // Interpret UTF-8 void symbol 'ø' as ()
  if (strcmp(buffer, "ø") == 0) {
    buffer[0] = '(';
    buffer[1] = ')';
    buffer[2] = '\0';
    len = 2;
  }

  char *val = copyChunk(buffer, len);

  if (isAllUnder(val)) {
    return setValue(TkUnder, val, src);
  }

  if (isEmbedded(*val)) {
    return setValue(TkEmbed, val, src);
  }

  if (isTypename(val)) {
    return setValue(TkName, val, src);
  }

  if (isKeyword(val)) {
    return setValue(TkKey, val, src);
  }

  return setValue(TkId, val, src);
}

/**
 * Get human readable text position as 'file:line:column'
 */
static inline char *extractSrc(Reader *reader) {
  char *result = NULL;
  const char *path = reader->name;
  size_t line = reader->ln;
  size_t col = reader->col;
  if (asprintf(&result, "%s:%zu:%zu", path, line, col) == -1) {
    fatalOut(__FILE__, __func__, MSG_OOM);
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }
  return result;
}

/**
 * The core of the lexer/tokenizer
 */
static Token *readNext(Reader *reader) {
  skipPattern(&isSpace, reader);
  char *src = extractSrc(reader);

  if (!currentChar(reader)) {
    return createToken(TkEOF, src);
  }

  char ch = currentChar(reader);
  return isCommentStart(ch)  ? readComment(reader, src)
         : isTempStart(ch)   ? readText(reader, src, false)
         : isRawStart(ch)    ? readText(reader, src, true)
         : isDigit(ch)       ? readNumber(reader, src)
         : isPunctuation(ch) ? readPunctuation(reader, src)
         : isOperator(ch)    ? readOperator(reader, src)
                             : readIdentifier(reader, src);
}

/**
 * Basic syntax validation based on token sequence
 */
static inline void checkInvalidSyntax(const Token *prev, const Token *next) {
  auto_str err = NULL;

  // sequence of identical type
  if (prev->type != TkPunct && prev->type != TkOp &&
      (prev->type == next->type)) {
    err = join3("sequence of ", tokType(next->type), "s");
  }

  // number followed by...
  else if (prev->type == TkNumb || prev->type == TkHuge) {
    switch (next->type) {
      case TkKey:
      case TkId:
      case TkName:
      case TkText:
      case TkTemp:
      case TkRaw:
      case TkEmbed:
        err = join2("number followed by ", tokType(next->type));
        break;
      default:
        break;
    }
  }

  // text followed by...
  else if (prev->type == TkRaw || prev->type == TkTemp ||
           prev->type == TkText) {
    switch (next->type) {
      case TkKey:
      case TkId:
      case TkName:
      case TkNumb:
      case TkHuge:
      case TkText:
      case TkTemp:
      case TkRaw:
      case TkEmbed:
        err = join2("text followed by ", tokType(next->type));
        break;
      default:
        break;
    }
  }

  // identifier followed by...
  else if (prev->type == TkKey || prev->type == TkId || prev->type == TkName) {
    switch (next->type) {
      case TkKey:
      case TkId:
      case TkName:
      case TkNumb:
      case TkHuge:
      case TkText:
      case TkTemp:
      case TkRaw:
      case TkEmbed:
        err = join2("identifier followed by ", tokType(next->type));
        break;
      default:
        break;
    }
  }

  if (err) {
    errorOut(__func__, err, next->src);
  }
}

/**
 * Add the next available token from source to tokenized contents
 */
static inline Token *addNextFromSource(Reader *reader) {
  Token *tok = readNext(reader);
  if (reader->head) {
    checkInvalidSyntax(reader->current, tok);
    reader->current = reader->current->next = tok;
  } else {
    reader->current = reader->head = tok;
  }
  return tok;
}

Reader *tokenize(Reader *reader) {
  if (!reader->source) {
    fatalOut(__FILE__, __func__, "no input");
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }

  if (debugLogs) {
    printSourceInput(reader->source);
  }

  while (addNextFromSource(reader)->type != TkEOF) {
    continue;
  }

  reader->current = reader->head;  // reset for parsing
  return reader;
}

Reader *tokenizeFile(char *filepath) {
  Reader *reader = createReader(filepath);
  if (readFile(&reader->source, filepath, "r") == -1) {
    logError(filepath, __func__, strerror(errno));
  }
  return tokenize(reader);
}

/**
 * JSON syntax validation based on token type.
 */
static inline bool isValidJsonTok(const Token *tok) {
  switch (tok->type) {
    case TkEOF:
    case TkKey:   // true, false, null ('infinity' too, not JSON strict)
    case TkNumb:  // numbers
    case TkRaw:   // strings
      return true;

    case TkPunct:
      return *tok->val != '(' && *tok->val != ')' && *tok->val != ';';

    case TkOp:
      return *tok->val == '-';  // for negative numbers

    default:
      return false;
  }
}

/**
 * Returns true if the token can represent a JSON value.
 */
static inline bool isJsonValueToken(const Token *tok) {
  if (tok->type == TkOp && *tok->val == '-') {
    return true;  // we allow minus sign, as it can be followed by a number
  }
  if (tok->type == TkRaw || tok->type == TkNumb || tok->type == TkKey) {
    return true;  // strings, numbers, true, false, null
  }
  if (tok->type == TkPunct) {
    return *tok->val == '{' || *tok->val == '[';  //  objects/arrays
  }
  return false;
}

/**
 * Validates whether two JSON tokens can validly appear in sequence.
 */
static inline bool isValidJsonSeq(const Token *prev, const Token *next) {
  assert(prev != NULL);
  assert(next != NULL);

  // 1. If prev is a minus sign, next must be a number (e.g. "val": -123)
  if (prev->type == TkOp && *prev->val == '-') {
    return next->type == TkNumb;
  }

  // 2. If prev is a string that acts like a key, we expect a colon
  if (prev->type == TkRaw && isPunctTok(next, ':')) {
    return true;
  }

  if (prev->type == TkPunct) {
    switch (*prev->val) {
      // 3. If prev is a colon or comma, next must be a JSON value
      case ':':
      case ',':
        return isJsonValueToken(next);

      // 4. If prev is an opening object
      case '{':
        return isPunctTok(next, '}') || isJsonValueToken(next);

      // 5. If prev is an opening array
      case '[':
        return isPunctTok(next, ']') || isJsonValueToken(next);

      // 6. If prev is a closing brace/array ('}' or ']')
      case '}':
      case ']':
        if (next->type == TkEOF) {
          return true;
        }
        if (next->type == TkPunct) {
          return *next->val == ',' || *next->val == '}' || *next->val == ']';
        }
        return false;

      default:
        return false;
    }
  }

  // 7. If prev is a JSON value
  if (prev->type == TkRaw || prev->type == TkNumb || prev->type == TkKey) {
    if (next->type == TkEOF) {
      return true;
    }
    if (next->type == TkPunct) {
      return *next->val == ',' || *next->val == '}' || *next->val == ']';
    }
  }

  return false;
}

/**
 * Add the next available token from JSON to tokenized contents
 */
static inline Token *addNextFromJson(Reader *reader) {
  // Skip newline tokens; JSON does not require them
  // (and we don't want them to appear in the final token list)
  Token *tok = NULL;
  do {
    tok = readNext(reader);
  } while (isPunctTok(tok, '\n'));

  // Check for invalid token types
  if (!isValidJsonTok(tok)) {
    logAlert("parseJson", "invalid token", tok->src);
    return NULL;
  }

  // We disallow text interpolation in JSON mode, so we treat TkTemp
  // or TkText as raw strings (TkRaw) to avoid code injection.
  if (tok->type == TkTemp || tok->type == TkText) {
    tok->type = TkRaw;
  }

  if (reader->head) {
    if (!isValidJsonSeq(reader->current, tok)) {
      logAlert("parseJson", "invalid syntax", tok->src);
      return NULL;
    }
    reader->current = reader->current->next = tok;
  } else {
    reader->current = reader->head = tok;
  }
  return tok;
}

bool tokenizeJson(Reader *reader) {
  if (!reader->source) {
    return false;
  }

  Token *tok = NULL;
  do {
    tok = addNextFromJson(reader);
    if (!tok) {
      return false;
    }
  } while (tok->type != TkEOF);

  reader->current = reader->head;  // reset for parsing
  return reader;
}

// see: readNumber
char *unparseHugeInt(const HugeInt num, bool prefix) {
  char *buffer = FRY_ALLOC(HUGE_INT_SIZE * HUGE_FRAG + 2 + 1);  // +"0x"+'\0'

  char *current = buffer;
  if (prefix) {
    current += snprintf(current, 3, "0x");
  }

  bool hasPrintedFirstGroup = false;
  for (int i = HUGE_INT_SIZE - 1; i >= 0; i--) {
    if (hasPrintedFirstGroup) {
      current += snprintf(current, HUGE_FRAG + 1, "%08x", num[i]);
    } else if (num[i]) {
      current += snprintf(current, HUGE_FRAG + 1, "%x", num[i]);
      hasPrintedFirstGroup = true;
    } else {
      // skip leading zeroes
    }
  }

  // In case the number is 0
  if (!hasPrintedFirstGroup) {
    current += snprintf(current, 2, "0");
  }

  return FRY_REALLOC(buffer, current - buffer + 1);
}

// see: readText
char *unparseText(const char *text, bool isRaw) {
  char *buffer = FRY_ALLOC(INIT_B_LEN);

  size_t bufferSize = INIT_B_LEN;
  const char delimiter = isRaw ? MARK_RAW : MARK_TEM;

  buffer[0] = delimiter;  // add opening delimiter
  size_t pos = 1;

  for (size_t i = 0; text[i]; i++) {
    if (pos >= bufferSize - 6) {  // account for octal + delimiter + \0
      bufferSize *= 2;
      buffer = FRY_REALLOC(buffer, bufferSize);
    }

    if (text[i] == '\\' && (isRaw || text[i + 1] != '{')) {
      buffer[pos++] = '\\';
      buffer[pos++] = '\\';
    } else if (text[i] == delimiter) {
      buffer[pos++] = '\\';
      buffer[pos++] = delimiter;
    } else if (text[i] == '\b') {
      buffer[pos++] = '\\';
      buffer[pos++] = 'b';
    } else if (text[i] == '\f') {
      buffer[pos++] = '\\';
      buffer[pos++] = 'f';
    } else if (text[i] == '\n') {
      buffer[pos++] = '\\';
      buffer[pos++] = 'n';
    } else if (text[i] == '\r') {
      buffer[pos++] = '\\';
      buffer[pos++] = 'r';
    } else if (text[i] == '\t') {
      buffer[pos++] = '\\';
      buffer[pos++] = 't';
    } else if (text[i] == '\033') {
      buffer[pos++] = '\\';
      buffer[pos++] = 'e';
    } else if (iscntrl(text[i])) {
      pos += snprintf(&buffer[pos], bufferSize - pos, "\\%03o", (Byte)text[i]);
    } else {
      buffer[pos++] = text[i];
    }
  }
  buffer[pos++] = delimiter;
  buffer[pos++] = '\0';

  return FRY_REALLOC(buffer, pos);
}
