/**
 * @file parser.c
 * @brief Parses tokenized reader contents into FatScript AST
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-22
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "parser.h"

#include "../runtime/embed/commands.h"
#include "../sdk/memory.h"
#include "lexer.h"
#include "patterns.h"

bool isParseOnly = false;
bool showHints = false;

static void (*handleIssue)(const char *, const char *,
                           const char *) = &logAlert;

static Node *maybeCall(Node *expr, Reader *reader);
static Node *parseExpression(Reader *reader);
static Node *maybeBinary(Node *left, int myPrec, Reader *reader);
static Node *parseDelimited(Reader *reader, const char *end,
                            Node *(*parseFn)(Reader *));

static inline char *getSrc(Reader *reader) { return reader->current->src; }

static void discardNode(Node *node) {
  if (!node) {
    return;
  }

  atomic_fetch_sub(&activeMemory, 1);
  free(node->val);
  free(node->src);
  free(node);
}

static inline int getPrecedence(OpType op) {
  switch (op) {
    case OpAssign:
    case OpLoop:
    case OpTap:
      return 1;
    case OpIncrement:
    case OpDecrement:
    case OpMulBy:
    case OpPowBy:
    case OpDivBy:
    case OpModBy:
    case OpCoAssign:
    case OpProcedure:
    case OpLambda:
    case OpImport:
    case OpSwitch:
      return 2;
    case OpCase:
    case OpIf:
      return 3;
    case OpOr:
    case OpCoalesce:
      return 4;
    case OpAnd:
      return 5;
    case OpLess:
    case OpMore:
    case OpLessEq:
    case OpMoreEq:
    case OpEqual:
    case OpNotEq:
    case OpRange:
    case OpHORange:
      return 6;
    case OpPlus:
    case OpMinus:
      return 7;
    case OpMultiply:
    case OpSlash:
    case OpPercent:
      return 8;
    case OpPow:
      return 9;
    case OpDot:
    case OpIfDot:
      return 10;
    default:
      return 0;
  }
}

TokType hasContent(Reader *reader) {
  while (isPunctSep(reader->current) || isCommTok(reader->current)) {
    skipTok(__func__, reader);  // skip eol, commas and comments
  }
  return reader->current->type;
}

/**
 * Highlight potential security risks or privacy concerns in source.
 */
static void sensitiveCallDetected(const char *name, const char *src) {
  if (strcmp(name, "shell") == 0 || strcmp(name, "capture") == 0 ||
      strcmp(name, "fork") == 0 || strcmp(name, "kill") == 0 ||
      strcmp(name, "getEnv") == 0 || strcmp(name, "eval") == 0 ||
      strcmp(name, "write") == 0 || strcmp(name, "append") == 0 ||
      strcmp(name, "mkDir") == 0 || strcmp(name, "remove") == 0 ||
      strcmp(name, "unsafeCStr") == 0 || strcmp(name, "unsafePeek") == 0) {
    logHints(__func__, name, src);
  }
}

/**
 * Consumes current token converting into fat node.
 */
static Node *fromCurrent(Reader *reader, NodeType type) {
  Token *tok = reader->current;
  char *src = getSrc(reader);
  Node *node = createNode(type, src, NULL);

  switch (type) {
    case FatVoid:
      free(tok->val);  // free keyword string (null)
      node->ck = checkVoid;
      break;

    case FatEmbed:
      if (showHints) {
        sensitiveCallDetected(tok->val + 1, src);
      }
      node->op = getEbCmd(tok->val + 1);  // ignore '$' char
      if (node->op == EbUnknown) {
        handleIssue("parseEmbedded", MSG_U_EB_CMD, src);
      }
      free(tok->val);  // free keyword string (embedded)
      break;

    case FatUnary:
      node->op = getOpType(tok->val);
      free(tok->val);  // free operator string
      break;

    case FatBoolean:
      node->num.b = *tok->val == 't';
      free(tok->val);  // free keyword string (boolean)
      node->ck = checkBoolean;
      break;

    case FatHugeInt:
      node->num.h = FRY_ALLOC(sizeof(HugeInt));
#ifdef DEBUG
      if (!hugeSet(*node->num.h, tok->val)) {
        // Should not happen as lexer is responsible to provide valid string
        fatalOut(__FILE__, src, "failed to parse HugeInt");
        exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
      }
#else
      hugeSet(*node->num.h, tok->val);
#endif
      free(tok->val);  // clean up string (huge int)
      node->ck = checkHugeInt;
      break;

    case FatText:
    case FatTemp:
      node->num.s = tok->num < MAX_TO_INT ? (size_t)tok->num : strlen(tok->val);
      node->val = tok->val;
      node->ck = checkText;
      break;

    case FatEntry:
      // Lift 'self' and 'root' keywords into embedded commands
      if (strcmp(tok->val, "self") == 0 || strcmp(tok->val, "root") == 0) {
        node->op = getEbCmd(tok->val);
        node->type = FatEmbed;
        free(tok->val);  // free keyword string (embedded)
        break;
      }
      FALL_THROUGH;

    default:
      node->num.f = tok->num;
      node->val = tok->val;
  }

#ifdef DEBUG
  logTrace2(__FILE__, tokType(tok->type), src, fatType(node->type));
#endif

  advanceTok(__func__, reader);  // consume current token
  return node;
}

static Type *parseTypecheck(Reader *reader) {
  Token *tok = reader->current;

  if (!isPunctTok(tok, ':') || !tok->next || tok->next->type != TkName) {
    return NULL;  // type Any
  }

  char *types[TYPE_MAX + 1] = {0};
  int includes = 1;

  skipTok(__func__, reader);  // skip ':'
  types[0] = reader->current->val;
  reader->current->val = NULL;
  skipTok(__func__, reader);  // clean up name token

  tok = reader->current;
  while (isOpTok(tok, "/") && tok->next && tok->next->type == TkName) {
    skipTok(__func__, reader);  // skip '/'
    if (includes < TYPE_MAX) {
      types[includes++] = reader->current->val;
      reader->current->val = NULL;
    } else {
      logAlert(__func__, "type constrain ignored", getSrc(reader));
    }
    skipTok(__func__, reader);  // clean up name token
    tok = reader->current;
  }

  Type *result = NULL;
  if (includes > 1) {
    result = createComposite(NULL, types);
  } else {
    result = setType(types[0], NULL);
  }

  // Clean up consumed names
  freeEachStrInList(types);

  return result;
}

static inline Node *parseList(Reader *reader) {
  Node *node = createNode(FatList, strDup(getSrc(reader)), NULL);
  bool state = reader->isParen;
  reader->isParen = false;
  node->body = parseDelimited(reader, "]", &parseExpression);
  reader->isParen = state;
  node->ck = checkList;
  return node;
}

static inline Node *parseScope(Reader *reader) {
  Node *node = createNode(FatScope, strDup(getSrc(reader)), NULL);
  bool state = reader->isParen;
  reader->isParen = false;
  node->body = parseDelimited(reader, "}", &parseExpression);
  reader->isParen = state;
  node->ck = checkScope;
  return node;
}

static inline Node *parseParenthesis(Reader *reader) {
  Node *node = createNode(FatBlock, strDup(getSrc(reader)), NULL);
  bool state = reader->isParen;
  reader->isParen = true;
  node->body = parseDelimited(reader, ")", &parseExpression);
  reader->isParen = state;

  // Pick up method return type (if defined)
  Type *ck = parseTypecheck(reader);
  if (ck) {
    node = maybeBinary(node, 0, reader);
    if (IS_FAT_TYPE(node, FatMethod)) {
      node->ck = ck;
    } else {
      logAlert(__func__, "typecheck ignored", getSrc(reader));
    }
  }
  return node;
}

/**
 * Due to how the parsing is processed we may need to reorganize
 * the AST structure to be able to interpret it with less effort.
 * (like a tree optimization, pulls out FatExpr(dot)/FatDynamic)
 */
static inline Node *reverseDynamic(Node *call) {
  Node *node = call;
  if (node->tail) {
    while (node->tail->tail) {
      node = node->tail;
    }
    if (node->tail->type == FatExpr && node->tail->op == OpDot) {
      Node *tail = node->tail->head;
      node->tail->head = call;
      call = node->tail;
      node->tail = tail;
    }
  }
  return call;
}

static Node *parseCall(Node *node, Reader *reader, const char *end) {
#ifdef DEBUG
  logTrace2(__FILE__, __func__, getSrc(reader),
            node && node->val ? node->val : "(anonymous)");
#endif

  if (showHints && IS_FAT_TYPE(node, FatEntry)) {
    sensitiveCallDetected(node->val, node->src);
  }

  Node *call = createNode(FatCall, strDup(getSrc(reader)), NULL);
  call->head = node;
  bool state = reader->isCall;
  reader->isCall = true;
  call->body = parseDelimited(reader, end, &parseExpression);
  reader->isCall = state;
  call->tail = maybeCall(NULL, reader);

  if (*end == ']') {
    // Validate and lift into dynamic node, if applicable
    if (!call->body || call->body->seq) {
      handleIssue("parseDynamic", "invalid access", call->src);
      return call;
    }
    if (!call->tail) {
      call->type = FatExpr;
      call->op = OpDot;
      Node *dynWrapper = createNode(FatDynamic, strDup(call->body->src), NULL);
      dynWrapper->body = call->body;
      call->body = dynWrapper;
      return call;
    }
  }

  return reverseDynamic(call);
}

static bool isUnary(const Token *tok) {
  return isOpTok(tok, "!") || isOpTok(tok, "!!") || isOpTok(tok, "..") ||
         isOpTok(tok, "..<") || isOpTok(tok, "-") || isOpTok(tok, "~");
}

static void mutableDeclarationHasNoEffect(const Node *node) {
  auto_str val = toString(node);
  logAlert(__func__, val, node ? node->src : "(unkown)");
}

static Node *parseAtom(Reader *reader) {
  if (!hasContent(reader)) {
    return NULL;
  }

  Token *tok = reader->current;

#ifdef DEBUG
  const char *src = getSrc(reader);
#endif

  if (isUnary(tok)) {
    if (*tok->val == '~') {
      skipTok(__func__, reader);  // skip '~'
      Node *mutable = parseAtom(reader);
      if (IS_FAT_TYPE(mutable, FatEntry)) {
        mutable->op = true;  // enable mutability
      } else {
        mutableDeclarationHasNoEffect(mutable);
      }
      return mutable;
    }

    if (*tok->val == '-' && tok->next && tok->next->type == TkNumb) {
      skipTok(__func__, reader);  // skip '-'
      Node *negative = parseAtom(reader);
      negative->num.f = -negative->num.f;
      return negative;
    }

    // otherwise...
    Node *unary = fromCurrent(reader, FatUnary);
    unary->body = maybeCall(parseAtom(reader), reader);
    return unary;
  }

  // unary method
  if (isOpTok(tok, "->") || isOpTok(tok, "<>")) {
    return NULL;
  }

  if (isPunctTok(tok, '[') || isPunctTok(tok, '{') || isPunctTok(tok, '(')) {
#ifdef DEBUG
    logTrace2(__FILE__, __func__, src, "expression");
#endif

    const bool modeCase = reader->isCase;      // store case mode
    const bool modeIfElse = reader->isIfElse;  // store if-else mode
    const bool modeCall = reader->isCall;      // store call mode

    reader->isCase = false;    // disable case temporarily
    reader->isIfElse = false;  // disable if-else temporarily
    reader->isCall = false;    // disable call temporarily

    Node *expr = NULL;
    switch (tok->val[0]) {
      case '[':
        expr = parseList(reader);
        break;

      case '{':
        expr = parseScope(reader);
        break;

      default:  // '('
        expr = parseParenthesis(reader);
    }

    reader->isCase = modeCase;      // restore case mode
    reader->isIfElse = modeIfElse;  // restore if-else mode
    reader->isCall = modeCall;      // restore call mode

    return expr;
  }

  if (tok->type == TkId || tok->type == TkUnder) {
    Node *identifier = fromCurrent(reader, FatEntry);
    if (!reader->isIfElse) {
      identifier->ck = parseTypecheck(reader);
    }
    return identifier;
  }

  if (tok->type == TkName) {
    Node *type = fromCurrent(reader, FatType);
    type->ck = setType(type->val, NULL);
    return maybeCall(type, reader);
  }

  if (isKeyTok(tok, "???")) {
    return fromCurrent(reader, FatVoid);  // missing/unimplemented sign
  }

  if (isKeyTok(tok, "infinity")) {
    tok->num = INFINITY;
    tok->type = TkNumb;
  }
  if (tok->type == TkNumb) {
    Node *number = fromCurrent(reader, FatNumber);
    number->ck = checkNumber;
    return number;
  }

  if (tok->type == TkHuge) {
    return fromCurrent(reader, FatHugeInt);
  }

  if (tok->type == TkText || tok->type == TkTemp || tok->type == TkRaw) {
    NodeType type = tok->type == TkTemp ? FatTemp : FatText;
    Node *value = fromCurrent(reader, type);
    if (!reader->isIfElse && isPunctTok(reader->current, ':')) {
      tok = reader->current;

      if (tok->next && (tok->next->type != TkId && tok->next->type != TkName)) {
#ifdef DEBUG
        logTrace2(__FILE__, __func__, src, "type Entry (JSON-like)");
#endif
        advanceTok(__func__, reader);  // consume ':'
        free(tok->val);                // clean up unused data ':'
        Node *node = createNode(FatAssign, tok->src, NULL);
        node->op = OpAssign;
        value->type = FatEntry;  // use raw as entry name
        value->ck = NULL;
        node->head = value;
        node->body = parseExpression(reader);

        return node;
      }

      errorOut(__func__, "unexpected type", getSrc(reader));
    }

    return value;
  }

  if (isKeyTok(tok, "null")) {
    return fromCurrent(reader, FatVoid);
  }

  if (isKeyTok(tok, "true") || isKeyTok(tok, "false")) {
    return fromCurrent(reader, FatBoolean);
  }

  if (tok->type == TkEmbed) {
    return fromCurrent(reader, FatEmbed);
  }

  if (reader->isCase && isPunctTok(tok, '}')) {
    return NULL;
  }

  errorOut(__func__, "unexpected token", getSrc(reader));
  return fromCurrent(reader, FatInvalid);
}

static Node *maybeCall(Node *expr, Reader *reader) {
  Token *tok = reader->current;

  if (isPunctTok(tok, '{')) {
    handleIssue("parseCall", MSG_B_SYN, tok->src);
  }

  return isPunctTok(tok, '(')   ? parseCall(expr, reader, ")")
         : isPunctTok(tok, '[') ? parseCall(expr, reader, "]")
                                : expr;
}

static Node *parseExpression(Reader *reader) {
  return maybeBinary(maybeCall(parseAtom(reader), reader), 0, reader);
}

static Node *unwrap(Node *node) {
  if (IS_FAT_TYPE(node, FatScope)) {
    Node *aux = node->body;
    node->body = NULL;
    discardNode(node);
    return aux;
  }
  return node;
}

static Node *parseCase(Token *tok, Node *expr, Node *then, Reader *reader) {
  Node *node = createNode(FatCase, tok->src, NULL);
  node->head = expr;
  node->body = then;
  bool caseMode = reader->isCase;  // store case mode
  reader->isCase = true;           // enable case mode for this expression

  if (hasContent(reader)) {
    if (reader->current->type == TkUnder) {
      skipTok(__func__, reader);  // skip '_'
      if (getOpType(reader->current->val) == OpCase) {
        skipTok(__func__, reader);  // skip '=>'
        hasContent(reader);         // skip blank lines
        node->tail = parseExpression(reader);
      } else {
        errorOut(__func__, "outcome expected", getSrc(reader));
      }

    } else {
      node->tail = parseExpression(reader);
      if (node->tail && node->tail->type != FatCase) {
        errorOut(__func__, "catch-all expected", getSrc(reader));
        node->tail = NULL;
      }
    }
  }

  if (IS_FAT_TYPE(node->body, FatScope)) {
    node->body->type = FatBlock;
  }
  if (IS_FAT_TYPE(node->tail, FatScope)) {
    node->tail->type = FatBlock;
  }
  reader->isCase = caseMode;  // restore case mode
  return node;
}

/**
 * Check if "If" (?) has an "Else" (:) counterpart.
 */
static bool lookAheadElse(Token *tok, Reader *reader) {
  if (isPunctTok(tok, ':')) {
    return true;
  }

  if ((isPunctEol(tok) || isCommTok(tok)) && lookAheadElse(tok->next, reader)) {
    skipTok(__func__, reader);  // skip punct/comment
    return true;
  }

  return false;
}

static Node *parseIf(Token *tok, Node *expr, Node *then, Reader *reader) {
  Node *node = createNode(FatCase, tok->src, NULL);
  node->head = expr;
  node->body = unwrap(then);

  if (lookAheadElse(reader->current, reader)) {
    skipTok(__func__, reader);  // skip ':'
    if (hasContent(reader)) {
      node->tail = unwrap(parseExpression(reader));
    }
  }

  reader->isIfElse = false;  // unset if-else mode
  if (reader->isCase) {
    Node *block = createNode(FatBlock, strDup(tok->src), NULL);
    block->body = node;
    return block;  // prevent ambiguity on outcome of a switch case
  }
  return node;
}

static Node *parseLoop(Token *tok, Node *left, Node *right) {
  Node *node = createNode(FatLoop, tok->src, NULL);

  if (!left || !right) {
    handleIssue(__func__, MSG_B_SYN, tok->src);
    discardNode(node);
    discardNode(left);
    discardNode(right);
    return NULL;
  }

  node->body = unwrap(right);

  if (left->type == FatAssign) {
    node->head = left->body;
    left->body = node;
    return left;
  }

  node->head = left;
  return node;
}

/**
 * Rotate and rebalance the dot-expression AST
 * (move all dot operations to the head)
 */
static Node *rebalanceDotTree(Node *root) {
  while (IS_FAT_TYPE(root->body, FatExpr) && root->body->op == OpDot) {
    Node *newRoot = root->body;
    Node *newHead = root;
    newHead->body = newRoot->head;
    newRoot->head = rebalanceDotTree(newHead);
    root = newRoot;
  }
  return root;
}

static inline bool isPrimitiveName(const char *type) {
  return !strcmp(type, "Void") || !strcmp(type, "Boolean") ||
         !strcmp(type, "Number") || !strcmp(type, "HugeInt") ||
         !strcmp(type, "Text") || !strcmp(type, "Chunk") ||
         !strcmp(type, "Error");
}

static inline const char *getPrimitiveName(NodeType type) {
  switch (type) {
    case FatVoid:
      return "Void";
    case FatBoolean:
      return "Boolean";
    case FatNumber:
      return "Number";
    case FatHugeInt:
      return "HugeInt";
    case FatText:
    case FatTemp:
      return "Text";
    case FatChunk:
      return "Chunk";
    case FatError:
      return "Error";
    default:
      return NULL;
  }
}

static void questionableTypeCast(Node *castTo, Node *node) {
  const char *sourceType = getPrimitiveName(node->type);
  if (!sourceType) {
    return;
  }
  const char *targetType = castTo->val;
  if (isPrimitiveName(targetType) && !FAST_STR_EQ(sourceType, targetType)) {
    auto_str msg = join3(sourceType, " as ", targetType);
    logHints(__func__, msg, castTo->src);
  }
}

/**
 * Apply transparent parse-time AST optimization
 */
static inline Node *microInterpretExpr(Node *node) {
  if (!node->head || !node->body) {
    return node;
  }

  if (node->op == OpDot && node->body->type == FatExpr) {
    node = rebalanceDotTree(node);
  } else if (showHints && node->op == OpMultiply &&
             node->head->type == FatType) {
    questionableTypeCast(node->head, node->body);
  }

  if (node->head->type != node->body->type) {
    return node;
  }

  Node *a = node->head;
  Node *b = node->body;
  OpType op = node->op;

  switch (a->type) {
    case FatNumber:
      switch (op) {
        case OpPlus:
          a->num.f += b->num.f;
          break;
        case OpMinus:
          a->num.f -= b->num.f;
          break;
        case OpMultiply:
          a->num.f *= b->num.f;
          break;
        case OpPow:
          a->num.f = pow(a->num.f, b->num.f);
          break;
        case OpSlash:
          a->num.f /= b->num.f;
          break;
        case OpPercent:
          a->num.f = fmod(a->num.f, b->num.f);
          break;

        default:
          return node;
      }
      break;

    case FatList:
      if (op == OpPlus) {
        Node *last = a->body;
        if (last) {
          while (last->seq) {
            last = last->seq;
          }
          last->seq = b->body;
        } else {
          a->body = b->body;
        }
        b->body = NULL;
        break;
      }
      return node;

    case FatText:
      if (op == OpPlus) {
        const size_t sizeA = a->num.s;
        const size_t sizeB = b->num.s;
        const size_t totalSize = sizeA + sizeB;
        char *newText = FRY_REALLOC(a->val, totalSize + 1);
        memcpy(newText + sizeA, b->val, sizeB);
        newText[totalSize] = '\0';
        a->val = newText;
        a->num.s = totalSize;
        break;
      }
      return node;

    default:
      return node;
  }

  node->head = NULL;
  node->body = NULL;
  discardNode(node);
  discardNode(b);
  return a;
}

static void extractSwitchOrCases(Node *caseNode) {
  while (caseNode) {
    Node *exp = caseNode->head;

    if (IS_FAT_TYPE(exp, FatExpr) && exp->op == OpOr) {
      // Split the OpOr into left (a) and right (b) side
      Node *a = exp->head;
      Node *b = exp->body;

      // Repurpose the FatExpr as FatCase only with 'b' as condition
      // (also reuse the outcome part with same logic of 'a')
      exp->type = FatCase;
      exp->head = b;
      exp->body = caseNode->body;
      exp->tail = caseNode->tail;

      // Relink the head case condition to 'a' and alternative to 'b'
      caseNode->head = a;
      caseNode->tail = exp;

      // Recursively check the updated head case...
    } else {
      // Continue with the next case in the chain
      caseNode = caseNode->tail;
    }
  }
}

static Node *createBinary(Token *tok, OpType op, Node *left, Node *right,
                          Reader *reader) {
#ifdef DEBUG
  if (traceLogs) {
    auto_str msg = join4("'", tok->val, "'" GUIDE, opType(op));
    logTrace2(__FILE__, __func__, tok->src, msg);
  }
#endif

  NodeType type = op == OpAssign ? FatAssign : FatExpr;
  Node *node = createNode(type, tok->src, NULL);
  node->op = op;

  bool isChained = op == OpDot || op == OpIfDot;
  if (right) {
    if (isChained) {
      // Validate and lift into dynamic node, if applicable
      if (right->type == FatList && right->body && !right->body->seq) {
        right->type = FatDynamic;

        // If scope, lift into Scoped Block
      } else if (right->type == FatScope) {
        right->type = FatBlock;
      }

      // Apply other syntax validation logic for chained access
      switch (right->type) {
        case FatEntry:
        case FatType:
        case FatBlock:
        case FatDynamic:
        case FatCall:
          node->body = right;
          break;
        case FatExpr:
          if (right->op == OpDot) {
            node->body = right;
            break;
          }
          FALL_THROUGH;
        default:
          handleIssue("parseDot", "invalid chaining", tok->src);
          discardNode(right);
      }
    } else if (op == OpSwitch) {
      // Validated and reorganize
      if (right->type == FatScope && IS_FAT_TYPE(right->body, FatCase)) {
        node->type = FatSwitch;
        node->body = right->body;
        right->body = NULL;
        discardNode(right);
        extractSwitchOrCases(node->body);
      } else {
        handleIssue("parseSwitch", MSG_B_SYN, tok->src);
        node->body = right;
      }
    } else {
      node->body = right;
    }
  }

  if (left) {
    if (isChained && left->type == FatUnary) {
      // Apply unary transformation on chained expression (swap outwards)
      node->head = left->body;
      left->body = node;
      return left;
    }

    if (op == OpAssign && left->type == FatList) {
      // Validate and lift left-hand to dynamic node
      if (left->body && !left->body->seq) {
        left->type = FatDynamic;
        node->head = left;
      } else {
        handleIssue("parseAssign", MSG_IDR, tok->src);
        discardNode(left);
      }
      return node;
    }

    if (op == OpImport && reader->isMethod && left->type == FatEntry &&
        isAllUnder(left->val)) {
      logAlert("parseMethod", "local import inside body", tok->src);
    }

    node->head = left;
  }

  return node->type == FatExpr ? microInterpretExpr(node) : node;
}

static bool lookAheadOp(Token *tok, Reader *reader) {
  // Exclude look-ahead of for unary operators
  if (tok->type == TkOp) {
    if (!isUnary(tok)) {
      return true;
    }
    // ...except for ambiguous minus within parenthesis
    if (*tok->val == '-' && reader->isParen) {
      return true;
    }
    // ...and for range operators within calls/parenthesis
    if (*tok->val == '.' && (reader->isCall || reader->isParen)) {
      return true;
    }
    return false;
  }

  if ((isPunctEol(tok) || isCommTok(tok)) && lookAheadOp(tok->next, reader)) {
    skipTok(__func__, reader);  // skip punct/comment
    return true;
  }

  return false;
}

static inline const char *getArgName(const Node *arg) {
  return arg->type == FatAssign ? arg->head->val : arg->val;
}

static bool isArgUnique(Node *arg) {
  const char *name = getArgName(arg);
  for (Node *other = arg->seq; other; other = other->seq) {
    if (FAST_STR_EQ(name, getArgName(other))) {
      return false;
    }
  }
  return true;
}

static char *localEntries[LOCAL_STYLE_MAX];

static void declareLocalEntry(Node *entry) {
  // If key starts with MARK_UND, silence this warn
  if (*entry->val == MARK_UND) {
    return;
  }

  for (int i = 0; i < LOCAL_STYLE_MAX; i++) {
    if (localEntries[i] == NULL) {
      localEntries[i] = entry->val;
      return;
    }
    if (FAST_STR_EQ(entry->val, localEntries[i])) {
      return;
    }
  }
  logHints("parseMethod", "too many local entries", entry->src);
}

static bool isDeclaredEntry(char *name) {
  for (int i = 0; localEntries[i] && i < LOCAL_STYLE_MAX; i++) {
    if (FAST_STR_EQ(name, localEntries[i])) {
      localEntries[i] = name;
      return true;
    }
  }
  return false;
}

static bool isDeclaredArg(const char *name, Node *args) {
  for (Node *arg = args; arg; arg = arg->seq) {
    if (FAST_STR_EQ(name, getArgName(arg))) {
      return true;
    }
  }
  return false;
}

/**
 * Heuristic function to highlight implicit method parameters
 * (won't catch all, just most obviously strange ones)
 */
static void implicitParameter(Node *node, Node *args) {
  if (!node) {
    return;
  }

  switch (node->type) {
    case FatBlock:
      for (node = node->body; node; node = node->seq) {
        implicitParameter(node, args);
      }
      return;

    case FatLoop:
      // Could be using a mapper?
      if (IS_FAT_TYPE(node->body, FatEntry)) {
        implicitParameter(node->head, args);
        return;
      }
      FALL_THROUGH;
    case FatDynamic:
    case FatUnary:
      implicitParameter(node->head, args);
      implicitParameter(node->body, args);
      implicitParameter(node->tail, args);
      return;

    case FatCase:
      implicitParameter(node->head, args);
      for (Node *thenBlock = node->body; thenBlock;
           thenBlock = thenBlock->seq) {
        implicitParameter(thenBlock, args);
      }
      for (Node *elseBlock = node->tail; elseBlock;
           elseBlock = elseBlock->seq) {
        implicitParameter(elseBlock, args);
      }
      return;

    case FatMethod:
      if (node->val) {
        declareLocalEntry(node);
      }
      return;

    case FatAssign:
      if (IS_FAT_TYPE(node->head, FatEntry)) {
        declareLocalEntry(node->head);
      } else if (IS_FAT_TYPE(node->head, FatScope)) {
        // Resolve destructuring assignment
        for (Node *entry = node->head->body; entry; entry = entry->seq) {
          if (entry->type == FatEntry) {
            declareLocalEntry(entry);
          }
        }
      }
      implicitParameter(node->body, args);
      implicitParameter(node->tail, args);
      return;

    case FatCall:
      if (IS_FAT_TYPE(node->head, FatType) &&
          strncmp(node->head->val, "Using", 5) == 0 &&
          IS_FAT_TYPE(node->body, FatText)) {
        declareLocalEntry(node->body);
      } else {
        // Check call site arguments
        for (Node *aux = node->body; aux; aux = aux->seq) {
          implicitParameter(aux, args);
        }
        for (Node *aux = node->tail; aux; aux = aux->seq) {
          implicitParameter(aux, args);
        }
      }
      return;

    case FatExpr:
      if (node->op == OpDot || node->op == OpIfDot) {
        if (IS_FAT_TYPE(node->body, FatCall)) {
          implicitParameter(node->body, args);
        }
      } else if (node->op != OpImport) {
        implicitParameter(node->head, args);
        implicitParameter(node->body, args);
        implicitParameter(node->tail, args);
      }
      return;

    case FatEntry:
      if (!isDeclaredArg(node->val, args)) {
        if (*node->val == MARK_UND) {
          // If key starts with MARK_UND, silence this warn
        } else if (!isDeclaredEntry(node->val)) {
          logHints(__func__, node->val, node->src);
        }
      }
      return;

    default:
      return;
  }
}

static char *unusedEntrySrc = NULL;

static int countUsages(Node *node, const char *name) {
  if (!node) {
    return 0;
  }

  int acc = 0;
  if (((node->type == FatEntry || node->type == FatMethod) &&
       strEq(node->val, name)) ||
      (node->type == FatTemp && strstr(node->val, name))) {
    unusedEntrySrc = node->src;
    acc = 1;
  }
  acc += countUsages(node->head, name);
  acc += countUsages(node->body, name);
  acc += countUsages(node->tail, name);
  acc += countUsages(node->seq, name);
  return acc;
}

static void possiblyUnusedParameter(Node *arg, Node *methodBody) {
  while (arg) {
    const char *name = getArgName(arg);
    if (*name != MARK_UND && !countUsages(methodBody, name)) {
      logHints(__func__, name, arg->src);
    }
    arg = arg->seq;
  }
}

static void unusedDeclaration(Node *methodBody) {
  for (int i = 0; localEntries[i] && i < LOCAL_STYLE_MAX; i++) {
    if (countUsages(methodBody, localEntries[i]) == 1) {
      logHints(__func__, localEntries[i], unusedEntrySrc);
    }
  }
}

static Node *parseProcedure(char *src, Node *left, Node *right) {
  Node *procedure = createNode(FatProcedure, src, NULL);
  if (IS_FAT_TYPE(right, FatScope)) {
    right->type = FatBlock;
  }
  procedure->body = right;

  if (left) {
    if (left->type != FatType) {
      errorOut(__func__, MSG_B_SYN, src);
      discardNode(left);
      return procedure;
    }
    procedure->ck = left->ck;
    discardNode(left);
  }

  // Perform code style static analysis on probe mode
  if (showHints) {
    memset(localEntries, 0, sizeof(char *) * LOCAL_STYLE_MAX);
    implicitParameter(procedure->body, procedure->head);
    unusedDeclaration(procedure->body);
  }

  return procedure;
}

static Node *parseMethod(char *src, Node *left, Node *right) {
  Node *method = createNode(FatMethod, src, NULL);
  if (IS_FAT_TYPE(left, FatBlock)) {
    method->head = left->body;
    method->ck = left->ck;
    left->body = NULL;
    discardNode(left);
  } else {
    method->head = left;
  }
  method->body = right;
  if (IS_FAT_TYPE(right, FatScope)) {
    right->type = FatBlock;
  }

  // Validate method parameters
  int parameterCount = 0;
  for (Node *slot = method->head; slot; slot = slot->seq) {
    bool isValidArgType = true;
    switch (slot->type) {
      case FatEntry:
        if (slot->op) {
          isValidArgType = false;
          break;
        }
        break;
      case FatAssign:
        if (IS_FAT_TYPE(slot->head, FatEntry) && !slot->head->op &&
            slot->body) {
          switch (slot->body->type) {
            case FatVoid:
            case FatBoolean:
            case FatNumber:
            case FatHugeInt:
            case FatText:
            case FatEntry:
            case FatList:
            case FatScope:
              break;
            default:
              isValidArgType = false;
          }
          break;
        }
        FALL_THROUGH;
      default:
        isValidArgType = false;
    }

    if (!isValidArgType || !isArgUnique(slot)) {
      errorOut(__func__, MSG_I_PAR, src);
      method->head = NULL;
      method->body = NULL;
      break;
    }
    parameterCount++;
  }

  // Perform code style static analysis on probe mode
  if (showHints) {
    if (parameterCount > ARG_STYLE_MAX) {
      logHints(__func__, "signature complexity", src);
    }
    memset(localEntries, 0, sizeof(char *) * LOCAL_STYLE_MAX);
    implicitParameter(method->body, method->head);
    possiblyUnusedParameter(method->head, method->body);
    unusedDeclaration(method->body);
  }

  return method;
}

static void tagTailRecursiveCall(Node *ast, const char *name) {
  if (!ast) {
    return;
  }

  // Unwrap the block of code
  if (ast->type == FatBlock) {
    ast = ast->body;
    if (!ast) {
      return;
    }
  }

  // Move to the last statement
  while (ast->seq) {
    ast = ast->seq;
  }

  // Handle branching recursively
  if (ast->type == FatCase) {
    tagTailRecursiveCall(ast->body, name);  // then branch
    tagTailRecursiveCall(ast->tail, name);  // else branch
    return;
  }
  if (ast->type == FatSwitch) {
    tagTailRecursiveCall(ast->body, name);
    return;
  }

  if (ast->type != FatCall) {
    return;  // body/branch does not end with call
  }

  if (ast->tail) {
    return;  // cannot optimize nested calls x(a)(b)(c)..
  }

  if (!IS_FAT_TYPE(ast->head, FatEntry)) {
    return;  // cannot optimize non-named calls (-> x..)(a)
  }

  if (!FAST_STR_EQ(ast->head->val, name)) {
    return;  // cannot optimize non-recursive calls
  }

  if (showHints) {
    logHints(__func__, name, ast->src);
  }
  ast->op = true;  // tag for TRO
}

static inline bool isImport(const Node *node) {
  return IS_FAT_TYPE(node, FatExpr) && node->op == OpImport;
}

/**
 * Parse expressions recursively into abstract syntax tree
 */
static Node *maybeBinary(Node *left, int myPrec, Reader *reader) {
  Token *tok = reader->current;

  if (tok->type != TkOp && !lookAheadOp(tok, reader)) {
    return left;
  }

  tok = reader->current;  // might have moved forward due to lookAheadOp

  OpType op = getOpType(tok->val);
  int hisPrec = getPrecedence(op);
  if (hisPrec <= myPrec) {
    return left;
  }

  advanceTok(__func__, reader);  // consume operator

  // Parse with different modes according to operator
  Node *right = NULL;
  Node *next = NULL;
  bool methodMode = reader->isMethod;
  switch (op) {
    case OpProcedure:
    case OpLambda:
    case OpCase:
    case OpIf:
      reader->isMethod =
        op == OpProcedure || op == OpLambda;  // set method mode
      reader->isIfElse = op == OpIf;          // set if-else mode
      if (hasContent(reader)) {
        right = parseExpression(reader);  // skip blank lines
        reader->isMethod = methodMode;
      } else {
        errorOut("parseExpression", MSG_U_EOF, getSrc(reader));
        return NULL;
      }
      break;

    case OpRange:
      // catch something like myText(3..), which is valid range syntax
      if (hasContent(reader) && isPunctTok(reader->current, ')')) {
        break;
      }
      FALL_THROUGH;

    default:
      next = parseAtom(reader);
      right = maybeBinary(maybeCall(next, reader), hisPrec, reader);
  }

  // Prevent invalid import syntax (nested into expression)
  if (isImport(left)) {
    handleIssue("parseImport", MSG_B_SYN, tok->src);
    discardNode(left);
    return right;
  }
  if (isImport(right)) {
    handleIssue("parseImport", MSG_B_SYN, tok->src);
    discardNode(right);
    return left;
  }

  // Apply main binary construction logic
  Node *binary = NULL;
  switch (op) {
    case OpProcedure:
      binary = parseProcedure(tok->src, left, right);
      break;

    case OpLambda:
      binary = parseMethod(tok->src, left, right);
      break;

    case OpCase:
      binary = parseCase(tok, left, right, reader);
      break;

    case OpIf:
      binary = parseIf(tok, left, right, reader);
      break;

    case OpLoop:
      binary = parseLoop(tok, left, right);
      break;

    case OpAssign:
      if (IS_FAT_TYPE(left, FatEntry) &&
          (IS_FAT_TYPE(right, FatMethod) || IS_FAT_TYPE(right, FatProcedure))) {
        if (!checkAlias(left->ck, right)) {
          handleIssue(
            right->type == FatMethod ? "parseMethod" : "parseProcedure",
            MSG_MISMATCH " on declaration", tok->src);
        }
        right->val = left->val;  // copy name as method method
        right->op = left->op;    // copy mutable flag
        left->val = NULL;
        binary = right;
#ifdef DEBUG
        logTrace2(__FILE__, "parseMethod", right->src, right->val);
#endif

        // Discard left "Entry" method name node and clean up token source
        discardNode(left);
        free(tok->src);

        // If method takes arguments explicitly, check for TRO opportunities
        if (binary->head) {
          tagTailRecursiveCall(binary->body, binary->val);
        } else if (showHints && binary->type == FatMethod) {
          logHints("possiblyUseProcedure", binary->val, binary->src);
        }
        break;

      } else if (IS_FAT_TYPE(left, FatType) && right &&
                 (right->type == FatBlock || right->type == FatScope)) {
        // Accept types declared with curly brackets (unwrap)
        binary = createBinary(tok, op, left, right->body, reader);

        // Discard curly brackets "Scope" wrapper node
        discardNode(right);
        break;
      }
      FALL_THROUGH;
    default:
      binary = createBinary(tok, op, left, right, reader);
  }

  free(tok->val);  // clean up unused data (operator)
  return maybeBinary(binary, myPrec, reader);
}

/**
 * Parse until end marker is found with custom parse function
 */
static Node *parseDelimited(Reader *reader, const char *end,
                            Node *(*const parseFn)(Reader *)) {
#ifdef DEBUG
  if (debugLogs) {
    logTrace2(__FILE__, __func__, getSrc(reader), join2("until ", end));
  }
#endif

  Node *node = NULL;
  Node *block = NULL;
  skipTok(__func__, reader);  // skip start delimiter

  while (reader->current->type != TkEOF) {
    if (hasContent(reader) && isPunctTok(reader->current, *end)) {
      break;
    }

    if (node) {
      node->seq = parseFn(reader);
      node = node->seq;
    } else {
      node = block = parseFn(reader);
    }
  }

  // Handle missing end delimiter
  if (reader->current->type == TkEOF) {
    const char *src = block ? block->src : "(unknown)";
    errorOut(__func__, getSrc(reader), join4("no '", end, "' after ", src));
    return NULL;
  }

  skipTok(__func__, reader);  // skip end delimiter
  return block;
}

Node *parse(Reader *reader) {
  if (!reader->head) {
    tokenize(reader);
  }

#ifdef DEBUG
  if (traceLogs) {
    logMarker("PARSE");
    auto_str token = ofToken(reader->current);
    logTrace2(__func__, "start", getSrc(reader), token);
  }
#endif

  // Some issues are treated in stricter way for bundling.
  // Display as error when probing, anticipating failures;
  // log as warning during runtime (likely/imminent error).
  handleIssue = isParseOnly ? &errorOut : &logAlert;

  Node *program = NULL;
  Node *prev = NULL;

  for (int block = 1; hasContent(reader); block++) {
#ifdef DEBUG
    if (traceLogs) {
      logBlock(block);
    }
#endif

    Node *parsed = parseExpression(reader);
    if (!parsed) {
      continue;  // some parsing errors could return NULL
    }

    if (prev) {
      prev->seq = parsed;
    } else {
      program = parsed;
    }
    prev = parsed;
  }

  skipTok(__func__, reader);  // skip eof token

#ifdef DEBUG
  lockResource(&memoryLock);
  if (program) {  // 'fix' valgrind leak-check false positives
    program->programs = parsedPrograms;
    parsedPrograms = program;
  }
  unlockResource(&memoryLock);
#endif

  return program;
}
