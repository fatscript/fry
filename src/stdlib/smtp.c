/**
 * @file smtp.c
 * @brief SMTP handling framework (libcurl wrapper)
 * @author Antonio Prates <hello@aprates.dev>
 * @version 3.2.0
 * @date 2024-08-22
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../runtime/interpreter.h"

static const char* LIB_SMTP =
    "# fat.smtp - SMTP handling framework\n"
    "\n"
    "ContactInfo = (\n"
    "  email: Text\n"
    "  name: Text = ''  # optional\n"
    ")\n"
    "\n"
    "## Configure the SMTP parameters (raises error on failure)\n"
    "config = (\n"
    "  from: ContactInfo\n"
    "  server: Text\n"
    "  username: Text\n"
    "  password: Text\n"
    "  useSSL: Boolean = true\n"
    "): Void -> $smtpConfig\n"
    "\n"
    "## Send an email (raises error on failure)\n"
    "send = (\n"
    "  to: List/ContactInfo\n"
    "  subject: Text\n"
    "  body: Text\n"
    "): Text -> $smtpSend  # returns message UUID on success\n";

#ifndef __EMSCRIPTEN__

static __thread char* smtpFrom = NULL;         // email only
static __thread char* smtpContactInfo = NULL;  // name and email
static __thread char* smtpServer = NULL;
static __thread char* smtpUsername = NULL;
static __thread char* smtpPassword = NULL;
static __thread bool smtpUseSSL = true;

struct UploadStatus {
  char** payload;
  int linesRead;
};

static size_t payloadSource(void* ptr, size_t size, size_t nmemb, void* userp) {
  struct UploadStatus* uploadCtx = (struct UploadStatus*)userp;

  // Calculate the buffer size available
  size_t bufferSize = size * nmemb;  // (usually 16kb ~ 64kb)

  if (!bufferSize) {
    return 0;
  }

  const char* data = uploadCtx->payload[uploadCtx->linesRead];

  if (data) {
    size_t len = strlen(data);
    if (len > bufferSize) {
      logAlert(__FILE__, __func__, "message is too long");
      len = bufferSize;  // ensure we do not write beyond the available space
    }
    memcpy(ptr, data, len);
    uploadCtx->linesRead++;
    return len;
  }

  return 0;
}

/**
 * Get the contact as string in the form "[name] <[email]>"
 */
static char* getContactInfo(Scope* contact, bool includeName, Context* ctx) {
  Node* email = getParameter(contact, "email", FatText, ctx);
  if (IS_FAT_ERROR(email)) {
    return NULL;
  }

  auto_str normalizedEmail = strTrim(email->val, email->num.s);
  if (!*normalizedEmail) {
    createError("blank email", true, ckValueError, ctx);
    return NULL;  // email can't be blank
  }

  if (!includeName) {
    return join3("<", normalizedEmail, ">");
  }

  Node* name = getParameter(contact, "name", FatText, ctx);
  if (IS_FAT_ERROR(name)) {
    return NULL;
  }

  auto_str normalizedName = strTrim(name->val, name->num.s);
  return *normalizedName ? join4(normalizedName, " <", normalizedEmail, ">")
                         : join3("<", normalizedEmail, ">");
}

/**
 * Get recipients as a comma separated lists of contacts via getContactInfo
 */
static char* joinRecipients(Scope* list, Context* ctx) {
  if (!list || !list->entries) {
    return NULL;
  }

  char** recipients = FRY_CALLOC((list->size + 1) * sizeof(char*));
  long i = 0;
  for (Entry* entry = list->entries; entry; entry = entry->next) {
    if (IS_FAT_TYPE(entry->data, FatScope) && entry->data->scp) {
      char* contact = getContactInfo(entry->data->scp, true, ctx);
      if (!contact) {
        freeStrList(recipients);
        return NULL;  // bad contact, fail operation
      }
      recipients[i++] = contact;
    }
  }

  char* result = joinSep(recipients, ", ");
  freeStrList(recipients);
  return result;
}

/**
 * Configure the SMTP parameters (raises error on failure)
 */
static Node* smtpConfig(Scope* scope, Context* ctx) {
  Node* from = getParameter(scope, "from", FatScope, ctx);
  if (IS_FAT_ERROR(from)) {
    return from;
  }

  Node* server = getParameter(scope, "server", FatText, ctx);
  if (IS_FAT_ERROR(server)) {
    return server;
  }

  Node* username = getParameter(scope, "username", FatText, ctx);
  if (IS_FAT_ERROR(username)) {
    return username;
  }

  Node* password = getParameter(scope, "password", FatText, ctx);
  if (IS_FAT_ERROR(password)) {
    return password;
  }

  Node* useSSL = getParameter(scope, "useSSL", FatBoolean, ctx);
  if (IS_FAT_ERROR(useSSL)) {
    return useSSL;
  }

  free(smtpFrom);
  free(smtpContactInfo);
  free(smtpServer);
  free(smtpUsername);
  free(smtpPassword);

  smtpFrom = getContactInfo(from->scp, false, ctx);
  smtpContactInfo = getContactInfo(from->scp, true, ctx);
  smtpServer = copyChunk(server->val, server->num.s);
  smtpUsername = copyChunk(username->val, username->num.s);
  smtpPassword = copyChunk(password->val, password->num.s);
  smtpUseSSL = useSSL->num.b;
  return NULL;
}

/**
 * Send an email (raises error on failure)
 */
static Node* smtpSend(Scope* scope, Context* ctx) {
  if (isJailMode) {
    return createError("send " MSG_JAIL, true, ckCallError, ctx);
  }

  char* domain = smtpFrom ? strchr(smtpFrom, '@') : NULL;
  if (!domain) {
    return createError("bad SMTP config", true, ckValueError, ctx);
  }

  Node* to = getParameter(scope, "to", FatList, ctx);
  if (IS_FAT_ERROR(to)) {
    return to;
  }

  Node* subject = getParameter(scope, "subject", FatText, ctx);
  if (IS_FAT_ERROR(subject)) {
    return subject;
  }

  Node* body = getParameter(scope, "body", FatText, ctx);
  if (IS_FAT_ERROR(body)) {
    return body;
  }

  // Build from field
  auto_str fromField = join3("From: ", smtpContactInfo, "\r\n");

  // Build to field
  auto_str strRecipients = joinRecipients(to->scp, ctx);
  if (!strRecipients) {
    return ctx->failureEvent
               ? ctx->failureEvent
               : createError("bad 'to' field", true, ckValueError, ctx);
  }
  auto_str toField = join3("To: ", strRecipients, "\r\n");

  // Build subject field
  auto_str subjectField = join3("Subject: ", subject->val, "\r\n");

  // Build date field
  char date[128] = {0};
  time_t now = time(NULL);
  struct tm* tmInfo = localtime(&now);
  strftime(date, sizeof(date), "Date: %a, %d %b %Y %H:%M:%S %z\r\n", tmInfo);

  // Build message-id field
  auto_str uuid = generateUuid();
  auto_str messageId = join4("Message-ID: <", uuid, domain, "\r\n");

  // Build content type field (hardcoded for now)
  char* typeField = "Content-Type: text/plain; charset=UTF-8\r\n";

  // Build the message payload
  char* payload[] = {fromField, toField, subjectField, date, typeField,
                     messageId, "\r\n",  body->val,    NULL};

  struct UploadStatus uploadCtx = {.payload = payload, .linesRead = 0};

  CURL* session = curl_easy_init();
  if (session) {
    curl_easy_setopt(session, CURLOPT_URL, smtpServer);
    curl_easy_setopt(session, CURLOPT_MAIL_FROM, smtpFrom);

    struct curl_slist* recipients = NULL;
    for (Entry* entry = to->scp->entries; entry; entry = entry->next) {
      if (IS_FAT_TYPE(entry->data, FatScope) && entry->data->scp) {
        recipients = curl_slist_append(
            recipients, getContactInfo(entry->data->scp, false, ctx));
      }
    }
    curl_easy_setopt(session, CURLOPT_MAIL_RCPT, recipients);

    curl_easy_setopt(session, CURLOPT_READFUNCTION, payloadSource);
    curl_easy_setopt(session, CURLOPT_READDATA, &uploadCtx);
    curl_easy_setopt(session, CURLOPT_UPLOAD, 1L);

    // Set username and password for SMTP authentication
    curl_easy_setopt(session, CURLOPT_USERNAME, smtpUsername);
    curl_easy_setopt(session, CURLOPT_PASSWORD, smtpPassword);

    if (smtpUseSSL) {
      curl_easy_setopt(session, CURLOPT_USE_SSL, (long)CURLUSESSL_ALL);
    }

    if (debugLogs) {
      curl_easy_setopt(session, CURLOPT_VERBOSE, 1L);
    }

    CURLcode curlCode = curl_easy_perform(session);
    Node* error = NULL;
    if (curlCode != CURLE_OK) {
      error = createError((char*)curl_easy_strerror(curlCode), true, NULL, ctx);
    }

    // Cleanup
    curl_slist_free_all(recipients);
    curl_easy_cleanup(session);

    return error ? error : runtimeTextDup(uuid, ctx);
  }

  return createError(MSG_NO_SESS, true, NULL, ctx);
}

#endif
