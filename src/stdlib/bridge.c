/**
 * @file bridge.c
 * @author Antonio Prates <hello@aprates.dev>
 * @brief Bridge between FatScript and external C libraries
 * @version 4.1.0
 * @date 2025-01-26
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../runtime/interpreter.h"

static const char *LIB_BRIDGE =
  "# fat.bridge Bridge between FatScript and external C libraries\n"
  "\n"
  "CPointer = Chunk\n"
  "\n"
  "CType = Number\n"
  "\n"
  "ctype = {\n"
  "  sint     = CType *  0  # int             (FatNumber compatible)\n"
  "  sintP    = CType *  1  # int*            (FatNumber compatible)\n"
  "  uint     = CType *  2  # unsigned int    (FatNumber compatible)\n"
  "  uintP    = CType *  3  # unsigned int*   (FatNumber compatible)\n"
  "  float    = CType *  4  # float           (FatNumber compatible)\n"
  "  floatP   = CType *  5  # float*          (FatNumber compatible)\n"
  "  double   = CType *  6  # double          (FatNumber compatible)\n"
  "  doubleP  = CType *  7  # double*         (FatNumber compatible)\n"
  "  schar    = CType *  8  # char            (FatChunk  compatible)\n"
  "  scharP   = CType *  9  # char*           (FatChunk  compatible)\n"
  "  uchar    = CType * 10  # unsigned char   (FatChunk  compatible)\n"
  "  ucharP   = CType * 11  # unsigned char*  (FatChunk  compatible)\n"
  "  sshort   = CType * 12  # short           (FatNumber compatible)\n"
  "  sshortP  = CType * 13  # short*          (FatNumber compatible)\n"
  "  ushort   = CType * 14  # unsigned short  (FatNumber compatible)\n"
  "  ushortP  = CType * 15  # unsigned short* (FatNumber compatible)\n"
  "  slong    = CType * 16  # long            (FatNumber compatible)\n"
  "  slongP   = CType * 17  # long*           (FatNumber compatible)\n"
  "  ulong    = CType * 18  # unsigned long   (FatNumber compatible)\n"
  "  ulongP   = CType * 19  # unsigned long*  (FatNumber compatible)\n"
  "  string   = CType * 20  # dyn-alloc. null-term. char*  (FatText)\n"
  "  void     = CType * 21  # void            (FatVoid   compatible)\n"
  "  voidP    = CType * 22  # void*           (FatChunk  compatible)\n"
  "}\n"
  "\n"
  "## Dynamic Link Library handle\n"
  "DLL = (\n"
  "  ## Argument slot\n"
  "  filename: Text  # the shared object to load\n"
  "\n"
  "  apply = <> $loadDLL\n"
  ")\n"
  "\n"
  "## Foreign Function Interface binding\n"
  "FFI = (\n"
  "  ## Argument slot\n"
  "  lib: DLL\n"
  "  name: Text  # function in the DLL to bind\n"
  "  in: List/CType\n"
  "  out: CType"
  "\n"
  "  call = (\n"
  "    _0 = null\n"
  "    _1 = null\n"
  "    _2 = null\n"
  "    _3 = null\n"
  "    _4 = null\n"
  "    _5 = null\n"
  "    _6 = null\n"
  "    _7 = null\n"
  "    _8 = null\n"
  "    _9 = null\n"
  "  ): Any -> $callFFI\n"
  ")\n"
  "\n"
  "## Read ptr as null-terminated C-string\n"
  "unsafeCStr = (ptr: CPointer): Text -> $unsafeCStr\n"
  "\n"
  "## Read ptr considering offset and length\n"
  "unsafePeek = (ptr: CPointer, offset: Number, len: Number): Chunk ->"
  " $unsafePeek\n"
  "\n"
  "## Release ownership of memory pointed by node\n"
  "detachNode = (node: Any): Void -> $detachNode\n"
  "\n"
  "## Marshal a FatScript value to a CType\n"
  "marshal = (val: Any, type: CType): Chunk -> $marshal\n"
  "\n"
  "## Unmarshal from a CType back to FatScript\n"
  "unmarshal = (raw: Chunk, type: CType): Any -> $unmarshal\n"
  "\n"
  "## Get the errno for the last FFI call\n"
  "getErrno = Number <> $getErrno\n"
  "\n"
  "## Get the number of bytes for CType\n"
  "sizeOf = (type: CType): Number -> $sizeOf\n";

#ifdef FFI_SUPPORT

static __thread int lastCallErrno = 0;

// note: if updated, args must be reflected in FFI.call and paramKeys
#define MAX_FFI_ARGS 10

// Array of argument keys
static const char *paramKeys[MAX_FFI_ARGS] = {"_0", "_1", "_2", "_3", "_4",
                                              "_5", "_6", "_7", "_8", "_9"};

// This CTypeId enum needs to be kept in sync with typeMappings
// so that val2ffi can correctly map FatScript values for C
typedef enum {
  CTypeSInt,     // 0
  CTypeSIntP,    // 1
  CTypeUInt,     // 2
  CTypeUIntP,    // 3
  CTypeFloat,    // 4
  CTypeFloatP,   // 5
  CTypeDouble,   // 6
  CTypeDoubleP,  // 7
  CTypeSChar,    // 8
  CTypeSCharP,   // 9
  CTypeUChar,    // 10
  CTypeUCharP,   // 11
  CTypeSShort,   // 12
  CTypeSShortP,  // 13
  CTypeUShort,   // 14
  CTypeUShortP,  // 15
  CTypeSLong,    // 16
  CTypeSLongP,   // 17
  CTypeULong,    // 18
  CTypeULongP,   // 19
  CTypeString,   // 20
  CTypeVoid,     // 21
  CTypeVoidP,    // 22
  CTypeUnknown   // 23
} CTypeId;

// Array of mappings
static ffi_type **typeMappings = NULL;
static void initializeTypeMappings(void) {
  if (typeMappings) {
    return;
  }
  typeMappings = FRY_ALLOC(sizeof(ffi_type *) * 23);
  typeMappings[CTypeSInt] = &ffi_type_sint;
  typeMappings[CTypeSIntP] = &ffi_type_pointer;
  typeMappings[CTypeUInt] = &ffi_type_uint;
  typeMappings[CTypeUIntP] = &ffi_type_pointer;
  typeMappings[CTypeFloat] = &ffi_type_float;
  typeMappings[CTypeFloatP] = &ffi_type_pointer;
  typeMappings[CTypeDouble] = &ffi_type_double;
  typeMappings[CTypeDoubleP] = &ffi_type_pointer;
  typeMappings[CTypeSChar] = &ffi_type_schar;
  typeMappings[CTypeSCharP] = &ffi_type_pointer;
  typeMappings[CTypeUChar] = &ffi_type_uchar;
  typeMappings[CTypeUCharP] = &ffi_type_pointer;
  typeMappings[CTypeSShort] = &ffi_type_sshort;
  typeMappings[CTypeSShortP] = &ffi_type_pointer;
  typeMappings[CTypeUShort] = &ffi_type_ushort;
  typeMappings[CTypeUShortP] = &ffi_type_pointer;
  typeMappings[CTypeSLong] = &ffi_type_slong;
  typeMappings[CTypeSLongP] = &ffi_type_pointer;
  typeMappings[CTypeULong] = &ffi_type_ulong;
  typeMappings[CTypeULongP] = &ffi_type_pointer;
  typeMappings[CTypeString] = &ffi_type_pointer;
  typeMappings[CTypeVoid] = &ffi_type_void;
  typeMappings[CTypeVoidP] = &ffi_type_pointer;
}

static CTypeId getCTypeId(const double num) {
  CTypeId id = num;
  if (0 <= id && id < CTypeUnknown) {
    return id;
  }
  return CTypeUnknown;  // unknown type
}

static bool val2ffi(const OpCode typeId, Node *arg, void *ptr) {
  switch (typeId) {
    case CTypeSInt:
      if (arg->type == FatNumber) {
        *(int *)ptr = (int)arg->num.f;
        return true;
      }
      break;

    case CTypeSIntP:
      if (arg->type == FatNumber) {
        arg->num.sint = (int)arg->num.f;  // cast to C type
        *(int **)ptr = &(arg->num.sint);
        return true;
      }
      break;

    case CTypeUInt:
      if (arg->type == FatNumber && arg->num.f >= 0.0) {
        *(unsigned int *)ptr = (unsigned int)arg->num.f;
        return true;
      }
      break;

    case CTypeUIntP:
      if (arg->type == FatNumber && arg->num.f >= 0.0) {
        arg->num.uint = (unsigned int)arg->num.f;  // cast to C type
        *(unsigned int **)ptr = &(arg->num.uint);
        return true;
      }
      break;

    case CTypeFloat:
      if (arg->type == FatNumber) {
        *(float *)ptr = (float)arg->num.f;
        return true;
      }
      break;

    case CTypeFloatP:
      if (arg->type == FatNumber) {
        arg->num.cflt = (float)arg->num.f;  // cast to C type
        *(float **)ptr = &(arg->num.cflt);
        return true;
      }
      break;

    case CTypeDouble:
      if (arg->type == FatNumber) {
        *(double *)ptr = arg->num.f;
        return true;
      }
      break;

    case CTypeDoubleP:
      if (arg->type == FatNumber) {
        *(double **)ptr = &(arg->num.f);  // directly point to node buffer
        return true;
      }
      break;

    case CTypeSChar:
    case CTypeUChar:
      if (arg->type == FatChunk && arg->num.s == 1) {
        *(char *)ptr = *arg->val;
        return true;
      }
      break;

    case CTypeSCharP:
    case CTypeUCharP:
    case CTypeVoidP:
      if (arg->type == FatChunk) {
        *(char **)ptr = arg->val;  // directly point to node buffer
        return true;
      }
      break;

    case CTypeSShort:
      if (arg->type == FatNumber) {
        *(short *)ptr = (short)arg->num.f;
        return true;
      }
      break;

    case CTypeSShortP:
      if (arg->type == FatNumber) {
        arg->num.sshort = (short)arg->num.f;  // cast to C type
        *(short **)ptr = &(arg->num.sshort);
        return true;
      }
      break;

    case CTypeUShort:
      if (arg->type == FatNumber && arg->num.f >= 0.0) {
        *(unsigned short *)ptr = (unsigned short)arg->num.f;
        return true;
      }
      break;

    case CTypeUShortP:
      if (arg->type == FatNumber && arg->num.f >= 0.0) {
        arg->num.ushort = (unsigned short)arg->num.f;  // cast to C type
        *(unsigned short **)ptr = &(arg->num.ushort);
        return true;
      }
      break;

    case CTypeSLong:
      if (arg->type == FatNumber) {
        *(long *)ptr = (long)arg->num.f;
        return true;
      }
      break;

    case CTypeSLongP:
      if (arg->type == FatNumber) {
        arg->num.slong = (long)arg->num.f;  // cast to C type
        *(long **)ptr = &(arg->num.slong);
        return true;
      }
      break;

    case CTypeULong:
      if (arg->type == FatNumber && arg->num.f >= 0.0) {
        *(unsigned long *)ptr = (unsigned long)arg->num.f;
        return true;
      }
      break;

    case CTypeULongP:
      if (arg->type == FatNumber && arg->num.f >= 0.0) {
        arg->num.ulong = (unsigned long)arg->num.f;  // cast to C type
        *(unsigned long **)ptr = &(arg->num.ulong);
        return true;
      }
      break;

    case CTypeString:
      if (arg->type == FatText) {
        *(char **)ptr = arg->val;  // directly point to node buffer
        return true;
      }
      break;

    default:
      break;
  }

  return false;
}

static Node *ffi2val(const OpCode typeId, void *retBuff, Context *ctx) {
  switch (typeId) {
    case CTypeSInt:
      return runtimeNumber(*(int *)retBuff, ctx);

    case CTypeUInt:
      return runtimeNumber(*(unsigned int *)retBuff, ctx);

    case CTypeFloat:
      return runtimeNumber(*(float *)retBuff, ctx);

    case CTypeDouble:
      return runtimeNumber(*(double *)retBuff, ctx);

    case CTypeSChar:
    case CTypeUChar:
      return runtimeChunk(copyChunk((char *)retBuff, 1), 1, ctx);

    case CTypeSShort:
      return runtimeNumber(*(short *)retBuff, ctx);

    case CTypeUShort:
      return runtimeNumber(*(unsigned short *)retBuff, ctx);

    case CTypeSLong:
      return runtimeNumber(*(long *)retBuff, ctx);

    case CTypeULong:
      return runtimeNumber(*(unsigned long *)retBuff, ctx);

    case CTypeString:
      if (*(char **)retBuff) {
        return runtimeText(*(char **)retBuff, strlen(*(char **)retBuff), ctx);
      }
      return NULL;

    // For implementing FFI it's usually sufficient to treat all pointers as
    // equivalent to a void pointer...
    case CTypeSIntP:
    case CTypeUIntP:
    case CTypeFloatP:
    case CTypeSCharP:
    case CTypeUCharP:
    case CTypeSShortP:
    case CTypeUShortP:
    case CTypeSLongP:
    case CTypeULongP:
    case CTypeVoidP:
      if (*(char **)retBuff) {
        Node *cPtr = runtimeChunk(*(char **)retBuff, 0, ctx);
        cPtr->ck = ckCPointer;
        return cPtr;
      }
      return NULL;

    default:
      return NULL;
  }
}

/**
 * Load an external library and return it's dllSpace index
 */
static Node *loadDLL(Scope *scope, Context *ctx) {
  if (isJailMode) {
    return createError("loadDLL " MSG_JAIL, true, ckCallError, ctx);
  }

  Node *filename = getParameter(scope, "filename", FatText, ctx);
  if (IS_FAT_ERROR(filename)) {
    return filename;
  }

  int ret = initDLL(filename->val);
  switch (ret) {
    case -1:
      return createError(dlerror(), true, NULL, ctx);

    case -2:
      return createError("loadDLL " MSG_BMO, true, NULL, ctx);

    default:
      initializeTypeMappings();
      return runtimeNumber(ret, ctx);
  }
}

static void cleanupArgs(void *args[], unsigned int nargs) {
  for (unsigned int i = 0; i < nargs; i++) {
    free(args[i]);
  }
}

/**
 * Call via foreign function interface from a loaded DLL
 */
static Node *callFFI(Scope *scope, Context *ctx) {
  Node *instance = getInstance(FatScope, ctx);
  if (IS_FAT_ERROR(instance)) {
    return instance;
  }

  Scope *instanceScp = instance->scp;

  Node *lib = getParameter(instanceScp, "lib", FatNumber, ctx);
  if (IS_FAT_ERROR(lib)) {
    return lib;
  }
  DLL *dll = getDLL((int)lib->num.f);
  if (!dll) {
    return createError("DLL not loaded", true, ckValueError, ctx);
  }

  Node *name = getParameter(instanceScp, "name", FatText, ctx);
  if (IS_FAT_ERROR(name)) {
    return name;
  }

  // Note: func may actually take parameters as this is a generic fn pointer,
  // but it's declared as func(void) instead of func() to suppress warnings.
  void (*func)(void) = NULL;

  *(void **)(&func) = dlsym(dll->handle, name->val);
  if (!func) {
    char *msg = join3(dll->name, GUIDE "function not found" GUIDE, name->val);
    return createError(msg, false, ckValueError, ctx);
  }

  Node *in = getParameter(instanceScp, "in", FatList, ctx);
  if (IS_FAT_ERROR(in)) {
    return in;
  }
  if (in->scp->size > MAX_FFI_ARGS) {
    return createError("'in' has too may arguments", true, ckValueError, ctx);
  }

  Node *out = getParameter(instanceScp, "out", FatNumber, ctx);
  if (IS_FAT_ERROR(out)) {
    return out;
  }
  int retTypeId = getCTypeId(out->num.f);
  if (retTypeId == CTypeUnknown) {
    return createError("unknown 'out' ctype", true, ckValueError, ctx);
  }

  ffi_type *params[MAX_FFI_ARGS] = {0};
  void *values[MAX_FFI_ARGS] = {0};

  unsigned int i = 0;
  for (Entry *entry = in->scp->entries; entry; entry = entry->next) {
    // Get the type mapping from parameter types (input) into 'params' vector
    int argTypeId = getCTypeId(entry->data->num.f);
    if (argTypeId == CTypeUnknown) {
      cleanupArgs(values, i);
      char *msg = join2("unknown ctype for param: ", paramKeys[i]);
      return createError(msg, false, ckTypeError, ctx);
    }
    params[i] = typeMappings[argTypeId];

    // Bridge the FatScript values as C types into 'values' vector
    Node *val = getValueOf(scope, paramKeys[i]);
    values[i] = FRY_ALLOC(params[i]->size);
    if (!val2ffi(argTypeId, val, values[i])) {
      cleanupArgs(values, i + 1);
      char *msg = join2("type mismatch for arg: ", paramKeys[i]);
      return createError(msg, false, ckTypeError, ctx);
    }

    i++;
  }
  const unsigned int nargs = i;

  // Prepare the call interface for a function call
  ffi_cif cif;
  ffi_type *retType = typeMappings[retTypeId];
  if (ffi_prep_cif(&cif, FFI_DEFAULT_ABI, nargs, retType, params) != FFI_OK) {
    cleanupArgs(values, nargs);
    char *msg = join2("failed to prepare FFI call" GUIDE, name->val);
    return createError(msg, false, NULL, ctx);
  }

#ifdef DEBUG
  if (traceLogs) {
    printf(MRG_STR "%s: %s/%s arg pointers\n", __func__, dll->name, name->val);
    for (i = 0; i < nargs; i++) {
      if (params[i] == &ffi_type_pointer) {
        printf(MRG_STR "%u: %" PRIxPTR "\n", i,
               (uintptr_t)(*(void **)values[i]));
      } else {
        printf(MRG_STR "%u: (local value)\n", i);
      }
    }
  }
#endif

  // Perform actual function call
  void *retBuff = retTypeId != CTypeVoid ? FRY_ALLOC(retType->size) : NULL;
  lastCallErrno = 0;
  ffi_call(&cif, func, retBuff, values);
  lastCallErrno = errno;

  // Bridge the C output value back to a FatScript value
  Node *result = ffi2val(retTypeId, retBuff, ctx);

  // Cast C pointer buffers back to FatScript values
  i = 0;
  for (Entry *entry = in->scp->entries; entry; entry = entry->next) {
    if (params[i] == &ffi_type_pointer) {
      Node *affectedNode = NULL;
      switch (getCTypeId(entry->data->num.f)) {
        case CTypeSIntP:
          affectedNode = getValueOf(scope, paramKeys[i]);
          affectedNode->num.f = (double)affectedNode->num.sint;
          break;

        case CTypeUIntP:
          affectedNode = getValueOf(scope, paramKeys[i]);
          affectedNode->num.f = (double)affectedNode->num.uint;
          break;

        case CTypeFloatP:
          affectedNode = getValueOf(scope, paramKeys[i]);
          affectedNode->num.f = (double)affectedNode->num.cflt;
          break;

        case CTypeSShortP:
          affectedNode = getValueOf(scope, paramKeys[i]);
          affectedNode->num.f = (double)affectedNode->num.sshort;
          break;

        case CTypeUShortP:
          affectedNode = getValueOf(scope, paramKeys[i]);
          affectedNode->num.f = (double)affectedNode->num.ushort;
          break;

        case CTypeSLongP:
          affectedNode = getValueOf(scope, paramKeys[i]);
          affectedNode->num.f = (double)affectedNode->num.slong;
          break;

        case CTypeULongP:
          affectedNode = getValueOf(scope, paramKeys[i]);
          affectedNode->num.f = (double)affectedNode->num.ulong;
          break;

        default:
          break;
      }
    }

    i++;
  }

  cleanupArgs(values, nargs);
  free(retBuff);

  return result;
}

/**
 * Read ptr as null-terminated C-string
 */
static Node *unsafeCStr(Scope *scope, Context *ctx) {
  if (isJailMode) {
    return createError("unsafeCStr " MSG_JAIL, true, ckCallError, ctx);
  }

  Node *ptr = getParameter(scope, "ptr", FatChunk, ctx);
  if (IS_FAT_ERROR(ptr)) {
    return ptr;
  }
  if (ptr->val == NULL || ptr->num.s != 0) {
    return createError("bad void pointer", true, ckValueError, ctx);
  }

  return runtimeTextDup(ptr->val, ctx);
}

/**
 * Read ptr considering offset and length
 */
static Node *unsafePeek(Scope *scope, Context *ctx) {
  if (isJailMode) {
    return createError("unsafePeek " MSG_JAIL, true, ckCallError, ctx);
  }

  Node *ptr = getParameter(scope, "ptr", FatChunk, ctx);
  if (IS_FAT_ERROR(ptr)) {
    return ptr;
  }
  if (ptr->val == NULL || ptr->num.s != 0) {
    return createError("bad void pointer", true, ckValueError, ctx);
  }

  Node *offset = getParameter(scope, "offset", FatNumber, ctx);
  if (IS_FAT_ERROR(offset)) {
    return offset;
  }
  if (offset->num.f < 0) {
    return createError(MSG_I_N_ARG GUIDE "offset", true, ckValueError, ctx);
  }

  Node *len = getParameter(scope, "len", FatNumber, ctx);
  if (IS_FAT_ERROR(len)) {
    return len;
  }
  if (len->num.f < 0) {
    return createError(MSG_I_N_ARG GUIDE "len", true, ckValueError, ctx);
  }

  const char *actualPtr = ptr->val + (size_t)offset->num.f;
  size_t nbytes = (size_t)len->num.f;

  return runtimeChunk(copyChunk(actualPtr, nbytes), nbytes, ctx);
}

static Node *detachNode(Scope *scope, Context *ctx) {
  Node *node = getValueOf(scope, "node");

  if (node && (node->type == FatText || node->type == FatChunk)) {
    node->val = node->type == FatText ? strDup("") : NULL;
    node->num.s = 0;
    return NULL;
  }

  return createError("nothing to detach", true, ckValueError, ctx);
}

/**
 * Marshal a FatScript value to a CType
 */
static Node *marshal(Scope *scope, Context *ctx) {
  Node *val = getValueOf(scope, "val");

  Node *type = getParameter(scope, "type", FatNumber, ctx);
  if (IS_FAT_ERROR(type)) {
    return type;
  }

  int typeId = getCTypeId(type->num.f);

  switch (typeId) {
    case CTypeVoid:
      return NULL;

    case CTypeSIntP:
    case CTypeUIntP:
    case CTypeFloatP:
    case CTypeSCharP:
    case CTypeUCharP:
    case CTypeSShortP:
    case CTypeUShortP:
    case CTypeSLongP:
    case CTypeULongP:
      return createError("unsupported marshaling", true, ckValueError, ctx);

    case CTypeUnknown:
      return createError("unknown ctype", true, ckValueError, ctx);

    default:
      break;
  }

  initializeTypeMappings();
  size_t nbytes = typeMappings[typeId]->size;
  void *buff = FRY_ALLOC(nbytes);
  if (!val2ffi(typeId, val, buff)) {
    free(buff);
    return createError("value or type unmatched", true, ckValueError, ctx);
  }

  return runtimeChunk(buff, nbytes, ctx);
}

/**
 * Unmarshal from a CType back to FatScript
 */
static Node *unmarshal(Scope *scope, Context *ctx) {
  Node *raw = getParameter(scope, "raw", FatChunk, ctx);
  if (IS_FAT_ERROR(raw)) {
    return raw;
  }

  Node *type = getParameter(scope, "type", FatNumber, ctx);
  if (IS_FAT_ERROR(type)) {
    return type;
  }

  int typeId = getCTypeId(type->num.f);

  switch (typeId) {
    case CTypeVoid:
      return NULL;

    case CTypeUnknown:
      return createError("unknown ctype", true, ckValueError, ctx);

    default:
      break;
  }

  initializeTypeMappings();
  if (raw->num.s != typeMappings[typeId]->size) {
    return createError("ctype data size unmatched", true, ckValueError, ctx);
  }

  return ffi2val(typeId, (void *)raw->val, ctx);
}

/**
 * Gets the errno for the last FFI call
 */
static Node *getErrno(Context *ctx) {
  return runtimeNumber(lastCallErrno, ctx);
}

/**
 * Gets the number of bytes for CType
 */
static Node *sizeOf(Scope *scope, Context *ctx) {
  Node *type = getParameter(scope, "type", FatNumber, ctx);
  if (IS_FAT_ERROR(type)) {
    return type;
  }

  initializeTypeMappings();
  return runtimeNumber(typeMappings[getCTypeId(type->num.f)]->size, ctx);
}

#endif
