/**
 * @file hmap.c
 * @brief Generic hash-map key-value store
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.0.0
 * @date 2025-01-04
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

static const char* LIB_HMAP =
  "# fat.extra.HashMap - Generic hash-map key-value store\n"
  "\n"
  "### A more performant in-memory key-value store replacement for\n"
  "### FatScript's default Scope implementation, for huge data-sets.\n"
  "\n"
  "_ <- fat.type.List\n"
  "_ <- fat.type.Scope\n"
  "_ <- fat.type.Error\n"
  "\n"
  "HashMap = (\n"
  "  ## Argument slot (optionally sets custom number of hash pots)\n"
  "  capacity: Number = 97  # https://planetmath.org/goodhashtableprimes\n"
  "\n"
  "  ## Constructor\n"
  "  apply = <> {\n"
  "    ### there is no benefit in using HashMap with less than 53 pots\n"
  "    capacity < 53 => ValueError('minimum HashMap capacity is 53')\n"
  "    _             => ({\n"
  "      capacity = capacity.{$numbTruncate}\n"
  "      pots = ..<capacity @ -> ({})\n"
  "    })\n"
  "  }\n"
  "\n"
  "  ## Common prototype methods\n"
  "  isEmpty  = Boolean <> !self.size\n"
  "  nonEmpty = Boolean <> !!self.size\n"
  "  size     = Number  <> { v = self.pots @ -> _.size, $sum }\n"
  "  toText   = Text    <> 'HashMap/{self.capacity}'\n"
  "  freeze   = Void    <> $freeze\n"
  "\n"
  "  ## HashMap prototype special methods\n"
  "  set = (key: Text, value: Any): Any -> {\n"
  "    hash = $getHash % self.capacity\n"
  "    self.pots[hash][key] = value\n"
  "  }\n"
  "\n"
  "  get = (key: Text): Any -> {\n"
  "    hash = $getHash % self.capacity\n"
  "    self.pots(hash)(key)\n"
  "  }\n"
  "\n"
  "  keys = Keyset <> (self.pots @ -> _.keys).flatten\n"
  ")\n";
