/**
 * @file opaque.c
 * @brief Soft encapsulation utility (layer of indirection)
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.0.0
 * @date 2025-01-10
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

static const char* LIB_OPAQUE =
  "# fat.extra.Opaque - Soft encapsulation utility\n"
  "\n"
  "_ <- fat.type.Scope\n"
  "\n"
  "Opaque = (\n"
  "  ## Includes\n"
  "  Scope\n"
  "\n"
  "  ## Argument slot (initial scope to protect)\n"
  "  data: Scope = {}\n"
  "\n"
  "  ## Constructor\n"
  "  apply = <> (data -> Opaque <> data)(Opaque * data.copy)\n"
  ")\n";
