/**
 * @file param.c
 * @brief Parameter presence and type verification
 * @author Antonio Prates <hello@aprates.dev>
 * @version 3.4.0
 * @date 2024-10-26
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

static const char* LIB_PARAM =
  "# fat.extra.Param - Parameter presence and type verification\n"
  "\n"
  "_ <- fat.type.Error\n"
  "\n"
  "Param = (\n"
  "  ## Arguments\n"
  "  _exp: Text  # parameter name to check in context\n"
  "  _typ: Type  # expected type of the evaluated value\n"
  "\n"
  "  strict: Boolean = false  # disable flexible/alias match\n"
  "\n"
  "  ## Constructor\n"
  "  apply = <> {\n"
  "    !_typ => ValueError('type definition not loaded')\n"
  "\n"
  "    (_exp >> { '_' | 'apply' | 'get' | '_exp' | '_typ' => true }) =>\n"
  "      ValueError('\\'{_exp}\\' is a restricted expression')\n"
  "\n"
  "    _ => self\n"
  "  }\n"
  "\n"
  "  ## Prototype method\n"
  "  get = <> {\n"
  "    { _exp, _typ } = self\n"
  "    value = (->$eval)('~' + _exp)\n"
  "\n"
  "    ~value == Void => KeyError(\n"
  "      '\\'{_exp}\\' not defined in the execution context'\n"
  "    )\n"
  "\n"
  "    ~value == _typ | (!self.strict & ~value <= _typ) => ~value\n"
  "\n"
  "    _ => TypeError(\n"
  "      ''\n"
  "      + '\\'{_exp}\\' of type \\'{(->$typeOf)(_typ)}\\' expected, '\n"
  "      + 'found \\'{(->$typeOf)(value)}\\''\n"
  "    )\n"
  "  }\n"
  ")\n"
  "\n"
  "### Utility to disable implicitParameter warnings\n"
  "UsingStrict = (_exp, _typ, apply = <> { Param(_exp,_typ,true).get, () })\n"
  "Using = (_exp, _typ, apply = <> { Param(_exp,_typ,false).get, () })\n";
