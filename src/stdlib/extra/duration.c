/**
 * @file duration.c
 * @brief Millisecond duration builder
 * @author Antonio Prates <hello@aprates.dev>
 * @version 3.4.1
 * @date 2024-12-22
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2023, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

static const char* LIB_DURATION =
  "# fat.extra.Duration - Millisecond duration builder\n"
  "\n"
  "## Millis is duration in milliseconds\n"
  "\n"
  "Duration = (\n"
  "  ## Argument slot\n"
  "  val: Number\n"
  "\n"
  "  ## Prototype conversion methods\n"
  "  nanos   = <> Millis * (self.val / 1.0e+6)\n"
  "  micros  = <> Millis * (self.val / 1000)\n"
  "  millis  = <> Millis * (self.val * 1)\n"
  "  seconds = <> Millis * (self.val * 1000)\n"
  "  minutes = <> Millis * (self.val * 60000)\n"
  "  hours   = <> Millis * (self.val * 3.6e+6)\n"
  "  days    = <> Millis * (self.val * 8.64e+7)\n"
  "  weeks   = <> Millis * (self.val * 6.048e+8)\n"
  ")\n";
