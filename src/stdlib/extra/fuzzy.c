/**
 * @file fuzzy.c
 * @brief Probabilistic values and fuzzy logic operations
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.0.0
 * @date 2025-01-04
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

static const char* LIB_FUZZY =
  "# fat.extra.Fuzzy - Probabilistic values and fuzzy logic operations\n"
  "\n"
  "Fuzzy = (\n"
  "  ## Argument slot\n"
  "  val: Number = 0.5\n"
  "\n"
  "  ## Constructor\n"
  "  apply = <> val\n"
  "\n"
  "  ## Common prototype methods\n"
  "  isEmpty = Boolean <> self <= 0\n"
  "  nonEmpty = Boolean <> self > 0\n"
  "  size = Number <> {\n"
  "    self <= 0    => 0\n"
  "    self <= 0.01 => 1\n"
  "    self >= 1    => 100\n"
  "    _            => (self * 100).{$numbTruncate}\n"
  "  }\n"
  "  toText = Text <> '{self.size}%'\n"
  "  freeze = Void <> $freeze\n"
  "\n"
  "  ## Fuzzy prototype special method\n"
  "  and = (other: Fuzzy): Fuzzy -> self * other\n"
  "  or = (other: Fuzzy): Fuzzy -> self + other - self * other\n"
  "  not = Fuzzy <> Fuzzy * (1 - self)\n"
  "  decide = Boolean <> $random < self\n"
  ")\n";
