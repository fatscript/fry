/**
 * @file storable.c
 * @brief Data store facilities
 *
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.0.0
 * @date 2025-01-18
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

static const char* LIB_STORABLE =
  "# fat.extra.Storable - Data store facilities\n"
  "\n"
  "_      <- fat.type.Error\n"
  "_      <- fat.type.Method\n"
  "_      <- fat.type.Scope\n"
  "_      <- fat.type.Text\n"
  "_      <- fat.type.Void\n"
  "file   <- fat.file\n"
  "sdk    <- fat.sdk\n"
  "enigma <- fat.enigma\n"
  "\n"
  "### Storable mixin provides a simple way to store and retrieve objects\n"
  "### from the filesystem. A type extending Storable will have its folder\n"
  "### and can be saved/loaded via serialized format (default is JSON).\n"
  "Storable = (\n"
  "  ## Mandatory prop, used as filename (defaults to random UUID)\n"
  "  id: Text = null\n"
  "\n"
  "  ## Constructor\n"
  "  apply = <> {\n"
  "    self.id.isEmpty ? self.id = enigma.genUUID\n"
  "    self\n"
  "  }\n"
  "\n"
  "  ## Codec for JSON data (override for custom serialization)\n"
  "  encode = (_): Text -> $stringify\n"
  "  decode = (json: Text): Any -> json.nonEmpty & json(0) == \"{\"\n"
  "    ? $fromJSON\n"
  "    : FileError('failed to decode')\n"
  "\n"
  "  ## Path for the type (override for custom folder)\n"
  "  ## Note: on web implementation you can't have slashes nor commas,\n"
  "  ## so you may need separate implementations for filesystem and web.\n"
  "  dataStore = Text <> sdk.typeOf(self)\n"
  "\n"
  "  ## Retrieves a list of identifiers for stored instances of the type\n"
  "  list = Keyset <> {\n"
  "    folder = self.dataStore\n"
  "    $isWeb              => $storableList\n"
  "    file.exists(folder) => file.lsDir(folder)\n"
  "    _                   => []\n"
  "  }\n"
  "\n"
  "  ## Loads object by id from the filesystem into a new type instance\n"
  "  ## Note: nested types are not recovered recursively, you may take\n"
  "  ## this limitation as a design guidance when using Storable mixin,\n"
  "  ## or, override the 'load' method with a more advanced strategy.\n"
  "  load = (id: Text): Any -> {\n"
  "    folder = self.dataStore\n"
  "    typename = sdk.typeOf(self)\n"
  "    path = '{folder}/{id}'\n"
  "    raw = $isWeb ? $storableLoad : file.read(path)\n"
  "    data = self.decode(raw)\n"
  "    data == Void  => null\n"
  "    data <= Error => data\n"
  "    _             => sdk.getDef(typename) * data\n"
  "  }\n"
  "\n"
  "  ## Saves object to the filesystem (creates type folder if not present)\n"
  "  save = Void <> {\n"
  "    folder = self.dataStore()\n"
  "    path = '{folder}/{self.id}'\n"
  "    data = self.encode(self)\n"
  "    $isWeb => $storableSave\n"
  "    _      => {\n"
  "      file.mkDir(folder, self('getEncryptionKey').nonEmpty)\n"
  "      file.write(path, data)\n"
  "    }\n"
  "  }\n"
  "\n"
  "  ## Deletes the file corresponding to id from the filesystem\n"
  "  erase = Void <> {\n"
  "    id = _ ?? self.id\n"
  "    id ? {\n"
  "      folder = self.dataStore\n"
  "      path = '{folder}/{id}'\n"
  "      $isWeb => $storableErase\n"
  "      _      => file.remove(path)\n"
  "    }\n"
  "  }\n"
  ")\n"
  "\n"
  "### The EncryptedStorable sub-type offers a safer way to store data\n"
  "EncryptedStorable = (\n"
  "  Storable\n"
  "\n"
  "  ## you need to provide an override implementation for getEncryptionKey\n"
  "  getEncryptionKey = Text <>\n"
  "    ValueError('getEncryptionKey is missing implementation')\n"
  "\n"
  "  ## Codec overrides for encrypted data\n"
  "  encode = (_): Text -> {\n"
  "    json = $stringify\n"
  "    enigma.encrypt(json, self.getEncryptionKey())\n"
  "  }\n"
  "  decode = (raw: Text): Any -> {\n"
  "    json = enigma.decrypt(raw, self.getEncryptionKey())\n"
  "    $fromJSON\n"
  "  }\n"
  ")\n";

#ifdef __EMSCRIPTEN__

#include "../../runtime/interpreter.h"

static inline bool isValidStorablePath(const char* path) {
  return countWord(path, "/") == 1 && !strchr(path, ',');
}

static inline bool isValidStorableFolder(const char* folder) {
  return !strchr(folder, '/') && !strchr(folder, ',');
}

static Node* storableSave(Scope* scope, Context* ctx) {
  Node* path = getParameter(scope, "path", FatText, ctx);
  if (IS_FAT_ERROR(path)) {
    return path;
  }

  Node* data = getParameter(scope, "data", FatText, ctx);
  if (IS_FAT_ERROR(data)) {
    return data;
  }

  if (!isValidStorablePath(path->val)) {
    char* msg = join3(path->val, GUIDE, "invalid storable path");
    return createError(msg, false, ckFileError, ctx);
  }

  if (!localStoreSet(path->val, data->val)) {
    char* msg = join3(path->val, GUIDE, "failed to store");
    return createError(msg, false, ckFileError, ctx);
  }

  return NULL;
}

static Node* storableLoad(Scope* scope, Context* ctx) {
  Node* path = getParameter(scope, "path", FatText, ctx);
  if (IS_FAT_ERROR(path)) {
    return path;
  }

  if (!isValidStorablePath(path->val)) {
    char* msg = join3(path->val, GUIDE, "invalid storable path");
    return createError(msg, false, ckFileError, ctx);
  }

  char* data = localStoreGet(path->val);
  return data ? runtimeText(data, strlen(data), ctx) : NULL;
}

static Node* storableList(Scope* scope, Context* ctx) {
  Node* folder = getParameter(scope, "folder", FatText, ctx);
  if (IS_FAT_ERROR(folder)) {
    return folder;
  }

  if (!isValidStorableFolder(folder->val)) {
    return createError("invalid storable folder", true, ckValueError, ctx);
  }

  auto_str contents = localStoreList(folder->val);
  char** splitted = *contents ? splitSep(contents, ",") : NULL;

  Scope* list = createList();
  pushStack(ctx, __func__, folder, list);
  if (splitted) {
    for (size_t i = 0; splitted[i]; i++) {
      Node* fragment = runtimeText(splitted[i], strlen(splitted[i]), ctx);
      addToList(list, fragment, ctx);
    }
    free(splitted);
  }
  Node* result = runtimeCollection(list, ctx);
  popStack(ctx, 1);
  return result;
}

static Node* storableErase(Scope* scope, Context* ctx) {
  Node* path = getParameter(scope, "path", FatText, ctx);
  if (IS_FAT_ERROR(path)) {
    return path;
  }

  if (!isValidStorablePath(path->val)) {
    char* msg = join3(path->val, GUIDE, "invalid storable path");
    return createError(msg, false, ckFileError, ctx);
  }

  localStoreRemove(path->val);
  return NULL;
}

#endif
