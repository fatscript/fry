/**
 * @file file.c
 * @brief File input and output operations
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-12
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../runtime/interpreter.h"

static const char* LIB_FILE =
  "# fat.file - File input and output operations\n"
  "\n"
  "FileInfo = (\n"
  "  modTime: Epoch  # last modified\n"
  "  size: Text      # bytes\n"
  ")\n"
  "\n"
  "## Extract base path of app call\n"
  "basePath = Text <> $basePath\n"
  "\n"
  "## Return canonical name for path\n"
  "resolve = (path: Text): Text -> $resolve\n"
  "\n"
  "## Check file exists on provided path\n"
  "exists = (path: Text): Boolean -> $exists\n"
  "\n"
  "## Read file from path (text mode)\n"
  "read = (path: Text): Text -> $read\n"
  "\n"
  "## Read file from path (binary mode)\n"
  "readBin = (path: Text): Chunk -> $readBin\n"
  "\n"
  "## Read system/virtual file from path\n"
  "readSys = (path: Text): Text -> $readSys\n"
  "\n"
  "## Write src to file\n"
  "write = (path: Text, src: Any): Void -> $write\n"
  "\n"
  "## Append to file\n"
  "append = (path: Text, src: Any): Void -> $append\n"
  "\n"
  "## Remove file\n"
  "remove = (path: Text): Void -> $remove\n"
  "\n"
  "## Check if path is a directory\n"
  "isDir = (path: Text): Boolean -> $isDir\n"
  "\n"
  "## Create a directory\n"
  "mkDir = (path: Text, safe: Boolean = false): Void -> $mkDir\n"
  "\n"
  "## Get list of files in a directory\n"
  "lsDir = (path: Text): List/Text -> $lsDir\n"
  "\n"
  "## Get file metadata\n"
  "stat = (path: Text): FileInfo -> $stat\n";

/**
 * Extract the base path where the app was called
 */
static Node* fileBasePath(Context* ctx) {
  return runtimeTextDup(basePath ? basePath : "", ctx);
}

/**
 * Return canonical name for path
 */
static Node* fileResolve(Scope* scope, Context* ctx) {
  Node* path = getParameter(scope, "path", FatText, ctx);
  if (IS_FAT_ERROR(path)) {
    return path;
  }

  char resolved[PATH_MAX] = {'\0'};

  if (!realpath(path->val, resolved)) {
    char* msg = join3(path->val, GUIDE, strerror(errno));
    return createError(msg, false, ckFileError, ctx);
  }

  return runtimeTextDup(resolved, ctx);
}

/**
 * Check file exists from text path and return boolean
 */
static Node* fileExists(Scope* scope, Context* ctx) {
  Node* path = getParameter(scope, "path", FatText, ctx);
  if (IS_FAT_ERROR(path)) {
    return path;
  }

  return RUNTIME_BOOLEAN(existsFile(path->val));
}

/**
 * Read file from text path (text mode) and return content or null
 */
static Node* fileRead(Scope* scope, Context* ctx) {
  Node* path = getParameter(scope, "path", FatText, ctx);
  if (IS_FAT_ERROR(path)) {
    return path;
  }

  char* result = NULL;
  ssize_t len = readFile(&result, path->val, "r");
  if (len == -1) {
    char* msg = join3(path->val, GUIDE, strerror(errno));
    return createError(msg, false, ckFileError, ctx);
  }
  return result ? runtimeText(result, (size_t)len, ctx) : NULL;
}

/**
 * Read file from text path (binary mode) and return content or null
 */
static Node* fileReadBin(Scope* scope, Context* ctx) {
  Node* path = getParameter(scope, "path", FatText, ctx);
  if (IS_FAT_ERROR(path)) {
    return path;
  }

  char* result = NULL;
  ssize_t len = readFile(&result, path->val, "rb");
  if (len == -1) {
    char* msg = join3(path->val, GUIDE, strerror(errno));
    return createError(msg, false, ckFileError, ctx);
  }
  return runtimeChunk(result, (size_t)len, ctx);
}

/**
 * Read system/virtual file from path and return content or null
 */
static Node* fileReadSys(Scope* scope, Context* ctx) {
  Node* path = getParameter(scope, "path", FatText, ctx);
  if (IS_FAT_ERROR(path)) {
    return path;
  }

  FILE* file = fopen(path->val, "r");
  if (!file) {
    char* msg = join3(path->val, GUIDE, strerror(errno));
    return createError(msg, false, ckFileError, ctx);
  }

  char tmp[1024];
  char* buffer = FRY_ALLOC(sizeof(tmp));
  size_t capacity = sizeof(tmp);
  size_t length = 0;

  while (fgets(tmp, sizeof(tmp), file)) {
    size_t tmpLen = strlen(tmp);
    if (length + tmpLen > capacity) {
      capacity *= 2;
      buffer = FRY_REALLOC(buffer, capacity);
    }
    memcpy(buffer + length, tmp, tmpLen);
    length += tmpLen;
  }
  fclose(file);

  buffer = FRY_REALLOC(buffer, length + 1);
  buffer[length] = '\0';  // ensure null-termination

  return runtimeText(buffer, length, ctx);
}

/**
 * Write src to path and return boolean success
 */
static Node* fileWrite(Scope* scope, Context* ctx, bool append) {
  if (isJailMode) {
    return createError("write " MSG_JAIL, true, ckCallError, ctx);
  }

  Node* path = getParameter(scope, "path", FatText, ctx);
  if (IS_FAT_ERROR(path)) {
    return path;
  }

  const char* file = path->val;

  Node* source = getValueOf(scope, "src");  // Any
  if (!source) {
    char* msg = join3(MSG_M_PAR, GUIDE, "src");
    return createError(msg, false, ckCallError, ctx);
  }

  char* buffer = NULL;
  size_t len = 0;

  char* mode = append ? "a" : "w";
  bool shouldFree = false;

  switch (source->type) {
    case FatChunk:
      // unless binary data, mode is already set
      mode = append ? "ab" : "wb";
      FALL_THROUGH;
    case FatText:
    case FatTemp:
      buffer = source->val;
      len = source->num.s;
      break;

    default:
      buffer = toJson(source, false);
      len = strlen(buffer);
      shouldFree = true;
  }

  bool success = writeFile(file, buffer, len, mode);
  if (shouldFree) {
    free(buffer);
  }
  if (!success) {
    char* msg = join3(path->val, GUIDE, strerror(errno));
    return createError(msg, false, ckFileError, ctx);
  }
  return NULL;
}

/**
 * Helper function to recursively remove files and directories
 */
static char* delTree(const char* path) {
  size_t len = strlen(path);
  if (!len) {
    return strDup("(empty)" GUIDE "fileRemove" GUIDE "invalid path");
  }

  struct stat statbuf = {0};
  if (lstat(path, &statbuf)) {
    return NULL;  // nothing to remove or is a link (don't follow)
  }

  // Check if the path is a symbolic link (just remove link, don't follow)
  if (S_ISLNK(statbuf.st_mode)) {
    if (remove(path) != 0) {
      return join3(path, GUIDE "fileRemove" GUIDE, strerror(errno));
    }
    return NULL;
  }

  if (S_ISDIR(statbuf.st_mode)) {
    DIR* dir = opendir(path);
    if (!dir) {
      return join3(path, GUIDE "fileRemove" GUIDE, "operation failed");
    }

    for (struct dirent* entry = readdir(dir); entry; entry = readdir(dir)) {
      if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
        continue;
      }

      // Path already ends with a slash, no need to add another...
      auto_str subpath = path[len - 1] == '/' ? join2(path, entry->d_name)
                                              : join3(path, "/", entry->d_name);

      char* err = delTree(subpath);
      if (err) {
        if (closedir(dir) != 0) {
          logAlert(path, __func__, "can't close directory");
        }
        return err;
      }
    }

    if (closedir(dir) != 0) {
      logAlert(path, __func__, "can't close directory");
    }
    if (rmdir(path) != 0) {
      return join3(path, GUIDE "fileRemove" GUIDE, strerror(errno));
    }
    return NULL;
  }

  if (remove(path) != 0) {
    return join3(path, GUIDE "fileRemove" GUIDE, strerror(errno));
  }
  return NULL;
}

/**
 * Erase file or directory and return boolean success
 */
static Node* fileRemove(Scope* scope, Context* ctx) {
  if (isJailMode) {
    return createError("remove " MSG_JAIL, true, ckCallError, ctx);
  }

  Node* path = getParameter(scope, "path", FatText, ctx);
  if (IS_FAT_ERROR(path)) {
    return path;
  }

  char* err = delTree(path->val);
  if (err) {
    return createError(err, false, ckFileError, ctx);
  }

  return NULL;
}

/**
 * Check if text path is a directory and return boolean
 */
static Node* fileIsDir(Scope* scope, Context* ctx) {
  Node* path = getParameter(scope, "path", FatText, ctx);
  if (IS_FAT_ERROR(path)) {
    return path;
  }

  return RUNTIME_BOOLEAN(isDir(path->val));
}

/**
 * Create directory and return boolean
 * (if safe dir gets 0700 permission instead of 0755)
 */
static Node* fileMkDir(Scope* scope, Context* ctx) {
  if (isJailMode) {
    return createError("mkDir " MSG_JAIL, true, ckCallError, ctx);
  }

  Node* pathNode = getParameter(scope, "path", FatText, ctx);
  if (IS_FAT_ERROR(pathNode)) {
    return pathNode;
  }

  Node* safe = getParameter(scope, "safe", FatBoolean, ctx);
  if (IS_FAT_ERROR(safe)) {
    return safe;
  }

  mode_t mode = safe->num.b ? 0700 : 0755;
  auto_str subpath = copyChunk(pathNode->val, pathNode->num.s);
  for (char* p = subpath + 1; *p; p++) {
    if (*p == '/') {
      *p = '\0';  // temporarily terminate the current subpath
      if (mkdir(subpath, mode) && errno != EEXIST) {
        char* msg = join3(subpath, GUIDE, strerror(errno));
        return createError(msg, false, ckFileError, ctx);
      }
      *p = '/';  // restore slash
    }
  }
  if (mkdir(pathNode->val, mode) && errno != EEXIST) {
    char* msg = join3(pathNode->val, GUIDE, strerror(errno));
    return createError(msg, false, ckFileError, ctx);
  }

  return NULL;
}

/**
 * Get list of files in a directory
 */
static Node* fileLsDir(Scope* scope, Context* ctx) {
  Node* path = getParameter(scope, "path", FatText, ctx);
  if (IS_FAT_ERROR(path)) {
    return path;
  }

  char** list = listDir(path->val);
  if (!list) {
    char* msg = join3(path->val, GUIDE, strerror(errno));
    return createError(msg, false, ckFileError, ctx);
  }

  Scope* contents = createList();
  pushStack(ctx, __func__, path, contents);

  for (size_t i = 0; list[i]; i++) {
    addToList(contents, runtimeText(list[i], strlen(list[i]), ctx), ctx);
  }

  Node* result = runtimeCollection(contents, ctx);

  popStack(ctx, 1);
  free(list);

  return result;
}

/**
 * Get file metadata
 */
static Node* fileStat(Scope* scope, Context* ctx) {
  Node* path = getParameter(scope, "path", FatText, ctx);
  if (IS_FAT_ERROR(path)) {
    return path;
  }

  struct stat fileInfo = {0};
  if (stat(path->val, &fileInfo)) {
    return createError("unable to get file metadata", true, ckFileError, ctx);
  }

  scope = createScope();
  scope->ck = setType("FileInfo", NULL);
  pushStack(ctx, __func__, path, scope);

  Node* modTime = runtimeNumber((double)fileInfo.st_mtime * 1000, ctx);
  modTime->ck = ckEpoch;
  addToScope(scope, "modTime", modTime);

  Node* size = runtimeNumber((double)fileInfo.st_size, ctx);
  addToScope(scope, "size", size);

  Node* statResponse = runtimeCollection(scope, ctx);

  popStack(ctx, 1);
  return statResponse;
}
