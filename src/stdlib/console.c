/**
 * @file console.c
 * @brief Console input and output operations
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-16
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../runtime/interpreter.h"

static const char* LIB_CONSOLE =
  "# fat.console - Console input and output operations\n"
  "\n"
  "### for fg and bg colors see fat.color library\n"
  "\n"
  "## Print msg to stdout, with newline\n"
  "log = (msg: Any, fg: Number = null, bg: Number = null): Void -> $log\n"
  "\n"
  "## Print msg to stdout, without newline\n"
  "print = (msg: Any, fg: Number = null, bg: Number = null): Void -> $print\n"
  "\n"
  "## Print msg to stderr, with newline\n"
  "stderr = (msg: Any, fg: Number = null, bg: Number = null): Void -> "
  "$stderr\n"
  "\n"
  "## Print msg and return input of stdin\n"
  "### alternative mode options: 'plain', 'quiet', 'secret'\n"
  "input = (msg: Any, mode: Text = null): Text -> $input\n"
  "\n"
  "## Flush stdout buffer\n"
  "flush = Void <> $flush\n"
  "\n"
  "## Clear stdout using ANSI escape codes\n"
  "cls = Void <> { msg = '\\e[H\\e[2J\\e[3J', $print }\n"
  "\n"
  "## Move cursor using ANSI escape codes\n"
  "moveTo = (x: Number, y: Number): Void -> $moveTo\n"
  "\n"
  "## Check if stdout is associated with a terminal device\n"
  "isTTY = Boolean <> $isTTY\n"
  "\n"
  "## Render progress bar, fraction 0 to 1\n"
  "showProgress = (label: Text, fraction: Number): Void -> $showProgress\n";

static const int progressLength = 30;
static __thread int progressPercentDone = 100;

/**
 * Print any msg to console (with/without newline and color)
 */
static Node* consPrint(Scope* scope, bool withNewLine, FILE* stream) {
  Node* fg = NULL;
  Node* bg = NULL;
  bool usingColor = false;

  auto_str msg = toString(getValueOf(scope, "msg"));

  // optional color parameters, only applicable if using a TTY
  if ((stream == stdout && stdoutIsTTY) || (stream == stderr && stderrIsTTY)) {
    fg = getValueOf(scope, "fg");
    bg = getValueOf(scope, "bg");
  }

  if (withNewLine) {
    endCursesMode();
    lockResource(&consoleLock);
  }

#ifdef USE_NCURSES
  if (cursesMode && stream == stdout) {
    // console.print can't apply color in curses mode when
    // built with ncurses as tuiBackend...
    // rebuild with fatcurses; or define color via usePair
    printw("%s", msg);  // print with ncurses
  } else {
#endif

    if (fg) {
      usingColor = true;
      printFgColor((int)fg->num.f, stream);
    }
    if (bg) {
      usingColor = true;
      printBgColor((int)bg->num.f, stream);
    }

    directPrint(msg, stream);

    if (usingColor) {
      directPrint(CL_RST, stream);
    }

#ifdef USE_NCURSES
  }
#endif

  if (withNewLine) {
    directPrint("\n", stream);
    unlockResource(&consoleLock);
  }

  return NULL;
}

static char* copyWithoutEnter(const char* s) {
  if (!s) {
    return NULL;
  }

  size_t len = strlen(s);
  return copyChunk(s, s[len - 1] == '\n' ? len - 1 : len);
}

/**
 * Print msg and return input of stdin (with readline or fgets based on mode)
 */
static Node* consInput(Scope* scope, Context* ctx) {
  endCursesMode();
  Node* val = getValueOf(scope, "msg");
  char* msg = val ? toString(val) : strDup("");
  char* input = NULL;

  // optional mode parameters
  const Node* mode = getValueOf(scope, "mode");

  lockResource(&consoleLock);

  if (IS_FAT_TYPE(mode, FatText)) {
    if (strcmp(mode->val, "plain") == 0) {
      char plain[BUFF_LEN] = {'\0'};  // assign '\0' to plain[0]

      // Read the line via fgets
      directPrint(msg, stdout);
      if (fgets(plain, BUFF_LEN - 1, stdin)) {
        if (!feof(stdin)) {
          input = copyWithoutEnter(plain);
        } else {
          putchar('\n');
        }
      } else {
        putchar('\n');
      }

    } else if (strcmp(mode->val, "quiet") == 0) {
      struct termios old_settings = {0};
      char buffer[BUFF_LEN] = {'\0'};  // assign '\0' to buffer[0]

      // Turn off echoing
      tcgetattr(STDIN_FILENO, &old_settings);
      struct termios new_settings = old_settings;
      new_settings.c_lflag &= (~ECHO);
      tcsetattr(STDIN_FILENO, TCSAFLUSH, &new_settings);

      // Read the line via fgets
      directPrint(msg, stdout);
      if (fgets(buffer, BUFF_LEN - 1, stdin)) {
        if (!feof(stdin)) {
          input = copyWithoutEnter(buffer);
        }
      }

      // Turn echoing back on
      tcsetattr(STDIN_FILENO, TCSAFLUSH, &old_settings);
      putchar('\n');

    } else if (strcmp(mode->val, "secret") == 0) {
      struct termios old_settings = {0};
      char secret[BUFF_LEN] = {'\0'};  // assign '\0' to secret[0]

      // Disable echo and canonical mode
      tcgetattr(STDIN_FILENO, &old_settings);
      struct termios new_settings = old_settings;
      new_settings.c_lflag &= ~(ECHO | ICANON);
      tcsetattr(STDIN_FILENO, TCSANOW, &new_settings);

      // Read the password character by character
      directPrint(msg, stdout);
      size_t len = 0;
      while (len < BUFF_LEN - 1) {
        int ch = getchar();

        if (ch == KEY_ENTER || ch == '\n' || ch == '\r') {
          break;
        }

        if (ch == KEY_ESC || ch == '\004') {
          tcsetattr(STDIN_FILENO, TCSANOW, &old_settings);
          putchar('\n');
          unlockResource(&consoleLock);
          free(msg);
          return NULL;
        }

        if (ch == KEY_BACKSPACE || ch == '\b' || ch == '\177') {
          if (len > 0) {
            len--;
            fputs("\b \b", stdout);
            while (len > 0 && (secret[len] & 0xC0) == 0x80) {
              len--;
            }
          }
          continue;
        }

        secret[len++] = (char)ch;

        // Only print '*' if not a continuation of multi-byte UTF-8 character
        if ((secret[len - 1] & 0xC0) != 0x80) {
          putchar('*');
        }
      }
      secret[len] = '\0';
      putchar('\n');

      // Restore the original terminal settings
      tcsetattr(STDIN_FILENO, TCSANOW, &old_settings);

      input = copyChunk(secret, len);

    } else {
      unlockResource(&consoleLock);
      free(msg);
      msg = join3("invalid input mode '", mode->val, "'");
      return createError(msg, false, ckValueError, ctx);
    }

    clearerr(stdin);

  } else {
    // Default mode using GNU readline
    input = readline(msg);
    if (input) {
      add_history(input);
    }

#ifdef __EMSCRIPTEN__
    char* temp = copyWithoutEnter(input);  // fix bug
    free(input);
    input = temp;
#endif
  }

  unlockResource(&consoleLock);
  free(msg);

  return input ? runtimeText(input, strlen(input), ctx) : NULL;
}

/**
 * Flush stdout buffer
 */
static Node* consFlush(void) {
  fflush(stdout);
  return NULL;
}

/**
 * Move cursor using ANSI escape codes
 */
static Node* consMoveTo(Scope* scope, Context* ctx) {
  Node* x = getParameter(scope, "x", FatNumber, ctx);
  if (IS_FAT_ERROR(x)) {
    return x;
  }

  Node* y = getParameter(scope, "y", FatNumber, ctx);
  if (IS_FAT_ERROR(y)) {
    return y;
  }

#ifdef USE_NCURSES
  if (cursesMode) {
    move(y->num.f, x->num.f);
    return NULL;
  }
#endif

  // Equivalent implementation (without the need for initscr)
  int correctedX = x->num.f < 0 ? 1 : x->num.f + 1;
  int correctedY = y->num.f < 0 ? 1 : y->num.f + 1;
  char buffer[64] = {'\0'};
  snprintf(buffer, sizeof(buffer), "\033[%d;%dH", correctedY, correctedX);
  directPrint(buffer, stdout);
  return NULL;
}

/**
 * Render progress bar, fraction 0 to 1 (overwriting the current line)
 */
static Node* consShowProgress(Scope* scope, Context* ctx) {
  Node* label = getParameter(scope, "label", FatText, ctx);
  if (IS_FAT_ERROR(label)) {
    return label;
  }

  Node* fraction = getParameter(scope, "fraction", FatNumber, ctx);
  if (IS_FAT_ERROR(fraction)) {
    return fraction;
  }

  // Restrict percent to range [0, 100]
  int percent = (int)fmin(fmax(ceil(fraction->num.f * 100), 0), 100);

  // Render progress bar if there is any change to be shown
  if (percent != progressPercentDone) {
    progressPercentDone = percent;

    if (stdoutIsTTY) {
#ifdef __EMSCRIPTEN__
      evalCount = 0;
      emscripten_sleep(JS_IO_LAG);
#endif

      int doneChars = percent * progressLength / 100;

      directPrint("\r", stdout);
      directPrint(label->val, stdout);
      if (percent < 33) {
        directPrint(" " CL_RED "(", stdout);
      } else if (percent < 66) {
        directPrint(" " CL_YEL "(", stdout);
      } else {
        directPrint(" " CL_GRN "(", stdout);
      }

      for (int i = 0; i < doneChars; i++) {
        directPrint("|", stdout);
      }
      if (progressLength - doneChars) {
        directPrint(".", stdout);
        directPrint(CL_WHT, stdout);
      }
      for (int i = 0; i < progressLength - doneChars - 1; i++) {
        directPrint(".", stdout);
      }
      directPrint(") ", stdout);
      char buffer[16] = {'\0'};
      snprintf(buffer, sizeof(buffer), "%3d%%", percent);
      directPrint(buffer, stdout);
      directPrint(CL_RST " ", stdout);
      fflush(stdout);

    } else if (percent == 100) {
      // not using a TTY, so only print once done
      directPrint(label->val, stdout);
      directPrint(" (", stdout);
      for (int i = 0; i < progressLength; i++) {
        directPrint("|", stdout);
      }
      directPrint(") 100%% ", stdout);
    }
  }

  return NULL;
}
