/**
 * @file sdk.c
 * @brief Fry's software development kit utilities
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-16
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../runtime/embed/dispatcher.h"
#include "../runtime/interpreter.h"
#include "../syntax/parser.h"

static const char* LIB_SDK =
  "# fat.sdk - Fry's software development kit utilities\n"
  "\n"
  "## Print abstract syntax tree of node\n"
  "ast = (_): Void -> $ast\n"
  "\n"
  "## Serializes node into JSON-like text\n"
  "stringify = (_): Text -> $stringify\n"
  "\n"
  "## Evaluates text as FatScript program\n"
  "eval = (_: Text): Any -> $eval\n"
  "\n"
  "## Return fry version\n"
  "getVersion = Text <> $version\n"
  "\n"
  "## Print execution context stack trace\n"
  "printStack = (depth: Number): Void -> $stack\n"
  "\n"
  "## Return fry library source code\n"
  "readLib = (ref: Text): Text -> $readLib\n"
  "\n"
  "## Return type of node (as text)\n"
  "typeOf = (_): Text -> $typeOf\n"
  "\n"
  "## Return info about declared types\n"
  "getTypes = List <> $getTypes\n"
  "\n"
  "## Return type definition by name\n"
  "getDef = (name: Text): Type -> $getDef\n"
  "\n"
  "## Get fry build metadata\n"
  "getMeta = Scope <> $getMeta\n"
  "\n"
  "## Set key for obfuscated bundles\n"
  "setKey = (key: Text): Void -> $setKey\n"
  "\n"
  "## Set memory limit (node count)\n"
  "setMem = (n: Number): Void -> $setMem\n"
  "\n"
  "## Run full GC, return elapsed in milliseconds\n"
  "runGC = Number <> $runGC\n"
  "\n"
  "## Single GC iteration, return elapsed in ms\n"
  "quickGC = Number <> $quickGC\n";

/**
 * Print abstract syntax tree of argument
 */
static Node* sdkAst(Scope* scope) {
  Node* node = getValueOf(scope, "_");
  if (!initVisit(__func__)) {
    fatalOut(__FILE__, __func__, MSG_U_REC);
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }
  printNode(node, IND_BASE, false);
  endVisit();
  return NULL;
}

/**
 * Evaluates text as FatScript program
 */
static Node* sdkEval(Scope* scope, Context* ctx) {
  Node* source = getParameter(scope, "_", FatText, ctx);
  if (IS_FAT_ERROR(source)) {
    return source;
  }

  auto_str wrapped = join3("(", source->val, ")");
  Node* result = evalSource(wrapped, ctx);

  return result;
}

/**
 * Serializes node into JSON-like text
 */
static Node* sdkStringify(Scope* scope, Context* ctx) {
  char* result = toJson(getValueOf(scope, "_"), false);
  return runtimeText(result, strlen(result), ctx);
}

/**
 * Provide self reference to prototype or copy of enclosing scope
 */
static Node* sdkSelf(Context* ctx) {
  if (ctx->selfRef) {
    return ctx->selfRef;
  }
  return runtimeCollection(copyCollection(ctx->enclosingI, false, ctx), ctx);
}

/**
 * Provide global scope reference
 */
static Node* sdkRoot(Context* ctx) {
  return runtimeCollection(globalScope, ctx);
}

/**
 * Print execution context stack trace
 */
static Node* sdkStack(Scope* scope, Context* ctx) {
  int depth = STACK_TRACE;

  Node* arg = getValueOf(scope, "depth");
  if (IS_FAT_TYPE(arg, FatNumber)) {
    depth = (int)arg->num.f;
  }

  logStack(ctx, depth);
  return NULL;
}

/**
 * Return fry version
 */
static Node* sdkVersion(Context* ctx) { return runtimeTextDup(getFryV(), ctx); }

/**
 * Print GNU warranty text
 */
static Node* sdkDisclaimer(void) {
  printVersion(VerWarranty);
  return NULL;
}

/**
 * Get the source of an embedded library
 */
static Node* sdkReadLib(Scope* scope, Context* ctx) {
  Node* ref = getParameter(scope, "ref", FatText, ctx);
  if (IS_FAT_ERROR(ref)) {
    return ref;
  }

  auto_str path = replaceAll(ref->val, ".", "/");
  return runtimeTextDup(getEmbeddedLib(path), ctx);
}

/**
 * Get the typename of a node as text
 */
static Node* sdkTypeOf(Scope* scope, Context* ctx) {
  char* typeName = nodeTypeToString(getValueOf(scope, "_"));
  return runtimeText(typeName, strlen(typeName), ctx);
}

/**
 * Return info about declared types
 */
static Node* sdkGetTypes(Context* ctx) {
  Scope* list = createList();
  pushStack(ctx, __func__, NULL, list);
  for (int hash = 0; hash < META_HASH; hash++) {
    for (Type* ck = metaSpace[hash]; ck; ck = ck->next) {
      if ((ck->alias && !ck->isComposite) || ck->def) {
        Scope* info = createScope();
        pushStack(ctx, __func__, NULL, info);
        addToScope(info, "name", runtimeTextDup(ck->name, ctx));
        if (ck->alias) {
          addToScope(info, "type", runtimeTextDup("alias", ctx));
          addToScope(info, "src", runtimeTextDup(ck->alias->name, ctx));
        } else {
          addToScope(info, "type", runtimeTextDup("definition", ctx));
          addToScope(info, "src", runtimeTextDup(ck->def->src, ctx));
        }
        addToList(list, runtimeCollection(info, ctx), ctx);
        popStack(ctx, 1);  // info
      }
    }
  }
  Node* result = runtimeCollection(list, ctx);
  popStack(ctx, 1);  // list
  return result;
}

/**
 * Return type definition by name
 */
static Node* sdkGetDef(Scope* scope, Context* ctx) {
  Node* name = getParameter(scope, "name", FatText, ctx);
  if (IS_FAT_ERROR(name)) {
    return name;
  }

  return evalType(name->val);
}

/**
 * Get fry build metadata
 */
static Node* sdkGetMeta(Context* ctx) {
  auto_str fryMeta = getFryMeta();
  auto_str wrapped = join3("(", fryMeta, ")");
  return evalSource(wrapped, ctx);
}

/**
 * Sets interpreter key to encode/decode obfuscated bundles
 */
static Node* sdkSetKey(Scope* scope, Context* ctx) {
  Node* key = getParameter(scope, "key", FatText, ctx);
  if (IS_FAT_ERROR(key)) {
    return key;
  }

  free(bundleKey);
  bundleKey = strDup(key->val);

  if (debugLogs && !bundleKey[0]) {
    logDebug(__FILE__, __func__, "using default key");
  }

  return NULL;
}

/**
 * Redefine number of maximum allowed memory nodes
 */
static Node* sdkSetMem(Scope* scope, Context* ctx) {
  Node* n = getParameter(scope, "n", FatNumber, ctx);
  if (IS_FAT_ERROR(n)) {
    return n;
  }
  if (n->num.f < 0) {
    return createError(MSG_I_N_ARG, true, ckValueError, ctx);
  }

  memoryLimit = safeValue(n->num.f, MEM_MAX, "memory nodes");
  memoryRedefined = true;  // see also: initFry

  if (debugLogs || showResult) {
    stderrStartLine(CL_YEL);
    fprintf(stderr, MRG_BLT "Memory limit was set to %ld nodes", memoryLimit);
    stderrEndLine();
  }

  return NULL;
}

/**
 * Run GC and return elapsed milliseconds
 */
static Node* sdkRunGC(Context* ctx) {
  if (!pthread_equal(pthread_self(), mainThreadId)) {
    return createError("GC can't run inside worker", true, NULL, ctx);
  }

  return runtimeNumber(fullGC(ctx), ctx);
}

/**
 * Run single GC iteration and return elapsed milliseconds
 */
static Node* sdkQuickGC(Context* ctx) {
  if (!pthread_equal(pthread_self(), mainThreadId)) {
    return createError("GC can't run inside worker", true, NULL, ctx);
  }

  return runtimeNumber(quickGC(ctx), ctx);
}

/**
 * Set keepDotFry flag (keep loaded config in scope)
 */
static Node* sdkKeepDotFry(void) {
  keepDotFry = true;
  return NULL;
}

/**
 * Get real memory usage (traverses all nodes)
 */
static Node* sdkBytesUsage(Context* ctx) {
  Node* result = runtimeNumber(realBytesUsage(), ctx);
  return result;
}

/**
 * Get nodes usage (just returns the node counter)
 */
static Node* sdkNodesUsage(Context* ctx) {
  Node* result = runtimeNumber((double)atomic_load(&activeMemory), ctx);
  return result;
}
