/**
 * @file socket.c
 * @brief TCP socket manipulation
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-22
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../runtime/interpreter.h"
#include "../sdk/server.h"

static const char *LIB_SOCKET =
  "# fat.socket - TCP socket manipulation\n"
  "\n"
  "ClientSocket = (\n"
  "  id: Number  # file descriptor\n"
  "  peer: Text  # connection meta\n"
  "  ssl: Chunk  # session internal\n"
  "\n"
  "  receive = (nBytes: Number): Chunk -> $sockReceive\n"
  "  send = (data: Chunk): Void -> $sockSend\n"
  "  close = Void <> $sockClose\n"
  ")\n"
  "\n"
  "ServerSocket = (\n"
  "  id: Number\n"
  "\n"
  "  accept = (wait: Number) -> $sockAccept\n"
  "  close = Void <> $sockClose\n"
  ")\n"
  "\n"
  "## Connects to a remote server\n"
  "connect = (\n"
  "  addr: Text\n"
  "  port: Number\n"
  "  useSSL: Boolean = false\n"
  "): ClientSocket -> $sockConnect\n"
  "\n"
  "## SSL configuration (client mode)\n"
  "verifySSL ?\?= (enabled: Boolean): Void -> $verifySSL\n"
  "\n"
  "## SSL configuration (server mode)\n"
  "setSSL ?\?= (certPath: Text, keyPath: Text): Void -> $setSSL\n"
  "\n"
  "## Starts a server on the given port\n"
  "bind = (port: Number): ServerSocket -> $sockBind\n";

/**
 * Starts a server on the given port.
 */
static Node *sockBind(Scope *scope, Context *ctx) {
  if (isJailMode) {
    return createError("sockBind " MSG_JAIL, true, ckCallError, ctx);
  }

  Node *port = getParameter(scope, "port", FatNumber, ctx);
  if (IS_FAT_ERROR(port)) {
    return port;
  }

  int serverFd = startListening((uint16_t)port->num.f);

  // Create and return a ServerSocket object
  Scope *serverSock = createScope();
  serverSock->ck = setType("ServerSocket", NULL);
  pushStack(ctx, __func__, NULL, serverSock);

  addToScope(serverSock, "id", runtimeNumber(serverFd, ctx));

  Node *result = runtimeCollection(serverSock, ctx);

  popStack(ctx, 1);
  return result;
}

static Node *createClientSocketScope(int fd, struct addrinfo *info, void *ssl,
                                     Context *ctx) {
  Scope *clientSock = createScope();
  clientSock->ck = setType("ClientSocket", NULL);
  pushStack(ctx, __func__, NULL, clientSock);

  // Store socket file descriptor as 'id'
  addToScope(clientSock, "id", runtimeNumber(fd, ctx));

  // Extract peer info as IP:port
  char peerStr[INET6_ADDRSTRLEN + 8] = {0};
  if (info->ai_family == AF_INET) {
    struct sockaddr_in *peerAddr = (struct sockaddr_in *)info->ai_addr;
    inet_ntop(AF_INET, &peerAddr->sin_addr, peerStr, sizeof(peerStr));
    size_t peerLen = strlen(peerStr);
    uint16_t port = ntohs(peerAddr->sin_port);
    snprintf(peerStr + peerLen, sizeof(peerStr) - peerLen, ":%d", port);
  } else if (info->ai_family == AF_INET6) {
    struct sockaddr_in6 *peerAddr6 = (struct sockaddr_in6 *)info->ai_addr;
    inet_ntop(AF_INET6, &peerAddr6->sin6_addr, peerStr, sizeof(peerStr));
    size_t peerLen = strlen(peerStr);
    uint16_t port = ntohs(peerAddr6->sin6_port);
    snprintf(peerStr + peerLen, sizeof(peerStr) - peerLen, ":%d", port);
  }

  // Store peer info
  addToScope(clientSock, "peer", runtimeTextDup(peerStr, ctx));

  // Store SSL session (pointer)
  if (ssl) {
    char *buff = FRY_ALLOC(sizeof(void *));
    memcpy(buff, &ssl, sizeof(void *));
    addToScope(clientSock, "ssl", runtimeChunk(buff, sizeof(void *), ctx));
  } else {
    addToScope(clientSock, "ssl", NULL);
  }

  Node *result = runtimeCollection(clientSock, ctx);

  popStack(ctx, 1);
  return result;
}

/**
 * Connects to a remote server.
 */
static Node *sockConnect(Scope *scope, Context *ctx) {
  if (isJailMode) {
    return createError("sockConnect " MSG_JAIL, true, ckCallError, ctx);
  }

  Node *addr = getParameter(scope, "addr", FatText, ctx);
  if (IS_FAT_ERROR(addr)) {
    return addr;
  }

  Node *port = getParameter(scope, "port", FatNumber, ctx);
  if (IS_FAT_ERROR(port)) {
    return port;
  }

  Node *useSSL = getParameter(scope, "useSSL", FatBoolean, ctx);
  if (IS_FAT_ERROR(useSSL)) {
    return useSSL;
  }

  char portStr[NUMBER_MAX_LENGTH + 1];
  int ret = snprintf(portStr, NUMBER_MAX_LENGTH + 1, "%.0f", port->num.f);
  if (ret == -1) {
    fatalOut(__FILE__, __func__, MSG_F_F_N);
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }

  // Resolve address
  struct addrinfo hints = {.ai_family = AF_INET, .ai_socktype = SOCK_STREAM};
  struct addrinfo *info = NULL;
  if (getaddrinfo(addr->val, portStr, &hints, &info) != 0) {
    return createError("failed to resolve hostname", true, NULL, ctx);
  }

  // Create socket
#ifdef __APPLE__
  int clientFd = socket(info->ai_family, info->ai_socktype, info->ai_protocol);
#else
  int clientFd = socket(info->ai_family, info->ai_socktype | SOCK_CLOEXEC,
                        info->ai_protocol);
#endif
  if (clientFd < 0) {
    freeaddrinfo(info);
    return createError("failed to create socket", true, NULL, ctx);
  }

  // Connect to server
  if (connect(clientFd, info->ai_addr, info->ai_addrlen) < 0) {
    close(clientFd);
    freeaddrinfo(info);
    return createError("failed to connect", true, NULL, ctx);
  }

// Handle SSL if needed
#ifdef SSL_SUPPORT
  SSL *ssl = NULL;
  if (useSSL->num.b) {
    if (initClientSSL() == 2) {
      close(clientFd);
      freeaddrinfo(info);
      return createError("SSL failed to initialize", true, NULL, ctx);
    }
    ssl = SSL_new(sslClientCtx);
    if (!ssl) {
      close(clientFd);
      freeaddrinfo(info);
      return createError("SSL failed to create session", true, NULL, ctx);
    }
    SSL_set_fd(ssl, clientFd);
    if (SSL_connect(ssl) != 1) {
      SSL_free(ssl);
      close(clientFd);
      freeaddrinfo(info);
      return createError("SSL handshake failed", true, NULL, ctx);
    }
  }
#else
  void *ssl = NULL;
#endif

  Node *clientSocket = createClientSocketScope(clientFd, info, ssl, ctx);

  // Free the addrinfo struct
  freeaddrinfo(info);

  return clientSocket;
}

/**
 * Accept incoming connection.
 */
static Node *sockAccept(Scope *scope, Context *ctx) {
  Node *instance = getInstance(FatScope, ctx);
  if (IS_FAT_ERROR(instance)) {
    return instance;
  }

  Node *sockFd = getParameter(instance->scp, "id", FatNumber, ctx);
  if (IS_FAT_ERROR(sockFd)) {
    return sockFd;
  }

  Node *wait = getParameter(scope, "wait", FatNumber, ctx);
  if (IS_FAT_ERROR(wait)) {
    return wait;
  }

  int serverFd = sockFd->num.f;

  struct sockaddr_storage clientAddr;  // Supports IPv4 and IPv6
  socklen_t clientLen = sizeof(clientAddr);

  int clientFd = sockAcceptTimeout(serverFd, (struct sockaddr *)&clientAddr,
                                   &clientLen, (int)wait->num.f);
  if (clientFd >= 0) {
#ifdef SSL_SUPPORT
    SSL *ssl = NULL;
    if (sslServerCtx) {
      ssl = handshakeSSL(clientFd);
      if (!ssl) {
        return NULL;
      }
    }
#else
    void *ssl = NULL;
#endif

    // Convert sockaddr to addrinfo
    struct addrinfo info = {0};
    info.ai_family = clientAddr.ss_family;
    info.ai_socktype = SOCK_STREAM;
    info.ai_protocol = IPPROTO_TCP;
    info.ai_addr = (struct sockaddr *)&clientAddr;
    info.ai_addrlen = clientLen;

    return createClientSocketScope(clientFd, &info, ssl, ctx);
  }

  if (errno == EAGAIN) {
    return NULL;
  }

  logAlert(__FILE__, __func__, strerror(errno));
  return createError(MSG_U_REQ, true, NULL, ctx);
}

/**
 * Send data over socket connection.
 */
static Node *sockSend(Scope *scope, Context *ctx) {
  Node *instance = getInstance(FatScope, ctx);
  if (IS_FAT_ERROR(instance)) {
    return instance;
  }

  Node *sockFd = getParameter(instance->scp, "id", FatNumber, ctx);
  if (IS_FAT_ERROR(sockFd)) {
    return sockFd;
  }

  void *ssl = NULL;
  Node *sslPtr = getValueOf(instance->scp, "ssl");
  if (sslPtr) {
    if (sslPtr->type != FatChunk || sslPtr->num.s != sizeof(void *)) {
      char *msg = join3(MSG_MISMATCH, GUIDE, "ssl");
      return createError(msg, false, ckTypeError, ctx);
    }
    memcpy(&ssl, sslPtr->val, sslPtr->num.s);
  }

  Node *data = getParameter(scope, "data", FatChunk, ctx);
  if (IS_FAT_ERROR(data)) {
    return data;
  }

  ssize_t bytes = sendMessage((int)sockFd->num.f, ssl, data->val, data->num.s);
  if (bytes < 0) {
    return createError("failed socket send", true, NULL, ctx);
  }

  return NULL;  // success
}

/**
 * Receive data over socket connection.
 */
static Node *sockReceive(Scope *scope, Context *ctx) {
  Node *instance = getInstance(FatScope, ctx);
  if (IS_FAT_ERROR(instance)) {
    return instance;
  }

  Node *sockFd = getParameter(instance->scp, "id", FatNumber, ctx);
  if (IS_FAT_ERROR(sockFd)) {
    return sockFd;
  }

  void *ssl = NULL;
  Node *sslPtr = getValueOf(instance->scp, "ssl");
  if (sslPtr) {
    if (sslPtr->type != FatChunk || sslPtr->num.s != sizeof(void *)) {
      char *msg = join3(MSG_MISMATCH, GUIDE, "ssl");
      return createError(msg, false, ckTypeError, ctx);
    }
    memcpy(&ssl, sslPtr->val, sslPtr->num.s);
  }

  Node *nBytes = getParameter(scope, "nBytes", FatNumber, ctx);
  if (IS_FAT_ERROR(nBytes)) {
    return nBytes;
  }
  if (nBytes->num.f <= 0) {
    return createError("nBytes must be at least 1", true, ckValueError, ctx);
  }

  size_t len = nBytes->num.f;
  char *buff = FRY_ALLOC(len);

  ssize_t bytes = receiveMessage((int)sockFd->num.f, ssl, buff, len);
  if (bytes < 0) {
    free(buff);
    return createError("failed socket receive", true, NULL, ctx);
  }
  if (bytes == 0) {
    free(buff);
    return runtimeChunk(NULL, 0, ctx);
  }

  buff = FRY_REALLOC(buff, bytes);
  return runtimeChunk(buff, bytes, ctx);
}

/**
 * Close socket connection.
 */
static Node *sockClose(Context *ctx) {
  Node *instance = getInstance(FatScope, ctx);
  if (IS_FAT_ERROR(instance)) {
    return instance;
  }

  Node *sockFd = getParameter(instance->scp, "id", FatNumber, ctx);
  if (IS_FAT_ERROR(sockFd)) {
    return sockFd;
  }

  if (instance->scp->ck == getType("ClientSocket")) {
    // Close the connection
    void *ssl = NULL;
#ifdef SSL_SUPPORT
    Node *sslPtr = getValueOf(instance->scp, "ssl");
    if (sslPtr) {
      if (sslPtr->type != FatChunk || sslPtr->num.s != sizeof(void *)) {
        char *msg = join3(MSG_MISMATCH, GUIDE, "ssl");
        return createError(msg, false, ckTypeError, ctx);
      }
      memcpy(&ssl, sslPtr->val, sslPtr->num.s);
    }
#endif
    closeSocket((int)sockFd->num.f, ssl);
  } else {
    close((int)sockFd->num.f);
  }

  // Try to prevent use after close
  Scope *old = instance->scp;
  Scope *closedSocket = createScope();
  closedSocket->ck = old->ck;
  unbindScope(instance);
  bindScope(instance, closedSocket);
  collectScope(old);
  return NULL;
}
