/**
 * @file failure.c
 * @author Antonio Prates <hello@aprates.dev>
 * @brief Error handling and exception management
 * @version 3.4.0
 * @date 2024-10-26
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../runtime/interpreter.h"

static const char* LIB_FAILURE =
  "# fat.failure Error handling and exception management\n"
  "\n"
  "## Apply generic error handler\n"
  "trap = Void <> {\n"
  "  handler = err -> {\n"
  "    msg = '\\n** {err}', fg = 1, code = 1\n"
  "    $endCurses, $flush, $stderr, $stack, $exit\n"
  "  }\n"
  "  $trap\n"
  "}\n"
  "\n"
  "## Set a handler for errors in context\n"
  "trapWith = (handler: Method): Void -> $trap\n"
  "\n"
  "## Unset error handler in context\n"
  "untrap = Void <> $untrap\n"
  "\n"
  "## Continue on error within unsafe method\n"
  "noCrash = (unsafe: Method): Any -> $noCrash\n";

static int getTrapLevel(Context* ctx) {
  for (int i = atomic_load(&ctx->top); i > 0; i--) {
    if (ctx->stack[i].scp == ctx->enclosingO) {
      if (ctx->stack[i - 1].scp != ctx->enclosingO) {
        return i;
      }
    }
  }
  return 0;
}

/**
 * Set a handler for errors in context
 */
static Node* failTrap(Scope* scope, Context* ctx) {
  Node* handler = getParameter(scope, "handler", FatMethod, ctx);
  if (IS_FAT_ERROR(handler)) {
    return handler;
  }

  const int trapLevel = getTrapLevel(ctx);

  if (ctx->stack[trapLevel].trap) {
    return createError("scope already trapped", true, ckAssignError, ctx);
  }

  lockResource(&ctx->lock);
  ctx->stack[trapLevel].trap = handler;
  unlockResource(&ctx->lock);

  return NULL;
}

/**
 * Unset error handler in context
 */
static Node* failUntrap(Context* ctx) {
  lockResource(&ctx->lock);
  ctx->stack[getTrapLevel(ctx)].trap = NULL;
  unlockResource(&ctx->lock);

  return NULL;
}

/**
 * Continue on error within unsafe method
 */
static Node* failNoCrash(Scope* scope, Context* ctx) {
  Node* unsafe = getParameter(scope, "unsafe", FatMethod, ctx);
  if (IS_FAT_ERROR(unsafe)) {
    return unsafe;
  }

  Node call = {.type = FatCall, .head = unsafe, .src = SRC_AUX};
  Node* result = NULL;
  pushStack(ctx, __func__, unsafe, scope);
  if (pthread_equal(pthread_self(), mainThreadId)) {
    bool crashOnErrorMode = crashOnError;
    crashOnError = false;
    result = evalMethodCall(scope, &call, unsafe, ctx);
    crashOnError = crashOnErrorMode;
  } else {
    result = evalMethodCall(scope, &call, unsafe, ctx);
  }
  popStack(ctx, 1);

  return result;
}
