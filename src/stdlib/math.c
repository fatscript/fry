/**
 * @file math.c
 * @brief Mathematical operations and functions
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-16
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../runtime/embed/dispatcher.h"
#include "../runtime/interpreter.h"

static const char* LIB_MATH =
  "# fat.math - Mathematical operations and functions\n"
  "\n"
  "## Constants\n"
  "e      = 2.71828182846  # natural logarithm\n"
  "maxInt = 9007199254740992\n"
  "minInt = -9007199254740992\n"
  "pi     = 3.14159265359  # ratio of circle to its diameter\n"
  "\n"

  "### Basic functions\n"
  "\n"
  "## Return absolute value of x\n"
  "abs = (x: Number): Number -> $abs\n"
  "\n"
  "## Return smallest integer >= x\n"
  "ceil = (x: Number): Number -> $ceil\n"
  "\n"
  "## Return largest integer <= x\n"
  "floor = (x: Number): Number -> $floor\n"
  "\n"
  "## Return true if x is infinity\n"
  "isInf = (x: Number): Boolean -> $isInf\n"
  "\n"
  "## Return true if x is not a number\n"
  "isNaN = (x: Any): Boolean -> $isNaN\n"
  "\n"
  "## Return logarithm (optionally of any base)\n"
  "logN = (x: Number, base: Number = e): Number ->\n"
  "  base == e ? $ln : $ln / logN(base)\n"
  "\n"
  "## Return pseudo-random, where 0 <= n < 1\n"
  "random = Number <> $random\n"
  "\n"
  "## Return the square root of x\n"
  "sqrt = (x: Number): Number -> $sqrt\n"
  "\n"
  "## Return the nearest integer to x\n"
  "round = (x: Number): Number -> $round\n"
  "\n"

  "### Trigonometric functions\n"
  "\n"
  "## Return the sine of x\n"
  "sin = (x: Number): Number -> $sin\n"
  "\n"
  "## Return the cosine of x\n"
  "cos = (x: Number): Number -> $cos\n"
  "\n"
  "## Return the tangent of x\n"
  "tan = (x: Number): Number -> $sin / $cos\n"
  "\n"
  "## Return the arcsine of x\n"
  "asin = (x: Number): Number -> $asin\n"
  "\n"
  "## Return the arccosine of x\n"
  "acos = (x: Number): Number -> $acos\n"
  "\n"
  "## Return the arctangent of x, y\n"
  "atan = (x: Number, y: Number = 1): Number -> $atan\n"
  "\n"
  "## Convert radians to degrees\n"
  "radToDeg = (r: Number): Number -> r * (180 / pi)\n"
  "\n"
  "## Convert degrees to radians\n"
  "degToRad = (d: Number): Number -> d * (pi / 180)\n"
  "\n"

  "### Hyperbolic functions\n"
  "\n"
  "## Return the hyperbolic sine of x\n"
  "sinh = (x: Number): Number -> (exp(x) - exp(-x)) / 2\n"
  "\n"
  "## Return the hyperbolic cosine of x\n"
  "cosh = (x: Number): Number -> (exp(x) + exp(-x)) / 2\n"
  "\n"
  "## Return the hyperbolic tangent of x\n"
  "tanh = (x: Number): Number -> sinh(x) / cosh(x)\n"
  "\n"

  "### Statistical functions\n"
  "\n"
  "## Return the mean of vector\n"
  "mean = (v: List/Number): Number -> $sum / v.{$listSize}\n"
  "\n"
  "## Return the median of vector\n"
  "median = (v: List/Number): Number -> {\n"
  "  s = v.{$listSort}  # sorted v\n"
  "  n = s.{$listSize}  # number of elements\n"
  "  n % 2 == 1 => s((n - 1) / 2)\n"
  "  _          => (s(n / 2 - 1) + s(n / 2)) / 2\n"
  "}\n"
  "\n"
  "## Return the standard deviation of vector\n"
  "sigma = (v: List/Number): Number -> {\n"
  "  m = mean(v)\n"
  "  sd = v @ x -> (x - m) ** 2  # squared diffs from mean\n"
  "  sqrt(sum(sd) / sd.{$listSize})\n"
  "}\n"
  "\n"
  "## Return the variance of vector\n"
  "variance = (v: List/Number): Number -> {\n"
  "  m = mean(v)\n"
  "  sd = v @ x -> (x - m) ** 2  # squared diffs from mean\n"
  "  mean(sd)\n"
  "}\n"
  "\n"
  "## Return maximum value in vector\n"
  "max = (v: List/Number): Number -> $max\n"
  "\n"
  "## Return the minimum value in vector\n"
  "min = (v: List/Number): Number -> $min\n"
  "\n"
  "## Return the sum of vector\n"
  "sum = (v: List/Number): Number -> $sum\n"
  "\n"

  "### Other functions\n"
  "\n"
  "## Return the factorial of x\n"
  "fact = (x: Number, acc = 1): Number -> {\n"
  "  x < 0 => null\n"
  "  x < 1 => acc\n"
  "  _     => fact(x - 1, acc * x)\n"
  "}\n"
  "\n"
  "## Return 'e' raised to the power of x\n"
  "exp = (x: Number): Number -> e ** x\n"
  "\n"
  "## Return the sigmoid of x\n"
  "sigmoid = (x: Number): Number -> 1 / (1 + exp(-x))\n"
  "\n"
  "## Return the ReLU of x\n"
  "relu = (x: Number): Number -> x < 0 ? 0 : x\n";

/**
 * Return result of math operation over x
 */
static Node* mathApply(Scope* scope, Context* ctx, MathType opType) {
  Node* x = getParameter(scope, "x", FatNumber, ctx);
  if (IS_FAT_ERROR(x)) {
    return x;
  }

  switch (opType) {
    case MtAbs:
      return runtimeNumber(fabs(x->num.f), ctx);

    case MtCeil:
      return runtimeNumber(ceil(x->num.f), ctx);

    case MtFloor:
      return runtimeNumber(floor(x->num.f), ctx);

    case MtRound:
      return runtimeNumber(round(x->num.f), ctx);

    case MtIsInf:
      return RUNTIME_BOOLEAN(isinf(x->num.f));

    case MtLog:
      return runtimeNumber(log(x->num.f), ctx);

    case MtSqrt:
      return runtimeNumber(sqrt(x->num.f), ctx);

    case MtSin:
      return runtimeNumber(sin(x->num.f), ctx);

    case MtCos:
      return runtimeNumber(cos(x->num.f), ctx);

    case MtAsin:
      return runtimeNumber(asin(x->num.f), ctx);

    case MtAcos:
      return runtimeNumber(acos(x->num.f), ctx);

    default:
      fatalOut(__FILE__, __func__, "unknown operation");
      exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }
}

/**
 * Return true if x is NaN
 */
static Node* mathIsNan(Scope* scope) {
  const Node* x = getValueOf(scope, "x");
  if (!IS_FAT_TYPE(x, FatNumber)) {
    return trueSingleton;
  }
  return RUNTIME_BOOLEAN(isnan(x->num.f));
}

/**
 * Return pseudo-random number, where 0 <= n < 1
 */
static Node* mathRandom(Context* ctx) {
  double n = 0;
  do {
    n = (double)(rnGen() - 1) / (double)UINT64_MAX;
  } while (floatEq(n, 1));  // too close to 1
  return runtimeNumber(n, ctx);
}

/**
 * Return the arctangent of x
 */
static Node* mathAtan(Scope* scope, Context* ctx) {
  Node* x = getParameter(scope, "x", FatNumber, ctx);
  if (IS_FAT_ERROR(x)) {
    return x;
  }

  Node* y = getParameter(scope, "y", FatNumber, ctx);
  if (IS_FAT_ERROR(y)) {
    return y;
  }

  return runtimeNumber(atan2(y->num.f, x->num.f), ctx);
}

/**
 * Return maximum value in the vector
 */
static Node* mathMax(Scope* scope, Context* ctx) {
  Node* v = getParameter(scope, "v", FatList, ctx);
  if (IS_FAT_ERROR(v)) {
    return v;
  }

  if (!v->scp || !v->scp->entries) {
    return NULL;
  }

  if (v->scp->entries->data->type != FatNumber) {
    return createError(MSG_MISMATCH, true, ckTypeError, ctx);
  }

  Node* x = v->scp->entries->data;
  for (Entry* entry = v->scp->entries->next; entry; entry = entry->next) {
    if (entry->data->num.f > x->num.f) {
      x = entry->data;
    }
  }

  return x;
}

/**
 * Return the minimum value in the vector
 */
static Node* mathMin(Scope* scope, Context* ctx) {
  Node* v = getParameter(scope, "v", FatList, ctx);
  if (IS_FAT_ERROR(v)) {
    return v;
  }

  if (!v->scp || !v->scp->entries) {
    return NULL;
  }

  if (v->scp->entries->data->type != FatNumber) {
    return createError(MSG_MISMATCH, true, ckTypeError, ctx);
  }

  Node* x = v->scp->entries->data;
  for (Entry* entry = v->scp->entries->next; entry; entry = entry->next) {
    if (entry->data->num.f < x->num.f) {
      x = entry->data;
    }
  }

  return x;
}

/**
 * Return the sum of the vector
 */
static Node* mathSum(Scope* scope, Context* ctx) {
  Node* v = getParameter(scope, "v", FatList, ctx);
  if (IS_FAT_ERROR(v)) {
    return v;
  }

  if (!v->scp || !v->scp->entries) {
    return runtimeNumber(0, ctx);
  }

  if (v->scp->entries->data->type != FatNumber) {
    return createError(MSG_MISMATCH, true, ckTypeError, ctx);
  }

  double x = 0;

  for (Entry* entry = v->scp->entries; entry; entry = entry->next) {
    x += entry->data->num.f;
  }

  return runtimeNumber(x, ctx);
}
