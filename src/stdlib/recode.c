/**
 * @file recode.c
 * @brief Data conversion between various formats
 * @note XML attributes and self-closing tags are not supported
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-16
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../runtime/interpreter.h"
#include "../sdk/rle.h"
#include "../syntax/lexer.h"
#include "../syntax/parser.h"

static const char* LIB_RECODE =
  "# fat.recode - Data conversion between various formats\n"
  "\n"
  "_ <- fat.type._\n"
  "\n"
  "## CSV parameters\n"
  "~ csvSeparator = ','\n"
  "~ csvQuote = '\"'\n"
  "\n"
  "## Convert text to void/boolean/number if applicable\n"
  "## (to disable inference, override like: `inferType = -> _`)\n"
  "~ inferType = (val: Text): Any -> $inferType\n"
  "\n"
  "## Minifies FatScript sources\n"
  "minify = (src: Text): Text -> $minify\n"
  "\n"
  "## Encode binary chunk to base64 text\n"
  "toBase64 = (data: Chunk): Text -> $toBase64\n"
  "\n"
  "## Decode base64 text to original format\n"
  "fromBase64 = (b64: Text): Chunk -> $fromBase64\n"
  "\n"
  "## Encode JSON from native types\n"
  "toJSON = (val: Any): Text -> $toJSON\n"
  "\n"
  "## Decode JSON to native types\n"
  "fromJSON = (json: Text): Any -> $fromJSON\n"
  "\n"
  "## Encode text to URL escaped text\n"
  "toURL = (text: Text): Text -> $toURL\n"
  "\n"
  "## Decode URL escaped text to original format\n"
  "fromURL = (url: Text): Text -> $fromURL\n"
  "\n"
  "## Build URL encoded Form Data from scope\n"
  "toFormData = (data: Scope): Text ->\n"
  "  (data @ key -> '{key}={toURL(data(key).toText)}').join('&')\n"
  "\n"
  "## Parse URL encoded Form Data to scope\n"
  "fromFormData = (data: Text): Scope -> data.split('&').reduce(\n"
  "  (acc: Scope, item: Text) -> {\n"
  "    kv = item.split('=')\n"
  "    kv.size == 2 & kv(1).nonEmpty ?\n"
  "      acc[kv(0)] = inferType(fromURL(kv(1)))\n"
  "    acc\n"
  "  }\n"
  "  {}\n"
  ")\n"
  "\n"
  "## Build CSV from rows\n"
  "toCSV = (header: List/Text, rows: List/Scope): Text -> {\n"
  "  csvSeparator = csvSeparator ?? ','\n"
  "  csvQuote = csvQuote ?? '\"'\n"
  "  escapedQuote = csvQuote + csvQuote\n"
  "  wrapField = (value: Any): Text -> {\n"
  "    field = value.toText.trim.replace(csvQuote, escapedQuote)\n"
  "    field.contains(csvQuote) | field.contains(csvSeparator)\n"
  "      ? csvQuote + field + csvQuote\n"
  "      : field\n"
  "  }\n"
  "  body = rows @ row -> {\n"
  "    selection = header @ key -> wrapField(row(key))\n"
  "    selection.join(csvSeparator)\n"
  "  }\n"
  "  ([header.map(wrapField).join(csvSeparator)]+body+['']).join('\\n')\n"
  "}\n"
  "\n"
  "## Parse CSV into rows\n"
  "fromCSV = (csv: Text): List/Scope -> {\n"
  "  csvSeparator = csvSeparator ?? ','\n"
  "  csvQuote = csvQuote ?? '\"'\n"
  "  rows: List/List/Text = $fromCSV\n"
  "  header = rows.head\n"
  "  rows.tail.map(values -> {\n"
  "      ~ row = {}\n"
  "      ..<header.size @ k -> {\n"
  "        row[header(k)] = inferType(values(k).trim)\n"
  "        ()\n"
  "      }\n"
  "      row\n"
  "    }\n"
  "  )\n"
  "}\n"
  "\n"
  "## Compress to RLE schema\n"
  "toRLE = (chunk: Chunk): Chunk -> $toRLE\n"
  "\n"
  "## Decompress from RLE schema\n"
  "fromRLE = (chunk: Chunk): Chunk -> $fromRLE\n"
  "\n"
  "## Create immutable copy\n"
  "toFrostCopy = (item: Any): Any -> {\n"
  "  item <= Scope => {\n"
  "    copy = item.vmap(toFrostCopy)\n"
  "    copy @ -> copy(_).{ $freeze }\n"
  "    copy.seal\n"
  "    copy\n"
  "  }\n"
  "  item <= List => item.map(toFrostCopy)\n"
  "  _            => item\n"
  "}\n"
  "\n"
  "## Create mutable copy\n"
  "toHotCopy = (item: Any): Any -> {\n"
  "  item <= Scope => item.vmap(toHotCopy)\n"
  "  item <= List  => item.map(toHotCopy)\n"
  "  _             => item\n"
  "}\n";

static inline char* tok2val(Token* tok) {
  switch (tok->type) {
    case TkNumb:
      return prettyNumber(NULL, tok->num);
    case TkHuge:
      return join2("0x", tok->val);
    case TkText:
    case TkTemp:
      return unparseText(tok->val, false);
    case TkRaw:
      return unparseText(tok->val, true);
    case TkEmbed:
      if (strcmp(tok->val, "$break") == 0) {
        return strDup("()");
      }
      FALL_THROUGH;
    default:
      return tok->val;
  }
}

/**
 * Convert text to void/boolean/number if applicable
 */
static Node* recodeInferType(Scope* scope, Context* ctx) {
  Node* val = getParameter(scope, "val", FatText, ctx);
  if (IS_FAT_ERROR(val)) {
    return val;
  }

  size_t len = val->num.s;

  if (len == 0) {
    return val;
  }

  if (len == 4) {
    if (strcasecmp(val->val, "null") == 0) {
      return NULL;
    }
    if (strcasecmp(val->val, "true") == 0) {
      return trueSingleton;
    }
  }

  if (len == 5) {
    if (strcasecmp(val->val, "false") == 0) {
      return falseSingleton;
    }
  }

  char ch = *(val->val);
  if (ch == '-' || isDigit(ch)) {
    errno = 0;
    char* failedChunk = NULL;
    double num = strtod(val->val, &failedChunk);
    if (errno || (failedChunk && *failedChunk != '\0')) {
      // bypass
    } else {
      return runtimeNumber(num, ctx);
    }
  }
  return val;
}

/**
 * Minifies FatScript sources (simplified from bundle impl.)
 */
static Node* recodeMinify(Scope* scope, Context* ctx) {
  Node* source = getParameter(scope, "src", FatText, ctx);
  if (IS_FAT_ERROR(source)) {
    return source;
  }

  char* buffer = strDup("");

  Reader* reader = createReader(__func__);
  reader->source = source->val;
  tokenize(reader);

  while (hasContent(reader)) {
    char* prevBuff = buffer;
    Token* tok = reader->current;
    char* val = tok2val(tok);

    const Token* next = advanceTok(__func__, reader);

    if (next) {
      char* separator = "";
      if (isPunctEol(next) || isCommTok(next)) {
        separator = ";";
      } else if (isPunctTok(next, ',')) {
        separator = ",";
      }

      buffer = join3(prevBuff, val, separator);
    } else {
      buffer = join2(prevBuff, val);
    }

    // Cleanup
    if (val != tok->val) {
      free(val);
    }
    if (prevBuff != buffer) {
      free(prevBuff);
    }
  }
  freeReaderAll(reader);

  return runtimeText(buffer, strlen(buffer), ctx);
}

/**
 * Encode binary chunk to base64 text
 */
static Node* recodeToBase64(Scope* scope, Context* ctx) {
  Node* data = getParameter(scope, "data", FatChunk, ctx);
  if (IS_FAT_ERROR(data)) {
    return data;
  }

  // Allocate buffer for encoded output
  char* encoded = FRY_ALLOC(b64EncodeLength(data->num.s) + 1);

  // Base64 encode the original binary data
  size_t len = b64Encode(data->val, data->num.s, encoded);
  encoded[len] = '\0';

  return runtimeText(FRY_REALLOC(encoded, len + 1), len, ctx);
}

/**
 * Decode base64 text to original format
 */
static Node* recodeFromBase64(Scope* scope, Context* ctx) {
  Node* b64 = getParameter(scope, "b64", FatText, ctx);
  if (IS_FAT_ERROR(b64)) {
    return b64;
  }

  // Allocate buffer for decoded output
  size_t decodeLen = b64DecodeLength(b64->num.s);
  char* decoded = FRY_ALLOC(decodeLen);

  // Base64 decode
  size_t len = b64Decode(b64->val, b64->num.s, decoded);
  if (len == 0) {
    free(decoded);
    return runtimeChunk(NULL, 0, ctx);
  }

  return runtimeChunk(FRY_REALLOC(decoded, len), len, ctx);
}

/**
 * Encode JSON from native types
 */
static Node* recodeToJSON(Scope* scope, Context* ctx) {
  char* json = toJson(getValueOf(scope, "val"), true);
  return runtimeText(json, strlen(json), ctx);
}

/**
 * Decode JSON to native types
 */
static Node* recodeFromJSON(Scope* scope, Context* ctx) {
  Node* json = getParameter(scope, "json", FatText, ctx);
  if (IS_FAT_ERROR(json)) {
    return json;
  }

  Reader* jsonReader = createReader(__func__);
  jsonReader->source = json->val;

  // Tokenize with json-aware tokenizer
  if (!tokenizeJson(jsonReader)) {
    freeReaderAll(jsonReader);
    return createError("invalid json", true, ckValueError, ctx);
  }

  // Parse JSON
  Node* jsonAST = parse(jsonReader);

  // Setup execution context
  Scope layer = {0};
  initLayer(&layer);
  pushStack(ctx, __func__, jsonAST, &layer);

  // Eval JSON (shielded)
  lockResource(&memoryLock);
  useCollector = false;
  trackParsed(jsonAST, ctx);
  Node* result = interpret(&layer, jsonAST, ctx);
  useCollector = true;
  unlockResource(&memoryLock);

  // Clean up
  wipeScope(&layer, NULL, false);  // reverse layerScope
  deleteReader(jsonReader);
  popStack(ctx, 1);  // layer

  return result;
}

/**
 * Encode text to URL escaped text
 */
static Node* recodeToURL(Scope* scope, Context* ctx) {
  Node* text = getParameter(scope, "text", FatText, ctx);
  if (IS_FAT_ERROR(text)) {
    return text;
  }

  char* encoded = FRY_ALLOC(text->num.s * 3 + 1);

  char* e = encoded;
  char* v = text->val;

  while (*v) {
    if (isalnum(*v) || *v == '-' || *v == '_' || *v == '.' || *v == '~') {
      *e++ = *v++;
    } else {
      // Using 4 bytes: 3 for the encoded char + 1 for the null-terminator
      snprintf(e, 4, "%%%02X", (Byte)*v++);
      e += 3;
    }
  }
  *e = '\0';

  size_t len = e - encoded;

  return runtimeText(FRY_REALLOC(encoded, len + 1), len, ctx);
}

/**
 * Decode URL escaped text to original format
 */
static Node* recodeFromURL(Scope* scope, Context* ctx) {
  Node* url = getParameter(scope, "url", FatText, ctx);
  if (IS_FAT_ERROR(url)) {
    return url;
  }

  char* decoded = FRY_ALLOC(url->num.s + 1);

  char* d = decoded;
  char* v = url->val;

  while (*v) {
    if (*v == '%' && isxdigit(*(v + 1)) && isxdigit(*(v + 2))) {
      unsigned int value = 0;
      sscanf(v + 1, "%2x", &value);
      *d++ = (char)value;
      v += 3;
    } else if (*v == '+') {
      *d++ = ' ';
      v++;
    } else {
      *d++ = *v++;
    }
  }
  *d = '\0';

  size_t len = d - decoded;

  return runtimeText(FRY_REALLOC(decoded, len + 1), len, ctx);
}

static Node* getLineValues(const char* csv, const size_t len, size_t* offset,
                           const char sepC, const char quoteC, Context* ctx) {
  Scope* fields = createList();

  if (len) {
    bool isInsideQuotes = false;
    size_t capacity = INIT_B_LEN;
    char* buffer = FRY_ALLOC(capacity);  // current field
    size_t j = 0;
    for (size_t i = *offset; i < len; i++) {
      char ch = csv[i];
      (*offset)++;  // increment when consuming character

      if (isInsideQuotes) {
        if (ch == quoteC) {
          if ((i + 1 < len) && csv[i + 1] == quoteC) {
            // Handle escaped quote
            buffer[j++] = quoteC;
            i++;  // skip the next quote
            (*offset)++;
          } else {
            // End of quoted field
            isInsideQuotes = false;
          }
        } else {
          // Add character to current field
          buffer[j++] = ch;
        }
      } else {
        if (ch == quoteC) {
          // Start of quoted field
          isInsideQuotes = true;
        } else if (ch == sepC) {
          // End of field
          buffer = FRY_REALLOC(buffer, j + 1);
          buffer[j] = '\0';
          addToList(fields, runtimeText(buffer, j, ctx), ctx);
          capacity = INIT_B_LEN;
          buffer = FRY_ALLOC(capacity);
          j = 0;
        } else if (ch == '\n') {
          // Non-quoted new-line
          break;
        } else {
          // Add character to current field
          buffer[j++] = ch;
        }
      }
      // Grow buffer if necessary
      if (j >= capacity) {
        capacity *= 2;
        buffer = FRY_REALLOC(buffer, capacity);
      }
    }

    buffer = FRY_REALLOC(buffer, j + 1);
    buffer[j] = '\0';
    addToList(fields, runtimeText(buffer, j, ctx), ctx);
  }

  return runtimeCollection(fields, ctx);
}

static Node* recodeFromCSV(Scope* scope, Context* ctx) {
  Node* csv = getParameter(scope, "csv", FatText, ctx);
  if (IS_FAT_ERROR(csv)) {
    return csv;
  }

  Node* separator = getParameter(scope, "csvSeparator", FatText, ctx);
  if (IS_FAT_ERROR(separator)) {
    return separator;
  }
  if (separator->num.s != 1) {
    return createError(MSG_I_C_ARG, true, ckValueError, ctx);
  }

  Node* quote = getParameter(scope, "csvQuote", FatText, ctx);
  if (IS_FAT_ERROR(quote)) {
    return quote;
  }
  if (quote->num.s != 1) {
    return createError(MSG_I_C_ARG, true, ckValueError, ctx);
  }

  Scope* rows = createList();
  const char sepC = *separator->val;
  const char quoteC = *quote->val;
  size_t len = csv->num.s;
  size_t offset = 0;

  // Remove trailing lines
  while (len > 0) {
    if (csv->val[len - 1] == '\n') {
      len--;
    } else {
      break;
    }
  }

  // Extract CSV values line by line
  while (offset < len) {
    Node* lineValues = getLineValues(csv->val, len, &offset, sepC, quoteC, ctx);
    addToList(rows, lineValues, ctx);
  }

  // List/List/Text (matrix)
  return runtimeCollection(rows, ctx);
}

/**
 * Compress to RLE schema
 */
static Node* recodeToRLE(Scope* scope, Context* ctx) {
  Node* chunk = getParameter(scope, "chunk", FatChunk, ctx);
  if (IS_FAT_ERROR(chunk)) {
    return chunk;
  }

  size_t compSize = 0;
  char* compressed = rleCompress(chunk->val, chunk->num.s, &compSize);

  return runtimeChunk(compressed, compSize, ctx);
}

/**
 * Decompress from RLE schema
 */
static Node* recodeFromRLE(Scope* scope, Context* ctx) {
  Node* chunk = getParameter(scope, "chunk", FatChunk, ctx);
  if (IS_FAT_ERROR(chunk)) {
    return chunk;
  }

  size_t decoSize = 0;
  char* decompressed = rleDecompress(chunk->val, chunk->num.s, &decoSize);

  if (decoSize > 0 && decompressed == NULL) {
    return createError("invalid RLE schema", true, ckValueError, ctx);
  }

  return runtimeChunk(decompressed, decoSize, ctx);
}
