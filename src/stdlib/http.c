/**
 * @file http.c
 * @brief HTTP handling framework
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-22
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../runtime/embed/dispatcher.h"
#include "../runtime/interpreter.h"
#include "../sdk/server.h"

static const char *LIB_HTTP =
  "# fat.http - HTTP handling framework\n"
  "\n"
  "Route = (\n"
  "  path: Text = '*'        # including wildcard\n"
  "  post: Method = null     # HttpRequest -> HttpResponse\n"
  "  get: Method = null      # HttpRequest -> HttpResponse\n"
  "  put: Method = null      # HttpRequest -> HttpResponse\n"
  "  delete: Method = null   # HttpRequest -> HttpResponse\n"
  "  options: Method = null  # HttpRequest -> HttpResponse\n"
  ")\n"
  "\n"
  "HttpRequest = (\n"
  "  method: Text               # POST, GET, PUT, DELETE\n"
  "  path: Text                 # all but query parameters\n"
  "  headers: List/Text         # flat list of headers\n"
  "  params: Scope/Text = null  # query parameters\n"
  "  body: Any = null           # aka request body\n"
  ")\n"
  "\n"
  "HttpResponse = (\n"
  "  status: Number = 200       # HTTP status code\n"
  "  headers: List/Text = null  # flat list of headers\n"
  "  body: Any = null           # aka response body\n"
  ")\n"
  "\n"
  "## CRUD request methods (client mode)\n"
  "setHeaders = (headers: List/Text): Void -> $setHeaders\n"
  "\n"
  "post = (url: Text, body: Any = null, wait: Number = 30000) -> {\n"
  "  method = 'POST'\n"
  "  $request\n"
  "}\n"
  "\n"
  "get = (url: Text, wait: Number = 30000) -> {\n"
  "  method = 'GET'\n"
  "  $request\n"
  "}\n"
  "\n"
  "put = (url: Text, body: Any = null, wait: Number = 30000) -> {\n"
  "  method = 'PUT'\n"
  "  $request\n"
  "}\n"
  "\n"
  "delete = (url: Text, wait: Number = 30000) -> {\n"
  "  method = 'DELETE'\n"
  "  $request\n"
  "}\n"
  "\n"
  "## Auxiliary method (client and server modes)\n"
  "setName = (name: Text): Void -> $setName\n"
  "\n"
  "## SSL configuration (client mode)\n"
  "verifySSL ?\?= (enabled: Boolean): Void -> $verifySSL\n"
  "\n"
  "## SSL configuration (server mode)\n"
  "setSSL ?\?= (certPath: Text, keyPath: Text): Void -> $setSSL\n"
  "\n"
  "## Endpoint provider (server mode)\n"
  "listen = (port: Number, routes: List/Route, msMax: Number = 0): Void -> "
  "$listen\n";

typedef struct CurlHeaderResult {
  Context *ctx;
  Scope *headers;
} CurlHeaderResult;

typedef struct CurlDataResult {
  char *data;
  size_t size;
} CurlDataResult;

typedef CURLcode CurlCode;

typedef CURL CurlSession;

__thread char *softwareName = NULL;

__thread CurlHeaders *headers = NULL;

#ifdef SSL_SUPPORT

static Node *httpSetSSL(Scope *scope, Context *ctx) {
  // Get configuration parameters (file paths)
  Node *certPath = getParameter(scope, "certPath", FatText, ctx);
  if (IS_FAT_ERROR(certPath)) {
    return certPath;
  }

  Node *keyPath = getParameter(scope, "keyPath", FatText, ctx);
  if (IS_FAT_ERROR(keyPath)) {
    return keyPath;
  }

  switch (initServerSSL(certPath->val, keyPath->val)) {
    case 1:
      return createError("SSL context already set", true, ckCallError, ctx);
    case 2:
      return createError("failed to create SSL context", true, NULL, ctx);
    case 3:
      return createError("SSL failed key validation", true, ckValueError, ctx);
    default:
      return NULL;
  }
}
#endif

static char *httpGetName(void) {
  if (!softwareName) {
    softwareName = join2("fry/", getFryV());
  }
  return softwareName;
}

static size_t headerCallback(char *buff, size_t size, size_t count, void *p) {
  CurlHeaderResult *result = (CurlHeaderResult *)p;
  size_t bytes = size * count;

  char *header = strTrim(buff, bytes);
  if (strchr(header, ':')) {
    addToList(result->headers, runtimeText(header, strlen(header), result->ctx),
              result->ctx);
  } else {
    free(header);
  }

  return bytes;
}

static size_t dataCallback(void *buff, size_t size, size_t count, void *p) {
  CurlDataResult *result = (CurlDataResult *)p;
  size_t bytes = size * count;
  result->data = FRY_REALLOC(result->data, result->size + bytes + 1);
  memcpy(result->data + result->size, buff, bytes);
  result->size += bytes;
  result->data[result->size] = '\0';
  return bytes;
}

static inline double getHttpStatus(CurlSession *session) {
  long status = 0;
  curl_easy_getinfo(session, CURLINFO_RESPONSE_CODE, &status);
  return (double)status;
}

/**
 * Try to infer if content is text from headers
 */
static bool contentTypeIndicatesText(Scope *headersList) {
  static const char *contentTypeHeader = "Content-Type:";
  const size_t contentTypeHeaderSize = strlen(contentTypeHeader);

  for (Entry *item = headersList->entries; item; item = item->next) {
    const char *header = item->data->val;

    if (strcasestr(header, contentTypeHeader)) {
      const char *contentType = header + contentTypeHeaderSize;
      while (*contentType == ' ') {
        contentType++;  // skip whitespace
      }

      // Check if the content type indicates text
      if (strcasestr(contentType, "text/") ||
          strcasestr(contentType, "application/x-www-form-urlencoded") ||
          strcasestr(contentType, "application/json") ||
          strcasestr(contentType, "application/xml")) {
        return true;
      }

      break;
    }
  }

  return false;
}

/**
 * Perform http/https request to url, with optional text body
 */
static Node *httpRequest(Scope *scope, Context *ctx) {
  if (isJailMode) {
    return createError("request " MSG_JAIL, true, ckCallError, ctx);
  }

  Node *method = getParameter(scope, "method", FatText, ctx);
  if (IS_FAT_ERROR(method)) {
    return method;
  }

  Node *url = getParameter(scope, "url", FatText, ctx);
  if (IS_FAT_ERROR(url)) {
    return url;
  }

  Node *wait = getParameter(scope, "wait", FatNumber, ctx);
  if (IS_FAT_ERROR(wait)) {
    return wait;
  }

  // Set content header and body (if defined)
  char *bodyVal = NULL;
  size_t bodyLen = 0;
  bool isBodyTransformed = false;
  char *contentType = "Content-Type: text/plain; charset=UTF-8";
  Node *body = getValueOf(scope, "body");
  if (body) {
    switch (body->type) {
      case FatChunk:
        contentType = "Content-Type: application/octet-stream";
        FALL_THROUGH;
      case FatText:
        bodyVal = body->val;
        bodyLen = body->num.s;
        break;

      default:
        contentType = "Content-Type: application/json; charset=UTF-8";
        bodyVal = toJson(body, true);
        bodyLen = strlen(bodyVal);
        isBodyTransformed = true;
    }
  }

  // Set default header (as fallback)
  CurlHeaders *defaultHeader = NULL;
  defaultHeader = curl_slist_append(defaultHeader, contentType);

  CurlSession *session = curl_easy_init();
  if (!session) {
    return createError(MSG_NO_SESS, true, NULL, ctx);
  }

  CurlDataResult result;
  result.size = 0;
  result.data = FRY_CALLOC(1);  // will be grown as needed

  CurlHeaderResult resCtx;
  resCtx.ctx = ctx;
  resCtx.headers = createList();

  curl_easy_setopt(session, CURLOPT_USERAGENT, httpGetName());
#ifndef __EMSCRIPTEN__
  if (!verifySSL) {  // should not require self-signed certificate!
    curl_easy_setopt(session, CURLOPT_SSL_VERIFYPEER, 0L);
    curl_easy_setopt(session, CURLOPT_SSL_VERIFYHOST, 0L);
  }
#endif
  curl_easy_setopt(session, CURLOPT_HTTPHEADER,
                   headers ? headers : defaultHeader);  // headers
  curl_easy_setopt(session, CURLOPT_BUFFERSIZE, 102400L);
  curl_easy_setopt(session, CURLOPT_TCP_KEEPALIVE, true);
  curl_easy_setopt(session, CURLOPT_NOPROGRESS, true);
  curl_easy_setopt(session, CURLOPT_MAXREDIRS, 50L);
  curl_easy_setopt(session, CURLOPT_TIMEOUT, (long)ceil(wait->num.f / 1000));
  curl_easy_setopt(session, CURLOPT_HEADERFUNCTION, headerCallback);
  curl_easy_setopt(session, CURLOPT_HEADERDATA, (void *)&resCtx);
  curl_easy_setopt(session, CURLOPT_WRITEFUNCTION, dataCallback);
  curl_easy_setopt(session, CURLOPT_WRITEDATA, (void *)&result);
  curl_easy_setopt(session, CURLOPT_CUSTOMREQUEST, method->val);  // method
  curl_easy_setopt(session, CURLOPT_URL, url->val);               // url
  if (bodyVal) {                                                  // body
    curl_easy_setopt(session, CURLOPT_POSTFIELDS, bodyVal);
    curl_easy_setopt(session, CURLOPT_POSTFIELDSIZE, bodyLen);
  }

  if (debugLogs) {
    curl_easy_setopt(session, CURLOPT_VERBOSE, 1L);
  }

  CurlCode curlCode = curl_easy_perform(session);
  curl_slist_free_all(defaultHeader);
  if (isBodyTransformed) {
    free(bodyVal);
  }

  if (curlCode != CURLE_OK) {
    char *msg = join3(curl_easy_strerror(curlCode), GUIDE, url->val);
    free(result.data);
    trackScope(resCtx.headers);
    curl_easy_cleanup(session);
    return createError(msg, false, NULL, ctx);
  }

  if (debugLogs) {
    auto_str strSize = ofInt((long)result.size);
    logDebug2(__FILE__, __func__, "bytes", strSize);
  }

  scope = createScope();
  scope->ck = setType("HttpResponse", NULL);
  pushStack(ctx, __func__, url, scope);

  Node *status = runtimeNumber(getHttpStatus(session), ctx);
  addToScope(scope, "status", status);

  Node *resHeaders = runtimeCollection(resCtx.headers, ctx);
  addToScope(scope, "headers", resHeaders);

  if (contentTypeIndicatesText(resCtx.headers)) {
    addToScope(scope, "body", runtimeText(result.data, result.size, ctx));
  } else if (result.size) {
    addToScope(scope, "body", runtimeChunk(result.data, result.size, ctx));
  } else {
    addToScope(scope, "body", NULL);
  }

  Node *httpResponse = runtimeCollection(scope, ctx);

  popStack(ctx, 1);
  curl_easy_cleanup(session);

  return httpResponse;
}

#ifndef __EMSCRIPTEN__

/**
 * Set headers of next requests
 */
static Node *httpSetHeaders(Scope *scope, Context *ctx) {
  Node *list = getParameter(scope, "headers", FatList, ctx);
  if (IS_FAT_ERROR(list)) {
    return list;
  }

  if (!list->scp || !list->scp->size || list->scp->ck != checkText) {
    return createError("invalid headers", true, ckValueError, ctx);
  }

  curl_slist_free_all(headers);
  headers = NULL;

  for (Entry *entry = list->scp->entries; entry; entry = entry->next) {
    headers = curl_slist_append(headers, entry->data->val);
  }

  return NULL;
}

static char *getHeadersEnd(const char *buffer) {
  char *headersEnd = strcasestr(buffer, "\r\n\r\n");
  if (headersEnd) {
    return headersEnd + 4;  // +4 for \r\n\r\n
  }
  return NULL;  // header not found
}

static long parseContentLength(const char *header) {
  const char *value = header + strlen("Content-Length:");
  while (*value == ' ') {
    value++;  // trim leading spaces if necessary
  }
  return strtol(value, NULL, 10);  // convert string to long
}

/**
 * Extract content length from early data
 */
static size_t getEarlyContentLength(const char *buffer) {
  const char *header = strcasestr(buffer, "\r\nContent-Length:");
  if (header) {
    errno = 0;
    long parsedLength = parseContentLength(header + 2);  // +2 for \r\n
    if (!errno && parsedLength >= 0) {
      return (size_t)parsedLength;
    }
  }
  return 0;  // header not found / malformed
}

/**
 * Check if has "Expect: 100-continue" header
 */
static bool getEarlyExpectsContinue(const char *buffer) {
  return strcasestr(buffer, "\r\nExpect: 100-continue") != NULL;
}

/**
 * Set user agent/server name
 */
static Node *httpSetName(Scope *scope, Context *ctx) {
  Node *name = getParameter(scope, "name", FatText, ctx);
  if (IS_FAT_ERROR(name)) {
    return name;
  }

  free(softwareName);
  softwareName = strDup(name->val);
  return NULL;
}

/**
 * Toggle SSL certificate verification
 */
static Node *httpVerifySSL(Scope *scope, Context *ctx) {
  Node *enabled = getParameter(scope, "enabled", FatBoolean, ctx);
  if (IS_FAT_ERROR(enabled)) {
    return enabled;
  }

  verifySSL = enabled->num.b;

  // Toggle client context option too
#ifdef SSL_SUPPORT
  if (sslClientCtx) {
    if (verifySSL) {
      // Require certificate verification
      SSL_CTX_set_verify(sslClientCtx, SSL_VERIFY_PEER, NULL);
    } else {
      // Explicitly disable verification
      SSL_CTX_set_verify(sslClientCtx, SSL_VERIFY_NONE, NULL);
    }
  }
#endif
  return NULL;
}

#define TIME_BUFF_LEN 32

static void getCurrentTime(char *buff) {
  struct tm date = {0};
  time_t utc = time(NULL);
  gmtime_r(&utc, &date);
  strftime(buff, TIME_BUFF_LEN, "%a, %d %b %Y %H:%M:%S %Z", &date);
}

static void respond(double tick, int fd, void *ssl, int code, Scope *resHeaders,
                    Node *body) {
  char *title = "OK";
  char *defaultMsg = "The request has been successfully processed.";
  char *additional = "";

  switch (code) {
    // Success
    case 200:
      // default values
      break;

    case 201:  // no automatic trigger
      title = "Created";
      defaultMsg = "The request successfully created a new resource.";
      break;

    case 202:  // no automatic trigger
      title = "Accepted";
      defaultMsg = "The request has been accepted, but still processing.";
      break;

    case 204:
      // The request was successful, but no content to return
      title = "No Content";
      defaultMsg = NULL;  // ensure nothing is returned
      break;

    case 205:  // no automatic trigger
      title = "Reset Content";
      defaultMsg = "The client shall reset the view of the content.";
      break;

    // Redirection
    case 301:  // no automatic trigger, use with Location header
      title = "Moved Permanently";
      defaultMsg = "This resource has been permanently moved.";
      break;

    // Client Error
    case 400:
      title = "Bad Request";
      defaultMsg = "Malformed request could not be understood by the server.";
      break;

    case 401:  // no automatic trigger
      title = "Unauthorized";
      defaultMsg = "The request requires user authentication.";
      break;

    case 403:  // no automatic trigger
      title = "Forbidden";
      defaultMsg = "Client not allowed to access the requested resource.";
      break;

    case 404:
      title = "Not Found";
      defaultMsg = "The resource could not be found on this server.";
      break;

    case 405:
      title = "Method Not Allowed";
      defaultMsg = "The requested method is not allowed on this server.";
      additional = "Allow: POST, GET, PUT, DELETE\r\n";
      break;

    case 408:
      title = "Request Timeout";
      defaultMsg = "Server aborted reading the request.";
      break;

    // Sever Error
    case 501:
      title = "Not Implemented";
      defaultMsg = "No server-side handler for this method.";
      break;

    default:
      code = 500;
      title = "Internal Server Error";
      defaultMsg = "An error happened while processing the request.";
  }

  // Obtain current date and time
  char strDate[TIME_BUFF_LEN] = {'\0'};
  getCurrentTime(strDate);

  // Prepare content type header
  char *strHeaders = "Content-Type: text/plain; charset=UTF-8";
  if (resHeaders) {
    strHeaders = joinListItems(resHeaders, "\r\n");
  } else if (body && body->type == FatChunk) {
    strHeaders = "Content-Type: application/octet-stream";
  } else if (body && body->type != FatText) {
    strHeaders = "Content-Type: application/json; charset=UTF-8";
  }

  // Create the full response header string using asprintf
  char *headerBuff = NULL;
  int headerLen =
    asprintf(&headerBuff,
             "HTTP/1.1 %d %s\r\n"      // http code and title
             "Date: %s\r\n"            // strDate
             "Server: %s\r\n"          // softwareName
             "%s%s\r\n"                // other headers
             "Connection: close\r\n",  // no persistent connections
             code, title, strDate, httpGetName(), additional, strHeaders);
  if (resHeaders) {
    free(strHeaders);  // only free if headers are provided by user
  }
  if (headerLen == -1) {
    fatalOut(__FILE__, __func__, MSG_OOM);
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }
  ssize_t bytesWritten = sendMessage(fd, ssl, headerBuff, headerLen);
  free(headerBuff);
  if (bytesWritten < 0) {
    closeSocket(fd, ssl);
    logError(__FILE__, __func__, MSG_F_RESP);
    return;
  }

  // Transmit content length and body (if any)
  char *bodyVal = NULL;
  size_t bodyLen = 0;
  bool isBodyTransformed = false;
  if (body) {
    switch (body->type) {
      case FatChunk:
      case FatText:
        bodyVal = body->val;
        bodyLen = body->num.s;
        break;

      default:
        bodyVal = toJson(body, true);
        bodyLen = strlen(bodyVal);
        isBodyTransformed = true;
    }
  } else {
    bodyVal = defaultMsg;
    bodyLen = bodyVal ? strlen(bodyVal) : 0;
  }

  if (bodyLen) {
    // Send length header
    headerLen = asprintf(&headerBuff, "Content-Length: %zu\r\n\r\n", bodyLen);
    if (headerLen == -1) {
      fatalOut(__FILE__, __func__, MSG_OOM);
      exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
    }
    bytesWritten = sendMessage(fd, ssl, headerBuff, headerLen);
    free(headerBuff);
    if (bytesWritten < 0) {
      closeSocket(fd, ssl);
      logError(__FILE__, __func__, MSG_F_RESP);
      return;
    }

    // Send body content
    bytesWritten = sendMessage(fd, ssl, bodyVal, bodyLen);
    if (isBodyTransformed) {
      free(bodyVal);
    }
    if (bytesWritten < 0) {
      closeSocket(fd, ssl);
      logError(__FILE__, __func__, MSG_F_RESP);
      return;
    }
  }

  // Close the connection
  closeSocket(fd, ssl);

  // Log response
  double ms = getCurrentMs(CLOCK_MONOTONIC) - tick;
  stderrStartLine(code < 300 ? CL_GRN : code < 500 ? CL_YEL : CL_RED);
  fprintf(stderr, MRG_BLT "%s > %d %s, %.0f ms", strDate, code, title, ms);
  stderrEndLine();
}

static bool matchRoute(const char *path, const char *uri) {
  if (*path == '*') {
    if (*(path + 1) == '\0') {
      return true;
    }

    for (; *uri != '\0'; uri++) {
      if (matchRoute(path + 1, uri)) {
        return true;
      }
    }
    return false;
  }

  if (*path == '\0' && *uri == '?') {
    return true;
  }

  if (*path == *uri) {
    return *path == '\0' ? true : matchRoute(path + 1, uri + 1);
  }

  return false;
}

static Scope *findRoute(Scope *routes, const char *uri) {
  Scope *route = NULL;

  // Store configs
  const bool modeDebug = debugLogs;
  debugLogs = false;  // don't log this insanity

  for (Entry *entry = routes->entries; entry; entry = entry->next) {
    if (entry->data && entry->data->scp) {
      Scope *maybeRoute = entry->data->scp;
      const Node *path = getValueOf(maybeRoute, "path");
      if (IS_FAT_TYPE(path, FatText)) {
        if (matchRoute(path->val, uri)) {
          route = maybeRoute;
          break;
        }
      }
    }
  }

  // Restore configs
  debugLogs = modeDebug;

  return route;
}

static Scope *getQueryParams(const char *str, Context *ctx) {
  char *queryString = strchr(str, '?');
  if (!queryString) {
    return NULL;
  }

  if (debugLogs) {
    logDebug(__FILE__, __func__, queryString);
  }

  Scope *result = createScope();
  result->ck = checkScope;
  pushStack(ctx, __func__, NULL, result);

  char *ptr1 = NULL;
  char *ptr2 = NULL;
  char *token = strtok_r(queryString + 1, "&", &ptr1);
  while (token) {
    const char *key = strtok_r(token, "=", &ptr2);
    const char *value = strtok_r(NULL, "=", &ptr2);

    // First occurrence of key "wins"
    if (key && value && !getValueOf(result, key)) {
      addToScope(result, key, runtimeTextDup(value, ctx));
    }
    token = strtok_r(NULL, "&", &ptr1);
  }
  queryString[0] = '\0';  // hide query params in path value

  popStack(ctx, 1);

  return result;
}

static Node *handle(Scope *scope, const char *method, const char *uri,
                    Scope *reqHeaders, const char *body, long bodyLen,
                    Node *handler, Context *ctx) {
  if (debugLogs) {
    logDebug2(__FILE__, "processing", method, uri);
  }

  Scope *requestScope = createScope();
  requestScope->ck = setType("HttpRequest", NULL);
  pushStack(ctx, __func__, NULL, requestScope);

  addToScope(requestScope, "method", runtimeTextDup(method, ctx));

  Scope *args = getQueryParams(uri, ctx);
  addToScope(requestScope, "path", runtimeTextDup(uri, ctx));
  if (args) {
    addToScope(requestScope, "params", runtimeCollection(args, ctx));
  }

  addToScope(requestScope, "headers", runtimeCollection(reqHeaders, ctx));

  if (body && bodyLen >= 0) {
    char *bodyVal = copyChunk(body, bodyLen);
    if (contentTypeIndicatesText(reqHeaders)) {
      addToScope(requestScope, "body", runtimeText(bodyVal, bodyLen, ctx));
    } else {
      addToScope(requestScope, "body", runtimeChunk(bodyVal, bodyLen, ctx));
    }
  }

  Node call = {.type = FatCall, .src = SRC_AUX};
  call.body = runtimeCollection(requestScope, ctx);
  popStack(ctx, 1);
  return evalMethodCall(scope, &call, handler, ctx);
}

static void logHttpListening(void) {
  stderrStartLine(CL_GRN);
#ifdef SSL_SUPPORT
  if (sslServerCtx) {
    fprintf(stderr, MRG_BLT "Listening at https://localhost:%" PRIu16,
            listenedPort);
  } else {
#endif
    fprintf(stderr, MRG_BLT "Listening at http://localhost:%" PRIu16,
            listenedPort);
#ifdef SSL_SUPPORT
  }
#endif
  fputs(" - use Ctrl+C to stop", stderr);
  stderrEndLine();
}

typedef enum {
  HttpNotAllowed,
  HttpPost,
  HttpGet,
  HttpPut,
  HttpDelete,
  HttpOptions,
} HttpMethod;

static HttpMethod parseMethod(const char *method) {
  return !method                      ? HttpNotAllowed
         : !strcmp(method, "POST")    ? HttpPost
         : !strcmp(method, "GET")     ? HttpGet
         : !strcmp(method, "PUT")     ? HttpPut
         : !strcmp(method, "DELETE")  ? HttpDelete
         : !strcmp(method, "OPTIONS") ? HttpOptions
                                      : HttpNotAllowed;
}

#define INITIAL_BUF_SIZE 2048  // read buffer min size (2K)
#define READ_CHUNK_SIZE 512    // chunk size for read iterations
#define WAIT_FOR_MORE_SEC 5    // max wait for more data (seconds)

static int waitForMoreData(int fd, bool isExpectingResponse) {
  fd_set readfds = {0};
  struct timeval tv = {0};

  FD_ZERO(&readfds);
  FD_SET(fd, &readfds);

  tv.tv_sec = WAIT_FOR_MORE_SEC;
  tv.tv_usec = 0;

  if (isExpectingResponse) {
    tv.tv_sec *= 2;  // wait for roundtrip
  }

  return select(fd + 1, &readfds, NULL, NULL, &tv);
}

/**
 * Endpoint provider (server mode)
 */
static Node *httpListen(Scope *scope, Context *ctx) {
  Node *port = getParameter(scope, "port", FatNumber, ctx);
  if (IS_FAT_ERROR(port)) {
    return port;
  }

  Node *routes = getParameter(scope, "routes", FatList, ctx);
  if (IS_FAT_ERROR(routes)) {
    return routes;
  }
  if (!routes->scp->entries || routes->scp->entries->data->type != FatScope) {
    return createError("invalid routes", true, ckValueError, ctx);
  }

  Node *msMax = getParameter(scope, "msMax", FatNumber, ctx);
  if (IS_FAT_ERROR(msMax)) {
    return msMax;
  }

  int serverFd = startListening((uint16_t)port->num.f);
  logHttpListening();

  for (size_t errCount = 0; errCount < MAX_LISTEN_RETRY; errCount++) {
    struct sockaddr_in client = {0};
    socklen_t clientLen = sizeof(client);

    // Wait for next request
#ifdef __APPLE__
    int clientFd = accept(serverFd, (struct sockaddr *)&client, &clientLen);
#else
    int clientFd =
      accept4(serverFd, (struct sockaddr *)&client, &clientLen, SOCK_CLOEXEC);
#endif
    if (clientFd < 0) {
      logAlert(__FILE__, __func__, strerror(errno));
      msSleep(WAIT_LISTEN_RETRY);
      continue;
    }
    errCount = 0;

    // Track response time and timeout mark
    double tick = getCurrentMs(CLOCK_MONOTONIC);
    double timeout = msMax->num.f > 0.0 ? tick + msMax->num.f : 0.0;

#ifdef SSL_SUPPORT
    SSL *ssl = NULL;
    if (sslServerCtx) {
      ssl = handshakeSSL(clientFd);
      if (!ssl) {
        continue;  // start over
      }
    }
#else
    void *ssl = NULL;
#endif

    // Allocate receive buffer
    size_t bufferSize = INITIAL_BUF_SIZE + 1;  // leave space for '\0'
    char *buffer = FRY_ALLOC(bufferSize);

    ssize_t bytes = 0;
    size_t readTotal = 0;
    ssize_t bytesAvailable = 0;
    size_t headerLen = 0;
    size_t bodyLen = 0;
    char *bodyStart = NULL;

    do {
      // Check for timeout
      if (timeout > 0 && getCurrentMs(CLOCK_MONOTONIC) > timeout) {
        respond(tick, clientFd, ssl, 408, NULL, NULL);
        clientFd = -1;  // respond closes connection (always)
        bytes = -1;
        break;
      }

      // Adjust receive buffer if necessary
      if (readTotal + READ_CHUNK_SIZE > bufferSize) {
        bufferSize = bufferSize * 2 - 1;
        buffer = FRY_REALLOC(buffer, bufferSize);
      }

      bytes =
        receiveMessage(clientFd, ssl, buffer + readTotal, READ_CHUNK_SIZE);
      if (bytes < 0) {
        break;
      }

      // Success
      readTotal += bytes;

      if (!bodyStart) {
        buffer[readTotal] = '\0';  // ensure null-termination
        bodyStart = getHeadersEnd(buffer);

        if (bodyStart) {
          // Set header length
          headerLen = bodyStart - buffer;

          // Check "Content-Length" header
          bodyLen = getEarlyContentLength(buffer);

          // Check for "Expect: 100-continue" header
          if (getEarlyExpectsContinue(buffer)) {
            const char *command = "HTTP/1.1 100 Continue\r\n\r\n";

            if (sendMessage(clientFd, ssl, command, strlen(command)) < 0) {
              logError(__FILE__, __func__, MSG_F_RESP);
              bytes = -1;
              break;
            }

            // Obtain current date and time
            char strDate[TIME_BUFF_LEN] = {'\0'};
            getCurrentTime(strDate);

            // Log response
            stderrStartLine(CL_GRA);
            fprintf(stderr, MRG_BLT "%s > Receiving long content...", strDate);
            stderrEndLine();

            bytesAvailable = waitForMoreData(clientFd, true);
            if (bytesAvailable <= 0) {
              respond(tick, clientFd, ssl, 400, NULL, NULL);
              clientFd = -1;  // respond closes connection (always)
              bytes = -1;
              break;
            }

            continue;
          }
        }
      }

      // Check if more data is waiting to be read
      if (headerLen && bodyLen) {
        if (readTotal - headerLen >= bodyLen) {
          break;  // we are done!
        }

        bytesAvailable = waitForMoreData(clientFd, false);
        if (bytesAvailable <= 0) {
          respond(tick, clientFd, ssl, 400, NULL, NULL);
          clientFd = -1;  // respond closes connection (always)
          bytes = -1;
          break;
        }

        continue;
      }

      if (ioctl(clientFd, FIONREAD, &bytesAvailable) == -1) {
        logAlert(__FILE__, __func__, strerror(errno));
        bytes = -1;
        break;
      }

    } while (bytesAvailable > 0);

    if (bytes <= 0 || readTotal <= 0) {
      if (bytes == 0) {
        logAlert(__FILE__, __func__, "connection closed by peer");
      }
      closeSocket(clientFd, ssl);  // close the socket without response
      free(buffer);
      continue;
    }

    buffer[readTotal] = '\0';  // ensure null-termination

    // Parse request method
    char *ptr = NULL;
    char *method = strtok_r(buffer, " ", &ptr);
    HttpMethod methodType = parseMethod(method);
    if (methodType == HttpNotAllowed) {
      respond(tick, clientFd, ssl, 405, NULL, NULL);
      free(buffer);
      continue;
    }

    // Parse request uri and protocol
    char *uri = strtok_r(NULL, " ", &ptr);
    if (!uri || !strtok_r(NULL, "\n", &ptr)) {        // ignore protocol version
      respond(tick, clientFd, ssl, 400, NULL, NULL);  // bad request
      free(buffer);
      continue;
    }

    // Log request reception
    char strDate[TIME_BUFF_LEN] = {'\0'};
    getCurrentTime(strDate);
    stderrStartLine(CL_BLU);
    fprintf(stderr, MRG_BLT "%s < %s %s", strDate, method, uri);
    stderrEndLine();

    // Parse request headers
    Scope *reqHeaders = createList();
    pushStack(ctx, __func__, routes, reqHeaders);
    for (char *rawHeader = strtok_r(NULL, "\n", &ptr); rawHeader;
         rawHeader = strtok_r(NULL, "\n", &ptr)) {
      char *header = strTrim(rawHeader, strlen(rawHeader));
      if (*header) {
        addToList(reqHeaders, runtimeText(header, strlen(header), ctx), ctx);
      } else {
        free(header);
        break;
      }
    }

    // Double-check if we received all bytes supposed to on the body
    if (bodyLen > readTotal - (ptr - buffer)) {
      respond(tick, clientFd, ssl, 400, NULL, NULL);  // bad request
      free(buffer);
      continue;
    }

    // Retrieve route handler
    Scope *route = findRoute(routes->scp, uri);

    if (route) {
      Node *handler = NULL;
      switch (methodType) {
        case HttpPost:
          handler = getValueOf(route, "post");
          break;

        case HttpGet:
          handler = getValueOf(route, "get");
          break;

        case HttpPut:
          handler = getValueOf(route, "put");
          break;

        case HttpDelete:
          handler = getValueOf(route, "delete");
          break;

        case HttpOptions:
          handler = getValueOf(route, "options");

          // Use default OPTIONS handler if none is defined for this route
          if (!handler) {
            Scope options = {.isList = true};
            initLayer(&options);

            // Allow-Origin Header
            const char *defaultAllowOrigin = "Access-Control-Allow-Origin: *";
            addToList(&options, runtimeTextDup(defaultAllowOrigin, ctx), ctx);

            // Allow-Headers Header
            const char *defaultAllowHeaders =
              "Access-Control-Allow-Headers: Accept, Accept-Language, "
              "Authorization, Content-Type, Cookie, Origin, User-Agent";
            addToList(&options, runtimeTextDup(defaultAllowHeaders, ctx), ctx);

            // Allow-Methods Header
            char *availableMethods[6] = {0};
            int index = 0;
            availableMethods[index++] = "Access-Control-Allow-Methods: OPTIONS";
            if (getValueOf(route, "post")) {
              availableMethods[index++] = "POST";
            }
            if (getValueOf(route, "get")) {
              availableMethods[index++] = "GET";
            }
            if (getValueOf(route, "put")) {
              availableMethods[index++] = "PUT";
            }
            if (getValueOf(route, "delete")) {
              availableMethods[index++] = "DELETE";
            }
            char *meths = joinSep(availableMethods, ", ");
            addToList(&options, runtimeText(meths, strlen(meths), ctx), ctx);

            // Send the response with a 204 status (No Content)
            respond(tick, clientFd, ssl, 204, &options, NULL);

            // Clean up
            wipeScope(&options, false, false);
            continue;
          }
          break;

        default:
          break;
      }

      if (IS_FAT_TYPE(handler, FatMethod)) {
        Node *res =
          handle(scope, method, uri, reqHeaders, ptr, bodyLen, handler, ctx);

        if (IS_FAT_TYPE(res, FatScope) && res->scp) {
          Node *status = getValueOf(res->scp, "status");
          int code = IS_FAT_TYPE(status, FatNumber) ? (int)status->num.f : 200;

          Node *resHeaders = getValueOf(res->scp, "headers");
          if (!IS_FAT_TYPE(resHeaders, FatList)) {
            resHeaders = NULL;
          }

          respond(tick, clientFd, ssl, code,
                  resHeaders ? resHeaders->scp : NULL,
                  getValueOf(res->scp, "body"));  // route handler result

        } else {
          respond(tick, clientFd, ssl, 500, NULL, NULL);
        }

        UNLOCK_NODE(res);  // allow GC
      } else {
        respond(tick, clientFd, ssl, 501, NULL, NULL);  // not implemented
      }
    } else {
      respond(tick, clientFd, ssl, 404, NULL, NULL);  // not found
    }

    popStack(ctx, 1);        // reqHeaders
    trackScope(reqHeaders);  // ensure it's tracked
    free(buffer);
  }

  fatalOut(__FILE__, __func__, MSG_U_REQ);
  exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
}

#endif
