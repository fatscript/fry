/**
 * @file enigma.c
 * @brief Cryptography, hash and UUID methods
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-22
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../runtime/interpreter.h"

static const char* LIB_ENIGMA =
  "# fat.enigma - Cryptography, hash and UUID methods\n"
  "\n"
  "## Get 32-bit hash of text\n"
  "getHash = (key: Text): Number -> $getHash\n"
  "\n"
  "## Generate a UUID (version 4)\n"
  "genUUID = Text <> $genUUID\n"
  "\n"
  "## Generate a random key of specified length\n"
  "genKey = (len: Number): Text -> $genKey\n"
  "\n"
  "## Simple 32-bit key derivation function (unsafe)\n"
  "derive = (\n"
  "  secret: Text\n"
  "  salt: Text = ''\n"
  "  iter: Number = 1  # number of iterations\n"
  "): Text -> $derive\n"
  "\n"
  "## Encrypt msg using key (unsafe)\n"
  "encrypt = (msg: Text, key: Text = null): Text -> $encrypt\n"
  "\n"
  "## Decrypt msg using key (unsafe)\n"
  "decrypt = (msg: Text, key: Text = null): Text -> $decrypt\n"
  "\n"
  "### Secure OpenSSL-based functions\n"
  "\n"
  "## Get secure hash using OpenSSL MDF\n"
  "digest = (data: Chunk, algo: Text = 'sha256'): HugeInt -> $digest\n"
  "\n"
  "## Generate cryptographically secure randomness\n"
  "bytes = (len: Number): Chunk -> $bytes\n"
  "\n"
  "## Derive key using OpenSSL PBKDF2\n"
  "pbkdf2 = (\n"
  "  secret: Text\n"
  "  salt: Text = ''\n"
  "  iter: Number = 1234    # number of iterations\n"
  "  algo: Text = 'sha256'  # SHA 1, 2, 3 families\n"
  "): HugeInt -> $pbkdf2\n"
  "\n"
  "## Computes auth-code using OpenSSL HMAC\n"
  "hmac = (data: Chunk, key: Chunk, algo: Text = 'sha256'): HugeInt -> "
  "$hmac\n"
  "\n"
  "## Encrypt data with key using OpenSSL AES-256-CCM\n"
  "encryptAES = (data: Chunk, key: Chunk): Chunk -> $encryptAES\n"
  "\n"
  "## Decrypt data with key using OpenSSL AES-256-CCM\n"
  "decryptAES = (data: Chunk, key: Chunk): Chunk -> $decryptAES\n";

/**
 * Build a 32-bit hash of msg via jesteress algorithm
 */
static Node* enigmaGetHash(Scope* scope, Context* ctx) {
  Node* key = getParameter(scope, "key", FatText, ctx);
  if (IS_FAT_ERROR(key)) {
    return key;
  }

  return runtimeNumber(jesteressHash32(key->val, key->num.s), ctx);
}

/**
 * Generate a UUID (version 4)
 */
static Node* enigmaGenUUID(Context* ctx) {
  return runtimeText(generateUuid(), 36, ctx);
}

/**
 * Generate a random key of specified length
 */
static Node* enigmaGenKey(Scope* scope, Context* ctx) {
  Node* len = getParameter(scope, "len", FatNumber, ctx);
  if (IS_FAT_ERROR(len)) {
    return len;
  }

  size_t keyLen = (size_t)len->num.f;
  return runtimeText(generateKey(keyLen), keyLen, ctx);
}

/**
 * Simple 32-bit key derivation function (unsafe)
 */
static Node* enigmaDerive(Scope* scope, Context* ctx) {
  Node* secret = getParameter(scope, "secret", FatText, ctx);
  if (IS_FAT_ERROR(secret)) {
    return secret;
  }

  Node* salt = getParameter(scope, "salt", FatText, ctx);
  if (IS_FAT_ERROR(salt)) {
    return salt;
  }

  Node* iter = getParameter(scope, "iter", FatNumber, ctx);
  if (IS_FAT_ERROR(iter)) {
    return iter;
  }
  int iterations = (int)iter->num.f;  // prevent integer overflow
  if (iterations < 1.0) {
    return createError(MSG_I_N_ARG, true, ckValueError, ctx);
  }

  auto_str combined = join2(salt->val, secret->val);
  size_t combinedLen = salt->num.s + secret->num.s;

  char* derived = deriveKey(combined, combinedLen, iterations);
  return runtimeText(derived, DERIVED_SIZE, ctx);
}

/**
 * Encode msg using secret key to url-safe characters
 */
static Node* enigmaEncrypt(Scope* scope, Context* ctx) {
  Node* msg = getParameter(scope, "msg", FatText, ctx);
  if (IS_FAT_ERROR(msg)) {
    return msg;
  }

  // optional parameter
  Node* key = getValueOf(scope, "key");
  const char* keyVal = IS_FAT_TYPE(key, FatText) ? key->val : NULL;

  char* result = xorEncode(keyVal, msg->val);
  return result ? runtimeText(result, strlen(result), ctx) : NULL;
}

/**
 * Decode msg using secret key to url-safe characters
 */
static Node* enigmaDecrypt(Scope* scope, Context* ctx) {
  Node* msg = getParameter(scope, "msg", FatText, ctx);
  if (IS_FAT_ERROR(msg)) {
    return msg;
  }

  // optional parameter
  Node* key = getValueOf(scope, "key");
  const char* keyVal = IS_FAT_TYPE(key, FatText) ? key->val : NULL;

  char* result = xorDecode(keyVal, msg->val);
  return result ? runtimeText(result, strlen(result), ctx) : NULL;
}

#ifdef SSL_SUPPORT

static const EVP_MD* getHashFunction(const char* algo) {
  return !strcmp(algo, "sha1")       ? EVP_sha1()
         : !strcmp(algo, "sha224")   ? EVP_sha224()
         : !strcmp(algo, "sha256")   ? EVP_sha256()
         : !strcmp(algo, "sha384")   ? EVP_sha384()
         : !strcmp(algo, "sha512")   ? EVP_sha512()
         : !strcmp(algo, "sha3_224") ? EVP_sha3_224()
         : !strcmp(algo, "sha3_256") ? EVP_sha3_256()
         : !strcmp(algo, "sha3_384") ? EVP_sha3_384()
         : !strcmp(algo, "sha3_512") ? EVP_sha3_512()
                                     : NULL;
}

static int getRecommendedKeySize(const char* algo) {
  return !strcmp(algo, "sha1")       ? 160 / 8
         : !strcmp(algo, "sha224")   ? 224 / 8
         : !strcmp(algo, "sha256")   ? 256 / 8
         : !strcmp(algo, "sha384")   ? 384 / 8
         : !strcmp(algo, "sha512")   ? 512 / 8
         : !strcmp(algo, "sha3_224") ? 224 / 8
         : !strcmp(algo, "sha3_256") ? 256 / 8
         : !strcmp(algo, "sha3_384") ? 384 / 8
         : !strcmp(algo, "sha3_512") ? 512 / 8
                                     : -1;
}

/**
 * Copy key into HugeInt
 */
static Node* keyAsHugeInt(unsigned char* key, int keySize, Context* ctx) {
  HugeInt hugeInt = {0};
  bytesToHuge(key, keySize, hugeInt);
  return runtimeHuge(hugeInt, ctx);
}

/**
 * Get secure hash using OpenSSL MDF
 */
static Node* enigmaDigest(Scope* scope, Context* ctx) {
  Node* data = getParameter(scope, "data", FatChunk, ctx);
  if (IS_FAT_ERROR(data)) {
    return data;
  }

  Node* algo = getParameter(scope, "algo", FatText, ctx);
  if (IS_FAT_ERROR(algo)) {
    return algo;
  }
  const EVP_MD* hashFn = getHashFunction(algo->val);
  if (!hashFn) {
    char* msg = join3("unknown '", algo->val, "' hash algo");
    return createError(msg, false, ckValueError, ctx);
  }

  EVP_MD_CTX* evpCtx = EVP_MD_CTX_new();
  if (!evpCtx) {
    return createError("failed to create digest context", true, NULL, ctx);
  }

  unsigned char hash[EVP_MAX_MD_SIZE] = {0};
  unsigned int hashSize = 0;

  bool success =
    EVP_DigestInit_ex(evpCtx, hashFn, NULL) &&
    EVP_DigestUpdate(evpCtx, (unsigned char*)data->val, data->num.s) &&
    EVP_DigestFinal_ex(evpCtx, hash, &hashSize);

  EVP_MD_CTX_free(evpCtx);

  if (!success) {
    return createError("digest failure", true, NULL, ctx);
  }

  return keyAsHugeInt(hash, hashSize, ctx);
}

/**
 * Generate cryptographically secure randomness
 */
static Node* enigmaBytes(Scope* scope, Context* ctx) {
  Node* len = getParameter(scope, "len", FatNumber, ctx);
  if (IS_FAT_ERROR(len)) {
    return len;
  }

  int bytesLen = len->num.f;
  if (bytesLen <= 0) {
    return createError("len must be at least 1", true, ckValueError, ctx);
  }

  if (RAND_status() != 1) {
    return createError("PRNG not initialized", true, NULL, ctx);
  }

  unsigned char* randomBytes = FRY_ALLOC(bytesLen);
  if (!RAND_bytes(randomBytes, bytesLen)) {
    free(randomBytes);
    return createError("failed to generate random bytes", true, NULL, ctx);
  }

  return runtimeChunk((char*)randomBytes, (size_t)bytesLen, ctx);
}

/**
 * Derive key using OpenSSL PBKDF2
 */
static Node* enigmaPbkdf2(Scope* scope, Context* ctx) {
  Node* secret = getParameter(scope, "secret", FatText, ctx);
  if (IS_FAT_ERROR(secret)) {
    return secret;
  }

  Node* salt = getParameter(scope, "salt", FatText, ctx);
  if (IS_FAT_ERROR(salt)) {
    return salt;
  }

  Node* iter = getParameter(scope, "iter", FatNumber, ctx);
  if (IS_FAT_ERROR(iter)) {
    return iter;
  }
  if (iter->num.f < 1.0) {
    return createError(MSG_I_N_ARG, true, ckValueError, ctx);
  }

  Node* algo = getParameter(scope, "algo", FatText, ctx);
  if (IS_FAT_ERROR(algo)) {
    return algo;
  }
  const EVP_MD* hashFn = getHashFunction(algo->val);
  if (!hashFn) {
    char* msg = join3("unknown '", algo->val, "' hash algo");
    return createError(msg, false, ckValueError, ctx);
  }

  const int keySize = getRecommendedKeySize(algo->val);
  assert(keySize != -1);

  unsigned char key[EVP_MAX_KEY_LENGTH] = {0};

  if (!PKCS5_PBKDF2_HMAC(secret->val, secret->num.s, (unsigned char*)salt->val,
                         salt->num.s, iter->num.f, hashFn, keySize, key)) {
    return createError("key derivation failure", true, NULL, ctx);
  }

  return keyAsHugeInt(key, keySize, ctx);
}

#if OPENSSL_VERSION_NUMBER >= OPENSSL_VERSION_WITH_NEW_TYPES

/**
 * Computes auth-code using OpenSSL HMAC
 */
static Node* enigmaHmac(Scope* scope, Context* ctx) {
  Node* data = getParameter(scope, "data", FatChunk, ctx);
  if (IS_FAT_ERROR(data)) {
    return data;
  }

  Node* key = getParameter(scope, "key", FatChunk, ctx);
  if (IS_FAT_ERROR(key)) {
    return key;
  }

  Node* algo = getParameter(scope, "algo", FatText, ctx);
  if (IS_FAT_ERROR(algo)) {
    return algo;
  }
  const EVP_MD* hashFn = getHashFunction(algo->val);
  if (!hashFn) {
    char* msg = join3("unknown '", algo->val, "' hash algo");
    return createError(msg, false, ckValueError, ctx);
  }

  EVP_MAC* mac = EVP_MAC_fetch(NULL, "HMAC", NULL);
  EVP_MAC_CTX* evpCtx = EVP_MAC_CTX_new(mac);
  if (!evpCtx) {
    return createError("failed to create mac context", true, NULL, ctx);
  }

  OSSL_PARAM params[] = {
    OSSL_PARAM_construct_utf8_string("digest", (char*)EVP_MD_name(hashFn), 0),
    OSSL_PARAM_END};

  unsigned char hmac[EVP_MAX_MD_SIZE] = {0};
  size_t hmacSize = 0;

  bool success =
    EVP_MAC_init(evpCtx, (unsigned char*)key->val, key->num.s, params) &&
    EVP_MAC_update(evpCtx, (unsigned char*)data->val, data->num.s) &&
    EVP_MAC_final(evpCtx, hmac, &hmacSize, sizeof(hmac));

  EVP_MAC_CTX_free(evpCtx);
  EVP_MAC_free(mac);

  if (!success) {
    return createError("hmac failure", true, NULL, ctx);
  }

  return keyAsHugeInt(hmac, hmacSize, ctx);
}

#endif

/**
 * Encrypt data with key using OpenSSL AES-256-CCM
 */
static Node* enigmaEncryptAES(Scope* scope, Context* ctx) {
  Node* data = getParameter(scope, "data", FatChunk, ctx);
  if (IS_FAT_ERROR(data)) {
    return data;
  }
  if (!data->num.s) {
    return createError("data cannot be empty", true, ckValueError, ctx);
  }

  Node* key = getParameter(scope, "key", FatChunk, ctx);
  if (IS_FAT_ERROR(key)) {
    return key;
  }
  if (key->num.s != 32) {
    return createError("key must be 32 bytes long", true, ckValueError, ctx);
  }

  // Create nonce for AES-CCM
  unsigned char iv[12];
  if (!RAND_bytes(iv, 12)) {
    return createError("failed to set initialization vector", true, NULL, ctx);
  }

  EVP_CIPHER_CTX* evpCtx = EVP_CIPHER_CTX_new();
  if (!evpCtx) {
    return createError(MSG_F_CIPH, true, NULL, ctx);
  }

  if (!EVP_EncryptInit_ex(evpCtx, EVP_aes_256_ccm(), NULL, NULL, NULL)) {
    EVP_CIPHER_CTX_free(evpCtx);
    return createError("encryption initialization failed", true, NULL, ctx);
  }

  if (!EVP_CIPHER_CTX_ctrl(evpCtx, EVP_CTRL_CCM_SET_IVLEN, 12, NULL) ||
      !EVP_CIPHER_CTX_ctrl(evpCtx, EVP_CTRL_CCM_SET_TAG, 16, NULL) ||
      !EVP_EncryptInit_ex(evpCtx, NULL, NULL, (unsigned char*)key->val, iv)) {
    EVP_CIPHER_CTX_free(evpCtx);
    return createError(MSG_F_IV, true, NULL, ctx);
  }

  unsigned char* package = FRY_ALLOC(12 + data->num.s + 16);

  // Copy the IV to the beginning of the encrypted data
  memcpy(package, iv, 12);

  int packageLen = 0;
  if (!EVP_EncryptUpdate(evpCtx, package + 12, &packageLen,
                         (unsigned char*)data->val, data->num.s)) {
    free(package);
    EVP_CIPHER_CTX_free(evpCtx);
    return createError("encryption failed", true, NULL, ctx);
  }

  int finalLen = 0;
  if (!EVP_EncryptFinal_ex(evpCtx, package + 12 + packageLen, &finalLen)) {
    free(package);
    EVP_CIPHER_CTX_free(evpCtx);
    return createError("encryption finalization failed", true, NULL, ctx);
  }
  packageLen += finalLen;

  if ((size_t)packageLen > 12 + data->num.s) {
    free(package);
    EVP_CIPHER_CTX_free(evpCtx);
    return createError(MSG_F_I_C, true, NULL, ctx);
  }

  if (!EVP_CIPHER_CTX_ctrl(evpCtx, EVP_CTRL_CCM_GET_TAG, 16,
                           package + 12 + packageLen)) {
    free(package);
    EVP_CIPHER_CTX_free(evpCtx);
    return createError("failed to add authentication tag", true, NULL, ctx);
  }

  EVP_CIPHER_CTX_free(evpCtx);

  // Return the full package: IV + encrypted data + tag
  return runtimeChunk((char*)package, 12 + packageLen + 16, ctx);
}

/**
 * Decrypt data with key using OpenSSL AES-256-CCM
 */
static Node* enigmaDecryptAES(Scope* scope, Context* ctx) {
  Node* package = getParameter(scope, "data", FatChunk, ctx);
  if (IS_FAT_ERROR(package)) {
    return package;
  }
  if (package->num.s < (12 + 16 + 1)) {
    return createError("invalid package length", true, ckValueError, ctx);
  }

  Node* key = getParameter(scope, "key", FatChunk, ctx);
  if (IS_FAT_ERROR(key)) {
    return key;
  }
  if (key->num.s != 32) {
    return createError("key must be 32 bytes long", true, ckValueError, ctx);
  }

  unsigned char* iv = (unsigned char*)package->val;
  unsigned char* encryptedData = (unsigned char*)package->val + 12;
  int encryptedLength = package->num.s - 12 - 16;
  unsigned char* tag = (unsigned char*)package->val + package->num.s - 16;

  EVP_CIPHER_CTX* evpCtx = EVP_CIPHER_CTX_new();
  if (!evpCtx) {
    return createError(MSG_F_CIPH, true, NULL, ctx);
  }

  if (!EVP_DecryptInit_ex(evpCtx, EVP_aes_256_ccm(), NULL, NULL, NULL)) {
    EVP_CIPHER_CTX_free(evpCtx);
    return createError("decryption initialization failed", true, NULL, ctx);
  }

  if (!EVP_CIPHER_CTX_ctrl(evpCtx, EVP_CTRL_CCM_SET_IVLEN, 12, NULL) ||
      !EVP_CIPHER_CTX_ctrl(evpCtx, EVP_CTRL_CCM_SET_TAG, 16, tag) ||
      !EVP_DecryptInit_ex(evpCtx, NULL, NULL, (unsigned char*)key->val, iv)) {
    EVP_CIPHER_CTX_free(evpCtx);
    return createError(MSG_F_IV, true, NULL, ctx);
  }

  unsigned char* decryptedData = FRY_ALLOC(encryptedLength);
  int outputLength = 0;

  // DecryptUpdate returns the plaintext equivalent to the input data length
  if (!EVP_DecryptUpdate(evpCtx, decryptedData, &outputLength, encryptedData,
                         encryptedLength)) {
    free(decryptedData);
    EVP_CIPHER_CTX_free(evpCtx);
    return createError("decryption failed", true, NULL, ctx);
  }

  int finalLen = 0;
  if (!EVP_DecryptFinal_ex(evpCtx, decryptedData + outputLength, &finalLen)) {
    free(decryptedData);
    EVP_CIPHER_CTX_free(evpCtx);
    return createError("decryption finalization failed", true, NULL, ctx);
  }
  outputLength += finalLen;

  EVP_CIPHER_CTX_free(evpCtx);

  if (outputLength > encryptedLength) {
    free(decryptedData);
    return createError(MSG_F_I_C, true, NULL, ctx);
  }

  return runtimeChunk((char*)decryptedData, outputLength, ctx);
}

#endif
