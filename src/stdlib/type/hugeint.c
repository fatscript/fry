/**
 * @file hugeint.c
 * @brief HugeInt prototype extensions
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.0.0
 * @date 2025-01-04
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../../runtime/interpreter.h"
#include "../../syntax/patterns.h"

static const char* LIB_HUGE_INT =
  "# fat.type.HugeInt - Prototype extensions\n"
  "\n"
  "HugeInt = (\n"
  "  ## Argument slot\n"
  "  val: Any = null\n"
  "\n"
  "  ## Constructor\n"
  "  apply = <> $hugeApply\n"
  "\n"
  "  ## Common prototype methods\n"
  "  isEmpty  = Boolean <> !self\n"
  "  nonEmpty = Boolean <> !!self\n"
  "  size     = Number  <> $hugeSize\n"
  "  toText   = Text    <> $toText\n"
  "  freeze   = Void    <> $freeze\n"
  "\n"
  "  ## HugeInt prototype special methods\n"
  "  modExp = (exp: HugeInt, mod: HugeInt): HugeInt -> $hugeModExp\n"
  "  toNumber = Number  <> $hugeToNum\n"
  "  toChunk  = Chunk   <> $hugeToChunk\n"
  ")\n";

/**
 * Return bool to huge / numb to huge /  text to huge
 */
static Node* hugeApply(Scope* scope, Context* ctx) {
  Node* val = getValueOf(scope, "val");

  HugeInt temp = {0};

  if (!val) {
    return runtimeHuge(temp, ctx);
  }

  switch (val->type) {
    case FatBoolean:
      if (val->num.b) {
        temp[0] = 1;
      }
      return runtimeHuge(temp, ctx);

    case FatNumber:
      if (0 <= val->num.f && val->num.f <= MAX_TO_INT) {
        char num[NUMBER_MAX_LENGTH + 1];
        uint64_t intPart = (uint64_t)val->num.f;  // truncates decimal part
        snprintf(num, NUMBER_MAX_LENGTH + 1, "%" PRIx64, intPart);
        if (!hugeSet(temp, num)) {
          return createError(MSG_I_N_ARG, true, ckValueError, ctx);
        }
      } else {
        return createError(MSG_I_N_ARG, true, ckValueError, ctx);
      }
      return runtimeHuge(temp, ctx);

    case FatError:
      return val;

    case FatChunk:
      return chunkToHugeImpl(val, ctx);

    case FatText:
      if (!isHex(val->val[0]) || !(val->val[1] == '\0' || isHex(val->val[1])) ||
          !hugeSet(temp, val->val)) {
        if (!isHugeHex(val->val) || !hugeSet(temp, val->val + 2)) {
          return createError(MSG_I_N_ARG, true, ckValueError, ctx);
        }
      }
      return runtimeHuge(temp, ctx);

    default:
      return createError(MSG_UNSUP, true, ckValueError, ctx);
  }
}

/**
 * Returns minimum number of bits needed for huge int representation
 */
static Node* hugeSize(Context* ctx) {
  Node* huge = getInstance(FatHugeInt, ctx);
  if (IS_FAT_ERROR(huge)) {
    return huge;
  }

  return runtimeNumber(minBitsToRepresent(*huge->num.h), ctx);
}

/**
 * Converts to number (with precision loss)
 */
static Node* hugeToNum(Context* ctx) {
  Node* huge = getInstance(FatHugeInt, ctx);
  if (IS_FAT_ERROR(huge)) {
    return huge;
  }

  return runtimeNumber(hugeToFloat(*huge->num.h), ctx);
}

/**
 * Encodes to binary representation
 */
static Node* hugeToChunk(Context* ctx) {
  Node* huge = getInstance(FatHugeInt, ctx);
  if (IS_FAT_ERROR(huge)) {
    return huge;
  }

  return hugeToChunkImpl(huge, ctx);
}

/**
 * Returns modular exponentiation of huge int
 */
static Node* hugeModExp(Scope* scope, Context* ctx) {
  Node* huge = getInstance(FatHugeInt, ctx);
  if (IS_FAT_ERROR(huge)) {
    return huge;
  }

  Node* exp = getParameter(scope, "exp", FatHugeInt, ctx);
  if (IS_FAT_ERROR(exp)) {
    return exp;
  }

  Node* mod = getParameter(scope, "mod", FatHugeInt, ctx);
  if (IS_FAT_ERROR(mod)) {
    return mod;
  }

  HugeInt temp = {0};

  if (hugeEq(*mod->num.h, temp)) {
    return createError(MSG_H_MOD_Z, true, ckValueError, ctx);
  }

  if (!hugeModExpImpl(temp, *huge->num.h, *exp->num.h, *mod->num.h)) {
    return createError(MSG_H_OVER, true, ckValueError, ctx);
  }

  return runtimeHuge(temp, ctx);
}
