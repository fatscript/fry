/**
 * @file text.c
 * @brief Text prototype extensions
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-20
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../../runtime/interpreter.h"
#include "../../syntax/formatter.h"

static const char* LIB_TEXT =
  "# fat.type.Text - Prototype extensions\n"
  "\n"
  "Text = (\n"
  "  ## Argument slot\n"
  "  val: Any = null\n"
  "\n"
  "  ## Constructor\n"
  "  apply = <> $textApply\n"
  "\n"
  "  ## Common prototype methods\n"
  "  isEmpty  = Boolean <> !self\n"
  "  nonEmpty = Boolean <> !!self\n"
  "  size     = Number  <> $textSize\n"
  "  toText   = Text    <> self\n"
  "  freeze   = Void    <> $freeze\n"
  "\n"
  "  ## Text prototype special methods\n"
  "  replace    = (old: Text, new: Text): Text -> $textReplace\n"
  "  indexOf    = (frag: Text): Number         -> $textIdxOf\n"
  "  contains   = (frag: Text): Boolean        -> $textIdxOf >= 0\n"
  "  count      = (frag: Text): Number         -> $textCount\n"
  "  startsWith = (frag: Text): Boolean -> {\n"
  "    !frag => true\n"
  "    _     => $textIdxOf == 0\n"
  "  }\n"
  "  endsWith   = (frag: Text): Boolean -> {\n"
  "    !frag => true\n"
  "    _     => self(-frag.size..) == frag\n"
  "  }\n"
  "  split      = (sep: Text): List   -> $textSplit\n"
  "  toLower    = Text                <> $textToLower\n"
  "  toUpper    = Text                <> $textToUpper\n"
  "  trim       = Text                <> $textTrim\n"
  "  isBlank    = Boolean             <> $textIsBlank\n"
  "  match      = (re: Text): Boolean -> $textMatch\n"
  "  groups     = (re: Text): Scope   -> $textGroups\n"
  "  repeat     = (n: Number): Text   -> $textRepeat\n"
  "  overlay    = (base: Text, align: Text = 'left'): Text -> $textOverlay\n"
  "  patch      = (i: Number, n: Number, val: Text = ''): Text -> {\n"
  "    i < 0 => val + self\n"
  "    _     => self(..<i) + val + self(i + n..)\n"
  "  }\n"
  "  toChunk    = Chunk <> $textToChunk\n"
  ")\n";

/**
 * Coerces value to text, same as .toText
 */
static Node* textApply(Scope* scope, Context* ctx) {
  Node* val = getValueOf(scope, "val");

  if (val) {
    char* result = toString(val);
    return runtimeText(result, strlen(result), ctx);
  }
  return runtimeText(strDup(""), 0, ctx);
}

/**
 * Return text length.
 */
static Node* textSize(Context* ctx) {
  Node* text = getInstance(FatText, ctx);
  if (IS_FAT_ERROR(text)) {
    return text;
  }
  return runtimeNumber((double)utf8len(text->val), ctx);
}

/**
 * Replace old with new (all).
 */
static Node* textReplace(Scope* scope, Context* ctx) {
  Node* text = getInstance(FatText, ctx);
  if (IS_FAT_ERROR(text)) {
    return text;
  }

  Node* oldWord = getParameter(scope, "old", FatText, ctx);
  if (IS_FAT_ERROR(oldWord)) {
    return oldWord;
  }

  Node* newWord = getParameter(scope, "new", FatText, ctx);
  if (IS_FAT_ERROR(newWord)) {
    return newWord;
  }

  char* result = replaceAll(text->val, oldWord->val, newWord->val);
  return runtimeText(result, strlen(result), ctx);
}

/**
 * Get fragment index, -1 if absent.
 */
static Node* textIdxOf(Scope* scope, Context* ctx) {
  Node* text = getInstance(FatText, ctx);
  if (IS_FAT_ERROR(text)) {
    return text;
  }

  Node* frag = getParameter(scope, "frag", FatText, ctx);
  if (IS_FAT_ERROR(frag)) {
    return frag;
  }

  const char* ptr = utf8strstr(text->val, frag->val);
  if (ptr) {
    size_t index = 0;
    for (char* c = text->val; c < ptr; c = utf8next(c)) {
      index++;
    }
    return runtimeNumber((double)index, ctx);
  }

  return runtimeNumber(-1.0, ctx);
}

/**
 * Get repetition count for fragment.
 */
static Node* textCount(Scope* scope, Context* ctx) {
  Node* text = getInstance(FatText, ctx);
  if (IS_FAT_ERROR(text)) {
    return text;
  }

  Node* frag = getParameter(scope, "frag", FatText, ctx);
  if (IS_FAT_ERROR(frag)) {
    return frag;
  }

  return runtimeNumber(countWord(text->val, frag->val), ctx);
}

/**
 * Split text by sep into list.
 */
static Node* textSplit(Scope* scope, Context* ctx) {
  Node* text = getInstance(FatText, ctx);
  if (IS_FAT_ERROR(text)) {
    return text;
  }

  Node* sep = getParameter(scope, "sep", FatText, ctx);
  if (IS_FAT_ERROR(sep)) {
    return sep;
  }

  char** splitted = NULL;
  if (!*sep->val) {
    size_t len = utf8len(text->val);
    char* str = text->val;
    splitted = FRY_ALLOC((len + 1) * sizeof(char*));
    for (size_t i = 0; i < len; i++) {
      splitted[i] = utf8char(str);
      str = utf8next(str);
      if (!str) {
        fatalOut(__FILE__, __func__, "bad UTF-8 char");
        exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
      }
    }
    splitted[len] = NULL;
  } else {
    splitted = splitSep(text->val, sep->val);
  }

  // Build list from splitted strings
  Scope* list = createList();
  pushStack(ctx, __func__, ctx->selfRef, list);
  if (splitted) {
    for (size_t i = 0; splitted[i]; i++) {
      Node* fragment = runtimeText(splitted[i], strlen(splitted[i]), ctx);
      addToList(list, fragment, ctx);
    }
    free(splitted);
  }
  Node* result = runtimeCollection(list, ctx);
  popStack(ctx, 1);
  return result;
}

/**
 * Return lowercase version of text.
 */
static Node* textToLower(Context* ctx) {
  Node* text = getInstance(FatText, ctx);
  if (IS_FAT_ERROR(text)) {
    return text;
  }

  char* result = copyChunk(text->val, text->num.s);
  utf8lower(result);
  return runtimeText(result, text->num.s, ctx);
}

/**
 * Return uppercase version of text.
 */
static Node* textToUpper(Context* ctx) {
  Node* text = getInstance(FatText, ctx);
  if (IS_FAT_ERROR(text)) {
    return text;
  }

  char* result = copyChunk(text->val, text->num.s);
  utf8upper(result);
  return runtimeText(result, text->num.s, ctx);
}

/**
 * Return trimmed version of text.
 */
static Node* textTrim(Context* ctx) {
  Node* text = getInstance(FatText, ctx);
  if (IS_FAT_ERROR(text)) {
    return text;
  }

  char* result = strTrim(text->val, text->num.s);
  return runtimeText(result, strlen(result), ctx);
}

/**
 * Return true if only whitespaces.
 */
static Node* textIsBlank(Context* ctx) {
  Node* text = getInstance(FatText, ctx);
  if (IS_FAT_ERROR(text)) {
    return text;
  }

  return RUNTIME_BOOLEAN(isBufferBlank(text->val));
}

/**
 * Return text is match for regex.
 */
static Node* textMatch(Scope* scope, Context* ctx) {
  Node* text = getInstance(FatText, ctx);
  if (IS_FAT_ERROR(text)) {
    return text;
  }

  Node* re = getParameter(scope, "re", FatText, ctx);
  if (IS_FAT_ERROR(re)) {
    return re;
  }

  return RUNTIME_BOOLEAN(matchRegex(re->val, text->val));
}

/**
 * Return matched regex groups.
 */
static Node* textGroups(Scope* scope, Context* ctx) {
  Node* text = getInstance(FatText, ctx);
  if (IS_FAT_ERROR(text)) {
    return text;
  }

  Node* re = getParameter(scope, "re", FatText, ctx);
  if (IS_FAT_ERROR(re)) {
    return re;
  }

  int count = 0;
  char** groups = regexGroups(re->val, text->val, &count);

  Scope* result = createScope();
  pushStack(ctx, __func__, text, result);
  char groupName[16];
  for (int i = 0; i < count; i++) {
    snprintf(groupName, sizeof(groupName), "_%d", i);
    if (groups[i]) {
      size_t len = strlen(groups[i]);
      addToScope(result, groupName, runtimeText(groups[i], len, ctx));
    } else {
      addToScope(result, groupName, NULL);
    }
  }
  popStack(ctx, 1);

  free(groups);

  return runtimeCollection(result, ctx);
}

/**
 * Return text repeated n times.
 */
static Node* textRepeat(Scope* scope, Context* ctx) {
  Node* text = getInstance(FatText, ctx);
  if (IS_FAT_ERROR(text)) {
    return text;
  }

  Node* n = getParameter(scope, "n", FatNumber, ctx);
  if (IS_FAT_ERROR(n)) {
    return n;
  }

  size_t chunkSize = text->num.s;
  size_t repeatCount = n->num.f > 0 ? (size_t)n->num.f : 0;
  size_t totalLen = chunkSize * repeatCount;

  char* repeated = FRY_ALLOC(totalLen + 1);
  for (size_t copied = 0; copied < totalLen; copied += chunkSize) {
    memcpy(repeated + copied, text->val, chunkSize);
  }
  repeated[totalLen] = '\0';  // null-terminate

  return runtimeText(repeated, totalLen, ctx);
}

/**
 * Return text overlaid on base (with optional alignment).
 */
static Node* textOverlay(Scope* scope, Context* ctx) {
  Node* text = getInstance(FatText, ctx);
  if (IS_FAT_ERROR(text)) {
    return text;
  }

  Node* base = getParameter(scope, "base", FatText, ctx);
  if (IS_FAT_ERROR(base)) {
    return base;
  }

  Node* align = getParameter(scope, "align", FatText, ctx);
  if (IS_FAT_ERROR(align)) {
    return align;
  }

  size_t start = 0;
  size_t shift = 0;

  size_t baseLen = utf8len(base->val);
  size_t textLen = utf8len(text->val);

  if (strcmp(align->val, "left") != 0) {
    if (baseLen >= textLen) {
      if (strcmp(align->val, "center") == 0) {
        start = (baseLen - textLen) / 2;
      } else if (strcmp(align->val, "right") == 0) {
        start = baseLen - textLen;
      } else {
        return createError(MSG_B_A_A, true, ckValueError, ctx);
      }
    } else {
      if (strcmp(align->val, "center") == 0) {
        shift = (textLen - baseLen) / 2;
      } else if (strcmp(align->val, "right") == 0) {
        shift = textLen - baseLen;
      } else {
        return createError(MSG_B_A_A, true, ckValueError, ctx);
      }
    }
  }

  // Allocate large enough buffer for UTF-8 handling
  char* combined = FRY_ALLOC(baseLen * 4 + 1);
  char* basePtr = base->val;
  char* textPtr = text->val;
  size_t combinedIndex = 0;

  // Move to start position in base
  for (size_t i = 0; i < start && *basePtr; i++) {
    int bytes = utf8bytes(basePtr);
    while (bytes-- && *basePtr) {
      combined[combinedIndex++] = *basePtr++;
    }
  }

  // Overlay text from shifted start
  for (size_t i = 0; i < shift && *textPtr; i++) {
    textPtr = utf8next(textPtr);
  }
  while (*textPtr && *basePtr) {
    int textBytes = utf8bytes(textPtr);
    while (textBytes-- && *textPtr) {
      combined[combinedIndex++] = *textPtr++;
    }
    basePtr = utf8next(basePtr);
  }

  // Append remaining base text if any
  while (*basePtr) {
    combined[combinedIndex++] = *basePtr++;
  }
  combined[combinedIndex] = '\0';  // null-terminate

  // Shrink the buffer to the actual used size
  combined = FRY_REALLOC(combined, combinedIndex + 1);
  return runtimeText(combined, combinedIndex, ctx);
}

/**
 * Encodes to binary representation.
 */
static Node* textToChunk(Context* ctx) {
  Node* text = getInstance(FatText, ctx);
  if (IS_FAT_ERROR(text)) {
    return text;
  }

  char* chunk = copyChunk(text->val, text->num.s);
  return runtimeChunk(chunk, text->num.s, ctx);
}

/**
 * Return self as text (generic, used by other native types)
 */
static Node* textToText(Context* ctx) {
  char* result = toString(ctx->selfRef);
  return runtimeText(result, strlen(result), ctx);
}
