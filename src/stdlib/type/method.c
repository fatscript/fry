/**
 * @file method.c
 * @brief Method prototype extensions
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.0.0
 * @date 2025-01-04
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../../runtime/interpreter.h"

static const char* LIB_METHOD =
  "# fat.type.Method - Prototype extensions\n"
  "\n"
  "Method = (\n"
  "  ## Argument slot\n"
  "  val: Any = null\n"
  "\n"
  "  ## Constructor\n"
  "  apply = <> $methApply\n"
  "\n"
  "  ## Common prototype methods\n"
  "  isEmpty  = Boolean <> false\n"
  "  nonEmpty = Boolean <> true\n"
  "  size     = Number  <> 1\n"
  "  toText   = Text    <> $toText\n"
  "  freeze   = Void    <> $freeze\n"
  "\n"
  "  ## Method prototype special method\n"
  "  arity    = Number  <> $methArity\n"
  ")\n"
  "\n"
  "## Set standard method type alias\n"
  "Procedure = Method\n";

/**
 * Return val wrapped into an method
 */
static Node* methApply(Scope* scope, Context* ctx) {
  Node* val = getValueOf(scope, "val");
  if (IS_FAT_TYPE(val, FatMethod)) {
    return val;
  }

  Node* method = createNode(FatMethod, SRC_RUN, ctx);
  method->body = val;
  return method;
}

/**
 * Return number of parameters method takes (arity)
 */
static Node* methArity(Context* ctx) {
  Node* method = getInstance(FatMethod, ctx);
  if (IS_FAT_ERROR(method)) {
    return method;
  }

  return runtimeNumber(getMethodArity(method), ctx);
}
