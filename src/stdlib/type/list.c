/**
 * @file list.c
 * @brief List prototype extensions
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-16
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../../runtime/interpreter.h"

static const char* LIB_LIST =
  "# fat.type.List - Prototype extensions\n"
  "\n"
  "List = (\n"
  "  ## Argument slot\n"
  "  val: Any = null\n"
  "\n"
  "  ## Constructor\n"
  "  apply = <> $listApply\n"
  "\n"
  "  ## Common prototype methods\n"
  "  isEmpty  = Boolean <> !self\n"
  "  nonEmpty = Boolean <> !!self\n"
  "  size     = Number  <> $listSize\n"
  "  toText   = Text    <> $toText\n"
  "  freeze   = Void    <> $freeze\n"
  "\n"
  "  ## List prototype special methods\n"
  "  join     = (sep: Text): Text    -> $listJoin\n"
  "  flatten  = List                 <> $listFlatten\n"
  "  find     = (p: Method): Any     -> $listFind\n"
  "  contains = (p: Method): Boolean -> $listFind != Void\n"
  "  filter   = (p: Method): List    -> self @ -> p(_) ? _\n"
  "  reverse  = List                 <> $listReverse\n"
  "  shuffle  = List                 <> $listShuffle\n"
  "  unique   = List                 <> $listUnique\n"
  "  sort     = List                 <> $listSort\n"
  "  sortBy   = (key: Any): List     -> $listSort\n"
  "  indexOf  = (item: Any): Number  -> $listIdxOf\n"
  "  head     = Any                  <> self ? self(0)\n"
  "  tail     = List                 <> self(1..)\n"
  "  map      = (m: Method): List    -> self @ -> m(_)\n"
  "  ## reduce usage note: 'm' shall take (acc, item) as parameters\n"
  "  ##                    'and return the updated accumulator value\n"
  "  reduce   = (m: Method, acc: Any = null): Any -> $listReduce\n"
  "  walk     = (m: Method): Void -> { self @ -> { m(_), () }, () }\n"
  "  patch    = (i: Number, n: Number, val: List = []): List -> {\n"
  "    i < 0 => val + self\n"
  "    _     => self(..<i) + val + self(i + n..)\n"
  "  }\n"
  "\n"
  "  ## Option compatibility\n"
  "  headOption = Any <> {\n"
  "    val = self ? self(0)\n"
  "    val != Void => Some * { val }\n"
  "    _           => None * {}\n"
  "  }\n"
  "  itemOption = (index: Number) -> self(index..index).headOption\n"
  "  findOption = (p: Method) -> [$listFind].headOption\n"
  ")\n";

/**
 * Return val wrapped into list
 */
static Node* listApply(Scope* scope, Context* ctx) {
  Scope* instance = createList();
  Node* val = getValueOf(scope, "val");
  if (val) {
    addToList(instance, val, ctx);
  }
  return runtimeCollection(instance, ctx);
}

/**
 * Join texts from a list using separator
 */
static Node* listJoin(Scope* scope, Context* ctx) {
  Node* list = getInstance(FatList, ctx);
  if (IS_FAT_ERROR(list)) {
    return list;
  }

  Node* sep = getParameter(scope, "sep", FatText, ctx);
  if (IS_FAT_ERROR(sep)) {
    return sep;
  }

  char* result = joinListItems(list->scp, sep->val);
  return runtimeText(result, strlen(result), ctx);
}

/**
 * Join list of lists into flat list
 */
static Node* listFlatten(Context* ctx) {
  Node* list = getInstance(FatList, ctx);
  if (IS_FAT_ERROR(list)) {
    return list;
  }

  Scope* original = list->scp;
  if (!original || !original->entries) {
    return runtimeCollection(original, ctx);  // already "flat"
  }

  if (!isAlias(checkList, original->ck)) {
    return createError("flatten requires matrix", true, ckValueError, ctx);
  }

  Scope* flattened = createList();
  for (Entry* x = original->entries; x; x = x->next) {
    if (x->data->scp) {
      for (Entry* y = x->data->scp->entries; y; y = y->next) {
        Node* error = addToList(flattened, y->data, ctx);
        if (error) {
          trackScope(flattened);
          return error;
        }
      }
    }
  }

  return runtimeCollection(flattened, ctx);
}

/**
 * Return selected item or null
 */
static Node* listFind(Scope* scope, Context* ctx) {
  Node* list = getInstance(FatList, ctx);
  if (IS_FAT_ERROR(list)) {
    return list;
  }

  Node* p = getParameter(scope, "p", FatMethod, ctx);
  if (IS_FAT_ERROR(p)) {
    return p;
  }

  if (!list->scp || !list->scp->entries) {
    return NULL;
  }

  const Node* firstArg = getFirstArgument(p->head);

  // Lists can only be processed with methods that match the type of list items
  if (!checkAlias(firstArg->ck, list->scp->entries->data)) {
    return createError("p" GUIDE MSG_MISMATCH, true, ckTypeError, ctx);
  }

  Scope layer = {0};
  initLayer(&layer);
  int stacked = mapPushStack(ctx, __func__, NULL, list->scp, p, &layer);
  Scope* prevEnclosing = scopeInward(&layer, ctx);

  Node* result = NULL;
  for (Entry* entry = list->scp->entries;
       !ctx->failureEvent && !result && entry; entry = entry->next) {
    // Inlined load method scope
    Entry arg = {.key.s = firstArg->val, .data = entry->data};
    layer.entries = &arg;
    layer.size = 1;

    // Apply predicate
    Memory* localMemBottom = ctx->temp;
    Node* check = interpretWithTRO(&layer, p->body, ctx);
    if (booleanOf(check)) {
      result = arg.data;
    }
    UNLOCK_NODE(check);

    // Reverse layerScope
    wipeScope(&layer, &arg, true);
    if (useCollector && ctx->tempCount) {
      microGC(NULL, localMemBottom, ctx);
    }
  }

  pthread_mutex_destroy(&layer.lock);
  scopeOutward(prevEnclosing, ctx);
  popStack(ctx, stacked);

  return ctx->failureEvent ? ctx->failureEvent : result;
}

/**
 * Return a reversed copy of list
 */
static Node* listReverse(Context* ctx) {
  Node* list = getInstance(FatList, ctx);
  if (IS_FAT_ERROR(list)) {
    return list;
  }

  return reverseList(list->scp, ctx);
}

/**
 * Return a shuffled copy of list
 */
static Node* listShuffle(Context* ctx) {
  Node* list = getInstance(FatList, ctx);
  if (IS_FAT_ERROR(list)) {
    return list;
  }

  Scope* shuffled = copyCollection(list->scp, false, ctx);
  if (shuffled && shuffled->entries) {
    long i = 1;
    for (Entry* a = shuffled->entries->next; a; a = a->next, i++) {
      Entry* b = getByIndex(shuffled, (long)(rnGen() % i));
      Node* aux = a->data;
      a->data = b->data;
      b->data = aux;
    }
  }

  return runtimeCollection(shuffled, ctx);
}

/**
 * listUnique auxiliary function
 */
static inline bool hasItemAlready(Entry* list, Entry* item) {
  for (Node* data = item->data; list; list = list->next) {
    if (nodeEq(list->data, data)) {
      return true;
    }
  }
  return false;
}

/**
 * Return only unique items of original list
 */
static Node* listUnique(Context* ctx) {
  Node* list = getInstance(FatList, ctx);
  if (IS_FAT_ERROR(list)) {
    return list;
  }

  Scope* unique = createList();
  if (list->scp) {
    for (Entry* entry = list->scp->entries; entry; entry = entry->next) {
      if (!hasItemAlready(unique->entries, entry)) {
        addToList(unique, entry->data, ctx);
      }
    }
  }

  return runtimeCollection(unique, ctx);
}

/**
 * Return a sorted copy of list (ascending)
 */
static Node* listSort(Scope* scope, Context* ctx) {
  Node* list = getInstance(FatList, ctx);
  if (IS_FAT_ERROR(list)) {
    return list;
  }

  if (!list->scp || !list->scp->entries) {
    return copyNode(list, false, ctx);
  }

  Node* key = NULL;
  Entry* keyParam = getEntryOf(scope, "key");

  if (keyParam) {
    key = keyParam->data;
    if (isAlias(checkScope, list->scp->ck)) {
      if (!IS_FAT_TYPE(key, FatText) || !*key->val) {
        return createError(MSG_I_ARG, true, ckValueError, ctx);
      }

    } else if (isAlias(checkList, list->scp->ck)) {
      if (!IS_FAT_TYPE(key, FatNumber) || key->num.f < 0) {
        return createError(MSG_I_ARG, true, ckValueError, ctx);
      }

    } else {
      return createError(MSG_MISMATCH, true, ckTypeError, ctx);
    }
  }

  Scope* sorted = copyCollection(list->scp, false, ctx);
  quicksort(sorted->entries, sorted->quick2, key);

  return runtimeCollection(sorted, ctx);
}

/**
 * Get item index, -1 if absent
 */
static Node* listIdxOf(Scope* scope, Context* ctx) {
  Node* list = getInstance(FatList, ctx);
  if (IS_FAT_ERROR(list)) {
    return list;
  }

  Node* item = getValueOf(scope, "item");
  if (!item) {
    return createError("cannot search List for null", true, ckValueError, ctx);
  }

  if (list->scp) {
    long index = 0;
    for (Entry* entry = list->scp->entries; entry; entry = entry->next) {
      if (nodeEq(entry->data, item)) {
        return runtimeNumber((double)index, ctx);
      }
      index++;
    }
  }

  return runtimeNumber(-1, ctx);
}

/**
 * Functional utility
 */
static Node* listReduce(Scope* scope, Context* ctx) {
  Node* list = getInstance(FatList, ctx);
  if (IS_FAT_ERROR(list)) {
    return list;
  }

  Node* acc = getValueOf(scope, "acc");

  if (!list->scp || !list->scp->entries) {
    return acc;
  }

  Node* m = getParameter(scope, "m", FatMethod, ctx);
  if (IS_FAT_ERROR(m)) {
    return m;
  }

  // Check if method signature accepts at least 2 parameters
  if (!(IS_FAT_TYPE(m->head, FatEntry) &&
        IS_FAT_TYPE(m->head->seq, FatEntry))) {
    return createError("invalid reducer", true, ckValueError, ctx);
  }

  // Extract method signature slot names as keys 1 and 2
  char* k1 = m->head->val;
  char* k2 = m->head->seq->val;

  // Check first element in the list as lists can only hold one type
  if (!checkAlias(m->head->seq->ck, list->scp->entries->data)) {
    return createError(join2(MSG_MISMATCH GUIDE, k2), false, ckTypeError, ctx);
  }

  if (debugLogs) {
    logDebug(__FILE__, __func__, "start");
  }

  Scope layer = {0};
  initLayer(&layer);
  int stacked = mapPushStack(ctx, __func__, acc, list->scp, m, &layer);
  Scope* prevEnclosing = scopeInward(&layer, ctx);

  bool isOrdered = strcmp(k1, k2) < 0;

  // Fallback to first element if no acc, and loop through the list
  Entry* entry = list->scp->entries;
  if (!acc) {
    acc = entry->data;
    entry = entry->next;
  }
  for (; !ctx->failureEvent && entry; entry = entry->next) {
    if (!checkAlias(m->head->ck, acc)) {
      pthread_mutex_destroy(&layer.lock);
      scopeOutward(prevEnclosing, ctx);
      popStack(ctx, stacked);
      char* msg = join2(MSG_MISMATCH GUIDE, k1);
      return createError(msg, false, ckTypeError, ctx);
    }

    // Inlined load method scope
    Entry arg1 = {.key.s = k1, .data = acc};
    Entry arg2 = {.key.s = k2, .data = entry->data};
    if (isOrdered) {
      layer.entries = &arg1;
      arg1.next = &arg2;
    } else {
      layer.entries = &arg2;
      arg2.next = &arg1;
    }
    layer.size = 2;

    // Apply reducer
    Memory* localMemBottom = ctx->temp;
    acc = interpretWithTRO(&layer, m->body, ctx);

    // Inlined custom wipeScope
    while (layer.entries) {
      Entry* next = layer.entries->next;
      if (layer.entries != &arg1 && layer.entries != &arg2) {
        freeScopeEntry(layer.entries);
      }
      layer.entries = next;
    }

    layer.quick1 = NULL;
    layer.quick2 = NULL;
    layer.cached = NULL;
    if (useCollector && ctx->tempCount) {
      microGC(acc, localMemBottom, ctx);
    }
  }

  pthread_mutex_destroy(&layer.lock);
  scopeOutward(prevEnclosing, ctx);
  popStack(ctx, stacked);

  if (debugLogs) {
    logDebug(__FILE__, __func__, "end");
  }

  return ctx->failureEvent ? ctx->failureEvent : acc;
}

/**
 * Return length of list pointed by selfRef
 */
static Node* listSize(Context* ctx) {
  Node* list = getInstance(FatList, ctx);
  if (IS_FAT_ERROR(list)) {
    return list;
  }

  return runtimeNumber(list->scp ? list->scp->size : 0L, ctx);
}
