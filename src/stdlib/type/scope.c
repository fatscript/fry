/**
 * @file scope.c
 * @brief Scope prototype extensions
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-16
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../../runtime/interpreter.h"

static const char* LIB_SCOPE =
  "# fat.type.Scope - Prototype extensions\n"
  "\n"
  "Keyset = List/Text\n"
  "\n"
  "Scope = (\n"
  "  ## Argument slot\n"
  "  val: Any = null\n"
  "\n"
  "  ## Constructor\n"
  "  apply = <> $scopApply\n"
  "\n"
  "  ## Common prototype methods\n"
  "  isEmpty  = Boolean <> !self\n"
  "  nonEmpty = Boolean <> !!self\n"
  "  size     = Number  <> $scopSize\n"
  "  toText   = Text    <> $toText\n"
  "  freeze   = Void    <> $freeze\n"
  "\n"
  "  ## Scope prototype special methods\n"
  "  seal     = Void    <> $seal\n"
  "  isSealed = Boolean <> $isSealed\n"
  "  copy     = Scope   <> $scopCopy\n"
  "  keys     = Keyset  <> $scopKeys\n"
  "  valuesOf = (t: Type): List -> self @ -> self(_) == t ? self(_)\n"
  "  pick     = (keys: Keyset): Scope -> $scopPick\n"
  "  omit     = (keys: Keyset): Scope -> $scopOmit\n"
  "  vmap     = (m: Method): Scope -> {\n"
  "    mapped = {}\n"
  "    self @ -> { mapped[_] = m(self(_)), () }\n"
  "    mapped\n"
  "  }\n"
  "\n"
  "  ## Option compatibility\n"
  "  maybe = (key: Text) -> {\n"
  "    val = self(key)\n"
  "    val != Void => Some * { val }\n"
  "    _           => None * {}\n"
  "  }\n"
  ")\n";

/**
 * Return val wrapped into scope, if not a scope
 */
static Node* scopApply(Scope* scope, Context* ctx) {
  Node* val = getValueOf(scope, "val");
  if (val && (val->type == FatScope || IS_FAT_ERROR(val))) {
    return val;
  }

  Scope* instance = createScope();
  if (val) {
    addToScope(instance, "val", val);
  }
  return runtimeCollection(instance, ctx);
}

/**
 * Return number of entries
 */
static Node* scopSize(Context* ctx) {
  Node* node = getInstance(FatScope, ctx);
  if (IS_FAT_ERROR(node)) {
    return node;
  }

  long size = node->scp ? node->scp->size - node->scp->blanks : 0L;
  return runtimeNumber(size, ctx);
}

/**
 * Return deep copy of scope
 */
static Node* scopCopy(Context* ctx) {
  Node* scope = getInstance(FatScope, ctx);
  if (IS_FAT_ERROR(scope)) {
    return scope;
  }

  if (!initVisit(__func__)) {
    fatalOut(__FILE__, __func__, MSG_U_REC);
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }
  Node* copy = copyNode(scope, true, ctx);
  endVisit();
  return copy;
}

/**
 * Return list of scope keys
 */
static Node* scopKeys(Context* ctx) {
  Node* self = getInstance(FatScope, ctx);
  if (IS_FAT_ERROR(self)) {
    return self;
  }

  Scope* keys = createList();
  pushStack(ctx, __func__, self, keys);
  if (self->scp) {
    for (Entry* entry = self->scp->entries; entry; entry = entry->next) {
      addToList(keys, runtimeTextDup(entry->key.s, ctx), ctx);
    }
  }
  Node* result = runtimeCollection(keys, ctx);
  popStack(ctx, 1);
  return result;
}

static char** getKeySet(Scope* list) {
  if (!list) {
    fatalOut(__FILE__, __func__, MSG_N_PTR);
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }
  char** set = FRY_ALLOC((list->size + 1) * sizeof(char*));
  long i = 0;
  for (Entry* entry = list->entries; entry; entry = entry->next) {
    if (!entry->data) {
      fatalOut(__FILE__, __func__, MSG_N_PTR);
      exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
    }
    bool isDuplicated = false;
    for (long j = 0; j < i; j++) {
      if (strcmp(set[j], entry->data->val) == 0) {
        isDuplicated = true;
        break;
      }
    }
    if (isDuplicated) {
      continue;
    }
    set[i++] = entry->data->val;
  }
  set[i] = NULL;
  return set;
}

/**
 * Filter scope by keys
 */
static Node* scopPick(Scope* scope, Context* ctx) {
  Node* self = getInstance(FatScope, ctx);
  if (IS_FAT_ERROR(self)) {
    return self;
  }

  Node* keys = getParameter(scope, "keys", FatList, ctx);
  if (IS_FAT_ERROR(keys)) {
    return keys;
  }

  Scope* copy = createScope();
  pushStack(ctx, __func__, self, copy);
  if (keys->scp && keys->scp->entries && keys->scp->entries->data) {
    if (keys->scp->entries->data->type != FatText) {
      trackScope(copy);
      popStack(ctx, 1);
      return createError(MSG_I_ARG, true, ckValueError, ctx);
    }
    char** uniqueList = getKeySet(keys->scp);

    for (char** ptr = uniqueList; *ptr; ptr++) {
      Node* value = getValueOf(self->scp, *ptr);
      addToScope(copy, *ptr, copyNode(value, false, ctx));
    }
    free(uniqueList);
  }
  Node* result = runtimeCollection(copy, ctx);
  popStack(ctx, 1);
  return result;
}

/**
 * Filter scope removing keys
 */
static Node* scopOmit(Scope* scope, Context* ctx) {
  Node* self = getInstance(FatScope, ctx);
  if (IS_FAT_ERROR(self)) {
    return self;
  }

  Node* keys = getParameter(scope, "keys", FatList, ctx);
  if (IS_FAT_ERROR(keys)) {
    return keys;
  }

  Scope* copy = createScope();
  pushStack(ctx, __func__, self, copy);
  if (keys->scp && keys->scp->entries && keys->scp->entries->data) {
    if (keys->scp->entries->data->type != FatText) {
      trackScope(copy);
      popStack(ctx, 1);
      return createError(MSG_I_ARG, true, ckValueError, ctx);
    }
  }
  char** uniqueList = getKeySet(keys->scp);
  for (Entry* entry = self->scp->entries; entry; entry = entry->next) {
    const char* key = entry->key.s;
    bool shouldOmit = false;
    for (char** ptr = uniqueList; *ptr; ptr++) {
      if (strcmp(key, *ptr) == 0) {
        shouldOmit = true;
        break;
      }
    }
    if (shouldOmit) {
      continue;
    }
    addToScope(copy, key, copyNode(entry->data, false, ctx));
  }
  free(uniqueList);
  Node* result = runtimeCollection(copy, ctx);
  popStack(ctx, 1);
  return result;
}

/**
 * Seal scope growth (entry addition is denied)
 */
static Node* scopSeal(Context* ctx) {
  Node* self = getInstance(FatScope, ctx);
  if (IS_FAT_ERROR(self)) {
    return self;
  }

  self->scp->isSealed = true;
  return NULL;
}

/**
 * Check if scope is sealed
 */
static Node* scopIsSealed(Context* ctx) {
  Node* self = getInstance(FatScope, ctx);
  if (IS_FAT_ERROR(self)) {
    return self;
  }

  return RUNTIME_BOOLEAN(self->scp->isSealed);
}

/**
 * Turn any entry into immutable
 */
static Node* scopFreeze(Context* ctx) {
  Node* self = ctx->selfRef;
  if (!self) {
    return createError(MSG_M_PAR, true, ckCallError, ctx);
  }

  self->op = false;
  return NULL;
}
