/**
 * @file lists.c
 * @brief Hacks list impl. over scope structure
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-22
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../interpreter.h"

/**
 * Determines the number of items skipped per jump for a given index.
 * balancing speed and traversal depth.
 * This function is tuned for performance based on empirical heuristics.
 */
static inline long adaptiveSkipMax(const long index) {
  return index <= 16     ? index / SKIP_LIST_MIN
         : index <= 32   ? 1 + index / 16
         : index <= 64   ? 2 + index / 32
         : index <= 128  ? 3 + index / 64
         : index <= 256  ? 4 + index / 128
         : index <= 512  ? 5 + index / 256
         : index <= 1024 ? 6 + index / 512
                         : 7 + index / SKIP_LIST_MAX;
}

Node *addToList(Scope *list, Node *node, Context *ctx) {
  assert(list != NULL);
  assert(list->isList);

  if (!node) {
    return NULL;
  }

  // If mutable, take copy and ensure copy is immutable
  if (node->op) {
    node = copyNode(node, false, ctx);
    node->op = false;
  }

  Entry *entry = createListEntry(list->size, node);

  lockResource(&list->lock);

  if (list->entries) {
    if (list->ck != node->ck && !checkAlias(list->ck, node)) {
      unlockResource(&list->lock);
      freeListEntry(entry);
      const char *type = fatType(node->type);
      char *msg = join4("adding ", type, " item to List/", list->ck->name);
      ctx->cur->node = node;  // ensure source reference to error
      return createError(msg, false, ckTypeError, ctx);
    }

    if (!adaptiveSkipMod(list->size)) {
      list->quick1->skip = entry;
      list->quick1 = entry;  // points skip-list "next"
    }

    list->quick2->next = entry;
    list->quick2 = entry;  // points "last"

  } else {
    list->ck = node->type == FatMethod      ? checkMethod
               : node->type == FatProcedure ? checkProcedure
                                            : node->ck;
    list->entries = entry;
    list->quick1 = entry;  // points skip-list "next"
    list->quick2 = entry;  // points "last"
  }
  list->size++;

  unlockResource(&list->lock);

  UNLOCK_NODE(node);
  return NULL;  // success
}

Entry *getByIndex(Scope *list, const long index) {
  assert(list != NULL);
  assert(list->isList);

  // First item (quick resolution)
  if (index == 0) {
    return list->entries;
  }

  // Last item (quick resolution)
  if (index == list->size - 1) {
    return list->quick2;
  }

  // Smart cache (quick resolution)
  if (list->cached) {
    long cacheDiff = index - list->cached->key.i;
    if (cacheDiff >= 0 && cacheDiff < SKIP_LIST_MIN) {
      for (long i = 0; i < cacheDiff; i++) {
        list->cached = list->cached->next;
        assert(list->cached != NULL);
      }
#ifdef DEBUG
      cacheHits++;
#endif
      return list->cached;
    }
  }

  // Find item by index with skip list support, traverse
  Entry *entry = list->entries;
  assert(entry != NULL);
  long pos = adaptiveSkipMax(index);
  for (long i = 0; i < pos; i++) {
    entry = entry->skip;
    assert(entry != NULL);
  }
  pos = adaptiveSkipMod(index);
  for (long i = 0; i < pos; i++) {
    entry = entry->next;
    assert(entry != NULL);
  }
  list->cached = entry;
  return entry;
}

Node *getInList(Scope *list, long index, Context *ctx) {
  assert(list != NULL);
  assert(list->isList);

#ifdef DEBUG
  if (debugLogs) {
    auto_str num = ofInt(index);
    logDebug2(__FILE__, __func__, MSG_INDEX, num);
  }
#endif

  if (!list->size) {
    return createError("list " MSG_IS_EMPTY, true, ckIndexError, ctx);
  }

  if (index < 0) {
    index = list->size + index;
  }

  if (index < 0 || index >= list->size) {
    return createError(MSG_BOUNDS, true, ckIndexError, ctx);
  }

  return getByIndex(list, index)->data;
}

Node *selectFromList(Scope *list, long start, long end, Context *ctx) {
  assert(list != NULL);
  assert(list->isList);

#ifdef DEBUG
  if (debugLogs) {
    auto_str num1 = ofInt(start);
    auto_str num2 = ofInt(end);
    auto_str msg = join4(MSG_INDEX GUIDE, num1, " : ", num2);
    logDebug(__FILE__, __func__, msg);
  }
#endif

  if (start < 0) {
    start = list->size + start;
  }
  if (start < 0) {
    start = 0;
  }
  if (end < 0) {
    end = list->size + end;
  }

  Scope *result = createList();

  bool isEmpty = !list->size || end < start || start >= list->size || end < 0;

  if (!isEmpty) {
    Entry *entry = getByIndex(list, start);
    while (entry && (start <= end)) {
      addToList(result, entry->data, ctx);
      entry = entry->next;
      start++;
    }
  }

  return runtimeCollection(result, ctx);
}

__attribute__((noinline))  // prevent buffer allocation in recursive call stack
static void
reverseChunk(Scope *dest, Entry *src, Entry *until, Context *ctx) {
  Node *buffer[SKIP_LIST_MAX];
  int pos = 0;

  for (; src && src != until; src = src->next) {
    buffer[pos++] = src->data;
  }
  while (pos) {
    addToList(dest, buffer[--pos], ctx);
  }
}

/**
 * Recursively reverses a list using skip pointers to reduce recursion depth.
 * It minimizes stack usage by handling elements in chunks instead of
 * deep recursion, making it more memory-efficient.
 */
static void reverseRecursive(Scope *dest, Entry *src, Context *ctx) {
  if (!src) {
    return;
  }

  reverseRecursive(dest, src->skip, ctx);
  reverseChunk(dest, src, src->skip, ctx);
}

Node *reverseList(Scope *list, Context *ctx) {
  assert(list != NULL);
  assert(list->isList);

  Scope *reversed = createList();
  pushStack(ctx, __func__, NULL, reversed);

  reverseRecursive(reversed, list->entries, ctx);

  Node *result = runtimeCollection(reversed, ctx);
  popStack(ctx, 1);
  return result;
}

/**
 * Quicksort helper to compare nodes (assumes nodes of same type)
 */
static bool isGreater(const Node *a, const Node *b, const Node *key) {
  assert(a != NULL);
  assert(b != NULL);
  assert(a->type == b->type);

  if (key) {
    if (a->scp->isList) {
      const long index = (long)key->num.f;
      if (index >= a->scp->size || index >= b->scp->size) {
        return index < b->scp->size;
      }
      a = getByIndex(a->scp, index)->data;
      b = getByIndex(b->scp, index)->data;
    } else {
      a = getValueOf(a->scp, key->val);
      b = getValueOf(b->scp, key->val);
    }

    if (!a || !b) {
      return !!b;
    }

    if (a->type != b->type) {
      return a > b;  // pointer address (arbitrary, but consistent)
    }
  }

  switch (a->type) {
    case FatBoolean:
      return a->num.b > b->num.b;

    case FatNumber:
      return a->num.f > b->num.f;

    case FatHugeInt:
      return hugeIsLess(*b->num.h, *a->num.h);

    case FatText:
    case FatTemp:
      return strcmp(a->val, b->val) > 0;

    case FatList:
    case FatScope:
      if (a->scp && b->scp) {
        return a->scp->size > b->scp->size;
      }
      return !!b->scp;

    default:
      return a > b;  // pointer address (arbitrary, but consistent)
  }
}

/**
 * Quicksort helper to swap nodes, in-place (no entry re-link is needed).
 */
static inline void swapNodes(Entry *a, Entry *b) {
  Node *aux = a->data;
  a->data = b->data;
  b->data = aux;
}

/**
 * Quicksort helper to find pivot (heuristic optimization).
 * This method is generally good at avoiding worst-case scenario of O(n^2)
 * time complexity, which can occur with a consistently bad pivot choice.
 */
static void setRandomPivot(Entry *head, Entry *last) {
  size_t length = 0;
  for (Entry *current = head; current != last; current = current->next) {
    length++;
  }

  size_t randomIndex = (size_t)rnGen() % length;
  Entry *pivot = head;
  for (size_t i = 0; i < randomIndex; i++) {
    pivot = pivot->next;
  }

  swapNodes(head, pivot);
}

/**
 * Quicksort, complexity is O(n log n) in the average case, recursive impl.
 */
void quicksort(Entry *head, Entry *last, Node *key) {
  while (head && last && head != last) {
    setRandomPivot(head, last);

    Entry *i = head;
    for (Entry *j = head->next; j && j != last->next; j = j->next) {
      if (isGreater(head->data, j->data, key)) {
        swapNodes((i = i->next), j);
      }
    }

    if (head != i) {
      swapNodes(head, i);
      quicksort(head, i, key);
    }
    head = i->next;
  }
}
