/**
 * @file scope.c
 * @brief Implements fat scopes / namespaces
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-18
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../interpreter.h"

void addToScope(Scope *scope, const char *key, Node *data) {
  assert(key != NULL);
  assert(!scope->isSealed);

  Entry *toAdd = createScopeEntry(key, data);

  lockResource(&scope->lock);

  if (scope->entries) {
    Entry *entry = scope->entries;
    Entry *prev = NULL;

    // If small list, use simple strategy
    if (scope->size < MIN_QUICK_REF) {
      while (entry) {
        int cmp = strcmp(entry->key.s, key);
        assert(cmp != 0);  // duplicate key
        if (cmp > 0) {
          break;  // found the insertion point
        }
        prev = entry;
        entry = entry->next;
      }
    } else {
      // Initialize reference tuple for skip list
      IntTuple refs;
      initQuickRefs(&refs, scope->size);

      long jumpStart = 0;
      Entry *skipList = entry;

      // Optimize the start of the search operation by jumping ahead
      if (scope->quick1 && strcmp(scope->quick1->next->key.s, key) < 0) {
        jumpStart = ((scope->size - 1) / 3) + 1;
        skipList = prev = scope->quick1;
        entry = prev->next;

        // Rebalance quick references
        if (scope->size % 3 == 0) {
          scope->quick1 = entry;
        }
      }

      bool done = false;

      // Start iterating from the (possibly adjusted) start point
      for (long i = jumpStart; entry; i++) {
        // Check if we are not in the range of quick refs
        if (entry->skip && entry->skip->next) {
          if ((i > refs._1 || i < refs._1 - SKIP_SIZE) &&
              (i > refs._2 || i < refs._2 - SKIP_SIZE)) {
            // Check if the key is bigger than the next skip entry's key.
            // If so, simply jump ahead...
            if (strcmp(entry->skip->next->key.s, key) < 0) {
              i += SKIP_SIZE;
              entry = entry->skip;
            }
          }
        }

        // If not done yet, check if key is found or insertion point
        if (!done) {
          int cmp = strcmp(entry->key.s, key);
          assert(cmp != 0);  // duplicate key
          if (cmp > 0) {
            done = true;  // found the insertion point
          } else {
            prev = entry;  // walk prev pointer
          }
        }

        // Handle skip and quick reference
        if (i) {
          // If it's time to set a skip link, do it
          if (i % SKIP_SIZE == 0) {
            skipList = (skipList->skip = entry);
          }

          // Update quick references
          if (i >= refs._2) {
            if (i == refs._2) {
              scope->quick2 = entry;
            }
            if (done) {
              break;
            }
          } else if (i == refs._1) {
            scope->quick1 = entry;
          }
        }

        entry = entry->next;
      }
    }

    if (prev) {
      toAdd->next = prev->next;
      prev->next = toAdd;
    } else {
      toAdd->next = scope->entries;
      scope->entries = toAdd;
    }
  } else {
    scope->entries = toAdd;
  }

  scope->size++;
  if (!data) {
    scope->blanks++;
  }
  scope->cached = toAdd;

  unlockResource(&scope->lock);

  UNLOCK_NODE(data);
}

/**
 * Traverse entries searching by key using skip list.
 */
static Entry *seekEntry(Entry *entry, const char *key) {
  for (; entry; entry = entry->next) {
    // Try to jump via skip list
    for (; entry->skip; entry = entry->skip) {
      int cmp = strcmp(entry->skip->key.s, key);
      if (cmp == 0) {
        return entry->skip;
      }
      if (cmp > 0) {
        break;
      }
    }

    // Walk in linked list
    int cmp = strcmp(entry->key.s, key);
    if (cmp == 0) {
      return entry;
    }
    if (cmp > 0) {
      break;
    }
  }
  return NULL;
}

static inline Entry *jumpSeek(Entry *entry, const char *key) {
  return entry ? seekEntry(entry, key) : NULL;
}

/**
 * Seek for key in scope using quick refs.
 */
static Entry *quickSeek(Scope *scope, const char *key) {
  Entry *res = jumpSeek(scope->quick2, key);
  if (res) {
    scope->cached = res;
    return res;
  }

  res = jumpSeek(scope->quick1, key);
  if (res) {
    scope->cached = res;
    return res;
  }

  res = seekEntry(scope->entries, key);
  if (res) {
    scope->cached = res;
    return res;
  }

  return NULL;
}

Entry *getEntryOf(Scope *scope, const char *key) {
  assert(scope != NULL);
  assert(!scope->isList);
  assert(key != NULL);

  Entry *cached = scope->cached;
  if (cached && FAST_STR_EQ(cached->key.s, key)) {
#ifdef DEBUG
    cacheHits++;
#endif
    return cached;
  }

  return quickSeek(scope, key);
}

Node *getValueOf(Scope *scope, const char *key) {
  Entry *entry = getEntryOf(scope, key);
#ifdef DEBUG
  if (debugLogs) {
    if (entry) {
      auto_str msg = toString(entry->data);
      logDebug2(__FILE__, __func__, key, msg);
    } else {
      logDebug2(__FILE__, __func__, key, MSG_N_FOUND);
    }
  }
#endif
  return entry ? entry->data : NULL;
}

static Entry *getCtxEntryOf(Context *ctx, const char *key) {
  assert(ctx != NULL);
  assert(key != NULL);

  Scope *prev[FIND_SCP_DEDUP + 1] = {0};  // null-terminated
  const Frame *stack = ctx->stack;

  Entry *entry = NULL;
  for (int i = atomic_load(&ctx->top); i; i--) {
    Scope *scope = stack[i].scp;
    if (scope && !scope->isList) {
      // Check if scope is in previous scopes (dedup)
      bool alreadyChecked = false;
      for (int j = 0; prev[j]; j++) {
        if (scope == prev[j]) {
          alreadyChecked = true;
          break;
        }
      }
      if (!alreadyChecked) {
        entry = getEntryOf(scope, key);
        if (entry) {
          break;
        }
        // Shift previous and add current scope to the start
        memmove(&prev[1], &prev[0], sizeof(Scope *) * (FIND_SCP_DEDUP - 1));
        prev[0] = scope;
      }
    }
  }

#ifdef DEBUG
  if (debugLogs) {
    if (entry) {
      auto_str msg = toString(entry->data);
      logDebug2(__FILE__, __func__, key, msg);
    } else {
      logDebug2(__FILE__, __func__, key, MSG_N_FOUND);
    }
  }
#endif
  return entry;
}

Node *getCtxValueOf(Context *ctx, const char *key) {
  Entry *entry = getCtxEntryOf(ctx, key);
  return entry ? entry->data : NULL;
}

Node *getParameter(Scope *scope, const char *name, NodeType type,
                   Context *ctx) {
  Node *node = getValueOf(scope, name);
  if (!node) {
    char *msg = join3(MSG_M_PAR, GUIDE, name);
    return createError(msg, false, ckCallError, ctx);
  }
  if (node->type != type) {
    char *msg = join3(MSG_MISMATCH, GUIDE, name);
    return createError(msg, false, ckTypeError, ctx);
  }
  return node;
}

/**
 * Sets an entry in the scope as null.
 * Returns an error if the entry is immutable.
 */
static Node *nullifyInScope(Scope *scope, const char *key, Context *ctx) {
#ifdef DEBUG
  if (debugLogs) {
    logDebug(__FILE__, __func__, key);
  }
#endif

  Entry *entry = getEntryOf(scope, key);
  if (!entry) {
    if (scope->isSealed) {
      return createError("scope is sealed", true, ckAssignError, ctx);
    }
    addToScope(scope, key, NULL);
    return NULL;
  }
  if (!entry->data) {
    return NULL;
  }

  if (!entry->data->op) {
    char *msg = join2("erase immutable" GUIDE, key);
    return createError(msg, false, ckAssignError, ctx);
  }

  lockResource(&scope->lock);
  Node *oldData = entry->data;
  entry->data = NULL;
  scope->blanks++;
  unlockResource(&scope->lock);

  if (oldData->meta) {
    atomic_fetch_sub(&oldData->meta->refs, 1);
  }
  return NULL;
}

static void logStoreEvent(const char *key, Node *val, OpCode isMut) {
  bool isTextType = val->type == FatText;
  auto_str msg = isTextType ? join3("'", val->val, "'") : toString(val);
  logDebug2(__FILE__, isMut ? "storeMut" : "storeImmut", key, msg);
}

/**
 * Apply copy-on-write strategy (reuse value if possible).
 */
static Node *cow(const char *key, Node *val, const OpCode isMut, Context *ctx) {
  // Is inserting mutable value or into a mutable entry?
  if (isMut || val->op) {
    // Is it from source or referenced somewhere already?
    if (!val->meta || atomic_load(&val->meta->refs)) {
      val = copyNode(val, false, ctx);  // untangle
    }

    // Apply mutability option of entry
    val->op = isMut;
  }

  if (debugLogs) {
    logStoreEvent(key, val, isMut);
  }

  return val;
}

/**
 * Updates or inserts a key-value pair into the scope.
 * Ensures correct type, immutability, and reference safety.
 */
Node *upsertNode(Scope *scope, const char *key, Node *val, const OpCode isMut,
                 Context *ctx) {
#ifdef DEBUG
  if (debugLogs) {
    logDebug(__FILE__, __func__, key);
  }
#endif

  Entry *entry = getEntryOf(scope, key);

  if (entry) {
    // Is existing empty
    if (entry->data == NULL) {
      // (scope->blanks-- happens inside swapEntryData with resource lock)
      return swapEntryData(scope, entry, cow(key, val, isMut, ctx));
    }

    Node *prevVal = entry->data;

    // Is previous value immutable?
    if (!prevVal->op) {
      char *msg = join2(MSG_REASSIGN GUIDE, key);
      return createError(msg, false, ckAssignError, ctx);
    }

    // Is entry already holding the exact same node?
    if (prevVal == val) {
      return val;  // short-circuit
    }

    // Are we respecting the base type?
    if (prevVal->type == val->type) {
      val = cow(key, val, true, ctx);
      if (prevVal->ck) {
        val->ck = prevVal->ck;  // keep previous type alias
      }
      // Note: allows transitioning mutable values to error
    } else if (val->type != FatError) {
      char *msg = join4(MSG_MISMATCH GUIDE, key, ": ", fatType(val->type));
      return createError(msg, false, ckTypeError, ctx);
    }

    return swapEntryData(scope, entry, val);
  } else if (scope->isSealed) {
    return createError("scope is sealed", true, ckAssignError, ctx);
  }

  val = cow(key, val, isMut, ctx);
  addToScope(scope, key, val);
  return val;
}
