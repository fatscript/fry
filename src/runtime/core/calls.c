/**
 * @file calls.c
 * @brief Perform calls of types, indexables, scopes and methods
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-16
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../../syntax/parser.h"
#include "../interpreter.h"

static inline Node *autoCall(Scope *scope, Node *procedure, Context *ctx);

/**
 * Resolve call argument, with method auto-call bypass
 * (detach is a remedy to a mysterious bug caused by GC misbehaving on type
 * instantiation - as using the direct value on type instances can cause GC
 * to collect those AST nodes corrupting the program source itself, but it's
 * not the case with method calls - yet, not sure why this is happening...)
 */
static Node *resolveArg(Scope *scope, Node *arg, bool detach, Context *ctx) {
  Node *resolved = NULL;

  if (IS_FAT_TYPE(arg, FatEntry)) {
    if (ctx->enclosingI != ctx->cur->scp) {
      resolved = getValueOf(ctx->enclosingI, arg->val);
    }
    if (!resolved) {
      resolved = getCtxValueOf(ctx, arg->val);
    }

    // If argument resolves to a procedure, call it immediately!
    // Note: the tilde '~' in '~procedure' sets op==true, signaling explicit
    // reference mode. Which bypasses autoCall and returns node itself.
    if (!arg->op && IS_FAT_TYPE(resolved, FatProcedure)) {
      resolved = autoCall(scope, resolved, ctx);
    }
  } else {
    resolved = evalNode(scope, arg, ctx);
  }

  if (!resolved) {
    return NULL;
  }

  // Is argument immutable?
  if (!resolved->op) {
    // Is node dynamic and no need to detach?
    if (resolved->meta || !detach) {
      return resolved;  // pass by reference
    }
  }

  // Otherwise, take a shallow copy and set argument as immutable
  resolved = copyNode(resolved, false, ctx);
  resolved->op = false;
  return resolved;
}

/**
 * Add call site arguments keyed by method signature to method scope.
 */
static char *loadMethodScope(Scope *callSiteScope, Scope *methodScope,
                             const Node *callAst, const Node *meth,
                             Context *ctx) {
  // Iterate over each parameter slot in the method signature
  Node *argExpr = callAst->body;
  if (meth->head) {
    long i = 1;
    for (Node *slotExpr = meth->head; slotExpr; slotExpr = slotExpr->seq, i++) {
      Node *slot = slotExpr;

      // If no argument is provided, use the default value from signature
      if (!argExpr) {
        if (slot->type == FatAssign) {
          evalAssign(methodScope, slot->head, slot->body, ctx);
          continue;
        }
        auto_str pos = ofInt(i);
        return join4("missing argument '", slot->val, "' at pos: ", pos);
      }

      Node *arg = argExpr;
      if (argExpr->type == FatAssign) {
        // Check names match (first time)
        if (argExpr->op) {
          const char *argName = argExpr->head->val;
          const char *paramName =
            slot->type == FatAssign ? slot->head->val : slot->val;
          if (!FAST_STR_EQ(argName, paramName)) {
            auto_str pos = ofInt(i);
            return join4("invalid name '", argName, "' at pos: ", pos);
          }
          argExpr->op = false;  // skip validation for subsequent calls
        }
        // Ignore assignment (hereby considered a label) and only take the value
        arg = argExpr->body;
      }

      // Apply default values for null arguments (shorthand)
      if (arg->type == FatVoid && slot->type == FatAssign) {
        evalAssign(methodScope, slot->head, slot->body, ctx);
        argExpr = argExpr->seq;
        continue;
      }

#ifdef DEBUG
      if (debugLogs) {
        const char *paramName =
          slot->type == FatAssign ? slot->head->val : slot->val;
        logDebug2(__FILE__, __func__, "resolve", paramName);
      }
#endif

      Node *resolved = resolveArg(callSiteScope, arg, false, ctx);

      // Apply default values for arguments resolved to null
      if (slot->type == FatAssign) {
        if (!resolved || resolved->type == FatVoid) {
          evalAssign(methodScope, slot->head, slot->body, ctx);
          argExpr = argExpr->seq;
          continue;
        }
        slot = slot->head;  // ...or use left-hand side only for following code
      }

      if (!checkAlias(slot->ck, resolved)) {
        auto_str argT = nodeTypeToString(resolved);
        const char *parT = slot->ck->name;
        auto_str info = join4(argT, " provided, but ", parT, " expected");
        return join4(MSG_MISMATCH GUIDE, slot->val, GUIDE, info);
      }

      // Add the resolved argument to method scope
      addToScope(methodScope, slot->val, resolved);

      argExpr = argExpr->seq;
    }
  } else if (argExpr) {
    // Ignore assignment (hereby considered a label) and only take the value
    Node *arg = argExpr->type == FatAssign ? argExpr->body : argExpr;
    Node *implicitArg = resolveArg(callSiteScope, arg, false, ctx);
    addToScope(methodScope, "_", implicitArg);  // MARK_UND
  }

  return NULL;
}

/**
 * Check for errors that ocurred with loadMethodScope.
 */
static Node *getLoadingError(char *err, Context *ctx) {
  Type *type = startsWith(err, MSG_MISMATCH) ? ckTypeError : ckCallError;
  return createError(err, false, type, ctx);
}

/**
 * Leaves loaded parameters GC ready ("unlocks" scope for GC).
 */
static inline void unloadMethodScope(Scope *methodScope) {
  trackScope(methodScope);
  collectScope(methodScope);  // try to immediately deallocate
}

/**
 * Create a curried method instance from a method and a call.
 */
static Node *curryCall(Scope *scope, const Node *method, Node *call,
                       Context *ctx) {
  if (debugLogs) {
    logDebug(__FILE__, __func__, method->val ? method->val : "(anonymous)");
  }

  // Load call site arguments
  Scope *methodScope = createScope();
  pushStack(ctx, __func__, call, methodScope);
  char *err = loadMethodScope(scope, methodScope, call, method, ctx);

  if (err) {
    unloadMethodScope(methodScope);  // reverse loadMethodScope
    popStack(ctx, 1);
    return getLoadingError(err, ctx);
  }

  // Create a new node and link the body of method as curried method
  Node *curriedMethod = createNode(method->body->type, SRC_RUN, ctx);
  curriedMethod->ck = method->body->ck;
  curriedMethod->head = method->body->head;
  curriedMethod->body = method->body->body;

  // Join args with previously curried scopes, if any
  if (method->scp) {
    Scope *jointScope = mergeScopes(method->scp, methodScope, ctx);
    unloadMethodScope(methodScope);
    methodScope = jointScope;
  }

  bindScope(curriedMethod, methodScope);  // partial apply

  popStack(ctx, 1);
  return curriedMethod;
}

static inline const char *methodName(const Node *call) {
  return call->head && call->head->val ? call->head->val : "(anonymous)";
}

Node *interpretWithTRO(Scope *scope, Node *methodBody, Context *ctx) {
  Node *result = interpret(scope, methodBody, ctx);

  while (IS_FAT_TYPE(result, FatCall)) {
    Memory *localMemBottom = ctx->temp;

    Node *method = ctx->lastMethod;  // fast-track recursion

    // Load parameters into a temporary scope
    Scope tempScope = {0};
    initLayer(&tempScope);
    char *err = loadMethodScope(scope, &tempScope, result, method, ctx);
    if (err) {
      wipeScope(&tempScope, NULL, false);  // reverse loadMethodScope
      return getLoadingError(err, ctx);
    }

    // Stack "compaction" step, trades space for housekeeping, by
    // replacing the execution scope in-place (clean and copy over)
    wipeScope(scope, NULL, true);
    for (Entry *entry = tempScope.entries; entry; entry = entry->next) {
      addToScope(scope, entry->key.s, entry->data);
    }
    wipeScope(&tempScope, NULL, false);  // clean up temporary scope

    if (debugLogs) {
      const char *name = method->val;
      logDebug2(__FILE__, __func__, stderrIsTTY ? CL_CYN "iter" : "iter", name);
    }
    result = interpret(scope, method->body, ctx);
    if (useCollector && ctx->tempCount) {
      microGC(result, localMemBottom, ctx);
    }
  }

  return result;
}

static Node *executeMethodBody(Scope *scope, Node *method, Context *ctx) {
  bool isCallMode = ctx->isWithinCall;
  ctx->isWithinCall = true;

  int stacked = 1;
  if (method->scp) {
    pushStack(ctx, __func__, method, method->scp);
    stacked = 2;
  }
  pushStack(ctx, __func__, method->body, scope);
  Scope *prevEnclosing = scopeInward(scope, ctx);
  Node *result = interpretWithTRO(scope, method->body, ctx);
  scopeOutward(prevEnclosing, ctx);
  popStack(ctx, stacked);

  ctx->isWithinCall = isCallMode;
  return result;
}

Node *evalMethodCall(Scope *scope, Node *call, Node *method, Context *ctx) {
  if (!method->body) {
    if (debugLogs) {
      logDebug2(__FILE__, __func__, stderrIsTTY ? CL_CYN "no-op" : "no-op",
                methodName(call));
    }
    return NULL;  // no-op
  }

  if (method->body->type == FatMethod || method->body->type == FatProcedure) {
    return curryCall(scope, method, call, ctx);
  }

  Memory *localMemBottom = ctx->temp;
  Scope methodLayer = {0};
  initLayer(&methodLayer);
  if (method->type == FatMethod) {
    char *err = loadMethodScope(scope, &methodLayer, call, method, ctx);
    if (err) {
      wipeScope(&methodLayer, NULL, false);  // reverse loadMethodScope
      return getLoadingError(err, ctx);
    }
  } else if (call->body) {  // presumed: method->type == FatProcedure
    char *err =
      join2(methodName(call), GUIDE "procedure cannot take arguments");
    return createError(err, false, ckCallError, ctx);
  }

  auto_str ref = NULL;
  if (debugLogs) {
    static unsigned int callId = 43690;  // aaaa
    if (asprintf(&ref, "%s #%04x", methodName(call), callId++) == -1) {
      fatalOut(__FILE__, __func__, MSG_OOM);
      exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
    }
    logDebug2(__FILE__, __func__, stderrIsTTY ? CL_CYN "begin" : "begin", ref);
  }

  // Store fast-track recursion reference into context
  Node *prevMethod = ctx->lastMethod;
  if (method->val) {
    ctx->lastMethod = method;
  }

  Node *result = executeMethodBody(&methodLayer, method, ctx);
  wipeScope(&methodLayer, NULL, false);  // reverse loadMethodScope
  if (useCollector && ctx->tempCount) {
    microGC(result, localMemBottom, ctx);
  }

  // Validate the type of returned value against method signature
  // note: regardless of signature, null and error are always "returnable"
  if (result && !checkAlias(method->ck, result) && !IS_FAT_ERROR(result)) {
    UNLOCK_NODE(result);
    const char *type = result->ck ? result->ck->name : fatType(result->type);
    char *msg = join3(methodName(call), GUIDE "bad return value" GUIDE, type);
    result = createError(msg, false, ckTypeError, ctx);
  }

  // Restore fast-track recursion reference into context
  ctx->lastMethod = prevMethod;

  if (debugLogs && ref) {
    logDebug2(__FILE__, __func__, stderrIsTTY ? CL_CYN "end" : "end", ref);
  }

  return result;
}

/**
 * Resolve index(es) of indexables (for Chunk, Text and List types).
 */
static Node *resolveIndexes(Scope *scope, const char *name, Node *arg,
                            const Node *entry, NodeTuple *range, Node *aux,
                            Context *ctx) {
  if (!arg || arg->seq) {
    return createError(join2(MSG_I_IDX, name), false, ckIndexError, ctx);
  }

  bool isRange = arg->op == OpRange || arg->op == OpHORange;

  // Extract index arguments of different valid syntaxes
  if (arg->type == FatUnary && isRange) {
    // (..i2) syntax
    aux->type = FatNumber;
    aux->num.f = 0;
    range->_1 = aux;  // fake node
    range->_2 = evalNode(scope, arg->body, ctx);
  } else if (arg->type == FatExpr && isRange) {
    range->_1 = evalNode(scope, arg->head, ctx);
    if (arg->body) {
      // (i1..i2) syntax
      range->_2 = evalNode(scope, arg->body, ctx);
    } else {
      // (i1..) syntax
      aux->type = FatNumber;
      aux->num.f = entry->scp ? (double)entry->scp->size : (double)entry->num.s;
      range->_2 = aux;  // fake node
    }
  } else {
    // (i1)
    range->_1 = evalNode(scope, arg, ctx);
  }

  // Check we have at least one valid index value or fail
  if (!IS_FAT_TYPE(range->_1, FatNumber)) {
    return createError(join2(MSG_I_IDX, name), false, ckIndexError, ctx);
  }

  // In case we do have a second value...
  if (range->_2) {
    // ensure it's valid
    if (range->_2->type != FatNumber) {
      return createError(join2(MSG_I_IDX, name), false, ckIndexError, ctx);
    }
  }

  return NULL;  // success
}

/**
 * Resolve indexed access (for Chunk, Text and List types).
 */
static Node *evalIndexed(Scope *scope, const char *name, Node *arg, Node *entry,
                         Context *ctx) {
  NodeTuple range = {0};
  Node aux = {0};  // node buffer for inferred indexes
  pushStack(ctx, __func__, entry, scope);

  Node *error = resolveIndexes(scope, name, arg, entry, &range, &aux, ctx);
  if (error) {
    popStack(ctx, 1);
    return error;
  }

  assert(range._1 != NULL);

  Node *result = NULL;
  if (range._2) {
    long start = (long)range._1->num.f;
    long end = (long)range._2->num.f;

    // Adjust start/end if using half-open range operator
    if (arg->op == OpHORange) {
      if (end) {
        end--;
      } else {
        start = 1;  // nothing to select
      }
    }

    switch (entry->type) {
      case FatChunk:
        result = selectFromChunk(entry, start, end, ctx);
        break;
      case FatText:
        result = selectFromText(entry, start, end, ctx);
        break;
      default:
        result = selectFromList(entry->scp, start, end, ctx);
    }
    UNLOCK_NODE(range._2);
  } else {
    long index = (long)range._1->num.f;
    switch (entry->type) {
      case FatChunk:
        result = getInChunk(entry, index, ctx);
        break;
      case FatText:
        result = getInText(entry, index, ctx);
        break;
      default:
        result = getInList(entry->scp, index, ctx);
    }
  }
  UNLOCK_NODE(range._1);

  popStack(ctx, 1);
  return result;
}

/**
 * Get the entry from Scope (see also setDynamic in chaining.c)
 */
static Node *getInScope(Node *scope, const Node *arg, Context *ctx) {
  static char key[NUMBER_MAX_LENGTH + 1];

  NodeType type = arg ? arg->type : FatVoid;
  Node *result = NULL;

  switch (type) {
    case FatNumber:
      result = getValueOf(scope->scp, prettyNumber(key, arg->num.f));
      break;

    case FatText:
      result = getValueOf(scope->scp, arg->val);
      break;

    default:
      result = createError(MSG_IDR, true, ckKeyError, ctx);
  }

  UNLOCK_NODE(scope);
  return result;
}

/**
 * Load default props from given type and/or included types.
 */
static void evalDefaults(Scope *instance, const Node *type, Context *ctx) {
  // Resolve undefined values from type default values
  for (Node *prop = type->head; prop; prop = prop->seq) {
    const char *propName = prop->val;
    if (!getEntryOf(instance, propName)) {
      Entry *entry = getEntryOf(type->body->scp, propName);
      if (entry) {
        Node *copy = NULL;
        Node *defaultValue = entry->data;
        if (defaultValue && defaultValue->scp) {
          if (!initVisit(__func__)) {
            fatalOut(__FILE__, __func__, MSG_U_REC);
            exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
          }
          copy = copyNode(defaultValue, true, ctx);
          endVisit();
        } else {
          copy = copyNode(defaultValue, false, ctx);
        }
        addToScope(instance, propName, copy);
      }
    }
  }

  if (!type->ck || !type->ck->include) {
    return;
  }

  // Resolve undefined values from included types (recursively)
  for (int i = 0; type->ck->include[i]; i++) {
    Type *base = resolveAlias(type->ck->include[i]);
    Node *include = base->def;
    if (include) {
      evalDefaults(instance, include, ctx);
    } else {
      auto_str msg = join2(MSG_M_INC ": ", base->name);
      stackOut(__FILE__, __func__, msg, ctx);  // non-trappable
    }
  }
}

static Node *getConstructor(Node *def) {
  Node *constructor = getValueOf(def->tail->scp, "apply");
  if (def->ck && def->ck->include) {
    for (int i = 0; !constructor && def->ck->include[i]; i++) {
      Type *base = resolveAlias(def->ck->include[i]);
      Node *include = base->def;
      if (include) {
        constructor = getConstructor(include);
      }
    }
  }
  return constructor;
}

static Node *failInstance(const char *name, const char *err, int i, Type *ck,
                          Scope *instance, Context *ctx) {
  popStack(ctx, 1);  // instance
  trackScope(instance);
  auto_str pos = ofInt(i);
  char *msg = join5(name, GUIDE, err, " at pos: ", pos);
  return createError(msg, false, ck, ctx);
}

static Node *instantiate(Scope *scope, const char *name, Node *arg, Node *type,
                         Context *ctx) {
  Type *alias = type->ck;

  // Use the base type definition
  type = resolveAlias(alias)->def;

  if (!type) {
    char *msg = join2(MSG_N_FOUND GUIDE, name);
    return createError(msg, false, ckKeyError, ctx);
  }

  // Load values from arguments
  Scope *instance = createScope();
  pushStack(ctx, __func__, type, instance);
  instance->ck = alias;  // keep type from alias
  Node *prop = type->head;
  bool mayHaveSkippedSlots = !arg;
  for (int i = 1; arg && prop; arg = arg->seq, prop = prop->seq, i++) {
    if (arg->type == FatAssign) {
      mayHaveSkippedSlots = true;
      Node *left = arg->head;
      for (Node *slot = type->head; slot; slot = slot->seq) {
        if (strEq(slot->val, left->val)) {
          left = slot;  // swap left side of expression to slot
          break;        // keep slot's original type and mutability options
        }
      }
      if (left != arg->head || isAlias(type->ck, checkVoid)) {
        Node *resolved = resolveArg(scope, arg->body, true, ctx);
        if (resolved) {
          Node *stored = evalAssign(instance, left, resolved, ctx);
          if (IS_FAT_TYPE(stored, FatError)) {
            UNLOCK_NODE(resolved);
            UNLOCK_NODE(stored);
            return failInstance(name, MSG_I_ARG, i, ckCallError, instance, ctx);
          }
        }
      } else {
        return failInstance(name, MSG_I_ARG, i, ckCallError, instance, ctx);
      }
    } else if (arg->val &&
               (arg->type == FatMethod || arg->type == FatProcedure)) {
      mayHaveSkippedSlots = true;
      Node *slot = NULL;
      // Check if method exists as parameter in the type definition
      for (slot = type->head; slot; slot = slot->seq) {
        if (strEq(slot->val, arg->val) && isAlias(slot->ck, checkMethod)) {
          break;
        }
      }
      if (slot) {
        Node *method = evalNode(instance, arg, ctx);
        method->op = slot->op;  // keep slot's mutability option
      } else {
        return failInstance(name, MSG_I_ARG, i, ckCallError, instance, ctx);
      }
    } else if (!mayHaveSkippedSlots) {
      Node *resolved = resolveArg(scope, arg, true, ctx);
      if (checkAlias(prop->ck, resolved)) {
        if (resolved) {
          Node *stored = evalAssign(instance, prop, resolved, ctx);
          if (IS_FAT_TYPE(stored, FatError)) {
            UNLOCK_NODE(resolved);
            UNLOCK_NODE(stored);
            return failInstance(name, MSG_I_ARG, i, ckCallError, instance, ctx);
          }
        }
      } else {
        UNLOCK_NODE(resolved);
        return failInstance(name, MSG_MISMATCH, i, ckTypeError, instance, ctx);
      }
      if (prop->seq && !arg->seq) {
        mayHaveSkippedSlots = true;  // enable mandatory arguments check
      }
    } else {
      // once mayHaveSkippedSlots can't assume order
      return failInstance(name, "unnamed arg", i, ckCallError, instance, ctx);
    }
  }

  // Load default values for missing arguments
  evalDefaults(instance, type, ctx);

  // Ensure all required properties were provided (explicitly or via defaults)
  if (mayHaveSkippedSlots) {
    for (prop = type->head; prop; prop = prop->seq) {
      if (!getEntryOf(instance, prop->val)) {
        popStack(ctx, 1);  // instance
        trackScope(instance);
        char *msg = join5(name, GUIDE, "no argument for '", prop->val, "'");
        return createError(msg, false, ckCallError, ctx);
      }
    }
  }

  Node *result = NULL;

  // Call apply method (constructor), if available
  Node *constructor = getConstructor(type);
  if (constructor) {
    Node aux = {.type = FatScope, .ck = alias, .scp = instance, .src = SRC_AUX};
    Node *previousSelf = ctx->selfRef;
    ctx->selfRef = &aux;
    Scope *prevEnclosing = scopeInward(instance, ctx);
    result = interpret(instance, constructor->body, ctx);
    scopeOutward(prevEnclosing, ctx);
    ctx->selfRef = previousSelf;
    if (result == &aux) {
      result = copyNode(&aux, false, ctx);  // untangle
    } else {
      trackScope(instance);
    }
  } else {
    result = runtimeCollection(instance, ctx);
  }

  // Set typecheck reference to proper alias (if applicable)
  if (result) {
    switch (result->type) {
      case FatProcedure:
      case FatMethod:
      case FatError:
      case FatVoid:
        break;  // do nothing

      case FatScope:
        if (result->scp) {
          result->scp->ck = alias;
        }
        break;

      default:
        result->ck = alias;
    }
  }

  popStack(ctx, 1);  // instance
  return result;
}

static inline bool canInterpretWithTRO(Node *call, Context *ctx) {
  if (!call->op) {
    return false;  // call not tagged as tail-recursive call
  }

  // If this call has been previously optimized, no need to check again
  static Node *optimizedCall = NULL;
  if (call == optimizedCall) {
    return true;
  }

  // If this call was previously found unsuitable for TRO, skip it
  static Node *evictedCall = NULL;
  if (call == evictedCall) {
    return false;
  }

  // Check if there is a failure handler, which prevents TRO
  int top = atomic_load(&ctx->top);
  for (int i = 0; (i <= 2) && (0 < top - i); i++) {
    if (ctx->stack[top - i].trap) {
      logAlert(call->head->val, "TRO skipped",
               "failure handler detected!\n"
               "To enable Tail Recursion Optimization, move 'failure.trap'\n"
               "out of the recursive method body, or wrap the call in double\n"
               "parentheses to suppress this warning without optimizing.");
      evictedCall = call;
      return false;
    }
  }

  // Passed check, apply TRO strategies
  optimizedCall = call;
  return true;
}

Node *evalCall(Scope *scope, Node *call, Node *callee, Context *ctx) {
  Node *head = call->head;
  char *name = IS_FAT_TYPE(head, FatEntry) ? head->val : "(anonymous)";
  Node *result = NULL;

  while (call) {
    if (!callee) {
      char *msg = join4("nothing to call" GUIDE, name, GUIDE, call->src);
      return createError(msg, false, ckCallError, ctx);
    }

    switch (callee->type) {
      case FatChunk:
      case FatText:
      case FatList:
        result = evalIndexed(scope, name, call->body, callee, ctx);
        break;

      case FatScope:
        result = getInScope(callee, evalNode(scope, call->body, ctx), ctx);
        break;

      case FatMethod:
        if (canInterpretWithTRO(call, ctx)) {
          return call;
        }
        FALL_THROUGH;
      case FatProcedure:
        result = evalMethodCall(scope, call, callee, ctx);
        break;

      case FatType:
        if (callee->ck->isComposite) {
          return createError(
            join4("composite type" GUIDE, callee->ck->name, GUIDE, call->src),
            false, ckCallError, ctx);
        }
        result = instantiate(scope, callee->ck->name, call->body, callee, ctx);
        break;

      default:
        UNLOCK_NODE(callee);
        return createError(join4("non-callable" GUIDE, name, GUIDE, call->src),
                           false, ckCallError, ctx);
    }

    // Prepare for looping over nested/chained calls
    name = "(nested)";
    UNLOCK_NODE(callee);
    callee = result;
    call = call->tail;
  }

  return result;
}
