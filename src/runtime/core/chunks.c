/**
 * @file chunks.c
 * @brief Binary data handling
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-22
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../interpreter.h"

char *forceToUtf8(const char *chunk, size_t len) {
  // The worst-case scenario is every byte turns into a replacement
  size_t maxOutputLen = len * 3 + 1;  // +1 for null-terminator
  char *result = FRY_ALLOC(maxOutputLen);

  size_t outputIndex = 0;
  for (size_t i = 0; i < len;) {
    int validBytes = chunk[i] ? utf8bytes(&chunk[i]) : 0;
    if (validBytes > 0 && (i + validBytes) <= len) {
      // Valid UTF-8 bytes, copy over
      memcpy(&result[outputIndex], &chunk[i], validBytes);
      outputIndex += validBytes;
      i += validBytes;
    } else {
      // Invalid UTF-8 section, skip over
      do {
        i++;
      } while (i < len && (!chunk[i] || !utf8bytes(&chunk[i])));
      if (i < len) {
        memcpy(&result[outputIndex], "\xEF\xBF\xBD", 3);
        outputIndex += 3;
      }
    }
  }

  result[outputIndex] = '\0';
  return FRY_REALLOC(result, outputIndex + 1);
}

Node *getInChunk(Node *node, long index, Context *ctx) {
#ifdef DEBUG
  if (debugLogs) {
    auto_str num = ofInt(index);
    logDebug2(__FILE__, __func__, MSG_INDEX, num);
  }
#endif

  if (!node->val) {
    return createError("chunk " MSG_IS_EMPTY, true, ckIndexError, ctx);
  }

  long length = node->num.s;
  if (index < 0) {
    index = length + index;
  }

  if (index < 0 || index >= length) {
    return createError(MSG_BOUNDS, true, ckIndexError, ctx);
  }

  return runtimeNumber((Byte)node->val[index], ctx);
}

Node *selectFromChunk(Node *node, long start, long end, Context *ctx) {
#ifdef DEBUG
  if (debugLogs) {
    auto_str num1 = ofInt(start);
    auto_str num2 = ofInt(end);
    auto_str msg = join4(MSG_INDEX GUIDE, num1, "..", num2);
    logDebug(__FILE__, __func__, msg);
  }
#endif

  long length = node->num.s;
  if (start < 0) {
    start = length + start;
  }
  if (start < 0) {
    start = 0;
  }
  if (end < 0) {
    end = length + end;
  }
  if (end >= length) {
    end = length - 1;
  }

  if ((!length) || (end < start) || (start >= length) || (end < 0)) {
    return runtimeChunk(NULL, 0, ctx);
  }

  size_t len = end - start + 1;
  return runtimeChunk(copyChunk(node->val + start, len), len, ctx);
}

Node *hugeToChunkImpl(Node *huge, Context *ctx) {
  int len = 0;
  char *val = hugeToBytes(*huge->num.h, &len);
  if (len == -1) {
    fatalOut(__FILE__, __func__, MSG_OOM);
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }

  return runtimeChunk(val, len, ctx);
}

Node *chunkToHugeImpl(Node *chunk, Context *ctx) {
  if (chunk->num.s > HUGE_INT_SIZE * sizeof(uint32_t)) {
    auto_str size = ofInt(HUGE_INT_SIZE * sizeof(uint32_t));
    char *msg = join3("size cannot exceed ", size, " bytes");
    return createError(msg, false, ckValueError, ctx);
  }

  HugeInt num = {0};
  bytesToHuge((unsigned char *)chunk->val, chunk->num.s, num);
  return runtimeHuge(num, ctx);
}
