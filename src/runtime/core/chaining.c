/**
 * @file chaining.c
 * @brief Handles dot operator sequences
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-16
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../interpreter.h"

/**
 * Get dynamic key val e.g: scope.[key], see also getInScope in calls.c
 */
static Node *setDynamic(Scope *scope, Node *dyn, Node *buff, Context *ctx) {
  Node *node = evalNode(scope, dyn->body, ctx);

  NodeType type = node ? node->type : FatVoid;
  Node *result = NULL;

  switch (type) {
    case FatNumber:
      buff->num.f = node->num.f;
      buff->val = prettyNumber(NULL, node->num.f);
      result = buff;
      break;

    case FatText:
      buff->num.f = INFINITY;  // used as marker
      buff->val = copyChunk(node->val, node->num.s);
      result = buff;
      break;

    default:
      result = createError(MSG_IDR, true, ckKeyError, ctx);
  }

  UNLOCK_NODE(node);
  return result;
}

static inline const char *getEntryName(const Node *node) {
  switch (node->type) {
    case FatEntry:
    case FatType:
      return node->val;

    case FatCall:
      return node->head->val;

    default:
      return NULL;
  }
}

static inline bool isDotOp(const Node *node) {
  return IS_FAT_TYPE(node, FatExpr) &&
         (node->op == OpDot || node->op == OpIfDot);
}

static inline Type *getNativeType(const NodeType type) {
  switch (type) {
    case FatVoid:
      return checkVoid;
    case FatBoolean:
      return checkBoolean;
    case FatNumber:
      return checkNumber;
    case FatHugeInt:
      return checkHugeInt;
    case FatChunk:
      return checkChunk;
    case FatText:
    case FatTemp:
      return checkText;
    case FatList:
      return checkList;
    case FatScope:
      return checkScope;
    case FatProcedure:
      return checkProcedure;
    case FatMethod:
      return checkMethod;
    case FatError:
      return checkError;
    default:
      return NULL;
  }
}

static inline Node *getTypeOf(Node *node) {
  Type *check = NULL;

  if (node->type == FatScope && node->scp) {
    check = node->scp->ck;  // actual type is set inside scope
  } else if (node->type == FatMethod) {
    check = checkMethod;  // method->ck is return type
  } else {
    check = resolveAlias(node->ck);
  }

  if (!check) {
    check = getNativeType(node->type);
  }

  return check ? check->def : NULL;
}

static inline Node *getPrototype(const Node *def) {
  return def ? def->tail : NULL;
}

/**
 * Automatically invokes a procedure without arguments.
 */
static inline Node *autoCall(Scope *scope, Node *procedure, Context *ctx) {
  pushStack(ctx, __func__, procedure, scope);
  Node call = {.type = FatCall, .head = procedure, .src = SRC_AUX};
  Node *result = evalMethodCall(scope, &call, procedure, ctx);
  popStack(ctx, 1);
  return result;
}

static bool canResolveViaProto(Node *proto, const char *name,
                               NodeTuple *result);

static inline bool canResolveViaInclude(const Type *type, const char *name,
                                        NodeTuple *result) {
  if (!type || !type->include) {
    return false;
  }

  for (int i = 0; type->include[i]; i++) {
    Type *base = resolveAlias(type->include[i]);
    Node *inProto = getPrototype(base->def);
    if (inProto) {
      if (canResolveViaProto(inProto, name, result)) {
        return true;
      }
    } else {
      auto_str msg = join3(type->name, GUIDE MSG_M_INC ": ", base->name);
      logAlert(__FILE__, __func__, msg);
    }
  }
  return false;
}

static bool canResolveViaProto(Node *proto, const char *name,
                               NodeTuple *result) {
  Node *node = getValueOf(proto->scp, name);
  if (node) {
    result->_1 = proto;
    result->_2 = node;
    return true;
  }
  return canResolveViaInclude(proto->ck, name, result);
}

/**
 * Overlay left scope and resolve right side of chained expression.
 */
static Node *resolveRight(Node *left, Node *right, Node *node, Context *ctx) {
  Node *result = NULL;

  switch (right->type) {
    case FatCall:
      pushStack(ctx, __func__, right, left->scp);
      result = evalCall(left->scp, right, node, ctx);
      popStack(ctx, 1);
      return result;

    case FatEntry:
      // If argument resolves to a procedure, call it immediately!
      // Note: the tilde '~' in '~procedure' sets op==true, signaling explicit
      // reference mode. Which bypasses autoCall and returns node itself.
      if (!right->op && IS_FAT_TYPE(node, FatProcedure)) {
        return autoCall(left->scp, node, ctx);
      }
      return node;

    default:
      return NULL;
  }
}

static Node *evalDot(Scope *scope, Node *node, Context *ctx);

static Node *resolveLeft(Scope *scope, Node *node, Context *ctx) {
  return isDotOp(node) ? evalDot(scope, node, ctx) : evalNode(scope, node, ctx);
}

static Node *evalDot(Scope *scope, Node *node, Context *ctx) {
  assert(node != NULL);

  Node *right = node->body;

  if (!right) {
    return NULL;
  }

  Node *result = NULL;

  // Eval scoped block
  if (right->type == FatBlock) {
    Node *left = resolveLeft(scope, node->head, ctx);
    Node *previousSelf = ctx->selfRef;
    ctx->selfRef = left;
    if (IS_FAT_TYPE(left, FatScope) && left->scp) {
      pushStack(ctx, __func__, right, left->scp);
      Scope *prevEnclosing = scopeInward(left->scp, ctx);
      result = interpret(left->scp, right, ctx);
      scopeOutward(prevEnclosing, ctx);
      popStack(ctx, 1);
    } else {
      result = interpret(scope, right, ctx);
    }
    ctx->selfRef = previousSelf;
    return result;
  }

  // dynBuff acts as a temporary placeholder for resolved key values
  Node dynBuff = {.type = FatEntry, .op = true, .val = NULL, .src = SRC_AUX};
  if (right->type == FatDynamic) {
    right = setDynamic(scope, right, &dynBuff, ctx);
  }

  const char *name = getEntryName(right);
  if (!name) {
    return createError("bad dynamic", true, ckKeyError, ctx);
  }

  Node *left = resolveLeft(scope, node->head, ctx);

  if (left) {
    pushStack(ctx, __func__, left, scope);

    if (right == &dynBuff) {
      if (left->type == FatScope) {
        // If dynamic Scope access, must be a value with this name
        Node *entry = getValueOf(left->scp, name);
        result = resolveRight(left, right, entry, ctx);
      } else {
        // If is a dynamic access to an indexable...
        if (isinf(dynBuff.num.f)) {
          // If key is non-numeric, fail!
          char *msg = join2(MSG_I_IDX, name);
          result = createError(msg, false, ckIndexError, ctx);
        } else {
          // Otherwise, if type is indexable, get value by index
          long index = dynBuff.num.f;
          switch (left->type) {
            case FatChunk:
              result = getInChunk(left, index, ctx);
              break;
            case FatText:
              result = getInText(left, index, ctx);
              break;
            case FatList:
              result = getInList(left->scp, index, ctx);
              break;
            default:
              result = createError(join2("non-indexable" GUIDE, node->src),
                                   false, ckIndexError, ctx);
              break;
          }
        }
      }
    } else {
      // If not a dynamic access, maybe it is a prototype member
      NodeTuple target = {0};
      Node *proto = getPrototype(getTypeOf(left));
      if (proto && canResolveViaProto(proto, name, &target)) {
        Node *previousSelf = ctx->selfRef;
        ctx->selfRef = left;
        result = resolveRight(target._1, right, target._2, ctx);
        ctx->selfRef = previousSelf;

      } else if (left->type == FatScope) {
        // If FatScope and not prototype member, may be a value with this name
        Entry *entry = getEntryOf(left->scp, name);
        if (entry) {
          result = resolveRight(left, right, entry->data, ctx);
        } else if (right->type == FatCall) {
          char *msg = join4("nothing to call" GUIDE, name, GUIDE, right->src);
          result = createError(msg, false, ckCallError, ctx);
        } else {
          char *msg = join4("key " MSG_N_FOUND GUIDE, name, GUIDE, right->src);
          result = createError(msg, false, ckKeyError, ctx);
        }

      } else {
        // We assume a failed prototype member call
        char *msg = join4("nothing to call" GUIDE, name, GUIDE, right->src);
        result = createError(msg, false, ckCallError, ctx);
      }
    }

    popStack(ctx, 1);  // left
    UNLOCK_NODE(left);
  } else if (node->op == OpDot) {
    Node *proto = getPrototype(checkVoid->def);  // lift null into Void type
    Node *voidProp = proto ? getValueOf(proto->scp, name) : NULL;

    if (voidProp) {  // can resolve via proto
      result = autoCall(proto->scp, voidProp, ctx);
    } else {  // left is expected to be defined
      char *msg = join3("can't resolve scope of '", name, "'");
      result = createError(msg, false, ckTypeError, ctx);
    }
  }  // else it's OpIfDot (just ignore, don't emit error)

  free(dynBuff.val);
  return result;
}

/**
 * Assign on chained inner entry, like: outer.inner = right
 */
static Node *innerAssign(Scope *scope, Node *left, Node *right, Context *ctx) {
  // Ensure assignment has a valid left-hand side (e.g. obj.prop = ...)
  if (!left || !left->body) {
    return createError("no left-hand side", true, ckAssignError, ctx);
  }

  Node *outer = resolveLeft(scope, left->head, ctx);
  if (!outer || IS_FAT_ERROR(outer)) {
    return outer;
  }

  pushStack(ctx, __func__, outer, scope);

  Node *inner = left->body;

  // dynBuff acts as a temporary placeholder for resolved key values
  Node dynBuff = {.type = FatEntry, .op = true, .val = NULL, .src = SRC_AUX};
  if (inner->type == FatDynamic) {
    inner = setDynamic(scope, inner, &dynBuff, ctx);
  }

  Node *result = NULL;
  if (outer->type == FatScope) {
    Node *value = evalNode(scope, right, ctx);
    result = evalAssign(outer->scp, inner, value, ctx);
    UNLOCK_NODE(value);
  } else if (!outer->op) {
    char *msg = join2(MSG_REASSIGN GUIDE, left->src);
    result = createError(msg, false, ckAssignError, ctx);
  } else if (inner == &dynBuff) {
    if (isinf(dynBuff.num.f)) {
      // If key is non-numeric, fail!
      char *msg = join2(MSG_I_IDX GUIDE, left->body->src);
      result = createError(msg, false, ckIndexError, ctx);
    } else {
      const long index = dynBuff.num.f;

      switch (outer->type) {
        case FatChunk:
          if (index < 0 || index > (long)outer->num.s) {
            result = createError(MSG_BOUNDS, true, ckIndexError, ctx);
          } else {
            Node *value = evalNode(scope, right, ctx);

            // Fails required type?
            if (!IS_FAT_TYPE(value, FatNumber)) {
              char *msg = join2(MSG_MISMATCH GUIDE, "Number required");
              result = createError(msg, false, ckAssignError, ctx);
            } else {
              // Fails required range [0-255]?
              if (value->num.f < 0 || value->num.f > 255) {
                result = createError(MSG_I_N_ARG, true, ckAssignError, ctx);
              } else {
                if (index == (long)outer->num.s) {
                  outer->num.s += 1;
                  outer->val = FRY_REALLOC(outer->val, outer->num.s);
                }
                outer->val[index] = (Byte)value->num.f;
                result = outer;
              }
            }
          }
          break;

        case FatText:
          if (index < 0 || index > (long)outer->num.s) {
            result = createError(MSG_BOUNDS, true, ckIndexError, ctx);
          } else {
            Node *value = evalNode(scope, right, ctx);

            // Fails required type?
            if (!IS_FAT_TYPE(value, FatText)) {
              char *msg = join2(MSG_MISMATCH GUIDE, "Text required");
              result = createError(msg, false, ckAssignError, ctx);
            } else {
              size_t charCount = utf8len(value->val);
              if (charCount != 1) {
                result = createError(MSG_I_C_ARG, true, ckAssignError, ctx);
              } else if (index == (long)outer->num.s) {
                int newBytes = utf8bytes(value->val);
                size_t newSize = outer->num.s + newBytes;
                outer->val = FRY_REALLOC(outer->val, newSize + 1);
                memcpy(outer->val + outer->num.s, value->val, newBytes);
                outer->num.s = newSize;
                outer->val[newSize] = '\0';
                result = outer;
              } else {
                char *ptr = outer->val;
                for (long i = 0; i < index; i++) {
                  ptr = utf8next(ptr);
                }
                int oldBytes = utf8bytes(ptr);
                int newBytes = utf8bytes(value->val);
                int diff = oldBytes - newBytes;
                if (diff) {
                  size_t newSize = outer->num.s - diff;
                  size_t oldOffset = ptr - outer->val;
                  outer->val = FRY_REALLOC(outer->val, newSize + 1);
                  ptr = outer->val +
                        oldOffset;  // recalculate ptr based on new address
                  outer->num.s = newSize;
                  size_t remaining = outer->num.s - (ptr - outer->val) - diff;
                  memmove(ptr + newBytes, ptr + oldBytes, remaining);
                  outer->val[newSize] = '\0';
                }
                memcpy(ptr, value->val, newBytes);
                result = outer;
              }
            }
          }
          break;

        case FatList:
          if (index < 0 || index > outer->scp->size) {
            result = createError(MSG_BOUNDS, true, ckIndexError, ctx);
          } else {
            Node *value = evalNode(scope, right, ctx);
            if (index == outer->scp->size) {
              Node *err = addToList(outer->scp, value, ctx);
              result = err ? err : outer;
            } else if (!value) {
              result =
                createError("can't nullify item", true, ckAssignError, ctx);
            } else {
              NodeType type = value->type;
              Entry *entry = getByIndex(outer->scp, index);
              Node *prevVal = entry->data;

              // Fails required type?
              if (prevVal->type != type || !checkType(prevVal->ck, value)) {
                const char *required =
                  prevVal->ck ? prevVal->ck->name : fatType(prevVal->type);
                char *msg = join3(MSG_MISMATCH GUIDE, required, " required");
                result = createError(msg, false, ckAssignError, ctx);
              } else {
                swapEntryData(outer->scp, entry,
                              value->op ? copyNode(value, false, ctx) : value);
                result = outer;
              }
            }
          }
          break;

        default:
          result = createError(join2("non-indexable" GUIDE, outer->src), false,
                               ckIndexError, ctx);
          break;
      }
    }
  } else {
    result = createError("failed to assign", true, ckAssignError, ctx);
  }

  popStack(ctx, 1);  // outer
  free(dynBuff.val);
  return result;
}
