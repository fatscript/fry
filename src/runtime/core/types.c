/**
 * @file types.c
 * @brief Implements type definitions
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-16
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../interpreter.h"

Node *evalType(const char *name) {
  Type *type = getType(name);
  Node *def = type ? type->def : NULL;
#ifdef DEBUG
  if (debugLogs) {
    logDebug2(__FILE__, __func__, name, def ? "defined" : "undefined");
  }
#endif
  return def;
}

Type *createAlias(const char *name, Type *forwardTo) {
  Type *alias = setType(name, NULL);

  // Prevent Any alias and/or alias redefinition
  if (!alias || alias->alias) {
    return NULL;
  }

  const Type *resolved = resolveAlias(forwardTo);

  // Prevent circular reference
  if (alias == resolved) {
    logAlert(__func__, "circular reference", name);
    return NULL;
  }

  // Create alias
  alias->alias = forwardTo;

  // If a native type was aliased, rewire!
  if (alias == checkVoid) {
    checkVoid = forwardTo;
  } else if (alias == checkBoolean) {
    checkBoolean = forwardTo;
  } else if (alias == checkNumber) {
    checkNumber = forwardTo;
  } else if (alias == checkHugeInt) {
    checkHugeInt = forwardTo;
  } else if (alias == checkChunk) {
    checkChunk = forwardTo;
  } else if (alias == checkText) {
    checkText = forwardTo;
  } else if (alias == checkList) {
    checkList = forwardTo;
  } else if (alias == checkScope) {
    checkScope = forwardTo;
  } else if (alias == checkMethod) {
    checkMethod = forwardTo;
  } else if (alias == checkError) {
    checkError = forwardTo;
  }

  alias->def = createNode(FatType, SRC_RUN, NULL);
  alias->def->ck = alias;
  alias->def->val = strDup(name);

  if (debugLogs) {
    logDebug2(__FILE__, __func__, name, forwardTo->name);
  }

  return alias;
}

static bool isComposable(Node *props) {
  if (!props) {
    return false;
  }

  if (props->type == FatType) {
    return true;
  }

  if (props->type == FatExpr && props->op == OpSlash) {
    return isComposable(props->head) && isComposable(props->body);
  }

  return false;
}

static void addToCompositeList(char **types, Node *props) {
  if (props->type == FatType) {
    int i = 0;
    for (; types[i]; i++) {
      if (i == TYPE_MAX) {
        fatalOut(__FILE__, __func__, MSG_BMO);
        exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
      }
    }
    types[i] = props->val;
  } else {  // props->type == FatExpr
    addToCompositeList(types, props->head);
    addToCompositeList(types, props->body);
  }
}

/**
 * Add type into metaspace
 */
static Node *storType(char *name, Node *props, Context *ctx) {
  if (isReservedName(name)) {
    char *msg = join2("typename is reserved" GUIDE, name);
    return createError(msg, false, ckValueError, ctx);
  }

  Node *node = NULL;

  // alias
  if (props->type == FatType && !props->seq) {
    if (isReservedName(props->val)) {
      char *msg = join2("alias not allowed" GUIDE, props->val);
      node = createError(msg, false, ckValueError, ctx);

    } else {
      Type *forwardTo = getType(props->val);
      Type *type = getType(name);

      // is redefining the name as a different alias?
      if (type && type->alias) {
        if (type->alias != forwardTo) {
          char *msg = join2(MSG_REASSIGN GUIDE, name);
          node = createError(msg, false, ckAssignError, ctx);

          // otherwise ignore redeclaration... (use previous)
        } else {
          node = type->def;
        }

        // create alias
      } else {
        Type *alias = createAlias(name, forwardTo);
        if (alias) {
          node = alias->def;
        } else {
          char *msg = join2("can't create alias to ", props->val);
          node = createError(msg, false, ckAssignError, ctx);
        }
      }
    }

    // create composite
  } else if (props->type == FatExpr && isComposable(props)) {
    char *types[TYPE_MAX + 1] = {0};
    addToCompositeList(types, props);
    Type *composite = createComposite(name, types);
    if (composite) {
      if (composite->def) {
        node = composite->def;
      } else {
        node = createNode(FatType, strDup(props->src), ctx);
        node->ck = composite;
        node->val = strDup(name);
        composite->def = node;
      }
    } else {
      node = createError(join2(MSG_I_CT GUIDE, name), false, ckValueError, ctx);
    }

    // declare type
  } else {
    Type *existing = getType(name);

    // Bail out on new attempt of defining same type
    // (there is also a "deep check" inside setType)
    if (existing && existing->def) {
      if (strcmp(existing->def->src, props->src) == 0) {
        return existing->def;
      }
    }

    Node *slots = NULL;
    Scope *defaults = createScope();
    pushStack(ctx, __func__, node, defaults);
    Scope *proto = createScope();
    pushStack(ctx, __func__, node, proto);

    Type *include[TYPE_MAX] = {0};
    int totalIncluded = 0;

    Node *current = NULL;
    Node *next = NULL;

    bool malformed = false;
    char *malSrc = NULL;

    for (Node *prop = props; !malformed && prop; prop = next) {
      next = prop->seq;
      Node *aux = NULL;

      switch (prop->type) {
        // prototype members
        case FatProcedure:
        case FatMethod:
          if (prop->val) {
            if (strcmp(prop->val, "apply") == 0 && prop->ck) {
              auto_str warn =
                join2(name, GUIDE "return type of apply won't be checked");
              logAlert(__FILE__, __func__, warn);
            }
            if (IS_FAT_ERROR(upsertNode(proto, prop->val, prop, false, ctx))) {
              auto_str err =
                join4(name, GUIDE "repeated '", prop->val, "' member");
              logError(__FILE__, __func__, err);
              malformed = true;
              malSrc = prop->src;
            }
          }
          break;

          // includes
        case FatType:
          if (totalIncluded < TYPE_MAX) {
            include[totalIncluded++] = setType(prop->val, NULL);
          } else {
            auto_str typeMax = ofInt(TYPE_MAX);
            auto_str err = join3(name, GUIDE "includes exceed ", typeMax);
            logError(__FILE__, __func__, err);
            malformed = true;
            malSrc = prop->src;
          }
          break;

          // ordinary props
        default:
          if (prop->type == FatAssign && IS_FAT_TYPE(prop->head, FatEntry)) {
            aux = prop->head;

            Node *stored = evalAssign(defaults, aux, prop->body, ctx);
            if (IS_FAT_TYPE(stored, FatError)) {
              auto_str err = join4(name, GUIDE "bad '", aux->val, "' prop");
              logError(__FILE__, __func__, err);
              malformed = true;
              malSrc = prop->src;
            }

          } else {
            aux = prop;
          }

          // add prop or fail
          if (IS_FAT_TYPE(aux, FatEntry)) {
            if (!slots) {
              slots = current = copyNode(aux, false, ctx);
              pushStack(ctx, __func__, slots, proto);
            } else {
              current->seq = copyNode(aux, false, ctx);
              current = current->seq;
            }
          } else {
            const char *type = fatType(aux ? aux->type : FatVoid);
            auto_str err = join4(name, GUIDE "'", type, "' unexpected");
            logError(__FILE__, __func__, err);
            malformed = true;
            malSrc = prop->src;
          }
          break;
      }
    }

    // Check for no repeated props
    if (!malformed) {
      for (Node *slot = slots; slot; slot = slot->seq) {
        for (Node *other = slot->seq; other; other = other->seq) {
          if (strcmp(slot->val, other->val) == 0) {
            auto_str err = join4(name, GUIDE "repeated '", slot->val, "' prop");
            logError(__FILE__, __func__, err);
            malformed = true;
            malSrc = other->src;
          }
        }
      }
    }

    if (malformed) {
      for (Node *slot = slots; slot; slot = slot->seq) {
        UNLOCK_NODE(slot);
      }
      trackScope(defaults);
      trackScope(proto);
      char *msg = join2("invalid type definition" GUIDE, malSrc);
      node = createError(msg, false, ckSyntaxError, ctx);
    } else {
      node = createNode(FatType, cpSrc(props), ctx);
      node->val = strDup(name);
      node->head = slots;
      node->body = runtimeCollection(defaults, ctx);
      node->tail = runtimeCollection(proto, ctx);

      // Register typedef (may point to previous definition, if exists)
      node->tail->ck = node->ck = setType(name, node);

      // Annotate includes if this is not a duplicate definition
      if (node->ck->def == node) {
        if (totalIncluded == 1 && *include == checkStrict) {
          // Don't include StrictType alone as it defeats its own purpose
        } else if (totalIncluded > 0) {
          node->ck->include = FRY_ALLOC(sizeof(Type *) * (totalIncluded + 1));
          for (int i = 0; i < totalIncluded; i++) {
            node->ck->include[i] = include[i];
          }
          node->ck->include[totalIncluded] = NULL;
        }

        if (debugLogs) {
          logDebug(__FILE__, __func__, name);
        }
      }
    }

    popStack(ctx, slots ? 3 : 2);  // runtime, proto->scp, slots?
  }

  return node;
}
