/**
 * @file loops.c
 * @brief Implements map, range, while loops
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-16
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "../interpreter.h"

static Node *getMapper(Node *body, Context *ctx) {
  if (!body) {
    return NULL;
  }
  if (body->type == FatMethod) {
    return body;
  }
  if (body->type == FatEntry) {
    Node *mapper = getCtxValueOf(ctx, body->val);
    return IS_FAT_TYPE(mapper, FatMethod) ? mapper : NULL;
  }
  return NULL;
}

int mapPushStack(Context *ctx, const char *func, Node *input, Scope *output,
                 Node *mapper, Scope *layer) {
  int stacked = 2;
  pushStack(ctx, func, input, output);
  if (mapper->scp) {
    pushStack(ctx, func, mapper, mapper->scp);
    stacked = 3;
  }
  pushStack(ctx, func, mapper, layer);
  return stacked;
}

/**
 * Calls a mapper function on an argument entry, executing the transformation.
 * Ensures temporary scope memory is properly cleaned after execution.
 */
static inline Node *mapApply(Scope *layer, Entry *arg, Node *fn, Context *ctx) {
  Memory *localMemBottom = ctx->temp;
  layer->entries = arg;
  layer->size = 1;
  Node *mapped = interpretWithTRO(layer, fn, ctx);
  wipeScope(layer, arg, true);
  if (useCollector && ctx->tempCount) {
    microGC(mapped, localMemBottom, ctx);
  }
  return mapped;
}

/**
 * Cleans up and exits mapping due to an error.
 * Ensures the layer mutex is destroyed to avoid memory leaks.
 */
static Node *mapFail(const char *func, Scope *layer, Node *mapped, Node *error,
                     Scope *res, Scope *prev, int stacked, Context *ctx) {
  pthread_mutex_destroy(&layer->lock);
  scopeOutward(prev, ctx);
  popStack(ctx, stacked);
  UNLOCK_NODE(mapped);
  trackScope(res);
  if (debugLogs) {
    logDebug(__FILE__, func, "fail");
  }
  return error;
}

static Node *mapSucceed(const char *func, Scope *layer, Scope *res, Scope *prev,
                        int stacked, Context *ctx) {
  Node *listResult = runtimeCollection(res, ctx);
  scopeOutward(prev, ctx);
  popStack(ctx, stacked);
  pthread_mutex_destroy(&layer->lock);
  if (debugLogs) {
    logDebug(__FILE__, func, "end");
  }
  return listResult;
}

static Node *mapScope(Node *coll, Node *mapper, Context *ctx) {
  if (!mapper) {
    return createError("map" GUIDE MSG_NO_METH, true, ckCallError, ctx);
  }

  const Node *firstArg = getFirstArgument(mapper->head);

  // Scope can only be mapped with methods that have Text as first parameter
  if (firstArg->ck && !isAlias(checkText, firstArg->ck)) {
    return createError("mapper" GUIDE MSG_MISMATCH, true, ckTypeError, ctx);
  }

  lockResource(&coll->scp->lock);
  Entry *entry = coll->scp->entries;
  unlockResource(&coll->scp->lock);

  Scope *result = createList();

  if (!entry) {
    return runtimeCollection(result, ctx);
  }

  if (debugLogs) {
    logDebug(__FILE__, __func__, "start");
  }

  Scope layer = {0};
  initLayer(&layer);
  int stacked = mapPushStack(ctx, __func__, coll, result, mapper, &layer);
  Scope *prev = scopeInward(&layer, ctx);

  while (entry) {
    if (!entry->data) {
      lockResource(&coll->scp->lock);
      entry = entry->next;
      unlockResource(&coll->scp->lock);
      continue;  // skip over null values (e.g. import flags)
    }

    Entry arg = {.key.s = firstArg->val,
                 .data = runtimeTextDup(entry->key.s, ctx)};

    lockResource(&coll->scp->lock);
    coll->scp->cached = entry;
    entry = entry->next;
    unlockResource(&coll->scp->lock);

    Node *mapped = mapApply(&layer, &arg, mapper->body, ctx);
    Node *err =
      ctx->failureEvent ? ctx->failureEvent : addToList(result, mapped, ctx);
    if (err) {
      return mapFail(__func__, &layer, mapped, err, result, prev, stacked, ctx);
    }
  }

  return mapSucceed(__func__, &layer, result, prev, stacked, ctx);
}

static Node *mapList(Node *coll, Node *mapper, Context *ctx) {
  if (!mapper) {
    return createError("map" GUIDE MSG_NO_METH, true, ckCallError, ctx);
  }

  Scope *result = createList();

  if (!coll->scp->entries) {
    return runtimeCollection(result, ctx);
  }

  const Node *firstArg = getFirstArgument(mapper->head);

  // Lists can only be processed with methods that match the type of list items
  if (!checkAlias(firstArg->ck, coll->scp->entries->data)) {
    return createError("mapper" GUIDE MSG_MISMATCH, true, ckTypeError, ctx);
  }

  if (debugLogs) {
    logDebug(__FILE__, __func__, "start");
  }

  Scope layer = {0};
  initLayer(&layer);
  int stacked = mapPushStack(ctx, __func__, coll, result, mapper, &layer);
  Scope *prev = scopeInward(&layer, ctx);

  // Note: lists are append only no lock needed to access head
  for (Entry *entry = coll->scp->entries; entry; entry = entry->next) {
    Entry arg = {.key.s = firstArg->val, .data = entry->data};

    Node *mapped = mapApply(&layer, &arg, mapper->body, ctx);
    Node *err =
      ctx->failureEvent ? ctx->failureEvent : addToList(result, mapped, ctx);
    if (err) {
      return mapFail(__func__, &layer, mapped, err, result, prev, stacked, ctx);
    }
  }

  return mapSucceed(__func__, &layer, result, prev, stacked, ctx);
}

static Node *mapRange(IntTuple range, Node *mapper, Context *ctx) {
  if (!mapper) {
    return createError("range" GUIDE MSG_NO_METH, true, ckCallError, ctx);
  }

  const Node *firstArg = getFirstArgument(mapper->head);

  // Range can only be processed with methods that take numbers as argument
  if (firstArg->ck && !isAlias(checkNumber, firstArg->ck)) {
    return createError("mapper" GUIDE MSG_MISMATCH, true, ckTypeError, ctx);
  }

  if (debugLogs) {
    logDebug(__FILE__, __func__, "start");
  }

  Scope *result = createList();

  Scope layer = {0};
  initLayer(&layer);
  int stacked = mapPushStack(ctx, __func__, NULL, result, mapper, &layer);
  Scope *prev = scopeInward(&layer, ctx);

  const long start = range._1;
  const long end = range._2;
  const bool isAsc = start < end;
  const long step = isAsc ? 1L : -1L;

  for (long i = start; isAsc ? i <= end : i >= end; i += step) {
    Entry arg = {.key.s = firstArg->val, .data = runtimeNumber((double)i, ctx)};

    Node *mapped = mapApply(&layer, &arg, mapper->body, ctx);
    Node *err =
      ctx->failureEvent ? ctx->failureEvent : addToList(result, mapped, ctx);
    if (err) {
      return mapFail(__func__, &layer, mapped, err, result, prev, stacked, ctx);
    }
  }

  return mapSucceed(__func__, &layer, result, prev, stacked, ctx);
}

static inline void adjustHalfOpenRange(IntTuple *range) { range->_2 -= 1L; }

static inline bool isRangeOp(const OpType op) {
  return op == OpRange || op == OpHORange;
}

static inline bool hasContextEnded(Context *ctx) {
  if (ctx->timeout > 0 && getCurrentMs(CLOCK_MONOTONIC) > ctx->timeout) {
    atomic_store(&ctx->isCanceled, true);
    return true;
  }
  return atomic_load(&ctx->isCanceled);
}

static Node *evalRangeLoop(Node *start, Node *end, OpType op, Node *loopBody,
                           Context *ctx) {
  IntTuple range = {start ? (long)start->num.f : 0L, (long)end->num.f};
  UNLOCK_NODE(start);
  UNLOCK_NODE(end);

  if (op == OpHORange) {
    adjustHalfOpenRange(&range);
    if (range._1 > range._2) {
      return runtimeCollection(createList(), ctx);
    }
  }
  return mapRange(range, getMapper(loopBody, ctx), ctx);
}

/**
 * Handles range-based mapping (..index, index..index), list mapping,
 * scope mapping, and while loops.
 */
static Node *evalLoop(Scope *scope, Node *node, Context *ctx) {
  if (!node->head) {
    return NULL;
  }

  Node *head = node->head;
  Node *body = node->body;
  Node *result = NULL;

  // Map over range (..index)
  if (head->type == FatUnary && isRangeOp(head->op)) {
    Node *end = evalNode(scope, head->body, ctx);
    if (IS_FAT_TYPE(end, FatNumber)) {
      return evalRangeLoop(NULL, end, head->op, body, ctx);
    }
    UNLOCK_NODE(end);
    return createError(MSG_I_IDX "range", true, ckIndexError, ctx);
  }

  // Map over range (index..index)
  if (head->type == FatExpr && isRangeOp(head->op)) {
    Node *start = evalNode(scope, head->head, ctx);
    Node *end = evalNode(scope, head->body, ctx);
    if (IS_FAT_TYPE(start, FatNumber) && IS_FAT_TYPE(end, FatNumber)) {
      return evalRangeLoop(start, end, head->op, body, ctx);
    }
    UNLOCK_NODE(start);
    UNLOCK_NODE(end);
    return createError(MSG_I_IDX "range", true, ckIndexError, ctx);
  }

  Node *entry = evalNode(scope, head, ctx);

  if (!entry) {
    return NULL;
  }

  if (entry->type == FatScope) {
    result = mapScope(entry, getMapper(body, ctx), ctx);

  } else if (entry->type == FatList) {
    result = mapList(entry, getMapper(body, ctx), ctx);

  } else if (IS_FAT_TYPE(body, FatMethod)) {
    result = createError("bad while-loop body", true, ckSyntaxError, ctx);
  } else {
    // While
    if (debugLogs) {
      logDebug(__FILE__, __func__, "start");
    }

    while (booleanOf(entry) && !hasContextEnded(ctx)) {
      UNLOCK_NODE(entry);
      UNLOCK_NODE(result);
      result = interpret(scope, body, ctx);

      if (ctx->failureEvent || IS_FAT_TYPE(result, FatError)) {
        break;
      }

      // Reevaluate condition
      entry = evalNode(scope, head, ctx);
    }

    if (debugLogs) {
      logDebug(__FILE__, __func__, "end");
    }
  }

  UNLOCK_NODE(entry);
  return result;
}
