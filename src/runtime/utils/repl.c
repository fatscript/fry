/**
 * @file repl.c
 * @brief Cool moves on the terminal for the fry repl
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-16
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "repl.h"

#include "../../syntax/formatter.h"
#include "../../syntax/patterns.h"
#include "../interpreter.h"

static int getOpenBrackets(const char* buffer) {
  int opened = 0;
  long i = indexOf(buffer, '{');
  if (i >= 0) {
    while (buffer[i] && hasBracket(buffer[i], &opened)) {
      i++;
    }
    if (!opened && buffer[i]) {
      return getOpenBrackets(&buffer[i]);
    }
  }
  return opened;
}

static const char* getInputPrompt(bool isBlank, int openCount) {
  if (isUtf8Mode) {
    return isBlank ? MRG_1ST : openCount ? MRG_OPN : MRG_NTH;
  }
  return MRG_INP;
}

#ifndef __EMSCRIPTEN__
// Emscripten build has just a mocked readline, no actual impl.

bool getInput(char* buffer) {
  endCursesMode();

  // Get user input
  int openCount = getOpenBrackets(buffer);
  const char* prompt = getInputPrompt(!*buffer, openCount);
  char* input = readline(prompt);

  // Has user pressed Ctrl+D ?
  if (!input) {
    return false;
  }

  bool hasInput = !isBufferBlank(input);

  // If buffer is empty and input is empty, just jump back 1 line
  if (!*buffer && !hasInput) {
    fputs("\033[1A", stdout);
    fflush(stdout);
    return true;
  }

  // Check we have space on the input buffer
  size_t len = strlen(input);
  size_t usedLen = strnlen(buffer, BUFF_LEN);
  if (usedLen + len >= BUFF_LEN) {
    fatalOut(__FILE__, __func__, MSG_BMO);
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }

  // Add line to the buffer
  snprintf(buffer + usedLen, BUFF_LEN - usedLen, "%s\n", input);

  // Store history (without semicolon)
  if (input[len - 1] == ';') {
    input[len - 1] = '\0';
  }
  add_history(input);

  free(input);
  fflush(stdout);
  return true;
}

#else  // __EMSCRIPTEN__

bool getInput(char* buffer) {
  endCursesMode();

  int openCount = getOpenBrackets(buffer);
  char* input = readline(getInputPrompt(!*buffer, openCount));

  size_t len = strlen(input);
  size_t usedLen = strnlen(buffer, BUFF_LEN);
  if (usedLen + len >= BUFF_LEN) {
    fatalOut(__FILE__, __func__, MSG_BMO);
    exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
  }

  strncat(buffer, input, BUFF_LEN - usedLen - 1);
  usedLen += len;

  free(input);
  fflush(stdout);
  return true;
}

#endif

char* getSessionHeader(void) {
  struct timespec t = {0};
  static char buff[H_BUFF_MAX] = "# -\n\n";
  clock_gettime(CLOCK_REALTIME, &t);
  time_t epoch = t.tv_sec + (time_t)timeOffset;
  strftime(buff, H_BUFF_MAX - 1, "# " DEFAULT_DATE_FMT "\n\n", gmtime(&epoch));
  return buff;
}
