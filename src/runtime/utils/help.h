/**
 * @file help.h
 * @brief Interactive REPL help
 * @author Antonio Prates <hello@aprates.dev>
 * @version 2.0.0
 * @date 2023-12-22
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2023, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#pragma once

#include "../interpreter.h"
#include "../../syntax/parser.h"

/**
 * @brief Ad-hoc run help method
 *
 * @param buffer user input
 * @return true if has processed input or provided help
 */
bool getHelp(const char *buffer);
