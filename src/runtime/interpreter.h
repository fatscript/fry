/**
 * @file interpreter.h
 * @brief Fry engine header
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-16
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#pragma once

#include "../sdk/sdk.h"

// interpreter options
extern bool showResult;    // show result at the end of execution (option)
extern bool keepDotFry;    // keep config file session for main session
extern bool isJailMode;    // restrict file system, network and sys calls
extern bool interactive;   // flag is running in interactive mode
extern int fryArgc;        // CLI arg count
extern char** fryArgv;     // CLI arg values (list)
extern double timeOffset;  // local time offset for conversions (in seconds)

// async control
extern pthread_mutex_t consoleLock;

/**
 * @brief Replaces any invalid UTF-8 sequences with 'U+FFFD'.
 * (implemented in chunks.c)
 *
 * @param chunk binary data to be converted
 * @param len length of the binary data in bytes
 * @return string (use free)
 */
char* forceToUtf8(const char* chunk, size_t len);

/**
 * @brief Get a byte from chunk as a new number.
 * (implemented in chunks.c)
 *
 * @param node chunk node (primitive)
 * @param index position to look up
 * @param ctx Context*
 * @return Node*
 */
Node* getInChunk(Node* node, long index, Context* ctx);

/**
 * @brief Select bytes from chunk and return as new chunk.
 * (implemented in chunks.c)
 *
 * @param node chunk node (primitive)
 * @param start position to start selection
 * @param end position to end selection
 * @param ctx Context*
 * @return Node*
 */
Node* selectFromChunk(Node* node, long start, long end, Context* ctx);

/**
 * @brief Converts HugeInt to Chunk.
 * (implemented in chunks.c)
 *
 * @param huge hugeint node (primitive)
 * @param ctx Context*
 * @return Node*
 */
Node* hugeToChunkImpl(Node* huge, Context* ctx);

/**
 * @brief Converts Chunk to HugeInt.
 * (implemented in chunks.c)
 *
 * @param chunk node (primitive)
 * @param ctx Context*
 * @return Node*
 */
Node* chunkToHugeImpl(Node* chunk, Context* ctx);

/**
 * @brief Interpret method body with tail-recursion optimization support.
 * (implemented in calls.c)
 *
 * @param scope collection of nodes (namespace)
 * @param methodBody abstract syntax tree (method body head)
 * @param ctx Context*
 * @return Node*
 */
Node* interpretWithTRO(Scope* scope, Node* methodBody, Context* ctx);

/**
 * @brief Eval call node, with method ast directly.
 * (implemented in calls.c)
 *
 * @param scope current namespace
 * @param call node
 * @param method method ast
 * @param ctx Context*
 * @return Node*
 */
Node* evalMethodCall(Scope* scope, Node* call, Node* method, Context* ctx);

/**
 * @brief Eval call on any callable (types, indexables, scopes and methods).
 * (implemented in calls.c)
 *
 * @param scope current namespace
 * @param call Node*
 * @param callee Node*
 * @param ctx Context*
 * @return Node*
 */
Node* evalCall(Scope* scope, Node* call, Node* callee, Context* ctx);

/**
 * @brief Append node to list collection, and also set quick refs.
 * (implemented in lists.c)
 *
 * @param list collection of nodes (list type)
 * @param node to append
 * @param ctx Context*
 * @return Node* (NULL on success, or error)
 */
Node* addToList(Scope* list, Node* node, Context* ctx);

/**
 * @brief Get entry by index in list, no input validation.
 * (implemented in lists.c)
 *
 * @param list collection of nodes (list type)
 * @param index position to look up
 * @return Entry*
 */
Entry* getByIndex(Scope* list, long index);

/**
 * @brief Try to skip a chunk of items on the list using quick refs.
 * (implemented in lists.c)
 *
 * @param list collection of nodes (list type)
 * @param index position to look up
 * @param ctx Context*
 * @return Node*
 */
Node* getInList(Scope* list, long index, Context* ctx);

/**
 * @brief Select items from list collection and return as new list.
 * (implemented in lists.c)
 *
 * @param list collection of nodes (list type)
 * @param start position to start selection
 * @param end position to end selection
 * @param ctx Context*
 * @return Node*
 */
Node* selectFromList(Scope* list, long start, long end, Context* ctx);

/**
 * @brief Reverse sequence from a list collection into new list.
 * (implemented in lists.c)
 *
 * @param list collection of nodes (list type)
 * @param ctx Context*
 * @return Node*
 */
Node* reverseList(Scope* list, Context* ctx);

/**
 * @brief Quicksort function for linked list, in-place operation.
 * (implemented in lists.c)
 *
 * @param head Entry*
 * @param last Entry*
 * @param key Node*
 */
void quicksort(Entry* head, Entry* last, Node* key);

/**
 * @brief Shorthand for stacking all necessary elements for a loop handler.
 * (implemented in loops.c)
 *
 * @param ctx Context*
 * @param func function reference
 * @param input Node*
 * @param output Scope*
 * @param mapper Node* (method to be applied)
 * @param layer Scope*
 * @return int
 */
int mapPushStack(Context* ctx, const char* func, Node* input, Scope* output,
                 Node* mapper, Scope* layer);

/**
 * @brief Joins entries of b over a.
 * (implemented in operations.c)
 *
 * @param a Scope*
 * @param b Scope*
 * @param ctx Context*
 * @return Scope* (new)
 */
Scope* mergeScopes(Scope* a, Scope* b, Context* ctx);

/**
 * @brief Adds a key-value pair to the scope, maintaining sorted order.
 * Uses a skip list for optimized lookup when scope size is large.
 * @note Do not add an existing key, check with getValueOf before.
 * (implemented in scopes.c)
 *
 * @param scope collection of nodes
 * @param key name of entry
 * @param data node to insert
 */
void addToScope(Scope* scope, const char* key, Node* data);

/**
 * @brief Search for entry by key in namespace (low-level).
 * (implemented in scopes.c)
 *
 * @param scope collection of nodes
 * @param key of entry to search
 * @return Entry* entry, if found, else NULL
 */
Entry* getEntryOf(Scope* scope, const char* key);

/**
 * @brief High-level search in namespace (with cache boost).
 * (implemented in scopes.c)
 *
 * @param scope collection of nodes (namespace)
 * @param key entry to search for
 * @return Node* entry value, if found, else NULL
 */
Node* getValueOf(Scope* scope, const char* key);

/**
 * @brief Search for entry, from inner to outer scopes.
 * (implemented in scopes.c)
 *
 * @param ctx Context*
 * @param key entry to search for
 * @return Node* entry if found, else NULL
 */
Node* getCtxValueOf(Context* ctx, const char* key);

/**
 * @brief Get value of entry on scope of specific type or return error.
 * (implemented in scopes.c)
 *
 * @param scope collection of nodes (namespace)
 * @param name of entry to search for (expected)
 * @param type of entry (expected)
 * @param ctx Context*
 * @return Node*
 */
Node* getParameter(Scope* scope, const char* name, NodeType type, Context* ctx);

/**
 * @brief Create or update an Entry in scope (generic upsert).
 * @note Make sure val is not NULL before calling this function.
 * (implemented in scopes.c)
 *
 * @param scope collection of nodes
 * @param key name of entry
 * @param val Node*
 * @param isMut insert as mutable
 * @param ctx Context*
 * @return Node*
 */
Node* upsertNode(Scope* scope, const char* key, Node* val, OpCode isMut,
                 Context* ctx);

/**
 * @brief Interpolates text evaluating fragments in curly braces as source.
 * (implemented in texts.c)
 *
 * @param text string to eval
 * @param ctx Context*
 * @return string (use free)
 */
char* interpolate(const char* text, Context* ctx);

/**
 * @brief Retrieves a single UTF-8 character at a given index as new text.
 * Handles negative indices by offsetting from the end.
 * (implemented in texts.c)
 *
 * @param node text node (primitive)
 * @param index position to look up
 * @param ctx Context*
 * @return Node*
 */
Node* getInText(const Node* node, long index, Context* ctx);

/**
 * @brief Extracts a UTF-8 substring from start to end indices.
 * Handles negative indices as offsets from the end.
 * Ensures selection remains within bounds.
 * (implemented in texts.c)
 *
 * @param node text node (primitive)
 * @param start position to start selection
 * @param end position to end selection
 * @param ctx Context*
 * @return Node*
 */
Node* selectFromText(const Node* node, long start, long end, Context* ctx);

/**
 * @brief Retrieve the type definition from meta space.
 * (implemented in types.c)
 *
 * @param name typename
 * @return Node*
 */
Node* evalType(const char* name);

/**
 * @brief Create a type alias.
 * (implemented in types.c)
 *
 * @param name aliased type
 * @param forwardTo type to be referenced
 *
 * @return Type*
 */
Type* createAlias(const char* name, Type* forwardTo);

/**
 * @brief Safely get value of instance via self access or return error.
 *
 * @param type the type of instance
 * @param ctx Context*
 * @return Node*
 */
Node* getInstance(NodeType type, Context* ctx);

/**
 * @brief Get the fist argument or implicit default argument.
 *
 * @param methodHead Node*
 * @return const Node*
 */
const Node* getFirstArgument(const Node* methodHead);

/**
 * @brief Eval assign between nodes.
 *
 * @param scope current namespace
 * @param left node (assign to)
 * @param right node (assign from)
 * @param ctx Context*
 * @return Node*
 */
Node* evalAssign(Scope* scope, Node* left, Node* right, Context* ctx);

/**
 * @brief Evaluate a single abstract syntax tree node.
 *
 * @param scope current namespace
 * @param node to eval
 * @param ctx Context*
 * @return Node*
 */
Node* evalNode(Scope* scope, Node* node, Context* ctx);

/**
 * @brief Interpret a sequence of abstract syntax trees.
 *
 * @param scope collection of nodes
 * @param program abstract syntax tree (sequence head)
 * @param ctx Context*
 * @return Node*
 */
Node* interpret(Scope* scope, Node* program, Context* ctx);

/**
 * @brief Interpret program source code.
 *
 * @param source code/text
 * @param ctx Context*
 * @return Node*
 */
Node* evalSource(const char* source, Context* ctx);

/**
 * @brief Interpret program from the root node.
 *
 * @param ast abstract syntax tree of program
 * @param name of file being interpreted
 * @param isRepl print final result with margin
 * @return exit code
 */
ExitCode fry(Node* ast, char* name, bool isRepl);

/**
 * @brief Start debug REPL session in current context.
 *
 * @param scope current namespace
 * @param node triggering embedded break command node
 * @param ctx Context*
 */
void breakpoint(Scope* scope, Node* node, Context* ctx);
