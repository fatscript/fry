/**
 * @file interpreter.c
 * @brief Fry engine implementation
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-16
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "interpreter.h"

#include "../syntax/parser.h"
#include "core/calls.c"
#include "core/chaining.c"
#include "core/chunks.c"
#include "core/lists.c"
#include "core/loops.c"
#include "core/operations.c"
#include "core/scopes.c"
#include "core/texts.c"
#include "core/types.c"
#include "embed/dispatcher.h"
#include "utils/repl.h"

bool showResult = false;
bool keepDotFry = false;
bool isJailMode = false;
bool interactive = false;
int fryArgc = 0;
char **fryArgv = NULL;
double timeOffset = 0;

pthread_mutex_t consoleLock = PTHREAD_MUTEX_INITIALIZER;

static __thread bool nullishCanCatch = false;

Node *getInstance(NodeType type, Context *ctx) {
  if (!ctx->selfRef) {
    return createError(MSG_M_PAR, true, ckCallError, ctx);
  }
  if (ctx->selfRef->type != type) {
    return createError(MSG_MISMATCH, true, ckTypeError, ctx);
  }
  return ctx->selfRef;
}

const Node *getFirstArgument(const Node *methodHead) {
  static const Node implicitArg = {
    .type = FatEntry, .val = "_", .src = SRC_AUX};

  if (methodHead) {
    if (methodHead->type == FatEntry) {
      return methodHead;
    }
    if (methodHead->type == FatAssign &&
        IS_FAT_TYPE(methodHead->head, FatEntry)) {
      return methodHead->head;
    }
  }
  return &implicitArg;
}

static char *evalImportPath(Scope *scope, Node *node, Context *ctx) {
  NodeType type = node ? node->type : FatVoid;

  switch (type) {
    case FatEntry:
    case FatType:
    case FatText:
      return strDup(node->val);

    case FatTemp:
      return interpolate(node->val, ctx);

    case FatExpr:
      if (node->op == OpSlash || node->op == OpDot) {
        char *path = evalImportPath(scope, node->head, ctx);
        if (!path) {
          return NULL;
        }
        char *next = evalImportPath(scope, node->body, ctx);
        if (!next) {
          free(path);
          return NULL;
        }
        const size_t sizeA = strlen(path);
        const size_t sizeB = strlen(next);
        const size_t totalSize = sizeA + sizeB;
        char *combined = FRY_REALLOC(path, totalSize + 2);
        combined[sizeA] = '/';
        memcpy(combined + sizeA + 1, next, sizeB);
        free(next);
        combined[totalSize + 1] = '\0';
        return combined;
      }
      FALL_THROUGH;

    default:
      return NULL;
  }
}

static Node *loadImport(Scope *scope, Reader *reader, const char *name,
                        char *flag, bool isNamed, Context *ctx) {
  if (debugLogs || showResult) {
    stderrStartLine(CL_YEL);
    fputs(MRG_BLT "Import ", stderr);
    fputs(reader->name, stderr);
    stderrEndLine();
  }

  // Store configs
  const bool modeDebug = debugLogs;
  const bool modeMain = ctx->isMain;

  debugLogs = false;    // disable debug logs temporarily
  ctx->isMain = false;  // run in module mode

#ifdef DEBUG
  debugLogs = traceLogs;
#endif

  Node *maybeError = NULL;
  if (isNamed) {
    addToScope(globalScope, flag, NULL);  // add import flag (global)
    Scope *namedImport = createScope();

    flag = strchr(flag, ':');
    flag[0] = '!';
    addToScope(namedImport, flag, NULL);  // add import flag (local)

    Node *module = runtimeCollection(namedImport, ctx);
    upsertNode(globalScope, name, module, false, ctx);

    bool isCallMode = ctx->isWithinCall;
    ctx->isWithinCall = false;
    maybeError = interpret(namedImport, parse(reader), ctx);
    ctx->isWithinCall = isCallMode;
  } else {
    addToScope(scope, flag, NULL);  // add import flag (local)
    maybeError = interpret(scope, parse(reader), ctx);  // local import
  }

  // Restore configs
  debugLogs = modeDebug;
  ctx->isMain = modeMain;

  if (IS_FAT_TYPE(maybeError, FatError)) {
    return maybeError;
  }

  UNLOCK_NODE(maybeError);
  return NULL;
}

static Node *evalImport(Scope *scope, Node *node, Context *ctx) {
  if (!IS_FAT_TYPE(node->head, FatEntry)) {
    return createError(MSG_I_IMP, true, ckSyntaxError, ctx);
  }

  // Extract import path
  auto_str path = evalImportPath(scope, node->body, ctx);
  if (!path) {
    return createError(MSG_I_IMP, true, ckSyntaxError, ctx);
  }

  // Ensure import only once
  const char *name = node->head->val;
  bool isNamed = !isAllUnder(name);
  auto_str flag = isNamed ? join4("!", name, ":", path) : join2("!", path);
  if (getCtxEntryOf(ctx, flag)) {
#ifdef DEBUG
    if (debugLogs) {
      logDebug2(__FILE__, __func__, "already imported", flag);
    }
#endif
    return NULL;
  }

  // Check name is still available in global scope for named imports
  if (isNamed && getEntryOf(globalScope, name)) {
    char *msg = join3("import name '", name, "' is taken");
    return createError(msg, false, ckKeyError, ctx);
  }

  // Load import
  Node *result = NULL;
  Reader *reader = createReader(path);
  const char *embedded = getEmbeddedLib(path);

  if (embedded) {  // from embedded code
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-qual"  // drop const qualifier
    reader->source = (char *)embedded;        // don't free...
#pragma GCC diagnostic pop
    bool modeHints = showHints;  // suppress code hints temporarily
    showHints = debugLogs;
    result = loadImport(scope, reader, name, flag, isNamed, ctx);
    showHints = modeHints;  // restore code hints config

  } else {  // from external file
    char *prevPath = ctx->importPath;
    char *addPath = canExpandPath(path) ? NULL : getBasePath(path);

    // push path into context (set next state)
    if (addPath) {
      ctx->importPath = prevPath ? join2(prevPath, addPath) : addPath;
      auto_str pathToCheck = join2(basePath ? basePath : "", ctx->importPath);
      if (!isDir(pathToCheck)) {
        ctx->importPath = NULL;  // reset import path to project base
      }
    }

    // try to read the file from combined path
    auto_str srcPath =
      prevPath && ctx->importPath ? join2(prevPath, path) : strDup(path);
    auto_str source = getSourceForPath(srcPath);

    // if found, process the source with loadImport
    if (source) {
      reader->source = source;
      result = loadImport(scope, reader, name, flag, isNamed, ctx);
    } else {
      char *msg = join2("import failed" GUIDE, path);
      result = createError(msg, false, ckFileError, ctx);
    }

    // pop context path (revert to prev state)
    if (addPath) {
      if (prevPath) {
        free(ctx->importPath);
      }
      free(addPath);
    }
    ctx->importPath = prevPath;
  }

  // Cleanup
  deleteReader(reader);
  return result;
}

static bool isVoidOrAny(const char *x) {
  return strcmp(x, "Void") == 0 || strcmp(x, "Any") == 0;
}

static char *expressionMsg(OpType op, const Node *a, const Node *b) {
  return join5(fatType(a->type), " <", opType(op), "> ", fatType(b->type));
}

static Node *expressionErr(OpType op, const Node *a, const Node *b,
                           Context *ctx) {
  static const char *err = MSG_UNSUP " expression" GUIDE;
  char *msg =
    join6(err, fatType(a->type), " <", opType(op), "> ", fatType(b->type));
  return createError(msg, false, ckTypeError, ctx);
}

static inline Node *cleanExpResult(Node *res, Node *a, Node *b, Context *ctx) {
  popStack(ctx, 2);  // b, a
  UNLOCK_NODE(a);
  UNLOCK_NODE(b);
  return res;
}

/**
 * Retrieve the error handler result from context.
 */
static Node *popErrorHandlerResult(Context *ctx) {
  lockResource(&ctx->lock);

  Node *result = ctx->unclaimed;
  ctx->unclaimed = NULL;
  ctx->hasHandledFailure = false;
  nullishCanCatch = true;

  unlockResource(&ctx->lock);
  return result;
}

/**
 * Perform tap operation using 'a' as input.
 */
static Node *evalTap(Node *a, Node *b, Context *ctx) {
  if (debugLogs) {
    logDebug(__FILE__, __func__, b->src);
  }

  const Node *firstArg = getFirstArgument(b->head);

  if (!checkAlias(firstArg->ck, a)) {
    char *msg = join2("tap" GUIDE MSG_MISMATCH GUIDE, firstArg->val);
    return createError(msg, false, ckTypeError, ctx);
  }

  Entry arg = {.key.s = firstArg->val, .data = a};
  Scope tapLayer = {0};
  initLayer(&tapLayer);
  tapLayer.entries = &arg;
  tapLayer.size = 1;

  Memory *localMemBottom = ctx->temp;
  Node *tapRet = executeMethodBody(&tapLayer, b, ctx);
  wipeScope(&tapLayer, &arg, false);
  if (useCollector && ctx->tempCount) {
    microGC(tapRet, localMemBottom, ctx);
  }

  if (IS_FAT_TYPE(tapRet, FatError)) {
    return tapRet;
  }

  UNLOCK_NODE(tapRet);
  return NULL;
}

static Node *evalExpression(Scope *scope, Node *node, Context *ctx) {
  OpCode op = node->op;

  bool isLogicalOp = op == OpAnd || op == OpOr;
  bool isTapOp = op == OpTap;

  // Resolve A (left-hand side)
  Node *a = node->head;
  bool isTypeA = IS_FAT_TYPE(a, FatType);
  if (!isTypeA || isLogicalOp) {
    a = evalNode(scope, a, ctx);
    if (ctx->unclaimed) {
      a = popErrorHandlerResult(ctx);
    }
  }

  // Quick-fail/succeed for logical AND/OR (short-circuit)
  if (op == OpAnd && !booleanOf(a)) {
    UNLOCK_NODE(a);
    return falseSingleton;
  }
  if (op == OpOr && booleanOf(a)) {
    UNLOCK_NODE(a);
    return trueSingleton;
  }

  pushStack(ctx, __func__, a, scope);

  // Resolve B (right-hand side)
  Node *b = node->body;
  bool isTypeB = false;
  if (b) {
    if (b->type == FatType) {
      isTypeB = true;
    }
    if (isTapOp && b->type == FatEntry) {
      b = getCtxValueOf(ctx, b->val);
    } else if (isTapOp && b->type == FatMethod) {
      // don't evaluate 'b' for anonymous methods
    } else if (!isTypeB || isLogicalOp) {
      b = evalNode(scope, b, ctx);
      if (ctx->unclaimed) {
        b = popErrorHandlerResult(ctx);
      }
    }
  }

  pushStack(ctx, __func__, b, scope);

  // Fullfil logical AND/OR with eval of b
  if (isLogicalOp) {
    return cleanExpResult(RUNTIME_BOOLEAN(booleanOf(b)), a, b, ctx);
  }

  Node *result = NULL;

  // Binary operations with both missing operands
  if (!a && !b) {
    if (op == OpEqual) {
      result = trueSingleton;
    } else if (op == OpNotEq) {
      result = falseSingleton;
    } else {
      char *msg = join3("missing both operands of '", opType(op), "'");
      result = createError(msg, false, ckValueError, ctx);
    }
    return cleanExpResult(result, a, b, ctx);
  }

  // Resolve tap operations
  if (isTapOp && IS_FAT_TYPE(b, FatMethod)) {
    Node *err = evalTap(a, b, ctx);
    return cleanExpResult(err ? err : a, a, b, ctx);
  }

  // Binary operations with a missing operand
  if (!a || !b) {
    const bool isVoidA = isTypeA && isVoidOrAny(a->val);
    const bool isVoidB = isTypeB && isVoidOrAny(b->val);
    if (op == OpEqual) {
      result = RUNTIME_BOOLEAN(a == b || isVoidA || isVoidB);
    } else if (a && op == OpNotEq) {
      result = RUNTIME_BOOLEAN(!isVoidA && a != b);
    } else if (b && op == OpNotEq) {
      result = RUNTIME_BOOLEAN(!isVoidB && a != b);
    } else if (!a && op == OpIncrement) {  // init from value
      result = evalAssign(scope, node->head, node->body, ctx);
    } else {
      char *msg = join3("missing an operand of '", opType(op), "'");
      result = createError(msg, false, ckValueError, ctx);
    }
    return cleanExpResult(result, a, b, ctx);
  }

  if (debugLogs) {
    char *msg = expressionMsg(op, a, b);
    logDebug(__FILE__, __func__, msg);
    free(msg);
  }

  // Fix isType for evaluated expressions
  isTypeA = a->type == FatType;
  isTypeB = b->type == FatType;

  if (isTypeA || isTypeB) {
    switch (op) {
      case OpLessEq:
        if (!isTypeA && isTypeB) {
          result = RUNTIME_BOOLEAN(checkAlias(b->ck, a));
        } else {
          result = expressionErr(op, a, b, ctx);
        }
        break;

      case OpEqual:
        if (isTypeA) {
          result = RUNTIME_BOOLEAN(checkType(a->ck, b));
        } else {
          result = RUNTIME_BOOLEAN(checkType(b->ck, a));
        }
        break;

      case OpNotEq:
        if (isTypeA) {
          result = RUNTIME_BOOLEAN(!checkType(a->ck, b));
        } else {
          result = RUNTIME_BOOLEAN(!checkType(b->ck, a));
        }
        break;

      case OpMultiply:  // Type cast
        if (isTypeA && !isTypeB) {
          if (a->ck->isComposite) {
            result = createError(MSG_CAST_C, true, ckValueError, ctx);
          } else {
            result = evalNode(scope, b, ctx);
            if (result) {
              switch (result->type) {
                case FatList:
                case FatScope:
                  if (result->scp) {
                    result->scp->ck = a->ck;
                  }
                  break;

                default:
                  result->ck = a->ck;
              }
            } else {
              result = createError(MSG_CAST_N, true, ckValueError, ctx);
            }
          }
          break;
        }
        FALL_THROUGH;

      default:
        result = expressionErr(op, a, b, ctx);
    }
  } else if (a->type == b->type) {
    switch (op) {
      case OpIncrement:
      case OpDecrement:
      case OpMulBy:
      case OpPowBy:
      case OpDivBy:
      case OpModBy:
        result = operCompound(op, a, b, ctx);
        if (!result) {
          result = expressionErr(op, a, b, ctx);
        }
        break;

      default:
        switch (a->type) {
          case FatBoolean:
            result = operBooleans(op, a, b, ctx);
            break;

          case FatChunk:
            result = operChunks(op, a, b, ctx);
            break;

          case FatNumber:
            result = operNumbers(op, a, b, ctx);
            break;

          case FatHugeInt:
            result = operHugeInts(op, a, b, ctx);
            break;

          case FatText:
            result = operTexts(op, a, b, ctx);
            break;

          case FatList:
            result = operLists(op, a, b, ctx);
            break;

          case FatScope:
            result = operScopes(op, a, b, ctx);
            break;

          case FatProcedure:
          case FatMethod:
            switch (op) {
              case OpEqual:
                result = RUNTIME_BOOLEAN(a->body == b->body);
                break;

              case OpNotEq:
                result = RUNTIME_BOOLEAN(a->body != b->body);
                break;

              default:
                result = expressionErr(op, a, b, ctx);
            }
            break;

          default:
            result = expressionErr(op, a, b, ctx);
        }
        break;
    }

  } else {
    result = expressionErr(op, a, b, ctx);
  }

  return cleanExpResult(result, a, b, ctx);
}

/**
 * Eval sequence of expressions into a list collection.
 */
static Node *evalList(Scope *scope, Node *head, Context *ctx) {
  Scope *list = createList();

  for (Node *node = head; node; node = node->seq) {
    Node *error = addToList(list, evalNode(scope, node, ctx), ctx);

    if (error) {
      trackScope(list);
      return error;
    }
  }

  return runtimeCollection(list, ctx);
}

/**
 * Eval program into scope collection, similar to interpret.
 */
static Node *evalScope(Node *program, Context *ctx) {
  Scope *scope = createScope();

  for (Node *node = program; node; node = node->seq) {
    Node *internalComputationResult =
      node->type == FatEntry
        ? evalAssign(scope, node, getCtxValueOf(ctx, node->val), ctx)
        : evalNode(scope, node, ctx);

    if (ctx->failureEvent) {
      trackScope(scope);
      return ctx->failureEvent;
    }

    UNLOCK_NODE(internalComputationResult);
  }

  scope->ck = checkScope;
  return runtimeCollection(scope, ctx);
}

/**
 * Copy entries of s2 into s1 from expression, e.g. { x, y, z } = s2
 */
static inline bool destructScp(Scope *s1, Scope *s2, Node *expr, Node **result,
                               Context *ctx) {
  for (; expr; expr = expr->seq) {
    if (expr->type == FatType) {
      continue;  // ignore (backwards-compatibility at no cost)
    }
    if (expr->type != FatEntry) {
      return false;
    }
    *result = evalAssign(s1, expr, getValueOf(s2, expr->val), ctx);
  }
  return true;
}

/**
 * Copy items of ls into s from expression, e.g. { a, b, c } = ls
 */
static inline bool destructList(Scope *s, Scope *ls, Node *expr, Node **result,
                                Context *ctx) {
  for (Entry *item = ls->entries; item && expr;
       item = item->next, expr = expr->seq) {
    if (expr->type != FatEntry) {
      return false;
    }
    if (isAllUnder(expr->val)) {
      continue;  // if key consists only of one or more MARK_UND, ignore item
    }
    *result = evalAssign(s, expr, item->data, ctx);
  }
  return true;
}

static char *assignMsg(const Node *left, const Node *right) {
  const char *leftType = left ? fatType(left->type) : "Void";
  const char *rightType = right ? fatType(right->type) : "Void";
  return join3(leftType, " <store> ", rightType);
}

static Node *assignErr(const Node *left, const Node *right, Context *ctx) {
  char *msg = join2(MSG_UNSUP GUIDE, assignMsg(left, right));
  return createError(msg, false, ckAssignError, ctx);
}

Node *evalAssign(Scope *scope, Node *left, Node *right, Context *ctx) {
  if (debugLogs) {
    char *msg = assignMsg(left, right);
    logDebug(__FILE__, __func__, msg);
    free(msg);
  }

  if (!left) {
    return assignErr(left, right, ctx);
  }

  // dynBuff acts as a temporary placeholder for resolved key values
  Node dynBuff = {.type = FatEntry, .op = true, .val = NULL, .src = SRC_AUX};
  if (left->type == FatDynamic) {
    left = setDynamic(scope, left, &dynBuff, ctx);
  }

  char *key = left->val;
  Node *result = NULL;

  if (!right) {
    if (left->type == FatEntry) {
      result = nullifyInScope(scope, key, ctx);
    } else if (left->type == FatExpr && left->op == OpDot) {
      result = innerAssign(scope, left, NULL, ctx);
    } else {
      result = assignErr(left, right, ctx);
    }
    free(dynBuff.val);
    return result;
  }

  pushStack(ctx, __func__, right, scope);

  Node *aux = NULL;
  switch (left->type) {
    case FatEntry:
      aux = right->type == FatMethod || right->type == FatProcedure
              ? right
              : evalNode(scope, right, ctx);
      if (!aux) {
        result = nullifyInScope(scope, key, ctx);
      } else if (!checkAlias(left->ck, aux)) {
        const char *rValType = aux->ck ? aux->ck->name : fatType(aux->type);
        const char *lValType = left->ck ? left->ck->name : fatType(left->type);
        char *msg = join6(MSG_CS " ", rValType, " to ", key, ": ", lValType);
        result = createError(msg, false, ckTypeError, ctx);
        UNLOCK_NODE(aux);  // allow GC
      } else {
        // The evaluated value of 'right' is held by 'aux' at this point
        if (aux->type == FatList && (left->op || aux->op)) {
          if (atomic_load(&aux->meta->refs) > 0) {
            aux = runtimeCollection(copyCollection(aux->scp, false, ctx), ctx);
          }
        }
        result = upsertNode(scope, key, aux, left->op, ctx);
        UNLOCK_NODE(aux);
      }
      break;

    case FatType:
      result = storType(key, right, ctx);
      break;

    case FatExpr:
      switch (left->op) {
        case OpDot:
          result = innerAssign(scope, left, right, ctx);
          break;
        default:
          result = assignErr(left, right, ctx);
      }
      break;

    case FatAssign:
      result = evalAssign(scope, left->body, right, ctx);
      result = evalAssign(scope, left->head, result, ctx);
      break;

    case FatScope:  // destructuring assignment
      aux = evalNode(scope, right, ctx);
      if (!(IS_FAT_TYPE(aux, FatScope) &&
            destructScp(scope, aux->scp, left->body, &result, ctx)) &&
          !(IS_FAT_TYPE(aux, FatList) &&
            destructList(scope, aux->scp, left->body, &result, ctx))) {
        char *msg = join2(MSG_B_DES GUIDE, left->src);
        result = createError(msg, false, ckAssignError, ctx);
      }
      UNLOCK_NODE(aux);
      break;

    case FatError:
      result = left;  // forward error
      break;

    default:
      result = assignErr(left, right, ctx);
  }

  popStack(ctx, 1);  // right
  UNLOCK_NODE(right);
  free(dynBuff.val);
  return result;
}

static Node *evalCoalescingAssign(Scope *scope, Node *left, Node *right,
                                  Context *ctx) {
  if (!left) {
    return assignErr(left, right, ctx);
  }

  // Resolve outer, if assign is prefixed like outer.left ??= right
  Node *outer = NULL;
  if (isDotOp(left)) {
    outer = resolveLeft(scope, left->head, ctx);
    if (IS_FAT_TYPE(outer, FatError)) {
      return outer;
    }
    if (!IS_FAT_TYPE(outer, FatScope)) {
      UNLOCK_NODE(outer);
      return assignErr(left, right, ctx);
    }
    left = left->body;
    scope = outer->scp;
  }

  // Resolve dynamic, e.g. [key] ??= right
  // dynBuff acts as a temporary placeholder for resolved key values
  Node dynBuff = {.type = FatEntry, .op = true, .val = NULL, .src = SRC_AUX};
  if (left->type == FatDynamic) {
    left = setDynamic(scope, left, &dynBuff, ctx);
  }

  // After evaluation if not of form entry ??= right, then fail
  if (!IS_FAT_TYPE(left, FatEntry)) {
    free(dynBuff.val);
    UNLOCK_NODE(outer);
    UNLOCK_NODE(right);
    return IS_FAT_TYPE(left, FatError) ? left : assignErr(left, right, ctx);
  }

  // If no value in scope for key (left->val) proceed with assign
  Entry *existingValue = getEntryOf(scope, left->val);
  Node *result = existingValue && existingValue->data
                   ? existingValue->data
                   : evalAssign(scope, left, right, ctx);
  free(dynBuff.val);
  UNLOCK_NODE(outer);
  UNLOCK_NODE(right);
  return result;
}

static inline Node *evalCase(Scope *scope, Node *node, Context *ctx) {
  Node *condition = evalNode(scope, node->head, ctx);
  Node *thenBlock = node->body;
  Node *elseBlock = node->tail;
  Node *result = NULL;

  const bool isConditionTruthy = booleanOf(condition);
  UNLOCK_NODE(condition);

  if (isConditionTruthy) {
    if (debugLogs) {
      logDebug(__FILE__, __func__, "chosen the truthy path");
    }
    result = interpret(scope, thenBlock, ctx);
  } else if (elseBlock) {
    if (debugLogs) {
      logDebug(__FILE__, __func__, "chosen the falsy path");
    }
    result = interpret(scope, elseBlock, ctx);
  } else if (debugLogs) {
    logDebug(__FILE__, __func__, "no-op");
  }

  return result;
}

static inline bool strictCheckType(const Type *ck, Node *node) {
  if (!node) {
    return isAlias(ck, checkVoid);
  }
  return checkType(ck, node);
}

static Node *evalSwitch(Node *value, Scope *scope, Node *node, Context *ctx) {
  int optionN = 1;
  while (IS_FAT_TYPE(node, FatCase)) {
    Node *alternative = evalNode(scope, node->head, ctx);
    const bool matchCase = IS_FAT_TYPE(alternative, FatType)
                             ? strictCheckType(alternative->ck, value)
                             : nodeEq(value, alternative);
    if (matchCase) {
      if (debugLogs) {
        auto_str op = ofInt(optionN);
        logDebug2(__FILE__, __func__, "optionN", op);
      }
      UNLOCK_NODE(value);
      UNLOCK_NODE(alternative);
      return interpret(scope, node->body, ctx);
    }
    UNLOCK_NODE(alternative);
    node = node->tail;  // move to the next case
    optionN++;
  }

  if (debugLogs) {
    logDebug(__FILE__, __func__, "default");
  }
  UNLOCK_NODE(value);
  return interpret(scope, node, ctx);  // no matching case found
}

static inline Node *evalNullish(Scope *scope, Node *node, Context *ctx) {
  Node *maybeValue = node->head;
  Node *fallback = node->body;
  nullishCanCatch = false;

  // Trap errors, executing the fallback branch on error
  Node handler = {.type = FatMethod, .body = fallback, .src = SRC_AUX};
  Node *originalTrap = ctx->cur->trap;
  ctx->cur->trap = &handler;
  // Use of interpret (instead of eval) ensures use of handler for failure
  // (see: nasty bug r24 on test/t068.fat)
  Node *result = interpret(scope, maybeValue, ctx);
  ctx->cur->trap = originalTrap;  // restore previous error handler

  if (result && !IS_FAT_ERROR(result)) {
    return result;
  }

  // Avoid double fallback evaluation when error handler has kicked in
  if (nullishCanCatch) {
    nullishCanCatch = false;
    return result;
  }

  return evalNode(scope, fallback, ctx);
}

static inline Node *evalMethod(Scope *scope, Node *node, Context *ctx) {
  if (!node->val) {
    return node;  // anonymous
  }

  // Allow method to "remember" the module were it was declared (bind)
  if (!ctx->isWithinCall && !scope->isLayer && !node->scp) {
    bindScope(node, scope);
  }

  return upsertNode(scope, node->val, node, node->op, ctx);
}

static void handleError(Scope *scope, Context *ctx) {
  if (debugLogs) {
    logDebug(__FILE__, __func__, "calling failure handler!");
  }

  // Prepare handler call and clean up stack
  lockResource(&ctx->lock);
  Node call = {.type = FatCall, .body = ctx->failureEvent, .src = SRC_AUX};
  Node *handler = ctx->cur->trap;
  ctx->failureEvent = NULL;
  ctx->cur->trap = NULL;
  unlockResource(&ctx->lock);

  // Execute the handler
  ctx->isHandlingFailure = true;
  Node *ret = evalMethodCall(scope, &call, handler, ctx);
  ctx->isHandlingFailure = false;

  // Set the handling result into context
  lockResource(&ctx->lock);
  ctx->unclaimed = ret;
  unlockResource(&ctx->lock);

  // Re-raise error output from handler
  if (IS_FAT_TYPE(ctx->unclaimed, FatError)) {
    lockResource(&ctx->lock);
    ctx->failureEvent = ctx->unclaimed;
    ctx->unclaimed = NULL;
    unlockResource(&ctx->lock);

    if (call.body != ctx->failureEvent) {
      UNLOCK_NODE(call.body);  // failureEvent
    }
    return;
  }

  // Update context status
  ctx->hasHandledFailure = true;
  UNLOCK_NODE(call.body);  // failureEvent
}

static inline Node *getCallee(Scope *scope, Node *callee, Context *ctx) {
  if (IS_FAT_TYPE(callee, FatEntry)) {
    if (ctx->lastMethod && FAST_STR_EQ(callee->val, ctx->lastMethod->val)) {
      return ctx->lastMethod;  // fast-track recursion
    }
    return getCtxValueOf(ctx, callee->val);
  }
  return evalNode(scope, callee, ctx);
}

Node *evalNode(Scope *scope, Node *node, Context *ctx) {
  if (!node) {
    return NULL;
  }

  Node *result = NULL;

  // "stack-less" operations (except entry auto-call)
  switch (node->type) {
    case FatVoid:
    case FatAny:
      return NULL;

    case FatBoolean:
    case FatNumber:
    case FatHugeInt:
    case FatChunk:
    case FatError:
    case FatText:
      return node;

    case FatEntry:
      result = getCtxValueOf(ctx, node->val);
      // If argument resolves to a procedure, call it immediately!
      // Note: the tilde '~' in '~procedure' sets op==true, signaling explicit
      // reference mode. Which bypasses autoCall and returns node itself.
      if (!node->op && IS_FAT_TYPE(result, FatProcedure)) {
        result = autoCall(scope, result, ctx);
      }
      return result;

    case FatList:
    case FatScope:
      if (node->scp) {
        return node;
      }
      break;

    case FatProcedure:
    case FatMethod:
      return evalMethod(scope, node, ctx);

    case FatType:
      return evalType(node->val);

    case FatEmbed:
      return callEmbedded(scope, node, ctx);

    case FatBlock:
      return interpret(scope, node->body, ctx);

    default:
      break;
  }

  char *msg = NULL;
  char *aux = NULL;

  // "stack-full" operations
  pushStack(ctx, __func__, node, scope);
  switch (node->type) {
    case FatTemp:
      aux = interpolate(node->val, ctx);
      result = runtimeText(aux, strlen(aux), ctx);
      break;

    case FatList:
      result = evalList(scope, node->body, ctx);
      break;

    case FatScope:
      result = evalScope(node->body, ctx);
      break;

    case FatCall:
      result = getCallee(scope, node->head, ctx);
      result = evalCall(scope, node, result, ctx);
      break;

    case FatAssign:
      result = evalAssign(scope, node->head, node->body, ctx);
      break;

    case FatCase:
      result = evalCase(scope, node, ctx);
      break;

    case FatSwitch:
      result = evalNode(scope, node->head, ctx);
      result = evalSwitch(result, scope, node->body, ctx);
      break;

    case FatUnary:
      result = operUnary(scope, node, ctx);
      break;

    case FatExpr:
      switch (node->op) {
        case OpImport:
          result = evalImport(scope, node, ctx);
          break;

        case OpDot:
        case OpIfDot:
          result = evalDot(scope, node, ctx);
          break;

        case OpCoalesce:
          result = evalNullish(scope, node, ctx);
          break;

        case OpCoAssign:
          result = evalCoalescingAssign(scope, node->head, node->body, ctx);
          break;

        default:
          result = evalExpression(scope, node, ctx);
      }
      break;

    case FatLoop:
      result = evalLoop(scope, node, ctx);
      break;

    default:
      msg = join2("unhandled ", fatType(node->type));
      result = createError(msg, false, NULL, ctx);
  }
#ifdef DEBUG
  if (traceLogs) {
    traceStackDepth(ctx);
    msg = ofPointer((void *)result);
    logTrace2(__FILE__, __func__, " return", msg);
    free(msg);
    stderrStartLine(CL_CYN);
    fputs(MRG_STR "(return of ", stderr);
    fputs(node->src, stderr);
    fputs(GUIDE, stderr);
    fputs(fatType(node->type), stderr);
    fputs(") ->", stderr);
    if (initVisit(__func__)) {
      printNode(result, 1, false);
      endVisit();
    } else {
      fputs(MSG_U_REC "\n", stderr);
    }
  }
#endif
  popStack(ctx, 1);

  // Handle error if a failure handler is available
  if (ctx->failureEvent && ctx->cur->trap) {
    handleError(scope, ctx);
  }
  return result;
}

Node *interpret(Scope *scope, Node *program, Context *ctx) {
  assert(scope != NULL);
  assert(ctx != NULL);

  Node *result = NULL;

  // Execute program statements in sequence
  for (Node *statement = program; statement; statement = statement->seq) {
    UNLOCK_NODE(result);  // allow GC for last computation

    if (hasContextEnded(ctx)) {
      return NULL;  // halt
    }

    // Interpret next block or evaluate "line by line"
    if (statement->type == FatBlock) {
      result = interpret(scope, statement->body, ctx);
    } else {
      result = evalNode(scope, statement, ctx);
    }

    // Eject on error
    if (ctx->failureEvent) {
      if (ctx->cur->trap) {
        handleError(scope, ctx);
        if (ctx->failureEvent) {
          if (result != ctx->failureEvent) {
            UNLOCK_NODE(result);
          }
          return ctx->failureEvent;
        }
      } else {
        if (result != ctx->failureEvent) {
          UNLOCK_NODE(result);
        }
        return ctx->failureEvent;
      }
    }
    if (ctx->hasHandledFailure) {
      UNLOCK_NODE(result);
      return popErrorHandlerResult(ctx);
    }

#ifdef __EMSCRIPTEN__
    if (evalCount++ >= JS_EVAL_MAX) {
      evalCount = 0;
      emscripten_sleep(JS_IO_LAG);  // prevent lock-up
    }
#endif
  }

  assert(trueSingleton->num.b != false);
  assert(falseSingleton->num.b != true);
  return result;
}

Node *evalSource(const char *source, Context *ctx) {
  if (!source || !*source) {
    return NULL;
  }

  if (debugLogs) {
    logDebug(__FILE__, __func__, source);
  }

  // Parse FatScript code
  Reader *reader = createReader(__func__);
  reader->source = (char *)source;  // don't free...
  Node *program = parse(reader);

  // Setup execution context
  Scope layer = {0};
  initLayer(&layer);
  pushStack(ctx, __func__, program, &layer);

  // Run code (shielded)
  lockResource(&memoryLock);
  bool isCollectorEnabled = useCollector;
  useCollector = false;
  trackParsed(program, ctx);
  Node *result = interpret(&layer, program, ctx);
  moveLocalToGlobalMemory(ctx);  // prevent microGC on evaluated AST
  useCollector = isCollectorEnabled;
  unlockResource(&memoryLock);

  // Clean up
  wipeScope(&layer, NULL, false);  // reverse layerScope
  deleteReader(reader);
  popStack(ctx, 1);  // layer

  return result;
}

static ExitCode interpretAndPrintResult(Scope *scope, Node *ast, bool isRepl,
                                        Context *ctx) {
  pushStack(ctx, __func__, ast, scope);
  Node *result = interpret(scope, ast, ctx);
  popStack(ctx, 1);

  if (debugLogs || showResult) {
    if (debugLogs) {
      logMarker("RESULT");
    }
    const bool useDecorator = isRepl && isUtf8Mode;
    if (IS_FAT_TYPE(result, FatText) && strEq(result->val, "fail")) {
      stderrLn(useDecorator ? MRG_RES "fail" : MRG_STR "fail", CL_RED);
    } else if (IS_FAT_TYPE(result, FatText) && strEq(result->val, "pass")) {
      stderrLn(useDecorator ? MRG_RES "pass" : MRG_STR "pass", CL_GRN);
    } else {
      if (!initVisit(__func__)) {
        fatalOut(__FILE__, __func__, MSG_U_REC);
        exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
      }
      printNode(result, IND_BASE, isRepl);
      endVisit();
    }
  }

  if (result) {
    switch (result->type) {
      case FatBoolean:
        return (ExitCode)result->num.b;
      case FatNumber:
        return (ExitCode)result->num.f;
      case FatText:
        return (ExitCode)strEq(result->val, "fail");
      default:
        break;
    }
  }
  return (ExitCode)EXIT_SUCCESS;
}

ExitCode fry(Node *ast, char *name, bool isRepl) {
  if (isAstOnly) {
    if (!initVisit(__func__)) {
      fatalOut(__FILE__, __func__, MSG_U_REC);
      exit(EXIT_FAILURE);  // mitigate false positive on auto_check.sh
    }
    printNodes(ast, IND_BASE);
    endVisit();
    if (!isParseOnly) {
      return hasFoundErrors;  // ast only (dry run)
    }
  }

  if (isParseOnly) {
    if (hasFoundErrors) {
      stderrStartLine(CL_RED);
      fputs(MRG_ERR "failed: ", stderr);
    } else {
      stderrStartLine(CL_GRN);
      fputs(MRG_BLT "checked: ", stderr);
    }
    fputs(name, stderr);
    stderrEndLine();
    return hasFoundErrors;  // probe only (dry run)
  }

  const clock_t tack = clock();                       // cpu-time start
  const double wall = getCurrentMs(CLOCK_MONOTONIC);  // wall-time start

  if (debugLogs) {
    logMarker("INTERPRET");
  }
  Context *globalCtx = createContext(true);
  ExitCode res = interpretAndPrintResult(globalScope, ast, isRepl, globalCtx);
  assert(atomic_load(&globalCtx->top) == 0);  // push-pop balance check
  freeContext(globalCtx);

  if (statsLogs) {
    logStats(clock() - tack, getCurrentMs(CLOCK_MONOTONIC) - wall);
  }

  return res;
}

/**
 * Output the source file and line number where the breakpoint is set.
 */
static inline void showBreakpointSrc(Node *node) {
  fputc('\n', stderr);
  stderrStartLine(CL_GRA);
  fprintf(stderr, " inspecting: %s", node->src);
  stderrEndLine();
}

/**
 * Allows users to inspect code during execution, like evaluating expressions,
 * viewing stack traces, unsetting the breakpoint and/or resuming execution.
 */
void breakpoint(Scope *scope, Node *node, Context *ctx) {
  endCursesMode();

  puts("");
  printVersion(VerDebugger);
  stdoutLn("      \\ /        - type expression to eval in context", CL_MGT);
  stdoutLn("      /_\\        - use $stack to show the stack trace", CL_MGT);
  stdoutLn("    /| Y |\\      - use $unset to disable a breakpoint", CL_MGT);
  stdoutLn("    / \\|/ \\      - use $continue or Ctrl+D to proceed", CL_MGT);

  bool showResultMode = showResult;
  bool crashOnErrorMode = crashOnError;

  showResult = true;
  crashOnError = false;

  showBreakpointSrc(node);

  // Enter loop to handle user input with special embedded commands support
  char buffer[BUFF_LEN] = {'\0'};
  while (getInput(buffer)) {
    if (utf8strstr(buffer, "$stack")) {
      stderrStartLine(CL_RED);
      fprintf(stderr, MRG_STR LB_STACK ": %11s > %s", __func__, node->src);
      stderrEndLine();
      logStack(ctx, stackDepth);
      showBreakpointSrc(node);
      buffer[0] = '\0';
      continue;
    }

    if (utf8strstr(buffer, "$unset")) {
      node->type = FatVoid;
      logAlert(__func__, "disabled for this session", node->src);
      showBreakpointSrc(node);
      buffer[0] = '\0';
      continue;
    }

    if (utf8strstr(buffer, "$break")) {
      logAlert(__func__, "recursive", "let's pretend it didn't happen");
      showBreakpointSrc(node);
      buffer[0] = '\0';
      continue;
    }

    if (utf8strstr(buffer, "$continue")) {
      break;
    }

    if (utf8strstr(buffer, "$exit")) {
      exitFry(EXIT_SUCCESS);
    }

    if (strnlen(buffer, BUFF_LEN)) {
      Reader *reader = createReader(__func__);
      reader->source = buffer;
      Node *program = parse(reader);
      if (program) {
        interpretAndPrintResult(scope, program, true, ctx);
        showBreakpointSrc(node);
      }
      deleteReader(reader);
      buffer[0] = '\0';
    }
  }

  showResult = showResultMode;
  crashOnError = crashOnErrorMode;

#ifndef __APPLE__
  if (isUtf8Mode) {
    fputs(*buffer ? "\033[1A" MRG_NTH : "\033[1A" MRG_1ST, stdout);
  } else {
    fputs("\033[1A" MRG_INP, stdout);
  }
#endif

  // Educate user about $continue command existence
  puts("$continue");
  logAlert(__func__, "resuming execution", node->src);
}
