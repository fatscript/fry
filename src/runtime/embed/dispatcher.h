/**
 * @file dispatcher.h
 * @brief Links commands to implementations
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-08
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#pragma once

#include "../../sdk/sdk.h"

typedef struct curl_slist CurlHeaders;

extern __thread char* softwareName;

extern __thread CurlHeaders* headers;

typedef enum {
  MtAbs,
  MtCeil,
  MtFloor,
  MtRound,
  MtIsInf,
  MtLog,
  MtSqrt,
  MtSin,
  MtCos,
  MtAsin,
  MtAcos,
} MathType;

/**
 * @brief Gets lib source, as if reading from a virtual file system
 *
 * @param ref path of lib to import
 * @return pointer to hardcoded string (don't use free)
 */
const char* getEmbeddedLib(const char* ref);

/**
 * @brief Call embedded function from command type
 *
 * @param scope Scope* parameters namespace
 * @param node triggering embedded command node
 * @param ctx Context*
 * @return Node*
 */
Node* callEmbedded(Scope* scope, Node* node, Context* ctx);
