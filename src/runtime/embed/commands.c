/**
 * @file embedded.c
 * @brief Embedded commands boilerplate
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-20
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "commands.h"

#include "../../sdk/sdk.h"

// Embedded command lookup table
static const EbMap ebMap[] = {

  // Async
  {EbAtomicCall, "atomic"},
  {EbAsyncStart, "asyncStart"},
  {EbAsyncCancel, "asyncCancel"},
  {EbAsyncAwait, "asyncAwait"},
  {EbAsyncIsDone, "asyncIsDone"},
  {EbSelfCancel, "selfCancel"},
  {EbProcessors, "processors"},

  // Bridge
  {EbLoadDLL, "loadDLL"},
  {EbCallFFI, "callFFI"},
  {EbUnsafeCStr, "unsafeCStr"},
  {EbUnsafePeek, "unsafePeek"},
  {EbDetachNode, "detachNode"},
  {EbMarshal, "marshal"},
  {EbUnmarshal, "unmarshal"},
  {EbGetErrno, "getErrno"},
  {EbSizeOf, "sizeOf"},

  // Color
  {EbDetectDepth, "detectDepth"},
  {EbTo8, "to8"},
  {EbTo16, "to16"},
  {EbTo256, "to256"},
  {EbTo24Bit, "to24Bit"},

  // Console
  {EbLog, "log"},
  {EbStderr, "stderr"},
  {EbPrint, "print"},
  {EbInput, "input"},
  {EbFlush, "flush"},
  {EbMoveTo, "moveTo"},
  {EbShowProgress, "showProgress"},
  {EbIsTTY, "isTTY"},

  // Curses
  {EbBox, "box"},
  {EbFill, "fill"},
  {EbClear, "clear"},
  {EbRefresh, "refresh"},
  {EbGetMax, "getMax"},
  {EbPrintAt, "printAt"},
  {EbMakePair, "makePair"},
  {EbUsePair, "usePair"},
  {EbFrameTo, "frameTo"},
  {EbSetMouse, "setMouse"},
  {EbReadKey, "readKey"},
  {EbReadText, "readText"},
  {EbFlushKeys, "flushKeys"},
  {EbEndCurses, "endCurses"},

  // Enigma
  {EbGetHash, "getHash"},
  {EbGenUUID, "genUUID"},
  {EbGenKey, "genKey"},
  {EbDerive, "derive"},
  {EbEncrypt, "encrypt"},
  {EbDecrypt, "decrypt"},
  {EbDigest, "digest"},
  {EbBytes, "bytes"},
  {EbPbkdf2, "pbkdf2"},
  {EbHmac, "hmac"},
  {EbEncryptAES, "encryptAES"},
  {EbDecryptAES, "decryptAES"},

  // Failure
  {EbTrap, "trap"},
  {EbUntrap, "untrap"},
  {EbNoCrash, "noCrash"},

  // File
  {EbBasePath, "basePath"},
  {EbResolve, "resolve"},
  {EbExists, "exists"},
  {EbRead, "read"},
  {EbReadBin, "readBin"},
  {EbReadSys, "readSys"},
  {EbWrite, "write"},
  {EbAppend, "append"},
  {EbRemove, "remove"},
  {EbIsDir, "isDir"},
  {EbMkDir, "mkDir"},
  {EbLsDir, "lsDir"},
  {EbStat, "stat"},

  // HTTP
  {EbRequest, "request"},
  {EbSetHeaders, "setHeaders"},
  {EbSetName, "setName"},
  {EbVerifySSL, "verifySSL"},
  {EbSetSSL, "setSSL"},
  {EbListen, "listen"},
  {EbPort, "port"},

  // Math
  {EbAbs, "abs"},
  {EbCeil, "ceil"},
  {EbFloor, "floor"},
  {EbRound, "round"},
  {EbIsInf, "isInf"},
  {EbIsNan, "isNaN"},
  {EbLn, "ln"},
  {EbRandom, "random"},
  {EbSqrt, "sqrt"},
  {EbSin, "sin"},
  {EbCos, "cos"},
  {EbAsin, "asin"},
  {EbAcos, "acos"},
  {EbAtan, "atan"},
  {EbMax, "max"},
  {EbMin, "min"},
  {EbSum, "sum"},

  // Recode
  {EbInferType, "inferType"},
  {EbMinify, "minify"},
  {EbToB64, "toBase64"},
  {EbFromB64, "fromBase64"},
  {EbToJSON, "toJSON"},
  {EbFromJSON, "fromJSON"},
  {EbToURL, "toURL"},
  {EbFromURL, "fromURL"},
  {EbFromCSV, "fromCSV"},
  {EbToRLE, "toRLE"},
  {EbFromRLE, "fromRLE"},

  // SDK
  {EbAst, "ast"},
  {EbEval, "eval"},
  {EbStringify, "stringify"},
  {EbRoot, "root"},
  {EbSelf, "self"},
  {EbStack, "stack"},
  {EbVersion, "version"},
  {EbWarranty, "warranty"},
  {EbReadLib, "readLib"},
  {EbTypeOf, "typeOf"},
  {EbGetTypes, "getTypes"},
  {EbGetDef, "getDef"},
  {EbIsMain, "isMain"},
  {EbGetMeta, "getMeta"},
  {EbSetKey, "setKey"},
  {EbSetMem, "setMem"},
  {EbRunGC, "runGC"},
  {EbQuickGC, "quickGC"},
  {EbKeepDotFry, "keepDotFry"},
  {EbBytesUsage, "bytesUsage"},
  {EbNodesUsage, "nodesUsage"},

  // SMTP
  {EbSmtpConfig, "smtpConfig"},
  {EbSmtpSend, "smtpSend"},

  // Socket
  {EbSockBind, "sockBind"},
  {EbSockConnect, "sockConnect"},
  {EbSockAccept, "sockAccept"},
  {EbSockSend, "sockSend"},
  {EbSockReceive, "sockReceive"},
  {EbSockClose, "sockClose"},

  // System
  {EbArgs, "args"},
  {EbExit, "exit"},
  {EbShell, "shell"},
  {EbCapture, "capture"},
  {EbFork, "fork"},
  {EbKill, "kill"},
  {EbGetEnv, "getEnv"},
  {EbGetLocale, "getLocale"},
  {EbSetLocale, "setLocale"},
  {EbGetMacId, "getMacId"},
  {EbBlockSig, "blockSig"},

  // Time
  {EbGetZone, "getZone"},
  {EbSetZone, "setZone"},
  {EbParse, "parse"},
  {EbFormat, "format"},
  {EbNow, "now"},
  {EbWait, "wait"},

  // Type: Boolean
  {EbBoolApply, "boolApply"},
  {EbBoolSize, "boolSize"},

  // Type: Chunk
  {EbChunApply, "chunApply"},
  {EbChunSize, "chunSize"},
  {EbChunToBytes, "chunToBytes"},
  {EbChunToText, "chunToText"},
  {EbChunToHuge, "chunToHuge"},
  {EbChunSeek, "chunSeek"},
  {EbChunSeekByte, "chunSeekByte"},

  // Type: List
  {EbListApply, "listApply"},
  {EbListSize, "listSize"},
  {EbListJoin, "listJoin"},
  {EbListFlatten, "listFlatten"},
  {EbListFind, "listFind"},
  {EbListReverse, "listReverse"},
  {EbListShuffle, "listShuffle"},
  {EbListUnique, "listUnique"},
  {EbListSort, "listSort"},
  {EbListIdxOf, "listIdxOf"},
  {EbListReduce, "listReduce"},

  // Type: Method
  {EbMethApply, "methApply"},
  {EbMethArity, "methArity"},

  // Type: Number
  {EbNumbApply, "numbApply"},
  {EbNumbSize, "numbSize"},
  {EbNumbFormat, "numbFormat"},
  {EbNumbTruncate, "numbTruncate"},

  // Type: HugeInt
  {EbHugeApply, "hugeApply"},
  {EbHugeSize, "hugeSize"},
  {EbHugeModExp, "hugeModExp"},
  {EbHugeToNum, "hugeToNum"},
  {EbHugeToChunk, "hugeToChunk"},

  // Type: Scope
  {EbScopApply, "scopApply"},
  {EbScopSize, "scopSize"},
  {EbScopCopy, "scopCopy"},
  {EbScopKeys, "scopKeys"},
  {EbScopPick, "scopPick"},
  {EbScopOmit, "scopOmit"},
  {EbScopSeal, "seal"},
  {EbScopIsSealed, "isSealed"},
  {EbScopFreeze, "freeze"},

  // Type: Text
  {EbTextApply, "textApply"},
  {EbTextSize, "textSize"},
  {EbTextReplace, "textReplace"},
  {EbTextIdxOf, "textIdxOf"},
  {EbTextCount, "textCount"},
  {EbTextSplit, "textSplit"},
  {EbTextToLower, "textToLower"},
  {EbTextToUpper, "textToUpper"},
  {EbTextTrim, "textTrim"},
  {EbTextIsBlank, "textIsBlank"},
  {EbTextMatch, "textMatch"},
  {EbTextGroups, "textGroups"},
  {EbTextRepeat, "textRepeat"},
  {EbTextOverlay, "textOverlay"},
  {EbTextToChunk, "textToChunk"},
  {EbToText, "toText"},

  // Type: Error
  {EbErrorApply, "errorApply"},

  // Other
  {EbIsWeb, "isWeb"},
  {EbResult, "result"},
  {EbDebug, "debug"},
  {EbTrace, "trace"},
  {EbBreak, "break"},

  // Emscripten only
  {EbSoundPlay, "soundPlay"},
  {EbSoundStop, "soundStop"},
  {EbStorableSave, "storableSave"},
  {EbStorableLoad, "storableLoad"},
  {EbStorableList, "storableList"},
  {EbStorableErase, "storableErase"},
};

const char* ebCmd(EbCmd cmd) {
  for (size_t i = 0; i < sizeof(ebMap) / sizeof(EbMap); i++) {
    if (ebMap[i].cmd == cmd) {
      return ebMap[i].name;
    }
  }
  return "unknown";  // not found
}

EbCmd getEbCmd(const char* name) {
  for (size_t i = 0; i < sizeof(ebMap) / sizeof(EbMap); i++) {
    if (FAST_STR_EQ(ebMap[i].name, name)) {
      return ebMap[i].cmd;
    }
  }
  return EbUnknown;  // not found
}
