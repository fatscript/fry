/**
 * @file embedded.h
 * @brief Embedded commands boilerplate
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-20
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#pragma once

/**
 * @brief Embedded command type.
 */
typedef enum {
  EbUnknown,

  // Async
  EbAtomicCall,   // asyncAtomic
  EbAsyncStart,   // asyncStart
  EbAsyncCancel,  // asyncCancel
  EbAsyncAwait,   // asyncAwait
  EbAsyncIsDone,  // asyncIsDone
  EbSelfCancel,   // asyncSelfCancel
  EbProcessors,   // asyncProcessors

  // Bridge
  EbLoadDLL,     // loadDLL
  EbCallFFI,     // callFFI
  EbUnsafePeek,  // unsafePeek
  EbUnsafeCStr,  // unsafeCStr
  EbDetachNode,  // detachNode
  EbMarshal,     // marshal
  EbUnmarshal,   // unmarshal
  EbGetErrno,    // getErrno
  EbSizeOf,      // sizeOf

  // Color
  EbDetectDepth,  // colorMaxColors
  EbTo8,          // colorTo8
  EbTo16,         // colorTo16
  EbTo256,        // colorTo256
  EbTo24Bit,      // colorTo24Bit

  // Console
  EbLog,           // consPrint (true, stdout)
  EbStderr,        // consPrint (false, stderr)
  EbPrint,         // consPrint (false, stdout)
  EbInput,         // consInput
  EbFlush,         // consFlush
  EbMoveTo,        // consMoveTo
  EbShowProgress,  // consShowProgress
  EbIsTTY,         // stdoutIsTTY

  // Curses
  EbBox,        // cursBox (false)
  EbFill,       // cursBox (true)
  EbClear,      // cursClear
  EbRefresh,    // cursRefresh
  EbGetMax,     // cursGetMax
  EbPrintAt,    // cursPrintAt
  EbMakePair,   // cursMakePair
  EbUsePair,    // cursUsePair
  EbFrameTo,    // cursFrameTo
  EbSetMouse,   // cursSetMouse
  EbReadKey,    // cursReadKey
  EbReadText,   // cursReadText
  EbFlushKeys,  // cursFlushKeys
  EbEndCurses,  // cursEndCurses

  // Enigma
  EbGetHash,     // enigmaGetHash
  EbGenUUID,     // enigmaGenUUID
  EbGenKey,      // enigmaGenKey
  EbDerive,      // enigmaDerive
  EbEncrypt,     // enigmaEncrypt
  EbDecrypt,     // enigmaDecrypt
  EbDigest,      // enigmaDigest
  EbBytes,       // enigmaBytes
  EbPbkdf2,      // enigmaPbkdf2
  EbHmac,        // enigmaHmac
  EbEncryptAES,  // enigmaEncryptAES
  EbDecryptAES,  // enigmaDecryptAES

  // Failure
  EbTrap,     // failTrap
  EbUntrap,   // failUntrap
  EbNoCrash,  // failNoCrash

  // File
  EbBasePath,  // fileBasePath
  EbResolve,   // fileResolve
  EbExists,    // fileExists
  EbRead,      // fileRead
  EbReadBin,   // fileReadBin
  EbReadSys,   // fileReadSys
  EbWrite,     // fileWrite (false)
  EbAppend,    // fileWrite (true)
  EbRemove,    // fileRemove
  EbIsDir,     // fileIsDir
  EbMkDir,     // fileMkDir
  EbLsDir,     // fileLsDir
  EbStat,      // fileStat

  // HTTP
  EbRequest,     // httpRequest
  EbSetHeaders,  // httpSetHeaders
  EbSetName,     // httpSetName
  EbVerifySSL,   // httpVerifySSL
  EbSetSSL,      // httpSetSSL
  EbListen,      // httpListen
  EbPort,        // httpPort

  // Math
  EbAbs,     // mathApply
  EbCeil,    // mathApply
  EbFloor,   // mathApply
  EbRound,   // mathRound
  EbIsInf,   // mathApply
  EbIsNan,   // mathIsNan
  EbLn,      // mathApply
  EbRandom,  // mathRandom
  EbSqrt,    // mathApply
  EbSin,     // mathApply
  EbCos,     // mathApply
  EbAsin,    // mathApply
  EbAcos,    // mathApply
  EbAtan,    // mathAtan
  EbMax,     // mathMax
  EbMin,     // mathMin
  EbSum,     // mathSum

  // Recode
  EbInferType,  // recodeInferType
  EbMinify,     // recodeMinify
  EbToB64,      // recodeToBase64
  EbFromB64,    // recodeFromBase64
  EbToJSON,     // recodeToJSON
  EbFromJSON,   // recodeFromJSON
  EbToURL,      // recodeToURL
  EbFromURL,    // recodeFromURL
  EbFromCSV,    // recodeFromCSV
  EbToRLE,      // recodeToRLE
  EbFromRLE,    // recodeFromRLE

  // SDK
  EbAst,         // sdkAst
  EbEval,        // sdkEval
  EbStringify,   // sdkStringify
  EbRoot,        // sdkRoot
  EbSelf,        // sdkSelf
  EbStack,       // sdkStack
  EbVersion,     // sdkVersion
  EbWarranty,    // sdkDisclaimer
  EbReadLib,     // sdkReadLib
  EbTypeOf,      // sdkTypeOf
  EbGetTypes,    // sdkGetTypes
  EbGetDef,      // sdkGetDef
  EbIsMain,      // sdkIsMain
  EbGetMeta,     // sdkGetMeta
  EbSetKey,      // sdkSetKey
  EbSetMem,      // sdkSetMem
  EbRunGC,       // sdkRunGC
  EbQuickGC,     // sdkQuickGC
  EbKeepDotFry,  // sdkKeepDotFry
  EbBytesUsage,  // sdkBytesUsage
  EbNodesUsage,  // sdkNodesUsage

  // SMTP
  EbSmtpConfig,  // smtpConfig
  EbSmtpSend,    // smtpSend

  // Socket
  EbSockBind,     // sockBind
  EbSockConnect,  // sockConnect
  EbSockAccept,   // sockAccept
  EbSockSend,     // sockSend
  EbSockReceive,  // sockReceive
  EbSockClose,    // sockClose

  // System
  EbArgs,       // systArgs
  EbExit,       // systExit
  EbShell,      // systShell
  EbCapture,    // systCapture
  EbFork,       // systFork
  EbKill,       // systKill
  EbGetEnv,     // systGetEnv
  EbGetLocale,  // systGetLocale
  EbSetLocale,  // systSetLocale
  EbGetMacId,   // systGetMacId
  EbBlockSig,   // systBlockSig

  // Time
  EbGetZone,  // timeGetZone
  EbSetZone,  // timeSetZone
  EbParse,    // timeParse
  EbFormat,   // timeFormat
  EbNow,      // timeNow
  EbWait,     // timeWait

  // Type: Boolean
  EbBoolApply,  // boolApply
  EbBoolSize,   // boolSize

  // Type: Chunk
  EbChunApply,     // chunApply
  EbChunSize,      // chunSize
  EbChunToBytes,   // chunToBytes
  EbChunToText,    // chunToText
  EbChunToHuge,    // chunToHuge
  EbChunSeek,      // chunSeek
  EbChunSeekByte,  // chunSeekByte

  // Type: List
  EbListApply,    // listApply
  EbListSize,     // listSize
  EbListJoin,     // listJoin
  EbListFlatten,  // listFlatten
  EbListFind,     // listFind
  EbListReverse,  // listReverse
  EbListShuffle,  // listShuffle
  EbListUnique,   // listUnique
  EbListSort,     // listSort
  EbListIdxOf,    // listIdxOf
  EbListReduce,   // listReduce

  // Type: Method
  EbMethApply,  // methApply
  EbMethArity,  // methArity

  // Type: Number
  EbNumbApply,     // numbApply
  EbNumbSize,      // numbSize
  EbNumbFormat,    // numbFormat
  EbNumbTruncate,  // numbTruncate

  // Type: HugeInt
  EbHugeApply,    // hugeApply
  EbHugeSize,     // hugeSize
  EbHugeModExp,   // hugeModExp
  EbHugeToNum,    // hugeToNum
  EbHugeToChunk,  // hugeToChunk

  // Type: Scope
  EbScopApply,     // scopApply
  EbScopSize,      // scopSize
  EbScopCopy,      // scopCopy
  EbScopKeys,      // scopKeys
  EbScopPick,      // scopPick
  EbScopOmit,      // scopOmit
  EbScopSeal,      // scopSeal
  EbScopIsSealed,  // scopIsSealed
  EbScopFreeze,    // scopFreeze

  // Type: Text
  EbTextApply,    // textApply
  EbTextSize,     // textSize
  EbTextReplace,  // textReplace
  EbTextIdxOf,    // textIdxOf
  EbTextCount,    // textCount
  EbTextSplit,    // textSplit
  EbTextToLower,  // textToLower
  EbTextToUpper,  // textToUpper
  EbTextTrim,     // textTrim
  EbTextIsBlank,  // textIsBlank
  EbTextMatch,    // textMatch
  EbTextGroups,   // textGroups
  EbTextRepeat,   // textRepeat
  EbTextOverlay,  // textOverlay
  EbTextToChunk,  // textToChunk
  EbToText,       // textToText

  // Type: Error
  EbErrorApply,  // errorApply

  // other
  EbIsWeb,
  EbResult,
  EbDebug,
  EbTrace,
  EbBreak,

  // emscripten only
  EbSoundPlay,
  EbSoundStop,
  EbStorableSave,
  EbStorableLoad,
  EbStorableList,
  EbStorableErase,

} EbCmd;

/**
 * @brief Embedded command tuple for lookup table (ebMap).
 */
typedef struct EbMap {
  EbCmd cmd;
  const char* name;
} EbMap;

/**
 * @brief Return string name of embedded command.
 *
 * @param cmd EbCmd (type id)
 * @return string
 */
const char* ebCmd(EbCmd cmd);

/**
 * @brief Get embedded command type id from a string.
 *
 * @param name string name of embedded command
 * @return EbCmd
 */
EbCmd getEbCmd(const char* name);
