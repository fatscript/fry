/**
 * @file dispatcher.c
 * @brief Links commands to implementations
 * @author Antonio Prates <hello@aprates.dev>
 * @version 4.1.0
 * @date 2025-02-20
 *
 * @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
 * Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
 * Licensed under the GNU General Public License v3.0.
 * See LICENSE file in the project root for full license.
 */

#include "dispatcher.h"

#include "../../sdk/server.h"
#include "../../stdlib/include.h"
#include "commands.h"

static const char* LIB_EXTRA =
  "_ <- fat.extra.Date\n"
  "_ <- fat.extra.Duration\n"
  "_ <- fat.extra.Fuzzy\n"
  "_ <- fat.extra.HashMap\n"
  "_ <- fat.extra.Logger\n"
  "_ <- fat.extra.Memo\n"
  "_ <- fat.extra.MouseEvent\n"
  "_ <- fat.extra.Opaque\n"
  "_ <- fat.extra.Option\n"
  "_ <- fat.extra.Param\n"
  "_ <- fat.extra.Sound\n"
  "_ <- fat.extra.Storable\n";

static const char* LIB_TYPES =
  "_ <- fat.type.Void\n"
  "_ <- fat.type.Boolean\n"
  "_ <- fat.type.Chunk\n"
  "_ <- fat.type.Number\n"
  "_ <- fat.type.HugeInt\n"
  "_ <- fat.type.Text\n"
  "_ <- fat.type.Method\n"
  "_ <- fat.type.List\n"
  "_ <- fat.type.Scope\n"
  "_ <- fat.type.Error\n";

static const char* LIB_LIBS =
  "_ <- fat.type._\n"
  "_ <- fat.extra._\n"
  "_ <- fat.async\n"
  "_ <- fat.bridge\n"
  "_ <- fat.color\n"
  "_ <- fat.console\n"
  "_ <- fat.curses\n"
  "_ <- fat.enigma\n"
  "_ <- fat.failure\n"
  "_ <- fat.http\n"
  "_ <- fat.file\n"
  "_ <- fat.math\n"
  "_ <- fat.recode\n"
  "_ <- fat.sdk\n"
  "_ <- fat.smtp\n"
  "_ <- fat.socket\n"
  "_ <- fat.system\n"
  "_ <- fat.time\n";

static const char* LIB_STD =
  "# Shorthand for importing all standard libs\n"
  "\n"
  "_       <- fat.type._\n"
  "_       <- fat.extra._\n"
  "async   <- fat.async\n"
  "bridge  <- fat.bridge\n"
  "color   <- fat.color\n"
  "console <- fat.console\n"
  "curses  <- fat.curses\n"
  "enigma  <- fat.enigma\n"
  "failure <- fat.failure\n"
  "http    <- fat.http\n"
  "file    <- fat.file\n"
  "math    <- fat.math\n"
  "recode  <- fat.recode\n"
  "sdk     <- fat.sdk\n"
  "smtp    <- fat.smtp\n"
  "socket  <- fat.socket\n"
  "system  <- fat.system\n"
  "time    <- fat.time\n";

static const char* getLibExtra(const char* ref) {
  return !strcmp(ref, "Date")         ? LIB_DATE
         : !strcmp(ref, "Duration")   ? LIB_DURATION
         : !strcmp(ref, "Fuzzy")      ? LIB_FUZZY
         : !strcmp(ref, "HashMap")    ? LIB_HMAP
         : !strcmp(ref, "Logger")     ? LIB_LOGGER
         : !strcmp(ref, "Memo")       ? LIB_MEMO
         : !strcmp(ref, "MouseEvent") ? LIB_MOUSE
         : !strcmp(ref, "Opaque")     ? LIB_OPAQUE
         : !strcmp(ref, "Option")     ? LIB_OPTION
         : !strcmp(ref, "Param")      ? LIB_PARAM
         : !strcmp(ref, "Sound")      ? LIB_SOUND
         : !strcmp(ref, "Storable")   ? LIB_STORABLE
         : !strcmp(ref, "_")          ? LIB_EXTRA
                                      : NULL;
}

static const char* getLibType(const char* ref) {
  return !strcmp(ref, "Boolean")   ? LIB_BOOLEAN
         : !strcmp(ref, "Chunk")   ? LIB_CHUNK
         : !strcmp(ref, "Error")   ? LIB_ERROR
         : !strcmp(ref, "List")    ? LIB_LIST
         : !strcmp(ref, "Method")  ? LIB_METHOD
         : !strcmp(ref, "Number")  ? LIB_NUMBER
         : !strcmp(ref, "HugeInt") ? LIB_HUGE_INT
         : !strcmp(ref, "Scope")   ? LIB_SCOPE
         : !strcmp(ref, "Text")    ? LIB_TEXT
         : !strcmp(ref, "Void")    ? LIB_VOID
         : !strcmp(ref, "_")       ? LIB_TYPES
                                   : NULL;
}

const char* getEmbeddedLib(const char* ref) {
  if (!ref || strncmp(ref, "fat/", 4) != 0) {
    return NULL;
  }
  ref += 4;  // move past "fat/"

  return !strncmp(ref, "extra/", 6)  ? getLibExtra(ref + 6)
         : !strncmp(ref, "type/", 5) ? getLibType(ref + 5)
         : !strcmp(ref, "async")     ? LIB_ASYNC
         : !strcmp(ref, "bridge")    ? LIB_BRIDGE
         : !strcmp(ref, "color")     ? LIB_COLOR
         : !strcmp(ref, "console")   ? LIB_CONSOLE
         : !strcmp(ref, "curses")    ? LIB_CURSES
         : !strcmp(ref, "enigma")    ? LIB_ENIGMA
         : !strcmp(ref, "failure")   ? LIB_FAILURE
         : !strcmp(ref, "http")      ? LIB_HTTP
         : !strcmp(ref, "file")      ? LIB_FILE
         : !strcmp(ref, "math")      ? LIB_MATH
         : !strcmp(ref, "recode")    ? LIB_RECODE
         : !strcmp(ref, "sdk")       ? LIB_SDK
         : !strcmp(ref, "std")       ? LIB_STD
         : !strcmp(ref, "smtp")      ? LIB_SMTP
         : !strcmp(ref, "socket")    ? LIB_SOCKET
         : !strcmp(ref, "system")    ? LIB_SYSTEM
         : !strcmp(ref, "time")      ? LIB_TIME
         : !strcmp(ref, "_")         ? LIB_LIBS
                                     : NULL;
}

Node* callEmbedded(Scope* scope, Node* node, Context* ctx) {
  EbCmd cmd = node->op;
  if (debugLogs) {
    logDebug(__FILE__, __func__, ebCmd(cmd));
  }

  switch (cmd) {
    // Async
    case EbAtomicCall:
      return asyncAtomic(scope, ctx);
    case EbAsyncStart:
      return asyncStart(ctx);
    case EbAsyncCancel:
      return asyncCancel(ctx);
    case EbAsyncAwait:
      return asyncAwait(ctx);
    case EbAsyncIsDone:
      return asyncIsDone(ctx);
    case EbSelfCancel:
      return asyncSelfCancel(ctx);
    case EbProcessors:
      return asyncProcessors(ctx);

#ifdef FFI_SUPPORT
    // Bridge
    case EbLoadDLL:
      return loadDLL(scope, ctx);
    case EbCallFFI:
      return callFFI(scope, ctx);
    case EbUnsafeCStr:
      return unsafeCStr(scope, ctx);
    case EbUnsafePeek:
      return unsafePeek(scope, ctx);
    case EbDetachNode:
      return detachNode(scope, ctx);
    case EbMarshal:
      return marshal(scope, ctx);
    case EbUnmarshal:
      return unmarshal(scope, ctx);
    case EbGetErrno:
      return getErrno(ctx);
    case EbSizeOf:
      return sizeOf(scope, ctx);
#endif

    // Color
    case EbDetectDepth:
      return colorDetectDepth(ctx);
    case EbTo8:
      return colorTo8(scope, ctx);
    case EbTo16:
      return colorTo16(scope, ctx);
    case EbTo256:
      return colorTo256(scope, ctx);
#ifndef USE_NCURSES
    case EbTo24Bit:
      return colorTo24Bit(scope, ctx);
#endif

    // Console
    case EbLog:
      return consPrint(scope, true, stdout);
    case EbStderr:
      return consPrint(scope, true, stderr);
    case EbPrint:
      return consPrint(scope, false, stdout);
    case EbInput:
      return consInput(scope, ctx);
    case EbFlush:
      return consFlush();
    case EbMoveTo:
      return consMoveTo(scope, ctx);
    case EbShowProgress:
      return consShowProgress(scope, ctx);
    case EbIsTTY:
      return RUNTIME_BOOLEAN(stdoutIsTTY);

    // Curses
    case EbBox:
      return cursBox(scope, ctx, false);
    case EbFill:
      return cursBox(scope, ctx, true);
    case EbClear:
      return cursClear(ctx);
    case EbRefresh:
      return cursRefresh(ctx);
    case EbGetMax:
      return cursGetMax(ctx);
    case EbPrintAt:
      return cursPrintAt(scope, ctx);
    case EbMakePair:
      return cursMakePair(scope, ctx);
    case EbUsePair:
      return cursUsePair(scope, ctx);
    case EbFrameTo:
      return cursFrameTo(scope, ctx);
#ifndef USE_NCURSES
    case EbSetMouse:
      return cursSetMouse(scope, ctx);
#endif
    case EbReadKey:
      return cursReadKey(ctx);
    case EbReadText:
      return cursReadText(scope, ctx);
    case EbFlushKeys:
      return cursFlushKeys();
    case EbEndCurses:
      return cursEndCurses();

    // Enigma
    case EbGetHash:
      return enigmaGetHash(scope, ctx);
    case EbGenUUID:
      return enigmaGenUUID(ctx);
    case EbGenKey:
      return enigmaGenKey(scope, ctx);
    case EbDerive:
      return enigmaDerive(scope, ctx);
    case EbEncrypt:
      return enigmaEncrypt(scope, ctx);
    case EbDecrypt:
      return enigmaDecrypt(scope, ctx);
#ifdef SSL_SUPPORT
    case EbDigest:
      return enigmaDigest(scope, ctx);
    case EbBytes:
      return enigmaBytes(scope, ctx);
    case EbPbkdf2:
      return enigmaPbkdf2(scope, ctx);
#if OPENSSL_VERSION_NUMBER >= OPENSSL_VERSION_WITH_NEW_TYPES
    case EbHmac:
      return enigmaHmac(scope, ctx);
#endif
    case EbEncryptAES:
      return enigmaEncryptAES(scope, ctx);
    case EbDecryptAES:
      return enigmaDecryptAES(scope, ctx);
#endif

    // Failure
    case EbTrap:
      return failTrap(scope, ctx);
    case EbUntrap:
      return failUntrap(ctx);
    case EbNoCrash:
      return failNoCrash(scope, ctx);

    // File
    case EbBasePath:
      return fileBasePath(ctx);
    case EbResolve:
      return fileResolve(scope, ctx);
    case EbExists:
      return fileExists(scope, ctx);
    case EbRead:
      return fileRead(scope, ctx);
    case EbReadBin:
      return fileReadBin(scope, ctx);
    case EbReadSys:
      return fileReadSys(scope, ctx);
    case EbWrite:
      return fileWrite(scope, ctx, false);
    case EbAppend:
      return fileWrite(scope, ctx, true);
    case EbRemove:
      return fileRemove(scope, ctx);
    case EbIsDir:
      return fileIsDir(scope, ctx);
    case EbMkDir:
      return fileMkDir(scope, ctx);
    case EbLsDir:
      return fileLsDir(scope, ctx);
    case EbStat:
      return fileStat(scope, ctx);

      // HTTP
    case EbRequest:
      return httpRequest(scope, ctx);
#ifndef __EMSCRIPTEN__
    case EbSetHeaders:
      return httpSetHeaders(scope, ctx);
    case EbSetName:
      return httpSetName(scope, ctx);
    case EbVerifySSL:
      return httpVerifySSL(scope, ctx);
    case EbListen:
      return httpListen(scope, ctx);
    case EbPort:
      return runtimeNumber(listenedPort, ctx);
#endif
#ifdef SSL_SUPPORT
    case EbSetSSL:
      return httpSetSSL(scope, ctx);
#endif

    // Math
    case EbAbs:
      return mathApply(scope, ctx, MtAbs);
    case EbCeil:
      return mathApply(scope, ctx, MtCeil);
    case EbFloor:
      return mathApply(scope, ctx, MtFloor);
    case EbRound:
      return mathApply(scope, ctx, MtRound);
    case EbIsInf:
      return mathApply(scope, ctx, MtIsInf);
    case EbIsNan:
      return mathIsNan(scope);
    case EbLn:
      return mathApply(scope, ctx, MtLog);
    case EbRandom:
      return mathRandom(ctx);
    case EbSqrt:
      return mathApply(scope, ctx, MtSqrt);
    case EbSin:
      return mathApply(scope, ctx, MtSin);
    case EbCos:
      return mathApply(scope, ctx, MtCos);
    case EbAsin:
      return mathApply(scope, ctx, MtAsin);
    case EbAcos:
      return mathApply(scope, ctx, MtAcos);
    case EbAtan:
      return mathAtan(scope, ctx);
    case EbMax:
      return mathMax(scope, ctx);
    case EbMin:
      return mathMin(scope, ctx);
    case EbSum:
      return mathSum(scope, ctx);

    // Recode
    case EbInferType:
      return recodeInferType(scope, ctx);
    case EbMinify:
      return recodeMinify(scope, ctx);
    case EbToB64:
      return recodeToBase64(scope, ctx);
    case EbFromB64:
      return recodeFromBase64(scope, ctx);
    case EbToJSON:
      return recodeToJSON(scope, ctx);
    case EbFromJSON:
      return recodeFromJSON(scope, ctx);
    case EbToURL:
      return recodeToURL(scope, ctx);
    case EbFromURL:
      return recodeFromURL(scope, ctx);
    case EbFromCSV:
      return recodeFromCSV(scope, ctx);
    case EbToRLE:
      return recodeToRLE(scope, ctx);
    case EbFromRLE:
      return recodeFromRLE(scope, ctx);

    // SDK
    case EbAst:
      return sdkAst(scope);
    case EbEval:
      return sdkEval(scope, ctx);
    case EbStringify:
      return sdkStringify(scope, ctx);
    case EbRoot:
      return sdkRoot(ctx);
    case EbSelf:
      return sdkSelf(ctx);
    case EbStack:
      return sdkStack(scope, ctx);
    case EbVersion:
      return sdkVersion(ctx);
    case EbWarranty:
      return sdkDisclaimer();
    case EbReadLib:
      return sdkReadLib(scope, ctx);
    case EbTypeOf:
      return sdkTypeOf(scope, ctx);
    case EbGetTypes:
      return sdkGetTypes(ctx);
    case EbGetDef:
      return sdkGetDef(scope, ctx);
    case EbIsMain:
      return RUNTIME_BOOLEAN(ctx->isMain);
    case EbGetMeta:
      return sdkGetMeta(ctx);
    case EbSetKey:
      return sdkSetKey(scope, ctx);
    case EbSetMem:
      return sdkSetMem(scope, ctx);
    case EbRunGC:
      return sdkRunGC(ctx);
    case EbQuickGC:
      return sdkQuickGC(ctx);
    case EbKeepDotFry:
      return sdkKeepDotFry();
    case EbBytesUsage:
      return sdkBytesUsage(ctx);
    case EbNodesUsage:
      return sdkNodesUsage(ctx);

#ifndef __EMSCRIPTEN__
    // SMTP
    case EbSmtpConfig:
      return smtpConfig(scope, ctx);
    case EbSmtpSend:
      return smtpSend(scope, ctx);

    // Socket
    case EbSockBind:
      return sockBind(scope, ctx);
    case EbSockConnect:
      return sockConnect(scope, ctx);
    case EbSockAccept:
      return sockAccept(scope, ctx);
    case EbSockSend:
      return sockSend(scope, ctx);
    case EbSockReceive:
      return sockReceive(scope, ctx);
    case EbSockClose:
      return sockClose(ctx);
#endif

    // System
    case EbArgs:
      return systArgs(ctx);
    case EbExit:
      return systExit(scope, ctx);
    case EbShell:
      return systShell(scope, ctx);
    case EbCapture:
      return systCapture(scope, ctx);
    case EbFork:
      return systFork(scope, ctx);
    case EbKill:
      return systKill(scope, ctx);
    case EbGetEnv:
      return systGetEnv(scope, ctx);
    case EbGetLocale:
      return systGetLocale(ctx);
    case EbSetLocale:
      return systSetLocale(scope);
    case EbGetMacId:
      return systGetMacId(ctx);
    case EbBlockSig:
      return systBlockSig(scope, ctx);

    // Time
    case EbGetZone:
      return timeGetZone(ctx);
    case EbSetZone:
      return timeSetZone(scope, ctx);
    case EbParse:
      return timeParse(scope, ctx);
    case EbFormat:
      return timeFormat(scope, ctx);
    case EbNow:
      return timeNow(ctx);
    case EbWait:
      return timeWait(scope, ctx);

    // Type: Boolean
    case EbBoolApply:
      return boolApply(scope);
    case EbBoolSize:
      return boolSize(ctx);

    // Type: Chunk
    case EbChunApply:
      return chunApply(scope, ctx);
    case EbChunSize:
      return chunSize(ctx);
    case EbChunToBytes:
      return chunToBytes(ctx);
    case EbChunToText:
      return chunToText(ctx);
    case EbChunToHuge:
      return chunToHuge(ctx);
    case EbChunSeek:
      return chunSeek(scope, ctx);
    case EbChunSeekByte:
      return chunSeekByte(scope, ctx);

    // Type: List
    case EbListApply:
      return listApply(scope, ctx);
    case EbListSize:
      return listSize(ctx);
    case EbListJoin:
      return listJoin(scope, ctx);
    case EbListFlatten:
      return listFlatten(ctx);
    case EbListFind:
      return listFind(scope, ctx);
    case EbListReverse:
      return listReverse(ctx);
    case EbListShuffle:
      return listShuffle(ctx);
    case EbListUnique:
      return listUnique(ctx);
    case EbListSort:
      return listSort(scope, ctx);
    case EbListIdxOf:
      return listIdxOf(scope, ctx);
    case EbListReduce:
      return listReduce(scope, ctx);

    // Type: Method
    case EbMethApply:
      return methApply(scope, ctx);
    case EbMethArity:
      return methArity(ctx);

    // Type: Number
    case EbNumbApply:
      return numbApply(scope, ctx);
    case EbNumbSize:
      return numbSize(ctx);
    case EbNumbFormat:
      return numbFormat(scope, ctx);
    case EbNumbTruncate:
      return numbTruncate(ctx);

      // Type: HugeInt
    case EbHugeApply:
      return hugeApply(scope, ctx);
    case EbHugeSize:
      return hugeSize(ctx);
    case EbHugeModExp:
      return hugeModExp(scope, ctx);
    case EbHugeToNum:
      return hugeToNum(ctx);
    case EbHugeToChunk:
      return hugeToChunk(ctx);

    // Type: Scope
    case EbScopApply:
      return scopApply(scope, ctx);
    case EbScopSize:
      return scopSize(ctx);
    case EbScopCopy:
      return scopCopy(ctx);
    case EbScopKeys:
      return scopKeys(ctx);
    case EbScopPick:
      return scopPick(scope, ctx);
    case EbScopOmit:
      return scopOmit(scope, ctx);
    case EbScopSeal:
      return scopSeal(ctx);
    case EbScopIsSealed:
      return scopIsSealed(ctx);
    case EbScopFreeze:
      return scopFreeze(ctx);

    // Type: Text
    case EbTextApply:
      return textApply(scope, ctx);
    case EbTextSize:
      return textSize(ctx);
    case EbTextReplace:
      return textReplace(scope, ctx);
    case EbTextIdxOf:
      return textIdxOf(scope, ctx);
    case EbTextCount:
      return textCount(scope, ctx);
    case EbTextSplit:
      return textSplit(scope, ctx);
    case EbTextToLower:
      return textToLower(ctx);
    case EbTextToUpper:
      return textToUpper(ctx);
    case EbTextTrim:
      return textTrim(ctx);
    case EbTextIsBlank:
      return textIsBlank(ctx);
    case EbTextMatch:
      return textMatch(scope, ctx);
    case EbTextGroups:
      return textGroups(scope, ctx);
    case EbTextRepeat:
      return textRepeat(scope, ctx);
    case EbTextOverlay:
      return textOverlay(scope, ctx);
    case EbTextToChunk:
      return textToChunk(ctx);
    case EbToText:
      return textToText(ctx);

    // Type: Error
    case EbErrorApply:
      return errorApply(scope, ctx);

    // other
    case EbIsWeb:
#ifdef __EMSCRIPTEN__
      return trueSingleton;
#else
      return falseSingleton;
#endif
    case EbResult:
      showResult = !showResult;
      return NULL;
#if defined(DEBUG) || defined(__EMSCRIPTEN__)
      // Debug logs may expose sensitive information. For security reasons,
      // toggling debug logs at runtime is restricted to non-production builds.
      // Enable debug logs at startup using the `fry --debug` flag or compile
      // with `./install.sh -DDEBUG` to use this feature.
    case EbDebug:
      statsLogs = debugLogs = !debugLogs;
      return NULL;
#endif
#ifdef DEBUG
    case EbTrace:
      statsLogs = debugLogs = traceLogs = !traceLogs;
      return NULL;
#endif
    case EbBreak:
      if (interactive) {
        breakpoint(scope, node, ctx);
      }
      return NULL;

#ifdef __EMSCRIPTEN__
    case EbSoundPlay:
      return soundPlay(scope, ctx);
    case EbSoundStop:
      return soundStop(scope, ctx);
    case EbStorableSave:
      return storableSave(scope, ctx);
    case EbStorableLoad:
      return storableLoad(scope, ctx);
    case EbStorableList:
      return storableList(scope, ctx);
    case EbStorableErase:
      return storableErase(scope, ctx);
#endif

    // not found
    default:
      if (strcmp(ebCmd(cmd), "unknown") != 0) {
        // we acknowledge the command's existence, but it's disabled by flags
        return createError(MSG_D_EB_CMD, true, ckCallError, ctx);
      }
      return createError(MSG_U_EB_CMD, true, ckCallError, ctx);
  }
}
