#!/usr/bin/env bash

# @file get_fry.sh
# @brief Fry multi-platform autoinstall script
# @author Antonio Prates <hello@aprates.dev>
# @version 2.1.0
# @date 2024-01-24
#
# @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
# Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
# Licensed under the GNU General Public License v3.0.
# See LICENSE file in the project root for full license.

# Detect the target platform
if [ "$(uname -s)" == "Darwin" ]
then
    targetPlatform="macos"
elif command -v termux-setup-storage &>/dev/null
then
    targetPlatform="termux"
elif [ -f /etc/os-release ]
then
    . /etc/os-release
    targetPlatform=$ID
else
    echo "Could not determine the target platform."
    exit 1
fi

# Ensure administrative privileges, if not on Termux
if [[ "$UID" -ne 0 ]] && [[ "$targetPlatform" != "termux" ]]
then
    echo "This script will require administrative privileges."
    exit 1
fi

installFryFromSource() {
    local tempDir
    tempDir=$(mktemp -d -t fry.XXXXXXXXXX)
    
    git clone -q --recursive https://gitlab.com/fatscript/fry.git "$tempDir"
    
    pushd "$tempDir" || exit
    
    ./install.sh
    
    if [ -d "/usr/share/man/man1" ]
    then
        echo -n '  '
        cp -fv man/fry.1 /usr/share/man/man1/
    fi
    
    if [ -d "/usr/share/licenses" ]
    then
        mkdir -p /usr/share/licenses/fry/
        echo -n '  '
        cp -fv LICENSE /usr/share/licenses/fry/
        cp -fv linenoise/LICENSE /usr/share/licenses/fry/LINENOISE_LICENSE
    fi
    
    if [ -d "/usr/share/nano" ]
    then
        echo -n '  '
        cp -fv extras/fat.nanorc /usr/share/nano/
    fi
    
    popd || exit
    
    # Cleanup: remove the temporary directory
    rm -rf "$tempDir"
    
    echo "All Done!"
}

# Handle installation based on detected platform
case $targetPlatform in
    arch|manjaro|manjaro-arm|arcolinux|endeavouros|garuda|chakra|parabola)
        echo "Detected Arch-based distribution."
        if command -v pamac &>/dev/null
        then
            pamac build fatscript-fry
        elif command -v yay &>/dev/null
        then
            yay -S fatscript-fry
        else
            # Neither pamac nor yay found (fallback).
            pacman -Syu --noconfirm --quiet git gcc curl openssl
            installFryFromSource
        fi
    ;;
    
    debian|ubuntu|linuxmint|popos|mxlinux|elementary)
        echo "Detected Debian/Ubuntu based distribution."
        apt update -qq
        apt install -qq -y git gcc libcurl4-openssl-dev libssl-dev
        installFryFromSource
    ;;
    
    fedora|mageia|korora|silverblue)
        echo "Detected Fedora based distribution."
        dnf -q install -y git gcc libcurl-devel openssl-devel
        installFryFromSource
    ;;
    
    centos|rhel|rocky|almalinux)
        echo "Detected CentOS/RHEL based distribution."
        yum -q install -y git gcc libcurl-devel openssl-devel
        installFryFromSource
    ;;
    
    opensuse*|gecko)
        echo "Detected openSUSE based distribution."
        zypper --quiet install -y git gcc libcurl-devel libopenssl-devel
        installFryFromSource
    ;;
    
    void|void-musl)
        echo "Detected Void Linux."
        xbps-install -Sy git gcc curl openssl-devel
        installFryFromSource
    ;;
    
    alpine)
        echo "Detected Alpine Linux."
        apk add -q git gcc libc-dev curl-dev openssl-dev
        installFryFromSource
    ;;
    
    macos)
        echo "Detected MacOS."
        if [ ! -d "/Library/Developer/CommandLineTools" ]
        then
            echo "Installing Xcode Command Line Tools..."
            xcode-select --install
        fi
        # brew install openssl
        installFryFromSource
    ;;
    
    termux)
        echo "Detected Termux."
        pkg install -qq git clang openssl
        installFryFromSource
    ;;
    
    *)
        echo "$targetPlatform not supported."
        exit 1
    ;;
esac
