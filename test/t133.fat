# frost and hot copies

recode  <- fat.recode
failure <- fat.failure

isFrozenElement = (scp: Scope, key: Text): Boolean -> {
  failure.trapWith(-> true)
  originalVal = scp(key)
  scp[key] = null         # should raize error if frozen
  scp[key] = originalVal  # restore the original value
  false
}

# Test `toFrostCopy` with a Scopes and Lists
scopeInput = {
  ~ a = 1
  ~ b = [
    {
      ~ c = 2
    }
  ]
}

frostCopy = recode.toFrostCopy(scopeInput)

r1 = frostCopy.a      == 1 & isFrozenElement(frostCopy, 'a')
r2 = frostCopy.b(0).c == 2 & isFrozenElement(frostCopy.b(0), 'c')

# Test `toFrostCopy` with a primitive
primitiveInput = 42
frostCopyPrimitive = recode.toFrostCopy(42)
r3 = frostCopyPrimitive == 42

# Ensure immutability of frozen Scope
testAddElements1 = <> {
  failure.trapWith(-> true)
  frostCopy.d = 4  # should raise an error
  false
}
r4 = testAddElements1
testAddElements2 = <> {
  failure.trapWith(-> true)
  frostCopy.b(0).d = 4  # should raise an error
  false
}
r5 = testAddElements2

# Test `toHotCopy` with a frozen Scope
hotCopy = recode.toHotCopy(frostCopy)
hotCopy.b[1] = { d = 4 }
r6 = hotCopy.b(1).d == 4

hotCopy.e = 7
r7 = hotCopy.e == 7

# Deeply nested structure
nestedInput = {
  ~ x = [ { ~ y = [ { ~ z = 3 } ] } ]
}

frostNested = recode.toFrostCopy(nestedInput)

# Check immutability at multiple levels
r8 = frostNested.x(0).y(0).z == 3 & isFrozenElement(frostNested.x(0).y(0), 'z')

# Modify after `toHotCopy`
hotNested = recode.toHotCopy(frostNested)
hotNested.x[0].y[0].z = 4
r9 = hotNested.x(0).y(0).z == 4

# Empty Scope
emptyScope = {}
frostEmptyScope = recode.toFrostCopy(emptyScope)
r10 = frostEmptyScope.size == 0
testAddElements3 = <> {
  failure.trapWith(-> true)
  frostEmptyScope.someKey = "someValue"  # should raise an error
  false
}
r11 = testAddElements3

# Verify `toHotCopy` on empty structures
hotEmptyScope = recode.toHotCopy(frostEmptyScope)

hotEmptyScope.newKey = "newValue"
r12 = hotEmptyScope.newKey == "newValue"

$result
r1 & r2 & r3 & r4 & r5 & r6 & r7 & r8 & r9 & r10 & r11 & r12 => 'pass'
_                                                            => 'fail'
