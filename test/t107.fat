# increment and decrement

# This is pretty much a bug, that has been turned into a language feature,
# because it actually makes sense, and would be very hard to fix, and also
# I fount it to be quite handy and useful in many scenarios. So impl. was
# refined and standardized for all types in fry v2.3.0.

~ b = true                # variable boolean 'b' at global scope
fn1External = <> b += !b  # will affect the external scope variable
fn1Local = <> b = !b      # will create a local immutable value equal to !b
r1 = fn1External == false & b == false & fn1Local == true & b == false

# note: booleans can't operate with decrement (-=), raises error
# b -= false  # Error: unsupported expression > Boolean <decrement> Boolean

~ n = 1                  # variable number 'n' at global scope
fn2External = <> n += 1  # will affect the external scope variable
fn2Local = <> n = n + 1  # will create a local immutable value equal to n + 1
r2 = fn2External == 2 & n == 2 & fn2Local == 3 & n == 2

fn3External = <> n -= 1  # will affect the external scope variable
fn3Local = <> n = n - 1  # will create a local immutable value equal to n - 1
r3 = fn3External == 1 & n == 1 & fn3Local == 0 & n == 1

_ <- fat.type.Chunk

~ c = Chunk('a')                  # variable chunk 'c' at global scope
fn4External = <> c += Chunk('b')  # will affect the external scope variable
fn4Local = <> c = c + Chunk('c')  # will create a local immutable value equal to c + 'c'
r4 = fn4External == Chunk('ab') & c == Chunk('ab') & fn4Local == Chunk('abc') & c == Chunk('ab')

# note: chunks can't operate with decrement (-=), raises error
# c -= Chunk('a')  # Error: unsupported expression > Chunk <decrement> Chunk

~ t = 'a'                  # variable text 't' at global scope
fn5External = <> t += 'b'  # will affect the external scope variable
fn5Local = <> t = t + 'c'  # will create a local immutable value equal to t + 'c'
r5 = fn5External == 'ab' & t == 'ab' & fn5Local == 'abc' & t == 'ab'

fn6External = <> t -= 'b'  # will affect the external scope variable
fn6Local = <> t = t - 'a'  # will create a local immutable value equal to t - 'a'
r6 = fn6External == 'a' & t == 'a' & fn6Local == '' & t == 'a'

~ l = [ 1 ]                  # variable list 'l' at global scope
fn7External = <> l += [ 2 ]  # will affect the external scope variable
fn7Local = <> l = l + [ 3 ]  # will create a local immutable value equal to l + [ 3 ]
r7 = fn7External == [ 1, 2 ] & l == [ 1, 2 ] & fn7Local == [ 1, 2, 3 ] & l == [ 1, 2 ]

fn8External = <> l -= [ 2 ]  # will affect the external scope variable
fn8Local = <> l = l - [ 1 ]  # will create a local immutable value equal to l - [ 1 ]
r8 = fn8External == [ 1 ] & l == [ 1 ] & fn8Local == [] & l == [ 1 ]

~ s = { a = 1 }                  # variable scope 's' at global scope
fn9External = <> s += { b = 2 }  # will affect the external scope variable
fn9Local = <> s = s + { c = 3 }  # will create a local immutable value equal to s + { c = 3 }
r9 = fn9External == { a = 1, b = 2 } & s == { a = 1, b = 2 } & fn9Local == { a = 1, b = 2, c = 3 } & s == { a = 1, b = 2 }

fn10External = <> s -= { b = 2 }  # will affect the external scope variable
fn10Local = <> s = s - { a = 1 }  # will create a local immutable value equal s - { a = 1 }
r10 = fn10External == { a = 1 } & s == { a = 1 } & fn10Local == {} & s == { a = 1 }

$result
r1 & r2 & r3 & r4 & r5 & r6 & r7 & r8 & r9 & r10 => 'pass'
_                                                => 'fail'
