# error on undefined key

failure <- fat.failure
_       <- fat.type.Scope
_       <- fat.extra.Option

scp = { x = 1, z = null }

isDefinedElementX = <> {
  failure.trapWith(-> false)
  scp.x  # direct access (dot-syntax)
  true
}

isUndefinedElementY = <> {
  failure.trapWith(-> true)
  scp.y  # direct access (dot-syntax), should raise error
  false
}

isDefinedElementZ = <> {
  failure.trapWith(-> false)
  scp.z  # direct access (dot-syntax), is null but key is set, no error
  true
}

r1 = isDefinedElementX & isUndefinedElementY & isDefinedElementZ

# Other ways to access a null value (won't raise error)
r2 = scp.{ y }      == null
r3 = scp.['y']      == null
r4 = scp['y']       == null
r5 = scp('y')       == null
r6 = scp.maybe('y') == None

# Basically scp.y is forbidden syntax because y was not defined in scp
# But this error gard only present with dot syntax access...
# So we can query an undefined value, without error, on it's own:

r7 = neverDefined == null

# Or also extract it without error

{ y } = scp
r8 = y == null

$result
r1 & r2 & r3 & r4 & r6 & r7 & r8 => 'pass'
_                                => 'fail'
