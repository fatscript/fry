# async workers and tasks

# run with --error

_    <- fat.async
time <- fat.time

global = { ~ testValue = 0 }

slowTask = (ms: Number): Number -> -> {
  time.wait(ms)
  global.testValue = ms  # modify external state
  ms
}

testTask1 = Worker(slowTask(42)).start

# testValue should still be 0 because task is async
r1 = global.testValue == 0

# testValue should be updated after worker completion
r2 = testTask1.await.result == 42
r3 = global.testValue       == 42

testTask2 = Worker(task = slowTask(500), wait = 50).start

# testValue should still be 42 because task is async
r4 = global.testValue == 42

# should raise AsyncError because timeout is less than execution time
r5 = testTask2.await  == AsyncError
r6 = global.testValue == 42  # should not have been altered

$result
r1 & r2 & r3 & r4 & r5 & r6 => 'pass'
_                           => 'fail'
