# index, sort and filter (lists)

_ <- fat.type.List

# Tests for indexOf
letters = [ 'a', 'b', 'c' ]
r1 = letters.indexOf('b') == 1
r2 = letters.indexOf('d') == -1

emptyList = []
r3 = emptyList.indexOf('a') == -1

duplicates = [ 'a', 'b', 'b', 'c' ]
r4 = duplicates.indexOf('b') == 1

# Tests for sortBy('key')
data = [
  { name = 'xyz', irrelevant = 5, number = 2 }
  { name = 'abc', irrelevant = 23, number = 3 }
  { name = 'klm', irrelevant = 14, number = 1 }
]

r5 = data.sortBy('name') == [
  { name = 'abc', irrelevant = 23, number = 3 }
  { name = 'klm', irrelevant = 14, number = 1 }
  { name = 'xyz', irrelevant = 5, number = 2 }
]

# Test sorting by a different key
r6 = data.sortBy('number') == [
  { name = 'klm', irrelevant = 14, number = 1 }
  { name = 'xyz', irrelevant = 5, number = 2 }
  { name = 'abc', irrelevant = 23, number = 3 }
]

# Test with some objects missing the sort key
dataWithMissingKey = [
  { name = 'xyz' }
  { notName = 23 }
  { name = 'abc' }
]

r7 = dataWithMissingKey.sortBy('name') == [
  { name = 'abc' }
  { name = 'xyz' }
  { notName = 23 }
]

# Test sorting a matrix by column index
matrix = [
  [ 1, 1 ]
  [ 1, 3 ]
  [ 1, 5 ]
  [ 1, 2 ]
]

r8 = matrix.sortBy(1) == [
  [ 1, 1 ]
  [ 1, 2 ]
  [ 1, 3 ]
  [ 1, 5 ]
]

# basic filter test

r9 = matrix.filter(item -> item(0)  == 8) == []
r10 = matrix.filter(item -> item(1) == 5) == [ [ 1, 5 ] ]

$result
r1 & r2 & r3 & r4 & r5 & r6 & r7 & r8 & r9 & r10 => 'pass'
_                                                => 'fail'
