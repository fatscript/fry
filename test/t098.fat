# param validation

_       <- fat.extra.Param
failure <- fat.failure

testFor = (param: Param, expect: Boolean): Boolean -> {
  runProcedure = <> {
    failure.trapWith(-> false)
    param.get
    true
  }
  expect == runProcedure
}

# Test with Boolean type
_ <- fat.type.Boolean
r1 = testFor(Param('undefined', Boolean), false)
r2 = testFor(Param('r1', Boolean), true)

# Test with Number type
_ <- fat.type.Number
num = 5
r3 = testFor(Param('num', Number), true)
r4 = testFor(Param('num', Boolean), false)

# Test with Text type
_ <- fat.type.Text
txt = "hello"
r5 = testFor(Param('txt', Text), true)

# Test with Option type
_ <- fat.extra.Option
opt = Some(5)
noneOpt = None()
r6 = testFor(Param('opt', Option), true)
r7 = testFor(Param('noneOpt', Option), true)

# Test with strict matching
# see: https://fatscript.org/en/syntax/types/#flexible-type-acceptance
r8 = testFor(Param('opt', Some, true), true)
r9 = testFor(Param('opt', Option, true), false)

# Test with List type
_ <- fat.type.List
lst = [ 1, 2, 3 ]
r10 = testFor(Param('lst', List), true)

# Test with composite type
# limitation: cannot define composite as a parameter, e.g.:
# testFor(Param('lst', List/Number) is not a valid syntax..
TextsList = List/Text
NumbersList = List/Number
r11 = testFor(Param('lst', TextsList), false)
r12 = testFor(Param('lst', NumbersList), true)

# Test with Method type
_ <- fat.type.Method
m = -> 5
r13 = testFor(Param('m', Method), true)

$result
r1 & r2 & r3 & r4 & r5 & r6 & r7 &
r8 & r9 & r10 & r11 & r12 & r13 => 'pass'
_                               => 'fail'
