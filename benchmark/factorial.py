import resource

def factorial(n):
    if n == 0:
        return 1
    return n * factorial(n - 1)

print(f"Result: {factorial(25)}")

# Get maximum memory usage of this process in kilobytes
memory_usage = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
print(f"Maximum memory used: {memory_usage} KB")
