function memoFibonacci() {
  const memo = {};
  return function fib(n) {
    if (n in memo) return memo[n];
    if (n <= 1) return n;
    return (memo[n] = fib(n - 1) + fib(n - 2));
  };
}

const fibonacci = memoFibonacci();
console.log(`Result: ${fibonacci(30)}`);

// Get maximum memory usage of this process in kilobytes
const memoryUsage = process.memoryUsage().rss / 1024;
console.log(`Maximum memory used: ${memoryUsage.toFixed(0)} KB`);
