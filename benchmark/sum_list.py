import resource

numbers = [i for i in range(100000)]
sum_value = sum(numbers)
print(f'Result: {sum_value}')

# Get maximum memory usage of this process in kilobytes
memory_usage = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
print(f"Maximum memory used: {memory_usage} KB")
