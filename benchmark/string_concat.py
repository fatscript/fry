import resource

result = ""
for i in range(100000):
    result += "hello"

print(f'Result: {len(result)}')

# Get maximum memory usage of this process in kilobytes
memory_usage = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
print(f"Maximum memory used: {memory_usage} KB")
