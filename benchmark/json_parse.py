import resource
import json

json_str = (
    '[' + 
    ('{"name":"John","age":30,"city":"New York"},' * 10000)[:-1] +
    ']')
obj = json.loads(json_str)
output = json.dumps(obj)
print(f'Result: {len(output)}')

# Get maximum memory usage of this process in kilobytes
memory_usage = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
print(f"Maximum memory used: {memory_usage} KB")
