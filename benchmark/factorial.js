// no imports needed

function factorial(n) {
  if (n === 0) return 1;
  return n * factorial(n - 1);
}

console.log(`Result: ${factorial(25)}`);

// Get maximum memory usage of this process in kilobytes
const memoryUsage = process.memoryUsage().rss / 1024;
console.log(`Maximum memory used: ${memoryUsage.toFixed(0)} KB`);
