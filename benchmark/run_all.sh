#!bash

TIMEFORMAT='Real wall time used: %R s'

echo
echo 'factorial: JavaScript'
time node factorial.js
echo
echo 'factorial: Python'
time python factorial.py
echo
echo 'factorial: FatScript'
time fry factorial.fat
echo

echo '--'

echo
echo 'fibonacci: JavaScript'
time node fibonacci.js
echo
echo 'fibonacci: Python'
time python fibonacci.py
echo
echo 'fibonacci: FatScript'
time fry fibonacci.fat
echo

echo '--'

echo
echo 'fibonacci_memo: JavaScript'
time node fibonacci_memo.js
echo
echo 'fibonacci_memo: Python'
time python fibonacci_memo.py
echo
echo 'fibonacci_memo: FatScript'
time fry fibonacci_memo.fat
echo

echo '--'

echo
echo 'json_parse: JavaScript'
time node json_parse.js
echo
echo 'json_parse: Python'
time python json_parse.py
echo
echo 'json_parse: FatScript'
time fry json_parse.fat
echo

echo '--'

echo
echo 'string_concat: JavaScript'
time node string_concat.js
echo
echo 'string_concat: Python'
time python string_concat.py
echo
echo 'string_concat: FatScript'
time fry string_concat.fat
echo

echo '--'

echo
echo 'sum_list: JavaScript'
time node sum_list.js
echo
echo 'sum_list: Python'
time python sum_list.py
echo
echo 'sum_list: FatScript'
time fry sum_list.fat
