// no imports needed

let result = "";
for (let i = 0; i < 100000; i++) {
    result += "hello";
}
console.log(`Result: ${result.length}`);

// Get maximum memory usage of this process in kilobytes
const memoryUsage = process.memoryUsage().rss / 1024;
console.log(`Maximum memory used: ${memoryUsage.toFixed(0)} KB`);
