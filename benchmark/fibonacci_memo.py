import resource
from functools import lru_cache

# None means unlimited cache size
@lru_cache(maxsize=None)
def memo_fibonacci(n):
    if n <= 1:
        return n
    return memo_fibonacci(n-1) + memo_fibonacci(n-2)

print(f'Result: {memo_fibonacci(30)}')

# Get maximum memory usage of this process in kilobytes
memory_usage = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
print(f"Maximum memory used: {memory_usage} KB")
