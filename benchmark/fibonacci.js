// no imports needed

function fibonacci(n) {
  if (n <= 1) return n;
  return fibonacci(n - 1) + fibonacci(n - 2);
}
console.log(`Result: ${fibonacci(30)}`); 

// Get maximum memory usage of this process in kilobytes
const memoryUsage = process.memoryUsage().rss / 1024;
console.log(`Maximum memory used: ${memoryUsage.toFixed(0)} KB`);
