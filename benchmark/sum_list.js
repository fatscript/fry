// no imports needed

let numbers = [];
for (let i = 0; i < 100000; i++) {
  numbers.push(i);
}
const sum = numbers.reduce((acc, val) => acc + val, 0);
console.log(`Result: ${sum}`);

// Get maximum memory usage of this process in kilobytes
const memoryUsage = process.memoryUsage().rss / 1024;
console.log(`Maximum memory used: ${memoryUsage.toFixed(0)} KB`);
