#!/usr/bin/env bash

echo
echo
./factorial_race.sh

echo
echo
./fibonacci_race.sh

echo
echo
./fibonacci_memo_race.sh

echo
echo
./json_parse_race.sh

echo
echo
./string_concat_race.sh

echo
echo
./sum_list_race.sh
