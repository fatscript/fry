import resource

def fibonacci(n):
    if n <= 1:
        return n
    return fibonacci(n-1) + fibonacci(n-2)
print(f'Result: {fibonacci(30)}')

# Get maximum memory usage of this process in kilobytes
memory_usage = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
print(f"Maximum memory used: {memory_usage} KB")
