// no imports needed


const json =
  "[" +
  '{"name":"John","age":30,"city":"New York"},'.repeat(10000).slice(0, -1) +
  "]";
const obj = JSON.parse(json);
const output = JSON.stringify(obj);
console.log(`Result: ${output.length}`);

// Get maximum memory usage of this process in kilobytes
const memoryUsage = process.memoryUsage().rss / 1024;
console.log(`Maximum memory used: ${memoryUsage.toFixed(0)} KB`);
