#!/usr/bin/env bash

# @file web_build.sh
# @brief Build script, web version using emscripten
# @author Antonio Prates <hello@aprates.dev>
# @version 4.0.0
# @date 2025-01-04
#
# @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
# Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
# Licensed under the GNU General Public License v3.0.
# See LICENSE file in the project root for full license.

# Emscripten, terser and clean-css-cli are needed for the web build:
# - https://emscripten.org
# - https://github.com/terser/terser
# - https://github.com/clean-css/clean-css-cli

# Emscripten config
export EMSDK_QUIET=1
source ../../emsdk/emsdk_env.sh  # adjust the path to your emsdk installation
compiler="emcc"

FRY_VERSION="$(git describe --abbrev=6 --dirty --always --tags)"

sdk="bin/b64.o bin/huge.o bin/rle.o bin/embedded.o bin/fatcurses.o \
bin/logger.o bin/memory.o bin/sdk.o bin/patterns.o bin/structures.o \
bin/sugar.o bin/crypto.o bin/webenv.o"

fry="bin/repl.o bin/help.o bin/formatter.o bin/lexer.o bin/parser.o \
bin/libs.o bin/interpreter.o"

flags="-DFRY_VERSION=\"$FRY_VERSION\" -s USE_SDL=2 -s USE_SDL_MIXER=2 -Os $*"

links="-s INITIAL_MEMORY=512mb -s MAXIMUM_MEMORY=2gb -s ALLOW_MEMORY_GROWTH=1 \
-s ASYNCIFY -s ASYNCIFY_STACK_SIZE=67108864 -s TOTAL_STACK=256mb \
-s EXIT_RUNTIME=1 -s WASM_BIGINT -s FETCH -s USE_SDL=2 -s USE_SDL_MIXER=2 \
-lm -lpthread --js-library src/sdk/webenv.js \
--preload-file bin/guacano.fat@/sample/guacano.fat \
--preload-file bin/menu.fat@/sample/games.fat \
--preload-file bin/pong.fat@/sample/pong.fat \
--preload-file bin/particles.fat@/sample/particles.fat \
--preload-file bin/snake.fat@/sample/snake.fat \
--preload-file bin/triangle.fat@/sample/triangle.fat"

emitFinalBinary="$compiler -O1"

finishUp() {
    rm bin/*.o
    
    # minify JS and CSS payload
    terser web/fry.js -o web/fry.min.js --compress --mangle --comments false
    terser web/script.js -o web/script.min.js --compress --mangle --comments false
    cleancss -o web/styles.min.css web/styles.css
    cleancss -o web/xterm.min.css web/xterm.css
}

# clean target folder
[ -d "bin" ] && rm -r bin
[ -f "web/fry.data" ] && rm web/fry.data
[ -f "web/fry.js" ] && rm web/fry.js
[ -f "web/fry.min.js" ] && rm web/fry.min.js
[ -f "web/fry.wasm" ] && rm web/fry.wasm
[ -f "web/script.min.js" ] && rm web/script.min.js
[ -f "web/styles.min.css" ] && rm web/styles.min.css
[ -f "web/xterm.min.css" ] && rm web/xterm.min.css

echo 'Building fry [web]:' && mkdir bin && echo -n '   sdk.'                       && \
$compiler $flags -o bin/b64.o         -c src/sdk/b64.c                             && \
$compiler $flags -o bin/rle.o         -c src/sdk/rle.c                             && \
$compiler $flags -o bin/huge.o        -c src/sdk/huge.c                            && \
$compiler $flags -o bin/fatcurses.o   -c src/sdk/fatcurses.c        && echo -n '.' && \
$compiler $flags -o bin/logger.o      -c src/sdk/logger.c           && echo -n '.' && \
$compiler $flags -o bin/memory.o      -c src/sdk/memory.c           && echo -n '.' && \
$compiler $flags -o bin/sdk.o         -c src/sdk/sdk.c              && echo -n '.' && \
$compiler $flags -o bin/structures.o  -c src/sdk/structures.c       && echo -n '.' && \
$compiler $flags -o bin/sugar.o       -c src/sdk/sugar.c            && echo -n '.' && \
$compiler $flags -o bin/crypto.o      -c src/sdk/crypto.c           && echo -n '.' && \
$compiler $flags -o bin/webenv.o      -c src/sdk/webenv.c                          && \
echo $' \e[34mdone ✔ \e[0m' && echo -n '   front-end'                              && \
$compiler $flags -o bin/patterns.o    -c src/syntax/patterns.c      && echo -n '.' && \
$compiler $flags -o bin/lexer.o       -c src/syntax/lexer.c         && echo -n '.' && \
$compiler $flags -o bin/parser.o      -c src/syntax/parser.c        && echo -n '.' && \
echo $' \e[34mdone ✔ \e[0m' && echo -n '   runtime'                                && \
$compiler $flags -o bin/embedded.o -c src/runtime/embed/commands.c && echo -n '..' && \
$compiler $flags -o bin/libs.o    -c src/runtime/embed/dispatcher.c && echo -n '.' && \
$compiler $flags -o bin/interpreter.o -c src/runtime/interpreter.c && echo -n '..' && \
echo $' \e[34mdone ✔ \e[0m' && echo -n '   tools'                                  && \
$compiler $flags -o bin/formatter.o   -c src/syntax/formatter.c     && echo -n '.' && \
$compiler $flags -o bin/repl.o        -c src/runtime/utils/repl.c   && echo -n '.' && \
$compiler $flags -o bin/help.o        -c src/runtime/utils/help.c   && echo -n '.' && \
$compiler $flags -o bin/fry.o         -c src/fry.c                 && echo -n '..' && \
echo $' \e[34mdone ✔ \e[0m' && echo 'Packing samples:'                             && \
fry -b bin/guacano.fat sample/guacano.fat                                          && \
fry -b bin/menu.fat sample/menu.fat                                                && \
fry -b bin/particles.fat sample/particles.fat                                      && \
fry -b bin/pong.fat sample/pong.fat                                                && \
fry -b bin/snake.fat sample/snake.fat                                              && \
fry -b bin/triangle.fat sample/triangle.fat                                        && \
echo -n '   linking..'                                                             && \
$emitFinalBinary -o web/fry.js  bin/fry.o  $fry $sdk $links       && echo -n '...' && \
echo $' \e[34mdone ✔ \e[0m' && finishUp "$@" || (echo ' -> failed!' && exit 1)
