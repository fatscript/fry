#!/usr/bin/env bash

# @file auto_check.sh
# @brief Code inspection tool
# @author Antonio Prates <hello@aprates.dev>
# @version 4.1.0
# @date 2025-02-16
#
# @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
# Copyright (c) 2022-2025, Antonio Prates <hello@aprates.dev>
# Licensed under the GNU General Public License v3.0.
# See LICENSE file in the project root for full license.

# many clang-tidy checks give false positives
# see other filtered options at .clang-tidy

echo 'Performing static analysis...'

run_clang_tidy() {
    clang-tidy -system-headers "$1"/* 2>/dev/null \
    | grep -v "fry.c:111:33: warning: Access to field" \
    | grep -v "parser.c:1356:42: warning: both sides of" \
    | grep -v "redundant continue statement at the end of loop" \
    | grep -v "clang-diagnostic-unused-command-line-argument" \
    | grep -v "function 'vsnprintf' is insecure" \
    | grep -v "function 'snprintf' is insecure" \
    | grep -v "function 'memmove' is insecure" \
    | grep -v "function 'strncat' is insecure" \
    | grep -v "function 'strncpy' is insecure" \
    | grep -v "function 'sprintf' is insecure" \
    | grep -v "function 'sscanf' is insecure" \
    | grep -v "function 'memcpy' is insecure" \
    | grep -v "function 'memset' is insecure" \
    | grep -v "uses identifier '_GNU_SOURCE'" \
    | grep -v "clang-diagnostic-error" \
    | grep -E -A 2 "(warning|error):"
}

run_clang_tidy src
run_clang_tidy src/core
run_clang_tidy src/libs
run_clang_tidy src/libs/extra
run_clang_tidy src/libs/type
run_clang_tidy src/sdk
run_clang_tidy src/util

cppcheck --template=vs --enable=all "src" 2>&1 \
| grep -v "src/stdlib/socket.c(111): warning: Size of pointer 'buff'" \
| grep -v "src/stdlib/socket.c(112): warning: Size of pointer 'buff'" \
| grep -v "src/runtime/core/loops.c(245): warning: Either the condition" \
| grep -v "'headerCallback' is a callback function" \
| grep -v "Common realloc mistake" \
| grep -v "Cppcheck does not need standard library headers to get proper results" \
| grep -v "error.c" \
| grep -v "src/sdk/huge.c" \
| grep -E -A 2 "(warning|error)"

# ...or maybe try directly: cppcheck --template=vs --enable=all "src"

# note: 'src/sdk/huge.c' is suppressed from cppcheck output
# due to false positives for null pointer dereference

for file in *.sh
do
    if [ -f "$file" ]
    then
        shellcheck -x --severity=error "$file"
    fi
done

./compile.sh -DDEBUG

echo 'Running leak check...'
./run_tests.sh --leak-check &>leak_check.log
cat leak_check.log | grep "ERROR SUMMARY" | grep -v "0 errors"
echo 'ALL DONE'
