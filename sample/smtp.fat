#!/usr/bin/env fry

# @file smtp.fat
# @brief SMTP library usage sample
# @author Antonio Prates <hello@aprates.dev>
# @version 3.4.0
# @date 2024-10-30
#
# @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
# Copyright (c) 2022-2024, Antonio Prates <hello@aprates.dev>
# Licensed under the GNU General Public License v3.0.
# See LICENSE file in the project root for full license.

smtp <- fat.smtp

smtp.config(
  from = ContactInfo('sender@example.com', 'Sender Name')
  server = 'smtps://smtp.example.com:1234'
  username = 'your_username'
  password = 'your_password'
)

smtp.send(
  to = [
    ContactInfo('recipient1@example.com', 'Recipient One')
    ContactInfo('recipient2@example.com')  # name is optional
  ]
  subject = 'Test Email'
  body = 'This is a test email sent using fat.smtp.'
)
