# Sample Games

You can enjoy the games by running the Docker image as follows:

```bash
docker run --rm -it fatscript/games
```

## Usage

- In the menu, use the arrow keys to select a game, then press Enter.
- To exit the menu or a game, press `q`.

## Legal Notes

The project sources are released under [GPLv3](LICENSE) © 2022-2024 Antonio Prates. You can check out the repository [here](https://gitlab.com/fatscript/fry).
