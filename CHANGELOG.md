# Changelog

Changes to the FatScript interpreter will be documented in this file.

next version:

- avoid UB on operChunks to safely handle zero-length sum (hardening)
- fix segmentation fault on standalone bridge functions (bugfix)
- add bridge.unsafeCStr method to enable reading C-string return vals
- add fat.socket library to stdlib (feature) !!!
- fix segmentation fault on TRO scope compaction logic (bugfix)
- add libffi to docker build image (fix missing)
- avoid undefined behavior on realloc with size signedness (hardening)
- ensure procedure autoCall bypass works as a method arg too (bugfix)
- harden local adjacency token check on fromJSON (hardening)
- allow transitioning mutable values to error type (better ergonomics)
- add support for FileLogger to optionally log withConsole (feature)
- add growth-based GC heuristic to control memory usage (optimization)
- add isBlank method to Text prototype (feature)
- add socket chat to samples folder (documentation / example)

## Version 4.0.0

- restructure project for modularity and clarity (code embellishment)
- enforce 4.x.x behavior (remove USE_STRICT_PROCEDURE flag gards)
- fix push-pop balance in innerAssign on error return branch (bugfix)
- add scope seal and entry freeze support (feature)
- add freeze method to all native types prototype extensions (feature)
- add argument label validation for method calls (hardening)
- add recursive equality limit for nested structures (hardening)
- use better precedence for prototype member resolution (breaking)
- provide proper error for empty key on List.sortBy method (bugfix)
- add Scope.valuesOf method to facilitate chained syntax (feature)
- remove bytes per node ratio statistics (was still mostly inaccurate)
- update REPL/Debugger ASCII art (futile embellishment)
- add Opaque type to extra pack for soft encapsulation (feature)
- fix Scope.vmap implementation to prevent TypeError (hardening)
- add toFrostCopy and toHotCopy methods to recode library (feature)
- add support for match on type option, e.g. Some(1) == Some(Number)
- add CSV support for quoting, escaped quotes, separators in quotes
- raise error for undefined key on dot access (hardening, breaking)

## Version 3.4.1

- fix segfault of toJson for cyclic/recursive structures (hardening)
- improve ast printing/logging for cyclic/recursive structures
- add questionableTypeCast code hint support (probe mode)
- make code hints display by default in repl mode (ergonomics)
- fix source bug in fat.extra.Duration (minor issue, nanos support)
- suppress code hints for fry embedded sources (unless debug mode)
- report bytes per node ratio statistics more accurately
- fix a bug affecting EncryptedStorable encode method

## Version 3.4.0

- introduce <> for procedures, no-arguments, only auto-callable
- procedures alias to zero-argument methods for compatibility
  (no breaking changes, but forward compatibility with v4.x.x)
- strict mode flag: -DUSE_STRICT_PROCEDURE enforces 4.0.0 behavior
  (if set, autoCall is restricted to procedures to reduce ambiguity)
- adds possiblyUseProcedure linting for probe mode
- update samples and test cases to reflect new procedures syntax

## Version 3.3.2

- improve formatter to ignore # symbols within quoted texts
- improve error reporting for unsupported deeply nested type checks
- implement deep type validation for List/Composite for correctness
  (note: now we traverse the list at runtime which most certainly
  adds overhead, but if dev is making use of this feature he expects
  the type system to add safety, and not to silently bypass checks)
- drop obscure JSON import feature (optimization / breaking change)
  (use "data = recode.fromJSON(file.read(filename))" instead)
- improve error feedback for listIdxOf
- drop unstable utf-8 string comparison optimizations (bugfix)
- improve formatter for minus operator in different syntax contexts

## Version 3.3.1

- fix build and test case of FFI integration for MacOS

## Version 3.3.0

- add file.readSys that supports read for system and virtual files
- fix inconsistent AST log formatting for HugeInt values
- transform node meta from doubly to single-linked list (optimization)
- fix docker build base image
- some minor optimizations from json_parse benchmark study
- add fat.bridge library for external C library support via libffi
- review Chunk constructor for Boolean and Number (breaking change):
  - removed Boolean argument support (this feature was unnecessary,
    as it had no practical use case)
  - for Number, Chunk now takes the argument as N bytes to calloc,
    instead of a single byte value (this is both more logical and has
    a practical use for FFI buffer creation)
- review Chunk.toText to just disregard section of nulls at the end
- add support for square bracket chaining and mutations on indexables
- reviewed logger to provide a better Chunk representation
- add vmap into Scope prototype member to provide map-values support
- fix bug on proper error trapping with nullish coalescing operator
- improve OpEqual comparison for methods (uses the body pointer now)
- add mouse events support for fat.curses and MouseEvent type/parser
- add color.to24Bit for true color support on console and fatcurses
- improve color.detectDepth for also matching true color support
- accept abbreviated HTML hex color codes codes e.g. #123 -> #112233
- use proper JS/CSS minification tools for web build
- fix deep scope comparison equality with similar sequences
- remove Method AST printing from scope traversal, better ergonomics
  (you still can peek Method AST, but only through direct reference)
- add groups method to Text prototype (advanced regex matching)
- add dynamic buffer growth for text interpolation (no more 16k limit)
- throw error on file.resolve instead of NULL return (breaking change)
- throw error on file.read instead of NULL return (breaking change)
- throw error on file.readBin instead of NULL return (breaking change)
- throw error on file.readSys instead of NULL return (breaking change)
- throw error on file.write instead of Boolean ret. (breaking change)
- throw error on file.append instead of Boolean ret. (breaking change)
- throw error on file.remove instead of Boolean ret. (breaking change)
- throw error on file.mkDir instead of Boolean ret. (breaking change)
- throw error on file.lsDir instead of NULL return (breaking change)
  (all in all the file methods throwing errors allows for programs to
  better customize handling decisions, and prevents silent failures)

## Version 3.2.0

- add fat.smtp lib to send emails via SMTP using libcurl
- prevent use of $debug embedded command on default build
- add benchmark scripts and code optimizations (performance)
- add automatic tail-recursion optimization support (TRO)
- improved stats/clock output logs when -c flag enabled

## Version 3.1.2

- harden parser for ambiguous case/else-if nesting (fix)
- support method local static binding (only for modules)
- fix evalSource early collection of AST via microGC
  (would cause method-AST built from eval to be corrupted)
- improve build portability with pkg-config
- use mutex for guarding async.atomic against Worker.cancel
- make $port embedded command state thread local
- minor update/review on HashMap implementation
- use alternate screen buffer for fatcurses implementation
  (the new implementation more closely resembles ncurses)
- prevent fatcurses char render on external positions (improved)
- better compatibility for moveTo and print methods (ncurses build)
- fix Text.overlay utf-8 support
- allow file.lsDir for arbitrary number of items to be returned
- prevent JS lockup (emscripten build)

## Version 3.1.1

- use binary data for enigma.digest and enigma.hmac

## Version 3.1.0

- only nullify scope entries, no remove (breaking change)
- fix deep scope comparisons containing null entries
- optimize scope prototype with scopKeys as embedded command
- add pick and omit methods to scope prototype extension
- add support for chained destructuring assignment
- add noCrash method into failure library as unsafe run wrapper
- add resolve method into file library (canonical name for path)
- fix http.listen bug corrupting stored HttpResponses
- harden parser against malformed method definition
- fast-track method reference into evalCall (sdk optimization)
- add digest, pbkdf2, hmac, encryptAES, decryptAES to enigma lib
  (when built with OpenSSL, strong and secure cryptographic
  functions are now accessible through a simple built-in API)
- add support for HugeInt to Number via Number constructor
- add support for HugeInt to Chunk type conversions
- add support for Chunk to HugeInt type conversions (up to 512 bytes)
- add toChunk method to Text prototype
- add enigma.bytes for cryptographically secure random bytes gen.
- add support for salt and iter parameters to legacy derive method
  (however pbkdf2 is more recommended, if using the emscripten build
  you still may need to rely on old enigma.derive, so new option can
  improve a bit on security matter by adding salt and iteration)
- prevent fatcurses char render on negative positions

## Version 3.0.3

- Fix critical bug with broken recode.fromBase64

## Version 3.0.2

- fix big wait times as in time.wait(infinity) behavior
- set x-www-form-urlencoded request body inferred as Text type
- add $port embedded command for checking the connected port
- support OPTIONS method to be implemented for http.listen
- add a default handler for OPTIONS method (http.listen)
- fix fromFormData type inference for empty string
- use default in methods with arguments that evaluate to null (fix)
- ensure null values are propagated into method scopes
  (correct lexical scoping in nested/recursive calls)
- ignore presence of null values in scope comparisons (fix)
- allow Number.truncate to propagate value's type alias
  (e.g. time.now.truncate will now still yield an Epoch value)
- skip items for destructList if key consists of underscores
- ensure list copy remains unaffected when original list is altered
- fix evalScope for entry assignment of errors from before
  (this bug was introduced by v3.0.1 due to silent errors fix)
- revert b64 as arg name for $fromBase64 (fixes fatpack comp. issue)

## Version 3.0.1

- fix double free on node source path/reference string
- ensure file.remove won't follow links (safety)
- use UNLOCK_NODE marco for removing node lock (sdk optimization)
- use lazy clean up for isFirstVisit/endVisit (sdk optimization)
- fix miss-freed entry on removeFromScope (bug)
- streamline removeFromScope logic (sdk optimization)
- fix silent errors on evalList and evalScope
- fix silent errors on type instantiation call
- treat assignments in method args as labels to prevent scope leakage
- use smarter cache resolution for list getByIndex (sdk optimization)
- harden async workers thread concurrency management (bug fixes)
- improve error clarity for type mismatch on method call
- add support for using slashes or dots in imports (syntactic sugar)
- fix worker cleanup strategy for thread-local cached values (bug)
- optimize math lib and implement math.round as embedded command
- fix bug with scope layer cleanup, when reused and mutex handling
- fix bug with fileMkDir reporting existing dir as error

## Version 3.0.0

- remove recode half-baked XML codec (deprecated since 2.5.1)
- remove console isTty method (deprecated since 2.5.1)
- remove fatpack (now exists in it's separated project/repo)
- fix operCompound error message on reassignment to immutable
- remove support for special entries (breaking-change)
  (now entries named with underscore work just the same
  as other entries with whatever name, immutable by default)
- remove support for non-range selections (deprecated since 2.5.1)
- update file.mkDir to create intermediate directories as needed
- update file.remove to remove files and directories recursively
- resolve local imports and fallback to project root if missing
- exclude embedded libraries from bundling process (breaking-change)
- remove minify source (-z option) support from cli options
  (simply use bundling tool or a build script with recode.minify)
- fix GC miss-freed default value of type instantiation call
- fix GC miss-freed error handler during mark and sweep
- fix bug with regular expression cache management
- add StrictType directive as a means to disable flexible acceptance
- refactor and improve Param and Using prototypes (optimization)
- add UsingStrict to fat.extra.Param library for strict matching
- review internal prettyNumber function, improves floating point
- add enclosing scope context-sensitive argument resolution
  (previously chained access would overshadow enclosing block
  which would produce very counter-intuitive call issues)
- remove sdk.autoGC configuration and support (optimization)
  (motivation: this feature would introduce overhead, and there was
  hardly a case where it could be effective and useful; if GC needs
  tunning, using runGC/quickGC strategically is the proper solution)
- harden file path expander/loader function (getSourceForPath)
- remove year and month approximations from Duration type (nonsense)
- check for presence of mandatory type parameters (breaking-change)
  (in accordance with how methods calls already work, hardening)
- improve default error handler
- add Fuzzy type to extra pack for chance based decisions
- separate concerns of sdk.stringify and recode.toJSON
  (toJSON produces always valid JSON at the expense of discarding
  values that can't be immediately represented into JSON, while
  stringify keeps Chunk as base64 and HugeInt as hexadecimal)
- harden Logger type and remove BoringLogger
- optimize stack management for mapping operations
- improve error feedback and hardening of storType (interpreter core)
- add support for equality and inequality comparison of methods
  (this is a simple check for the method pointer, it won't actually
  compare the method bodies, but it's good to check the reference)
- fix worker context timeout control logic
- allow http server to receive arbitrarily long request body
- harden parameter checking on mappers and find predicate
- add support for tap operator <<
- extend microGC usage to loops and other custom method executions
- add seek method to chunk prototype extension
- update and improve FatScript Playground pre-loaded samples
- revert of use monotonic coarse clock for timeout (compatibility)

## Version 2.6.0

- add extra compound assignment operators (\*=, /=, %=, \*\*=)
- fix bug with half-open range operator for selecting until zero
- add patch method to text, chunk and list prototype extensions
- disable continue on error for REPL if running with file
  (this is a safer default, specially when considered the REPL use
  as a means to fire up methods defined in a source file)
- rename code hint implicitArgument to implicitParameter (probe mode)
- review formatter function (remove spaces from blank lines)
- add Param and Using types for parameter presence and type checking
- improve static analysis for implicits detection (probe mode hints)
- fix bug with ternary outcome on switch cases (introduced in 2.1.x)
- use monotonic coarse clock for context timeout (optimization)
- add support for non-colored output (via piped output or TERM=dumb)

## Version 2.5.1

- forbid call syntax using square or curly brackets (at parse time)
- apply smart runtime node deduplication for booleans (optimization)
- fix SIGCHLD signal handling for systShell and systCapture methods
- fix potential file descriptor leak on systFork method impl.
- enhance store node copy-on-write smart eviction (optimization)
- implement recode.inferType as embedded command (optimization)
- annotate recode.numeric as deprecated (to be removed on 3.x.x)
- annotate recode.xmlWarnings as deprecated (to be removed on 3.x.x)
- annotate recode.toXML as deprecated (to be removed on 3.x.x)
- annotate recode.fromXML as deprecated (to be removed on 3.x.x)
- add console.isTTY and annotate console.isTty as deprecated (3.x.x)
- properly implement sdk.getMeta as embedded command (optimization)
- implement console.moveTo as embedded command (optimization)
- fix bug where src and dest may overlap in memcpy hugeMod
- no recode.minify for JSON files in fatpack (avoid malformed JSON)

## Version 2.5.0

- harden Storable by adopting fromJSON instead of sdk.eval
- add Option type to extra pack
- add maybe method to scope prototype extension
- add headOption, itemOption, findOption to list prototype extension
- fix switch case implementation for matching null values on types
- make scoped blocks switch not only context but also self reference
- decouple math library implementation from list prototype extension
- add parallel http request example to code samples
- add support for destructuring assignment on lists
- add support nullish coalescing assign operator (??=)
- use a slightly more optimized implementation for microGC
- add jail mode to restrict FS, network and sys calls
- optimize method instantiation and memory usage
- add support for switch alternative case match via OR operator
- add $break embedded command for setting breakpoints (debugger)
- support lazy values via memoized methods of arity zero
- add ??? as a null keyword for unimplemented semantics
- raise error on bad while syntax, hardening against infinite loop
- fix \*\* (exponentiation operator) precedence
- prevent null values on ?. (if-dot) be lifted to void prototype
- lift number format fmt string as %(fmt)f if % is not defined
- fix bug with logical and/or evaluation for types (check if defined)
- add map method to list prototype extension (to facilitate chaining)
- fix segmentation fault on empty first statement (e.g. '{} x')
- add type promotion support for List derivate (for consistency)
- add count(frag) method to text prototype extension
- fix align bug with overlay method for texts larger than base
- refine and review error subtypes emitted by interpreter

## Version 2.4.1

- use a slightly more optimized huge to number conversion
  (the optimized impl. also fixes a bug seen on iSH platform)
- use preemptive timeout handling during async.await
- use heuristics to skip over copying immutable list collections
- fix curses.flushKeys implementation for fatcurses back-end
- use FAST_STR_EQ marco for scope key lookup (sdk optimization)
- allow sorting a list of huge int or by huge int type
  (also harden sort strategy against potential bugs on un-sortable)
- add instructions on how to setup vim/neovim plugin
- fix bug where microGC would collect anonymous error handlers
- add microInterpretExpr function to optimize ast at parse time
- add truncate method to number prototype extension
- add keys method to scope prototype extension
- add walk method to list prototype extension
- fix chained assign evaluation (a = b = c = 0)
- simplify the assign evaluation internal logic (optimization)
- fix switch case implementation for allowing match on types
- improve error handling for nested error handler traps
- fix minus and range operator inlining with parenthesis
- improve logger mixing to allow asMessage method override
- fix switch-case operator precedence
- fix recode.inferType to identify number with uppercase exponent (E+)

## Version 2.4.0

- use dynamic scope registry, making scope allocation unlimited
- ensure numbers have no trailing zeroes after decimal point
- add support for switch-case operator >>
- fix bug where exitHooks caused deadlock if a thread had crashed
- fix bug where worker result got GC collected before return
- fix bug where worker await would prevent GC to kick in
- add atomic to async lib as mutex wrapper
- remove implicit file lock on every file operation
  (same protection can be obtained with async.atomic where needed)
- reduce memory footprint via microGC that runs after each methodCall
- add wait (timeout) property to worker with more efficient impl.
- remove task type (as it now is redundant with enhanced worker)
- implement workaround to execute async worker sync for emscripten
- allow await error to get stored as result and re-raised when called
- allow import errors to be returned from import operation
- add blockSig method to system lib
- add fill method to curses lib
- fix bug where curses.box would draw lines one char too short

## Version 2.3.0

- standardize increment and decrement operations for all types (fix)
- introduces += for booleans in conformance with auto-init effect
- improve random number generator initialization for emscripten
- add more static analysis checks on probe mode (improvement)
- add infinity as keyword pertaining to number type
- make behavior of parser more consistent for numbers like 1.0e+309
- fix potential memory leaks on failures within map operations
- add quickGC method to sdk lib, for high-throughput scenarios
- add setAutoGC method to sdk lib, for simplified GC management
- add processors method to async lib (get online processors count)

## Version 2.2.0

- ensure empty parentheses are recognized as valid return values (fix)
- adjust max safe value for double to integer conversion (optimization)
- add listReduce function as embedded command (optimization)
- enforce immutability in method parameters as per specification (fix)
- add alert for method parameters with mut operator (improved feedback)
- correct serious precedence bug with parenthesis resolution (fix)
- improve scopes cache to de-duplicate on lexical search (optimization)
- allow parsing HugeInt without 0x prefix (flexibility/improvement)
- avoid prefixing hex values with 0x on toText (better ergonomics)
- limit number conversion to huge int up to 2^53 (bug fix)
- add toNumber method to huge int prototype extension
- update isNan to isNaN in math lib, to agree with naming convention

## Version 2.1.1

- prevent auto-call on methods that take arguments (fix behavior)
- revert mitigate memory leak on local imports (caused other bugs)
- add alert for local import inside method body (at parse time)
- fix bug preventing correct local entry lookup on mapList
- fix list type check against mapper parameters on mapList
- update formatter rule for .~ chaining

## Version 2.1.0

- add ignore list support to fatpack (via .fatignore)
- fix segmentation fault on bad destructuring assignment
- add seekByte method to chunk prototype extension
- add localStorage support on web build via extra.Storable
- fix segmentation fault on sdk.eval and recode.fromJSON results
- limit ast wrapping to specific cases only (optimization)
- fix failure.trap handling for errors in list evaluations
- fix bug where import flags could end as keys on map operations
- add huge integer primary type and arithmetic operations
- add HugeInt prototype extensions and test cases
- add a python alternative implementation for fatpack
- add ssl support for http.listen via http.setSSL method
- rename long option of -z (--zip) to --minify
- method call stack logic review (optimization)
- fix segmentation fault on invalid import syntax
- add reduce method to list prototype extension
- fix bug on fat.nanorc syntax highlighting
- allow auto-call on all methods (except when capturing arguments)
- fix fromJSON bug on negative numbers (parser)
- add sdk.getTypes method capable of listing declared types
- make Type a reserved word, checks entry is a type definition
- remove buggy HashMap values method (won't fix)
- use a more collision resistant hash function for metaspace
- fix infinite loop on chained loop operations
- mitigate memory leak on local imports
- fix deep copy implementation for type instantiation
- add copy method to scope proto (using deep copy impl.)

## Version 2.0.1

- fix memory leak on http.listen server implementation
- fix http content-type automatic header on plain text requests
- harden recode.fromJSON (separate implementation from sdk.eval)
- fix node equality for chunk type
- fix file write method for empty chunk
- add minify method to recode library
- improve fatpack utility to also support minifying
- add sound support on web build via sdl2 (not perfect, but works)
- harden type definition logic against duplicate definition

## Version 2.0.0

- add scoped block syntax support
- move all scope resolution logic to stack
- remove unnecessary async contention on push/pop stack
- use base64 to encode/decode zcode (breaking change)
- add support for minifying source (non-recursive bundling)
- optimize entry insertion logic (smaller memory footprint)
- add new primitive type (Chunk) for binary data
- add readBin method to file lib
- add Chunk operations and prototype extensions
- rename zcode library to enigma (breaking change)
- add genKey and derive methods to enigma library
- rename enigma.getUuid to enigma.genUUID
- added zcode migration tool (use sample/zcode.fat for transition)
- remove extra.hex library (use test/t054.fat for transition)
- remove extra.regex library (use test/t077.fat for transition)
- add support for anonymous calls (-> \_ + 5)(4) == 9
- merge extra.mathex into math library
- add startsWith, endsWith, repeat and overlay to Text proto
- add support for 'self' and 'root' keywords (lifts to embedded)
- add recode library for data conversion between various formats
- move http escape/unescape methods to recode toURL/fromURL
- move json codec to recode
- add head and tail shorthand methods to list proto
- add binary data (chunk) support to http library
- review http naming convention on requests 'data' -> 'body'
- resolve types via metaspace
- add getDef method to sdk library
- add form data codec to recode
- move csv codec to recode
- move xml codec to recode
- remove extra.util library (use test/t072.fat for transition)
- move getElapsed method to time library
- remove extra.elapsed library
- move setKey, setMem and runGC to sdk library
- make type cast non-commutable (only 'type \* val' is valid)
- add fat.std shorthand for importing all standard libs
- remove isMain method from sdk lib (kept as embedded command only)
- add rle to recode and to fatpack

## Version 1.3.5

- review auto install process and readme instructions
- fix failure handling bug
- hardening of getFryMeta buffer management
- fix memory leak on type instantiation
- fix memory leak on scope evaluation
- fix misaligned pushStack on innerAssign
- allow sortBy method to sort a matrix by column index
- harden sort logic against worst-case scenario
- use timezone offsets in milliseconds for interoperability
- fix bug on splitting an empty string
- fix bug with half-open range operator corrupting operand
- add xml codec to lib extra
- fix memory leak on conditional flow evaluation
- add filter method to list type (shorthand)
- fix memory corruption on unparseText implementation

## Version 1.3.4

- move curses webenv implementation to fatcurses
- improve ast printing of destructuring assignment
- fix bug with package import with subfolder
- fix invalid memory write bug caused by joinSep
- fix bug with extra util fillWith method
- improve and harden GC recursion logic
- hardening of sdk for utf-8 string handling
- add support for \e as escaped code for escape
- add logging library to extra package
- add calendar library to extra package
- crash on unhandled error via failure trap
- add support for linking against linenoise
- fix bug on Any being aliased to another type
- some minor performance improvements
- fix bug where GC could unhandled failure event
- failure traps bound to stack instead of scope
- remove error field from scope struct
- add sortBy method to list type
- add indexOf method to list type
- add stat method to file lib
- rename conflicting delete to remove on file lib (breaking)

## Version 1.3.3

- add delete method to file lib
- add mkDir method to file lib
- fix type cast for evaluated expressions
- fix double fallback evaluation on nullish coalescing operator
- improve sdkEval implementation
- improve sdkTypeOf output for types
- add data store facilities to json lib via Storable types
- fix bug with storText reading after free
- fix possible undefined behavior with memcpy
- add web build via emscripten
- adds dynamic buffer growth for text tokens
- fix segmentation fault due to bug with replaceAll method
- fix import sequence for bundling (revert)
- fix bug with half-open range operator
- add fatpack text file packer utility to repo

## Version 1.3.2

- register named imports at global scope directly
- avoid starting curses mode just to get screen size
- apply relative paths for imports
- improve error handling within loops
- add half-open range operator
- add global scope $root embedded support
- fix bug with storText reading after free
- fix possible undefined behavior with memcpy

## Version 1.3.1

- fix memory leak on http server implementation
- improve details on man, readme, contributing
- fix wrong logic on jumpSeek scope, lookup boost
- inline all initNode for performance optimization
- harden against duplicate key on addToScope usages
- inline runtimeNode for performance optimization
- improve quicksort impl. for already sorted inputs
- harden list reverse implementations (stack use)
- avoid unnecessary type casts by using union type
- add generic error handler impl. to failure lib
- improve list indexed access with index cache
- fix scopes leak on type constructor
- fix memory leaks on async operations
- improve ast printing for cases structure
- fix isMain on bundled source output
- fix ascii-mode curses box render bug
- add fry cli meta information option
- fix text selection bug for final utf-8 chars

## Version 1.3.0

- add format method to Number proto
- remove inner prop of error type (was broken)
- improve list error generation (source ref)
- improve custom error generation (source ref)
- allow errors to be handled with flow control
- remove deprecated methods from v1.1.0
- allow destructuring assignment for types
- allow method declarations as type arguments
- add endpoint provider (server mode) to http lib
- fix memory leaks for locked unbound nodes
- add wall time to stats logger
- fix many different thread safety issues
- improve method type parameter acceptance
- fix memory leaks on fryrc execution
- reduce contention on multi-threaded workloads
- review code style (facilitate collaboration)
- add to8 and detectDepth methods to color lib
- add moveTo and isTty methods to console lib
- add basePath method to file lib
- add window resize support to curses readKey
- improve console lib support for non-tty output
- add selfCancel method to async lib
- add stringify method to sdk lib
- improve stack pop performance with batched pop
- add memoization utility class

## Version 1.2.0

- add getMacId method to system lib
- allow format with -p to print to stdout
- infer scope as base type for custom types
- add to16 method to color lib
- add support for flexible types (traits)
- extend composite type support for Scopes
- fix uncaught call to missing method name
- add async lib (still a bit rudimentary)
- add configurable stack size support
- add Duration custom type to extra pack
- fix bug on removing underscore value in scope
- fix non reentrant text interpolation
- harden color conversion
- uniformize time to ms on http lib
- somewhat handle utf-8 chars on Text
- add fork and kill methods to system lib
- add type cast support
- add Sound custom type to extra pack

## Version 1.1.0

- fix json encoder to escape newline char
- add dynamic auto-init for append
- add unique method to list type
- add sort method to list type
- add capture method to system lib
- add composite type support
- add support for hex octets, e.g. '\x6f' == 'o'
- add support for accents and emojis in identifiers
- add implicit argument syntax
- fix evaluation sequence for innerAssign
- add optional arguments support on method calls
- fix null assignment on typed entry
- fix alias override for method constructor
- fix type named-argument typechecking
- add help for repl to lib extra
- deprecate methods with optional args alternatives:
  - deprecate color.tint with console.print
  - deprecate time.date/fTime with time.format
  - deprecate time.epoch/pTime with time.parse
  - deprecate zcode.zDecode with zcode.decrypt
  - deprecate zcode.zEncode with zcode.encrypt
- add readText method to curses lib
- add 256 color support via color and console libs
- fix binary operations with both missing operands
- add timeout optional parameter on http lib methods
- add collection minus support
- add modes to console input method
- add HashMap custom type to extra pack
- better utf8 support for console and curses input
- better random number generator (bye C rand)
- add missing math functions for completeness
- add extended mathematical library (mathex)

## Version 1.0.1

- fix immutability violation on lists
- fix segfault on types wrongly removed by GC
- fix inconsistent empty assign on dot expression
- add VS Code extension links to extras section
- revert to original FatScript branding
- improve syntax highlighting for nano
- add isMain method to sdk lib
- fix missing inner scope redeclared alias bug
- fix bug with error handlers
- fix repl for multiline content (pasted)
- fix toJsonText of lib extra for quoted text
- prevent global handler on trapWith
- fix resource leak on readFile
- fix aliased native type alias resolution
- fix issues spotted by cppcheck
- fix basic syntax token sequence validations

## Version 1.0.0

- fix memory leak on curryCall
- fix segfault of bad trackParsed eval sequence
- fix semgrep-sast critical vulnerabilities
- fix segfault on bad chaining (invalid right)
- add inner prop support to error type
- improve error handling on list operations
- improve error handling for mapping
- add flushKeys method to curses lib
- fix bug with curried method calls
- optimize some hot-paths with profiler
- add guacano game to samples
- move dynamic syntax to parser
- add uuid generation method to zcode lib
- add box method to curses lib
- add find and contains methods to list type
- add setHeaders method to http lib
- fmtDate/fmtEpoch renamed to fTime and pTime
- add put and delete methods to http lib
- add type promotion for Boolean and Number
- fix bug with error handlers
- add regex support
- add shuffle method to list type

## Version 0.0.9

- make imports relative to entry point path
- add smart evict import for named imports
- allow lexer to parse the biggest possible double
- add support for package folder imports
- fix logical AND/OR by adopting quick-succeed
- fix error source presentation (again)
- add text padding to lib utils
- use file append for repl sessions
- fix dynamic entries type checking for collections
- move hardcoded values to parameters.h
- add elapsed library to extra pack
- full refactor of GC, scopes and memory management
- add runGC method to system lib
- unify proto-method symbol from ~> to ->
- fix unary operations for chained entries
- improve complex nested access
- add reverse method to list type
- full review on error generation
- add error sub-types
- fix bug with formatter on quoted texts
- add file append support
- optimize parser for mutable entries
- check type of method return value
- add better logger for stats
- fix memory leak for text interpolation
- fix http libcurl FatScript library

## Version 0.0.8

- move showProgress to console lib
- change proto-method symbol from <| to ~>
- add cls method to console
- add getEnv, get/set locale methods to system
- add eval, readLib and typeOf methods to sdk
- add lib extra with csv, hex and json codecs
- add indexOf and contains methods to text lib
- add list directory support to file lib
- add getHash method to zcode lib
- add numeric dynamic entries
- add method scope $self embedded support
- add proper markAndSweep GC support
- improve special keys support for curses lib
- implement multiple performance optimizations
- fix error types returned via apply method
- fix removeFromScope bug
- fix trimString bug
- fix numbApply bug
- fix mapCollection bugs
- fix destructuring assignment bug

## Version 0.0.7

- add save session support to repl
- add dockerfile
- add initial manpage
- improve includes and aliases
- refactor time library
- review README and CONTRIBUTING
- optimize memory dereference logic
- optimize some constructors
- optimize list collection with skip-list
- multi bug fixes and performance improvements

## Version 0.0.6

- add type inclusions, overrides and alias support
- add nested access support (list-list matrix)
- add text trim support
- update FatScript branding
- improve REPL look and feel
- improve OS support
- improve README setup instructions
- fix some minor bugs

## Version 0.0.5

- add error handling support via failure standard library
- add dynamic entry declaration syntax support
- add more samples and more tests
- print fry logs to stderr
- improve type definitions
- improve formatter
- rewrite chaining implementation (more robust and powerful)
- reduce node duplication (less memory consumption and better performance)
- improve and extended existing libraries
- fix memory leaks
- many other minor bug fixes

## Version 0.0.4

- add progress bar component
- add range loops support
- add curses lib wrapper
- reshape README file
- intercept interrupt signals
- fix memory management bugs
- fix text library bugs
- fix json imports
- many other bug fixes

## Version 0.0.3

- initial release
