# Contributing

Here are some ways you can contribute:

- [Report bugs and issues](https://gitlab.com/fatscript/fry/-/issues): If you encounter a problem or error while using `fry`, please let us know! Do you have ideas or suggestions for how we can improve our project? We want to hear from you!

- [Explore our YouTube content](https://www.youtube.com/@fatscript): Dive into our tutorials, behind-the-scenes insights, and surrounding topics in the FatScript YouTube channel. By engaging with our content, you help us grow our community, and sharing our videos helps spread the knowledge of FatScript to others!

- Spread the word: Help us build the FatScript community by sharing our project with others!

## How to Contribute Code

If you're a developer and you're interested in contributing code to `fry`, please follow these steps:

1. [Open an issue](https://gitlab.com/fatscript/fry/-/issues) to discuss what you would like to change, point out bugs you have found, or request new features. This will help us ensure that your contributions align with project goals and existing roadmap.

2. Fork the repository and create a branch for your changes.

3. Write code that adheres to established coding standards and practices. Please see the style guide below for guidance.

4. Write and run tests for your contributions, and ensure that all tests pass before submitting a merge request.

5. Check if [the FatScript specification](https://gitlab.com/fatscript/lang) needs to be updated as well.

6. Open a merge request and submit your changes for review.

7. Participate in the discussion and address any feedback or questions from the community.

8. Celebrate your contribution and add your name and email to the list of contributors below!

## Style Guide

When contributing code to `fry`, please follow these guidelines:

- Use Java-like naming conventions: `PascalCase` for enum values and typedefs, `camelCase` for functions and variables, and `CAPITAL_WORDS` for preprocessor macros.

- Use up to [ISO C17](<https://en.wikipedia.org/wiki/C17_(C_standard_revision)>) features.

- Use 2 spaces for indentation, and keep the opening brace on the same line.

- Limit line length to 80 characters, and consider using a code formatter to enforce project standards, see [.clang-format](.clang-format) file.

### Test your changes

- Check all tests in `run_tests.sh` do pass.

- Check also that `auto_check.sh` does not report any errors or warnings.

### Variable declarations

Declare identifiers close to where they are needed, as this allows for more aggressive compiler optimizations.

Try to keep names short but relevant: if code is sound, less comments are necessary.

## Debug options

Some of the debug/trace logs are excluded from regular compilation via `ifdef DEBUG` guards.

To reveal those, build with `DEBUG` flag:

```bash
./compile.sh -DDEBUG
```

## Semantic versioning

Given a version number MAJOR.MINOR.PATCH, increments mean:

- MAJOR version, incompatible changes
- MINOR version, new backwards compatible language features
- PATCH version, backwards compatible bug fixes and library extensions

## Project Structure Overview

Below is an overview of the main directories and their purposes:

- **`runtime/`**: The essential components for executing FatScript code:
  - Core functionality of the interpreter, including native types, scope handling, list operations etc.
  - Embedded commands that connect the stdlib implementation to the runtime. 
  - Utilities for runtime operations, such as the REPL and built-in help system.
  - The main interpreter logic that drives the execution of FatScript programs.

- **`sdk/`**: Foundational shared code providing lower-level utilities and abstractions for the project:
  - Memory management.
  - Logging utilities.
  - Encoding/decoding utilities.
  - Data structures, I/O and helper functions.

- **`stdlib/`**: The standard library available to FatScript programs, organized into modules:
  - General-purpose modules like `math`, `console`, `file`, `time`, `http`.
  - Native type extensions under `type/` (e.g. `Boolean`, `List`, `Text`).
  - Additional derived types under `extra/` (e.g. `Date`, `Memo`).

- **`syntax/`**: Handles the syntax analysis of FatScript programs, including:
  - Tokenization patterns and reader (lexer).
  - The parser to convert tokens into an abstract syntax tree (AST).
  - A formatter that provides source code formatting and bundling tools.

- **`test/`**: Contains tests to ensure the stability and reliability of `fry`.

For any questions or clarification on where to place new contributions, please reach out in your issue or merge request discussions.

## Contributors

- Antonio Prates - hello@aprates.dev
- João Carlos - fry.chasing598@passinbox.com
- Adrian Ho - the.gromgit@gmail.com

(when doing a merge request, please, add your name to this list)

Thank you for your support!
