#!/usr/bin/env bash

# @file install.sh
# @brief Standalone install script for fry man entry
# @author Antonio Prates <hello@aprates.dev>
# @version 2.0.0
# @date 2023-12-15
#
# @copyright fatscript/fry project <https://gitlab.com/fatscript/fry>
# Copyright (c) 2022-2023, Antonio Prates <hello@aprates.dev>
# Licensed under the GNU General Public License v3.0.
# See LICENSE file in the project root for full license.

sudo install -Dm644 fry.1 /usr/share/man/man1/
